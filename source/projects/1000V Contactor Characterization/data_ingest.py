import os
# import math
import time
import pandas as pd
import datetime as dt
import matplotlib.pyplot as plt
import numpy as np
import statistics
import logging
from configparser import ConfigParser



class data:
    def __init__(self):
        logging.basicConfig(format='%(asctime)s - %(levelname)s - %(message)s')
        self.logger = logging.getLogger()
        self.logger.setLevel(logging.INFO)

        self.logger.info('Loading Configuration')
        self.config = ConfigParser()
        self.config.read('config.ini')
        self.basic_info  = dict(self.config.items('Basic'))
        self.logger.info('---- Starting Data Ingest Process ----')

        self.conversion_Dict = {'Timestamp':'Time',
'DT8874V:Breaktor 1 V':'Breaktor 1 V',
'DT8874V:Breaktor 2 V':'Breaktor 2 V',
'DT8874V:Gigafuse V':'Gigafuse V',
'DT8874V:GXV611P V':'GXV611P V',
'DT8874V:Set up V':'Set up V',
'DT8874T:Breaktor 1 C':'Breaktor 1 C',
'DT8874T:Breaktor 2 C':'Breaktor 2 C',
'DT8874T:Breaktor 3 C':'Breaktor 3 C',
'DT8874T:Breaktor 4 C':'Breaktor 4 C',
'DT8874T:Breaktor side 1 C':'Breaktor side 1 C',
'DT8874T:Breaktor side 2 C':'Breaktor side 2 C',
'DT8874T:Busbar 1 C':'Busbar 1 C',
'DT8874T:Busbar 2 C':'Busbar 2 C',
'DT8874T:Busbar 3 C':'Busbar 3 C',
'DT8874T:Gigafuse 1 C':'Gigafuse 1 C',
'DT8874T:Gigafuse 2 C':'Gigafuse 2 C',
'DT8874T:Gigafuse side 1 C':'Gigafuse side 1 C',
'DT8874T:Gigafuse side 2 C':'Gigafuse side 2 C',
'DT8874T:Gigavac 1 C':'Gigavac 1 C',
'DT8874T:Gigavac 2 C':'Gigavac 2 C',
'DT8874T:Gigavac side 1 C':'Gigavac side 1 C',
'DT8874T:Gigavac side 2 C':'Gigavac side 2 C',
'DT8874T:Busbar 4 C':'Busbar 4 C',
'DT8874T:Busbar 5 C':'Busbar 5 C',
'DT8874T:Busbar 6 C':'Busbar 6 C',
'DT8874T:Heatsink 1 1 C':'Heatsink 1 1 C',
'DT8874T:Heatsink 1 2 C':'Heatsink 1 2 C',
'DT8874T:Heatsink 2 1 C':'Heatsink 2 1 C',
'DT8874T:Heatsink 2 2 C':'Heatsink 2 2 C',
'DT8874T:Ambient C':'Ambient C',
'GEN1:Voltage V':'HCPS Voltage V',
'GEN1:Current A':'HCPS Current A',
'BT12:Voltage V':'BT12 Voltage V',
'BT12:Current A':'BT12 Current A',
'BT5:Voltage V':'BT5 Voltage V',
'BT5:Current A':'BT5 Current A',
'RIGOL:Voltage V':'GV12 Voltage V',
'RIGOL:Current A':'GV12 Current A',
'DT8874T:Ambient box C':'Ambient box C',
'DT8874T:Breaktor 3 TC2 C':'Breaktor 3 TC2 C'}
        self.current_Dict = {
                            'Breaktor 1 V':'HCPS Current A',
                            'Breaktor 2 V':'HCPS Current A',
                            'Gigafuse V':'HCPS Current A',
                            'GXV611P V':'HCPS Current A',
                            # 'HCPS Voltage V':'HCPS Current A',
                            'BT12 Voltage V':'BT12 Current A',
                            'BT5 Voltage V':'BT5 Current A',
                            'GV12 Voltage V':'GV12 Current A'}
        self.pulse_category_Dict = {"Ambient Long Cycle" : 1,
                             "Ambient Mid Cycle" : 2,
                             "Heat WOT" : 3,
                             "Ambient WOT": 4}

        self.ProfileDict = {}
        self.GroupDict = {}

        self.Controller()


    def Controller(self):
        self.logger.info('Starting Controller')
        self.identify_files()
        self.process_all_files()

    def process_all_files(self):
        self.logger.info('Starting All File Process')
        for profile in self.ProfileDict.keys():
            self.logger.info("Starting Profile: {}".format(profile))
            files = self.ProfileDict[profile]
            localDir = self.basic_info['data_location'] + '/' + profile + '/'
            self.process_file(files, profile, localDir)

    def process_file(self, files, profile, root):
        self.logger.info('Starting Single File Process')
        if files == list:
            for i in range(0, len(files)):
                self.logger.debug('{}/{} Starting File: {}'.format(i, len(files), files[i]))
                df = self.ReadFile(root + files[i])
                self.SummarizeRawFile(df, profile,files[i])
        else:
            self.logger.debug('Starting File: {}'.format(files[0]))
            df = self.ReadFile(root + files[0])
            self.SummarizeRawFile(df, profile, files[0])

    def identify_files(self):
        self.logger.info('Starting Identify Files')
        profiles = [name for name in os.listdir(self.basic_info['data_location']) if os.path.isdir(os.path.join(self.basic_info['data_location'], name))]
        for i in profiles:
            localDir = self.basic_info['data_location'] + '/' + i
            files = os.listdir(localDir)
            files[:] = [x for x in files if any(y in x for y in ['.csv'])]
            self.ProfileDict[i] = files

    def SummarizeRawFile(self,raw_df,profile, file):
        self.logger.info('---- Starting Summary Data Process ----')
        df = raw_df.copy()
        CurrentSummary = pd.DataFrame()
        self.logger.info('---- Number of Cycles {} ----'.format(df.CurrentCycleCount.max()))
        for cycle in range(0,df.CurrentCycleCount.max()):
            self.logger.info('---- Starting Cycle: {} ----'.format(cycle))
            tdf = df[df['CurrentCycleCount'] == cycle]
            cycletime = tdf.Time.max() - tdf.Time.min()
            sdf = pd.DataFrame()
            for (columnName, columnData) in tdf.iteritems():
                if (columnName[-1] == 'V') & (columnName in list(self.current_Dict.keys())):
                    self.logger.debug('Voltage | {}'.format(columnName))
                    cdf = self.voltage_summary(columnName,tdf[[columnName,self.current_Dict[columnName]]])
                    # sdf = pd.concat([sdf, cdf], axis=1, join='inner')
                    sdf[cdf.columns] = cdf
                elif columnName[-1] == 'C':
                    # self.logger.debug('Temperature | {}'.format(columnName))
                    cdf = self.temp_summary(columnName,tdf[[columnName,'HCPS Current A']])
                    # print(cdf)
                    # sdf = pd.concat([sdf, cdf], axis=1, join='inner')
                    sdf[cdf.columns] = cdf
                else:
                    # self.logger.debug('Missed | {}'.format(columnName))/
                    pass
            sdf['CurrentCycle']=cycle
            sdf['CycleTime'] = cycletime.total_seconds()

            CurrentSummary = pd.concat([CurrentSummary,sdf])

        CurrentSummary.set_index(['CurrentCycle', 'CycleTime'],inplace=True)
        CurrentSummary.columns = CurrentSummary.columns.str.split('_',n=2,expand=True)
        CurrentSummary.sort_index(axis=1,inplace=True)
        CurrentSummary = CurrentSummary.stack(level=[0])
        CurrentSummary.columns = ['_'.join(col) for col in CurrentSummary.columns.values]
        CurrentSummary.reset_index(inplace=True, drop=False)
        CurrentSummary = CurrentSummary.rename(columns={'level_2': 'MeasID'})

        self.TimeSeriesPlot(CurrentSummary[['CurrentCycle','pulse_profile', 'MeasID', 'R_mean']], "Mean Resistance",'Rmean vs Current Cycle',profile)
        self.TimeSeriesPlot(CurrentSummary[['CurrentCycle','pulse_profile', 'MeasID', 'R_range']], "Range Resistance",'Rrange vs Current Cycle', profile)
        self.TimeSeriesPlot(CurrentSummary[['CurrentCycle','pulse_profile', 'MeasID', 'T_max']], "Max Temperature", 'Tmax vs Current Cycle', profile)
        self.TimeSeriesPlot(CurrentSummary[['CurrentCycle','pulse_profile', 'MeasID', 'T_range']], "Range Temperature", 'Trange vs Current Cycle', profile)

        self.OutputToExcel(CurrentSummary,'Current_Summary_'+profile+'.csv')

    def voltage_summary(self,signalname,df):
        columns = ['pulse_profile','A_mean','V_mean','V_max','V_min','V_std','V_slope','V_range','R_mean','R_max','R_min','R_std','R_slope','R_range']
        columns = [signalname +'_'+ s for s in columns]
        df = df.copy()
        df.columns = ['V','Current']
        df['R'] = df.V/df.Current
        df = df[df['Current'] > 100 ]
        df = df[1:-1]
        A_mean = df.Current.mean()
        pulse_profile = self.categorize_current(A_mean)
        V_mean = df.V.mean()
        V_max = df.V.max()
        V_min = df.V.min()
        V_std = df.V.std()
        V_range = V_max-V_min
        V_slope = df.V[:25].mean()-df.V[-25:].mean()
        R_mean = df.R.mean()
        R_max = df.R.max()
        R_min = df.R.min()
        R_std = df.R.std()
        R_range = R_max - R_min
        R_slope = df.R[:25].mean()-df.R[-25:].mean()
        odf = pd.DataFrame()
        row = [pulse_profile,A_mean, V_mean,V_max,V_min,V_std,V_slope,V_range,R_mean,R_max,R_min,R_std,R_slope,R_range]
        odf = odf.append(pd.Series(row, index=columns), ignore_index=True)
        return odf

    def temp_summary(self,signalname,df):
        columns = ['pulse_profile','A_mean','T_mean','T_max','T_min','T_range']
        columns = [signalname +'_'+ s for s in columns]
        # print(columns)
        df.columns = ['T', 'Current']
        Tdf = df['T'].copy()
        Adf = df[df['Current'] > 100]
        Adf = Adf[1:-1]
        A_mean = Adf.Current.mean()
        pulse_profile = self.categorize_current(A_mean)
        T_mean = Tdf.T.mean()
        T_max = Tdf.T.max()
        T_min = Tdf.T.min()
        T_range = Tdf.T.max() - Tdf.T.min()
        odf = pd.DataFrame()
        row = [pulse_profile,A_mean,T_mean,T_max,T_min,T_range]
        # print('A_mean : {}'.format(A_mean))
        # print('T_mean : {}'.format(T_mean))
        # print('T_max : {}'.format(T_max))
        # print('T_max : {}'.format(T_min))
        # print('T_range : {}'.format(T_range))
        odf = odf.append(pd.Series(row, index=columns), ignore_index=True)
        return odf

    def categorize_current(self,A):
        if A >= 1000:
            pulse = "Ambient WOT"
            return pulse
        elif A >= 800:
            pulse = "Heat WOT"
            return pulse
        elif A >= 600:
            pulse = "Ambient Mid Cycle"
            return pulse
        elif A >= 200:
            pulse = "Ambient Long Cycle"
            return pulse

    def ReadFile(self, file):
        os.chdir(self.basic_info['data_location'])
        df = pd.read_csv(file)
        df['Timestamp'] = pd.TimedeltaIndex(df['Timestamp'], unit='d') + dt.datetime(1899, 12, 30)
        try:
            df.drop(columns=['Time'],inplace=True)
        except:
            pass
        df.rename(columns=self.conversion_Dict, inplace=True)
        # df.set_axis([*df.columns[:-1], 'Current'], axis=1, inplace=True)
        df['CurrentCycleStart'] = np.where((df['HCPS Current A'] > 50) & (df['HCPS Current A'].shift(1) < 50), 1, 0)
        df['CurrentCycleCount'] = df['CurrentCycleStart'].cumsum()
        return df

    def loadExcel(self, file):
        df = pd.read_csv(file) #usecols=[0, 1, 2, 3, 4]
        df = df.apply(pd.to_numeric)
        return df

    def OutputToExcel(self, df, filename):
        os.chdir(self.basic_info['summary_location'])
        df.to_csv(path_or_buf=filename,index=False)

    def TimeSeriesPlot(self,df,yLabel, Title, file):  # Input subset of data for Plotting
        os.chdir(self.basic_info['chart_location'])
        # figure = plt.gcf()
        # figure.set_size_inches(18,8)
        # # plt.figure(figsize=(10, 6), dpi=120)
        fig ,ax = plt.subplots(figsize=(18, 8))
        pltDF = df.copy()
        print(pltDF)
        pltDF.dropna(inplace=True)
        pltDF.set_index(['CurrentCycle','pulse_profile','MeasID'],inplace=True)
        pltDF = pltDF.unstack()
        pltDF.columns = pltDF.columns.droplevel()
        pltDF.reset_index(drop=False,inplace=True)
        pltDF['pulse_profile'] = pltDF['pulse_profile'].replace(self.pulse_category_Dict)
        print(pltDF)
        pltDF.plot(ax=ax,x="CurrentCycle",y=pltDF.columns[2:])
        pltDF.plot(ax=ax,x="CurrentCycle",y='pulse_profile',secondary_y=True)
        plt.yticks(list(self.pulse_category_Dict.values()),list(self.pulse_category_Dict.keys()))

        plt.xlabel('CurrentCycle')
        plt.ylabel(yLabel)
        plt.title(Title)
        filename = str(file) + "-" + Title +'.jpg'
        plt.tight_layout()
        plt.savefig(filename, dpi=120)


if __name__ == "__main__":
    test = data()
