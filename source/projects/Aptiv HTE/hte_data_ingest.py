import os
import math
import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
import statistics
import logging
from openpyxl import load_workbook
from itertools import islice

class PTCE_data:
    def __init__(self):
        os.chdir('/projects/Aptiv')
        logging.basicConfig(
                            format='%(asctime)s - %(levelname)s - %(message)s') #filename='PTC_Data.log',
        self.logger = logging.getLogger()
        self.logger.setLevel(logging.DEBUG)
        self.logger.info('---- Starting Aptiv PTC Data Ingest Process ----')
        data_folder = r"raw_hte_data/"
        files = os.listdir(data_folder)
        files = [k for k in files if '.csv' in k]
        self.Controller(files,data_folder)

    def Controller(self, files, root):
        load = 1
        os.chdir('/projects/Aptiv')
        if load == 1:
            self.logger.info('Loading All Test Files')
            df = self.loadExcel(root, files)
            self.OutputToExcel(df, r'joined_raw_hte_data/Aptiv_PTC_Data.csv')
            self.logger.info('Completed File Loading')
        else:
            self.logger.info('Loading Joined Test File')
            df = pd.read_csv(r'joined_raw_ptce_data/Aptiv_PTC_Data.csv',
                             index_col=0,
                             header=0,
                             delimiter=',',
                             parse_dates=['Date', 'Time', 'Real Date','stepTime'],
                             infer_datetime_format=True,
                             low_memory=False)
            df['stepTime'] = pd.to_timedelta(df['stepTime'])
            print(df.stepTime.dtypes)
            self.logger.info('Completed Loading')

        colDF, SNlst = self.ParseSN(df)

        for sn in SNlst:
            self.logger.info('Starting SN: {} ({}/{})'.format(sn,SNlst.index(sn),len(SNlst)))
            snDF = colDF[colDF['SN'] == str(sn)]
            type = snDF.Polarity.unique().tolist()



    def loadExcel(self, root, files):
        # pd.set_option('display.max_rows', None, 'display.max_columns', 20)
        f = lambda x: (x.replace(",", "."))
        conv = { 'PB_1_08_T1':f,'PB_1_11_T1':f,'PB_1_24_T1':f,'PB_2_18_T1':f,'PB_2_19_T1':f,'PB_1_06_T1':f,
                 'PB_1_08_T2':f,'PB_1_11_T2':f,'PB_1_24_T2':f,'PB_2_18_T2':f,'PB_2_19_T2':f,'PB_1_06_T2':f,
                 'PB_1_08_T3':f,'PB_1_11_T3':f,'PB_1_24_T3':f,'PB_2_18_T3':f,'PB_2_19_T3':f,'PB_1_06_T3':f,
                 'PB_1_08_T4':f,'PB_1_11_T4':f,'PB_1_24_T4':f,'PB_2_18_T4':f,'PB_2_19_T4':f,'PB_1_06_T4':f,
                 'PB_1_08_T5':f,'PB_1_11_T5':f,'PB_1_24_T5':f,'PB_2_18_T5':f,'PB_2_19_T5':f,'PB_1_06_T5':f,
                 'PB_1_08_T6':f,'PB_1_11_T6':f,'PB_1_24_T6':f,'PB_2_18_T6':f,'PB_2_19_T6':f,'PB_1_06_T6':f,
                 'PB_1_08_T7':f,'PB_1_11_T7':f,'PB_1_24_T7':f,'PB_2_18_T7':f,'PB_2_19_T7':f,'PB_1_06_T7':f,
                 'NB_10_T1':f,'NB_12_T1':f,'NB_13_T1':f,'NB_14_T1':f,'NB_15_T1':f,'NB_11_T1':f,'NB_10_T2':f,
                 'NB_11_T2':f,'NB_12_T2':f,'NB_13_T2':f,'NB_14_T2':f,'NB_15_T2':f,'NB_10_T3':f,'NB_11_T3':f,
                 'NB_12_T3':f,'NB_13_T3':f,'NB_14_T3':f,'NB_15_T3':f,'PB_1_08_INP_OUT':f,'PB_1_11_INP_OUT':f,
                 'PB_1_24_INP_OUT':f,'NB_13_INP_OUT':f,'PB_2_18_INP_OUT':f,'PB_2_19_INP_OUT':f,'NB_10_INP_OUT':f,
                 'NB_11_INP_OUT':f,'NB_12_INP_OUT':f,'NB_14_INP_OUT':f,'NB_15_INP_OUT':f,'PB_1_06_INP_OUT':f,
                 'PB_1_08_OUT_DCFC':f,'PB_1_11_OUT_DCFC':f,'PB_1_24_OUT_DCFC':f,'PB_2_18_OUT_DCFC':f,
                 'PB_2_19_OUT_DCFC':f,'PB_1_06_OUT_DCFC':f,'NB_10_OUT_DCFC':f,'NB_11_OUT_DCFC':f,
                 'NB_12_OUT_DCFC':f,'NB_13_OUT_DCFC':f,'NB_14_OUT_DCFC':f,'NB_15_OUT_DCFC':f,'PB_1_08_PRIME':f,
                 'PB_1_11_PRIME':f,'PB_1_24_PRIME':f,'PB_2_18_PRIME':f,'PB_2_19_PRIME':f,'PB_1_06_PRIME':f,
                 'NB_10_PRIME':f,'NB_11_PRIME':f,'NB_12_PRIME':f,'NB_13_PRIME':f,'NB_14_PRIME':f,'NB_15_PRIME':f,
                 'PB_1_08_SEC':f,'PB_1_11_SEC':f,'PB_1_24_SEC':f,'PB_2_18_SEC':f,'PB_2_19_SEC':f,'PB_1_06_SEC':f,
                 'NB_10_SHUNT':f,'NB_11_SHUNT':f,'NB_12_SHUNT':f,'NB_13_SHUNT':f,'NB_14_SHUNT':f,'NB_15_SHUNT':f,
                 'PB_1_08_F_PRIME':f,'PB_1_11_F_PRIME':f,'PB_1_24_F_PRIME':f,'PB_2_18_F_PRIME':f,'PB_2_19_F_PRIME':f,
                 'PB_1_06_F_PRIME':f,'PB_1_08_F_SEC':f,'PB_1_11_F_SEC':f,'PB_1_24_F_SEC':f,'PB_2_18_F_SEC':f,
                 'PB_2_19_F_SEC':f,'PB_1_06_F_SEC':f,'CURRENT':f,'T_AMB_1':f,'T_AMB_2':f,'NB10_SHUNT_CURRENT':f,
                 'NB11_SHUNT_CURRENT':f,'NB12_SHUNT_CURRENT':f,'NB13_SHUNT_CURRENT':f,'NB14_SHUNT_CURRENT':f,
                 'NB15_SHUNT_CURRENT':f,'EXT_SET1':f,'EXT_SET2':f,'EXT_SET3':f,'EXT_SET4':f,'EXT_SET5':f,'EXT_SET6':f
        }

        na_values = ['+OVER', '-OVER', '#NOME?']
        df = pd.DataFrame()
        files = sorted(files)
        print(files)
        for i in range(0, len(files)):
            self.logger.info('Starting: {}'.format(files[i]))
            if i == 0:
                ldf = pd.read_csv(root+files[i],
                                  index_col=None,
                                  header=0,
                                  delimiter=';',
                                  converters=conv,
                                  parse_dates=['Date', 'Time', 'Real Date'],
                                  na_values=na_values,
                                  low_memory=False)

            elif i >= 1:
                ldf = pd.read_csv(root+files[i],
                                  index_col=None,
                                  header=0,
                                  delimiter='\t',
                                  converters=conv,
                                  parse_dates=['Date', 'Time', 'Real Date'],
                                  na_values=na_values,
                                  low_memory=False)
            lst = list(ldf.columns.values)
            droplst = list(filter(lambda x: str(x)[-2:] == '.1', lst))
            droplst.extend(['BALANCE', 'EXT_SET1', 'EXT_SET2', 'EXT_SET3', 'EXT_SET4', 'EXT_SET5', 'EXT_SET6'])
            ldf.drop(columns=droplst, inplace=True)
            ldf['T_AMB_2'] = pd.to_numeric(ldf['T_AMB_2'], errors='coerce')
            ldf['CURRENT'] = pd.to_numeric(ldf['CURRENT'], errors='coerce')

            print(ldf.info())
            df = df.append(ldf)
            del ldf

        self.logger.info('Finished loading files')
        cols = list(df.columns.values)
        cols = cols[4:]
        print(cols)

        i = 0
        for col in cols:
            i += 1
            self.logger.info('Clean up column {} / {}'.format(i , len(cols)+1))
            df[col] = pd.to_numeric(df[col], errors='coerce')

        df = df.dropna(thresh=100)
        df['stepTime'] = df.Time.diff()
        df = df[(df.stepTime < np.timedelta64(5, 'm')) & (df.stepTime > - np.timedelta64(5, 'm'))]
        df['TempCycle'] = np.where((df.T_AMB_2 >= 0) & (df.T_AMB_2.shift(-100) > 0) & (df.T_AMB_2.shift(1) < 0) & (df.T_AMB_2.shift(2) < 0) & (df.T_AMB_2.shift(3) < 0) &(df.T_AMB_2.shift(100) < 0), 1, 0)
        df['TempCycleCount'] = df['TempCycle'].cumsum()
        df['CURRENT'] = df['CURRENT'].fillna(0)
        df.loc[(df['CURRENT'] < 250) & (df['CURRENT'] > -250), 'CURRENT'] = 0
        df.loc[df['T_AMB_2'] < -60, 'T_AMB_2'] = np.nan
        df.loc[df['T_AMB_1'] < -60, 'T_AMB_1'] = np.nan
        df['CurrentCycleStart'] = np.where((df['CURRENT'] > 1 ) & (df['CURRENT'].shift(1) == 0) & (df['CURRENT'].shift(-1) >= 1) & (df['CURRENT'].shift(-3) >= 1), 1, 0)
        df['CurrentCycleEnd'] = np.where((df['CURRENT'] == 0 ) & (df['CURRENT'].shift(1) > 1) & (df['CURRENT'].shift(-1) == 0) & (df['CURRENT'].shift(3) >= 1), 1, 0)
        df['CurrentCycleCount'] = df['CurrentCycleStart'].cumsum()
        df['CurrentCycleCompare'] = df['CurrentCycleEnd'].cumsum()
        df['CurrentCycleIndicator'] = df['CurrentCycleCount'] - df['CurrentCycleCompare']
        df['PowerCycleStart'] = np.where((df['CURRENT'] > 1) &
                                         (df['CURRENT'].shift(1) == 0) &
                                         (df['CURRENT'].shift(-1000).rolling(min_periods=1, window=1000).mean() >= 10) &
                                         (df['CURRENT'].shift(1).rolling(min_periods=10, window=1000).mean() <= 1)
                                         , 1, 0)
        df['PowerCycleRollingEval150'] = df['CURRENT'].shift(1).rolling(min_periods=1, window=150).mean()
        df['PowerCycleRollingEval300'] = df['CURRENT'].shift(1).rolling(min_periods=1, window=300).mean()
        df['PowerCycleRollingEval1000'] = df['CURRENT'].shift(1).rolling(min_periods=1, window=1000).mean()
        df['PowerCycleEnd'] = np.where((df['CURRENT'] == 0) &
                                       (df['CURRENT'].shift(1) > 1) &
                                       (df['CURRENT'].shift(1).rolling(min_periods=1, window=1000).mean() >= 10) &
                                       (df['CURRENT'].shift(-1001).rolling(min_periods=10, window=1000).mean() <= 1)
                                       , 1, 0)

        df['PowerCycleCount'] = df['PowerCycleStart'].cumsum()
        df['PowerCycleCompare'] = df['PowerCycleEnd'].cumsum()
        df['PowerCycleIndicator'] = df['PowerCycleCount'] - df['PowerCycleCompare']


        self.logger.info('Finished profile characterization')
        return df

    def ParseSN(self, df):
        cols = list(df.columns.values)
        colDF = pd.DataFrame(cols, columns=['key'])

        colDF['Pos'] = np.where(colDF['key'].str[:2] == "PB", 'Pos', '')
        colDF['Neg'] = np.where(colDF['key'].str[:2] == "NB", 'Neg', '')
        colDF['Polarity'] = colDF.Pos + colDF.Neg
        colDF['F1type'] = np.where((colDF['Pos'] == 1) & (colDF['key'].str[3] == '1'), "Eaton", '')
        colDF['F2type'] = np.where((colDF['Pos'] == 1) & (colDF['key'].str[3] == '2'), "Mersen", '')
        colDF['Ftype'] = colDF.F1type + colDF.F2type
        colDF['SN'] = np.where((colDF['Pos'] == 'Pos'), colDF['key'].str[5:7], colDF['key'].str[3:5])

        colDF['measT'] = np.where(colDF['key'].str[-2:-1] == 'T', 1, 0)
        colDF['T1'] = np.where(colDF['key'].str[-2:] == 'T1', 'Input_BB', '')
        colDF['T2'] = np.where(colDF['key'].str[-2:] == 'T2', 'Output_BB', '')
        colDF['T3'] = np.where(colDF['key'].str[-2:] == 'T3', 'DCFC_BB', '')
        colDF['T4'] = np.where(colDF['key'].str[-2:] == 'T4', 'Sec_Fuse_Body', '')
        colDF['T5'] = np.where(colDF['key'].str[-2:] == 'T5', 'Prim_Fuse_Body', '')
        colDF['T6'] = np.where(colDF['key'].str[-2:] == 'T6', 'Sec_Fuse_Term', '')
        colDF['T7'] = np.where(colDF['key'].str[-2:] == 'T7', 'Prim_Fuse_Term', '')

        colDF['IOV'] = np.where(colDF['key'].str[-7:] == 'INP_OUT', 'INP_OUT', '')
        colDF['ODCFCV'] = np.where(colDF['key'].str[-8:] == 'OUT_DCFC', 'OUT_DCFC', '')
        colDF['FPV'] = np.where(colDF['key'].str[-7:] == 'F_PRIME', 'F_', '')
        colDF['FSV'] = np.where(colDF['key'].str[-5:] == 'F_SEC', 'F_', '')
        colDF['PV'] = np.where(colDF['key'].str[-5:] == 'PRIME', 'PRIME', '')
        colDF['SV'] = np.where(colDF['key'].str[-3:] == 'SEC', 'SEC', '')
        colDF['ShuntV'] = np.where(colDF['key'].str[6:12] == 'SHUNT', 'SHUNT', '')
        colDF['ShuntI'] = np.where(colDF['key'].str[-13:] == 'SHUNT_CURRENT', 'SHUNT_CURRENT', '')
        colDF[
            'Legend'] = colDF.T1 + colDF.T2 + colDF.T3 + colDF.T4 + colDF.T5 + colDF.T6 + colDF.T7 + colDF.IOV + colDF.ODCFCV + colDF.FPV + colDF.FSV + colDF.PV + colDF.SV + colDF.ShuntV + colDF.ShuntI
        colDF.drop(columns=['Pos', 'Neg', 'F1type', 'F2type', 'measT', 'T1', 'T2', 'T3', 'T4', 'T5', 'T6', 'T7', 'IOV',
                            'ODCFCV', 'FPV', 'FSV', 'PV', 'SV', 'ShuntV', 'ShuntI'], inplace=True)
        self.logger.info('Finished Column DF manipulations')
        SNlst = colDF.SN.unique()
        SNlst = [x for x in SNlst if x.isdigit()]

        return colDF, SNlst

    def OutputToExcel(self, df, filename):
        df.reset_index(inplace=True, drop=True)
        df.to_csv(path_or_buf=filename)

    def PlotAllSamples(self, signals): #Input subset of data for Plotting
        samples = signals['key'].nunique()
        print(signals['key'])
        self.logger.info('Number of Samples {}'.format(samples))
        keys = signals.key.unique()
        keys = sorted(keys)
        row, col = signals.shape
        print(col-2, samples)
        fig, axs = plt.subplots(nrows=col-2, ncols=len(keys)+1, sharey='row')

        for i in range(0, len(keys)):
            print('i={}'.format(i))
            tdf = signals.loc[signals['key'] == keys[i]]
            t2df = tdf.drop('key', axis=1)
            for j in range(0, col-2):
                t2df.plot(x=0, y=j+1, style='.', legend=False, ax=axs[j, i])
                t2df.plot(x=0, y=j+1, style='.', legend=False, label=keys[i][-10:],
                          ax=axs[j, len(keys)])
                if j == 0:
                    axs[0, i].set_title("{}".format(keys[i][-10:]))
                if i == 0:
                    axs[j, 0].set_ylabel(t2df.columns[j+1])

        plt.show()


if __name__ == "__main__":
    test = HTE_data()
