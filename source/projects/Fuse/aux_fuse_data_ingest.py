import os
# import math
import time
import datetime as dt
import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
import statistics
import logging


class aux_fuse_data:
    def __init__(self):
        logging.basicConfig(
            format='%(asctime)s - %(levelname)s - %(message)s')  # filename='PTC_Data.log',
        self.logger = logging.getLogger()
        self.logger.setLevel(logging.DEBUG)
        self.logger.info('---- Starting Aux Fuse Data Ingest Process ----')
        self.data = r'/Users/jtbruschetto/Documents/OneDrive - Rivian Automotive, LLC/PTC'
        os.chdir(self.data)
        self.root = r"/Users/jtbruschetto/Documents/OneDrive - Rivian Automotive, LLC/Projects/HVDB/Fuse/Python Data"
        self.summary_folder = r"/Users/jtbruschetto/Documents/OneDrive - Rivian Automotive, LLC/Projects/HVDB/Fuse/Python Data/summary_data"
        self.signalconversion60A ={
                                'Timestamp':'Timestamp',
                                'KSDAQ970A:Chn000 V':'B1_V',
                                'KSDAQ970A:Chn001 V':'B1F2_V',
                                'KSDAQ970A:Chn002 V':'B1F3_V',
                                'KSDAQ970A:Chn003 V':'B1F4_V',
                                'KSDAQ970A:Chn004 V':'B1F5_V',
                                'KSDAQ970A:Chn005 V':'B1F6_V',
                                'KSDAQ970A:Chn006 V':'B2_V',
                                'KSDAQ970A:Chn007 V':'B2F2_V',
                                'KSDAQ970A:Chn008 V':'B2F3_V',
                                'KSDAQ970A:Chn009 V':'B2F4_V',
                                'KSDAQ970A:Chn010 V':'B2F5_V',
                                'KSDAQ970A:Chn011 V':'B2F6_V',
                                'KSDAQ970A:Chn012 V':'B3_V',
                                'KSDAQ970A:Chn013 V':'B3F2_V',
                                'KSDAQ970A:Chn014 V':'B3F3_V',
                                'KSDAQ970A:Chn015 V':'B3F4_V',
                                'KSDAQ970A:Chn016 V':'B3F5_V',
                                'KSDAQ970A:Chn017 V':'B3F6_V',
                                'KSDAQ970A:Chn018 V':'Connector + harness_V',
                                'KSDAQ970A:Chn019 V':'Fan Voltage_V',
                                'KSDAQ970A:Chn020 C':'B1F2_C',
                                'KSDAQ970A:Chn021 C':'B1F3_C',
                                'KSDAQ970A:Chn022 C':'B1F4_C',
                                'KSDAQ970A:Chn023 C':'B1F5_C',
                                'KSDAQ970A:Chn024 C':'B1F6_C',
                                'KSDAQ970A:Chn025 C':'B2F2_C',
                                'KSDAQ970A:Chn026 C':'B2F3_C',
                                'KSDAQ970A:Chn027 C':'B2F4_C',
                                'KSDAQ970A:Chn028 C':'B2F5_C',
                                'KSDAQ970A:Chn029 C':'B2F6_C',
                                'KSDAQ970A:Chn030 C':'B3F2_C',
                                'KSDAQ970A:Chn031 C':'B3F3_C',
                                'KSDAQ970A:Chn032 C':'B3F4_C',
                                'KSDAQ970A:Chn033 C':'B3F5_C',
                                'KSDAQ970A:Chn034 C':'B3F6_C',
                                'KSDAQ970A:Chn035 C':'B3 harness out_C',
                                'KSDAQ970A:Chn036 C':'Connector side 1_C',
                                'KSDAQ970A:Chn037 C':'Connector side 2_C',
                                'KSDAQ970A:Chn038 C':'Connector harness out_C',
                                'KSDAQ970A:Chn039 C':'Ambient_C',
                                'KSDAQ970A:Chn040 C':'B1F1_C',
                                'KSDAQ970A:Chn041 C':'B1F7_C',
                                'KSDAQ970A:Chn042 C':'B1F8_C',
                                'KSDAQ970A:Chn043 C':'B2F1_C',
                                'KSDAQ970A:Chn044 C':'B2F7_C',
                                'KSDAQ970A:Chn045 C':'B2F8_C',
                                'KSDAQ970A:Chn046 C':'B3F1_C',
                                'KSDAQ970A:Chn047 C':'B3F7_C',
                                'KSDAQ970A:Chn048 C':'B3F8_C',
                                'KSDAQ970A:Chn049 C':'Ambient2_C',
                                'DEV003:Voltage V':'Voltage',
                                'DEV003:Current A':'Current'}
        self.Controller()

    def Controller(self):
        # self.LoadRawFiles()
        for rating in ['60A']: # '50A', '35A', '15A'
            self.SummarizeRawFile(rating)

    def SummarizeRawFile(self,rating):
        self.logger.info('---- Starting Summary Data Process for {} ----'.format(rating))
        filename = self.summary_folder + '/combined_'+rating+'_data.csv'
        df = pd.read_csv(filename, parse_dates=['Timestamp'], index_col=0)
        self.logger.info('File Loading Complete')
        df.rename(columns=self.signalconversion60A, inplace=True)
        print(df.shape)

        CurrentSummary = pd.DataFrame()
        self.logger.info('---- Number of Cycles {} ----'.format(df.CurrentCycleCount.max()))
        for cycle in range(0,df.CurrentCycleCount.max()):
            self.logger.info('---- Starting Cycle: {} ----'.format(cycle))
            tdf = df[df['CurrentCycleCount'] == cycle]
            cycletime = tdf.Timestamp.max() - tdf.Timestamp.min()
            sdf = pd.DataFrame()
            for (columnName, columnData) in tdf.iteritems():
                if columnName[-1] == 'V':
                    cdf = self.voltage_summary(columnName,tdf[[columnName,'Current']])
                    # sdf = pd.concat([sdf, cdf], axis=1, join='inner')
                    sdf[cdf.columns] = cdf
                elif columnName[-1] == 'C':
                    cdf = self.temp_summary(columnName,tdf[columnName])
                    # sdf = pd.concat([sdf, cdf], axis=1, join='inner')
                    sdf[cdf.columns] = cdf
            sdf['CurrentCycle']=cycle
            sdf['CycleTime'] = cycletime.total_seconds()

            CurrentSummary = pd.concat([CurrentSummary,sdf])
        CurrentSummary.set_index(['CurrentCycle', 'CycleTime'],inplace=True)
        CurrentSummary.columns = CurrentSummary.columns.str.split('_',n=2,expand=True)
        CurrentSummary.sort_index(axis=1,inplace=True)
        CurrentSummary = CurrentSummary.stack(level=[0,1])
        CurrentSummary.reset_index(inplace=True, drop=False)
        print(CurrentSummary)
        self.OutputToExcel(CurrentSummary,'Current_Summary_'+rating+'.csv')

    def voltage_summary(self,signalname,df):
        columns = ['V_mean','V_max','V_min','V_std','V_slope','R_mean','R_max','R_min','R_std','R_slope']
        columns = [signalname +'_'+ s for s in columns]
        df = df.copy()
        df.columns = ['V','Current']
        df['R'] = df.V/df.Current
        df = df[df['Current'] >5 ]
        V_mean = df.V.mean()
        V_max = df.V.max()
        V_min = df.V.min()
        V_std = df.V.std()
        V_slope = df.V[:25].mean()-df.V[-25:].mean()
        R_mean = df.R.mean()
        R_max = df.R.max()
        R_min = df.R.min()
        R_std = df.R.std()
        R_slope = df.R[:25].mean()-df.R[-25:].mean()
        odf = pd.DataFrame()
        row = [V_mean,V_max,V_min,V_std,V_slope,R_mean,R_max,R_min,R_std,R_slope]
        odf = odf.append(pd.Series(row, index=columns), ignore_index=True)
        return odf

    def temp_summary(self,signalname,df):
        columns = ['T_mean','T_max','T_min']
        columns = [signalname +'_'+ s for s in columns]
        df.columns = ['T']
        T_mean = df.T.mean()
        T_max = df.T.max()
        T_min = df.T.min()
        odf = pd.DataFrame()
        row = [T_mean,T_max,T_min]
        odf = odf.append(pd.Series(row, index=columns), ignore_index=True)
        return odf

    def WOTcycleEval(self, unit, wdf, wot_count, WOTSummaryTableCols):
        WOTSummaryTable = pd.DataFrame(columns=WOTSummaryTableCols)
        df = wdf.copy()
        columns = ['Time', 'Current', 'WOT', 'Vf', 'Tf']
        df.columns = columns
        df['stepTime'] = df.Time.diff().dt.total_seconds()
        df['Rf'] = df['Vf'] / df['Current']
        df['I2RT'] = df['Current'] * df['Current'] * df['stepTime'] * df['Rf']
        self.logger.info('---- Starting Unit {} Data Ingest Process ----'.format(unit))
        for i in range(1, wot_count):
            tdf = df[df['WOT'] == i].iloc[1:-1]
            time = tdf.stepTime.sum()
            minDUT = tdf.Tf.min()
            maxDUT = tdf.Tf.max()
            rangeDUT = maxDUT - minDUT
            meanCurrent = tdf.Current.mean()
            meanResistance = tdf.Rf.mean()
            stdResistance = tdf.Rf.std()
            maxResistance = tdf.Rf.max()
            minResistance = tdf.Rf.min()
            rangeResistance = maxResistance - minResistance
            meanVoltageDrop = tdf.Vf.mean()
            stdVoltageDrop = tdf.Vf.std()
            measI2RT = tdf.I2RT.sum()
            meanI2RT = meanCurrent * meanCurrent * time * meanResistance

            row = [i, unit, time, meanCurrent, meanResistance, stdResistance, maxResistance,
                   minResistance, rangeResistance, meanVoltageDrop, stdVoltageDrop, measI2RT, meanI2RT,
                   minDUT, maxDUT, rangeDUT, np.nan, np.nan, np.nan]
            row_df = pd.DataFrame(data=[row], columns=WOTSummaryTableCols)
            # WOTSummaryTable = WOTSummaryTable.append(pd.Series(row, index=WOTSummaryTable.columns), ignore_index=True)
            WOTSummaryTable = pd.concat(objs=[WOTSummaryTable, row_df], ignore_index=True)
        self.logger.info('---- Finished Unit {} Data Ingest Process ----'.format(unit))
        WOTSummaryTable['AccumCurrentTime'] = WOTSummaryTable.CycleTime.cumsum()
        WOTSummaryTable['AccumMeanI2RT'] = WOTSummaryTable.meanI2RT.cumsum()
        WOTSummaryTable['AccumMeasI2RT'] = WOTSummaryTable.measI2RT.cumsum()
        return WOTSummaryTable

    def LoadRawFiles(self):
        # File Cleanup
        self.logger.info('Starting Load Raw Files')
        os.chdir(self.data)
        df_15A = pd.DataFrame()
        df_35A = pd.DataFrame()
        df_50A = pd.DataFrame()
        df_60A = pd.DataFrame()

        for subdir, dirs, files in sorted(os.walk(self.data)):
            files[:] = [x for x in files if any(y in x for y in ['.csv'])]
            subdir_short = subdir[len(self.data)+1:]
            for file in sorted(files):
                fuse_rating = subdir_short.split('/')[0]
                self.logger.debug('Starting {} : {}'.format(fuse_rating,file))

                df, status = self.ReadFile(fuse_rating, os.path.join(subdir, file))
                if (fuse_rating == '15A') & (status == True):
                    df_15A = pd.concat([df_15A,df])
                elif (fuse_rating == '35A') & (status == True):
                    df_35A = pd.concat([df_35A,df])
                elif (fuse_rating == '50A') & (status == True):
                    df_50A = pd.concat([df_50A, df])
                if (fuse_rating == '60A') & (status == True):
                    df_60A = df_60A.append(df)



        df_15A['CurrentCycleCount'] = df_15A['CurrentCycleStart'].cumsum()
        df_35A['CurrentCycleCount'] = df_35A['CurrentCycleStart'].cumsum()
        df_50A['CurrentCycleCount'] = df_50A['CurrentCycleStart'].cumsum()
        df_60A['CurrentCycleCount'] = df_60A['CurrentCycleStart'].cumsum()

        self.logger.info('Started Save Raw File Process')
        self.OutputToExcel(df_15A, 'combined_15A_data.csv')
        self.logger.info('15A File Saved')
        self.OutputToExcel(df_35A, 'combined_35A_data.csv')
        self.logger.info('35A File Saved')
        self.OutputToExcel(df_50A, 'combined_50A_data.csv')
        self.logger.info('50A File Saved')
        self.OutputToExcel(df_60A, 'combined_60A_data.csv')
        self.logger.info('60A File Saved')

    def ReadFile(self,fuse, file):
        df = pd.read_csv(file)
        if len(df.columns) < 50:
            status = False
            print(len(df.columns),file)

        else:
            status = True
            # df.columns = self.signalconversion
            df.set_axis([*df.columns[:-1], 'Current'], axis=1, inplace=True)
            df['Timestamp'] = pd.TimedeltaIndex(df['Timestamp'], unit='d') + dt.datetime(1899, 12, 30)
            df['CurrentCycleStart'] = np.where((df['Current'] > 10) & (df['Current'].shift(1) < 10), 1, 0)
            df['Fuse_Rating'] = fuse

        return df, status

    def OutputToExcel(self, df, filename):
        filename = self.summary_folder + '/' + filename
        df.reset_index(inplace=True, drop=True)
        df.to_csv(path_or_buf=filename)


if __name__ == "__main__":
    test = aux_fuse_data()



