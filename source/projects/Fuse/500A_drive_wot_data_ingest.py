import os
# import math
import time
import datetime as dt
import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
import statistics
import logging


class WOT_data:
    def __init__(self):
        logging.basicConfig(
            format='%(asctime)s - %(levelname)s - %(message)s')  # filename='PTC_Data.log',
        self.logger = logging.getLogger()
        self.logger.setLevel(logging.DEBUG)
        self.logger.info('---- Starting WOT Data Ingest Process ----')
        self.root = r"/Users/jtbruschetto/Documents/OneDrive - Rivian Automotive, LLC/WOT"
        self.summary_folder = r"/Users/jtbruschetto/GIT/JBPL/source/projects/Fuse/summary_data"
        self.Controller()

    def Controller(self):
        # self.LoadRawFiles()
        self.ManipulateRawFile()

    def ManipulateRawFile(self):
        filename = self.summary_folder + '/' + 'combined_WOT_data.csv'
        df = pd.read_csv(filename,parse_dates=['Timestamp'],index_col=0)
        wot_count = df['CurrentCycleCount'].max()
        Units = {'1':['Vf1','Tf1'],
                 '2':['Vf2','Tf2'],
                 '3':['Vf3','Tf3'],
                 '4':['Vf4','Tf4'],
                 '5':['Vf5','Tf5'],
                 '6':['Vf6','Tf6'],
                 '7':['Vf7','Tf7'],
                 '8':['Vf8','Tf8'],
                 '9':['Vf9','Tf9'],
                 '10':['Vf10','Tf10'],
                 '11':['Vf11','Tf11'],
                 '12':['Vf12','Tf12'],
                 '13':['Vf13','Tf13'],
                 '14':['Vf14','Tf14'],
                 '15':['Vf15','Tf15'],
                 '16':['Vf16','Tf16']}

        print(df.info())
        WOTSummaryTable = pd.DataFrame(
            columns=['CurrentCycle', 'UnitID', 'CycleTime', 'meanCurrent', 'meanResistance', 'stdResistance',
                     'maxResistance', 'minResistance', 'rangeResistance', 'meanVoltageDrop',
                     'stdVoltageDrop', 'measI2RT', 'meanI2RT', 'minDUT', 'maxDUT', 'rangeDUT','AccumCurrentTime','AccumMeanI2RT','AccumMeasI2RT'])
        for key in Units:
            columns = ['Timestamp','Current','CurrentCycleCount']
            columns.append(Units[key][0])
            columns.append(Units[key][1])
            tdf = df[columns]
            WOTUnitData = self.WOTcycleEval(key,tdf,wot_count,WOTSummaryTable.columns)
            WOTSummaryTable = pd.concat([WOTSummaryTable,WOTUnitData],ignore_index=True)

        WOTSummaryTable['ThermalCycle'] = WOTSummaryTable['CurrentCycle'] // 20 + 1
        self.OutputToExcel(WOTSummaryTable,'WOT_eval_summary.csv')

    def WOTcycleEval(self,unit,wdf,wot_count,WOTSummaryTableCols):
        WOTSummaryTable = pd.DataFrame(columns=WOTSummaryTableCols)
        df=wdf.copy()
        columns = ['Time','Current','WOT','Vf','Tf']
        df.columns = columns
        df['stepTime'] = df.Time.diff().dt.total_seconds()
        df['Rf'] = df['Vf'] / df['Current']
        df['I2RT'] = df['Current'] * df['Current'] * df['stepTime'] * df['Rf']
        self.logger.info('---- Starting Unit {} Data Ingest Process ----'.format(unit))
        for i in range(1, wot_count):
            tdf = df[df['WOT'] == i].iloc[1:-1]
            time = tdf.stepTime.sum()
            minDUT = tdf.Tf.min()
            maxDUT = tdf.Tf.max()
            rangeDUT = maxDUT - minDUT
            meanCurrent = tdf.Current.mean()
            meanResistance = tdf.Rf.mean()
            stdResistance = tdf.Rf.std()
            maxResistance = tdf.Rf.max()
            minResistance = tdf.Rf.min()
            rangeResistance = maxResistance - minResistance
            meanVoltageDrop = tdf.Vf.mean()
            stdVoltageDrop = tdf.Vf.std()
            measI2RT = tdf.I2RT.sum()
            meanI2RT = meanCurrent * meanCurrent * time * meanResistance

            row = [i, unit, time, meanCurrent, meanResistance, stdResistance, maxResistance,
                   minResistance, rangeResistance, meanVoltageDrop, stdVoltageDrop, measI2RT, meanI2RT,
                   minDUT, maxDUT, rangeDUT,np.nan,np.nan,np.nan]
            row_df = pd.DataFrame(data=[row],columns=WOTSummaryTableCols)
            # WOTSummaryTable = WOTSummaryTable.append(pd.Series(row, index=WOTSummaryTable.columns), ignore_index=True)
            WOTSummaryTable = pd.concat(objs=[WOTSummaryTable,row_df],ignore_index=True)
        self.logger.info('---- Finished Unit {} Data Ingest Process ----'.format(unit))
        WOTSummaryTable['AccumCurrentTime'] = WOTSummaryTable.CycleTime.cumsum()
        WOTSummaryTable['AccumMeanI2RT'] = WOTSummaryTable.meanI2RT.cumsum()
        WOTSummaryTable['AccumMeasI2RT'] = WOTSummaryTable.measI2RT.cumsum()
        return WOTSummaryTable

    def LoadRawFiles(self):
        # File Cleanup
        files = os.listdir(self.root)
        files[:] = [x for x in files if any(y in x for y in ['.csv'])]
        files_df = pd.DataFrame(files,columns=['filename'])
        files_df[['Profile','Frequency','WOT_#','Date','Time']] = files_df.filename.str.split(pat=' ',expand=True)
        files_df = files_df.astype({'filename': 'str','WOT_#':'int64'})
        files_df.sort_values(by='WOT_#',ascending=True,inplace=True)
        # For Loop through Files sequentially
        MasterTableWOT = pd.DataFrame()
        MasterTableCurrentSummary = pd.DataFrame()
        wot_cycle = 0
        for file in files_df.filename.iteritems():
            print(file[1])
            raw_WOT_df, raw_WOT_summary_df = self.ReadFile(file[1])
            raw_WOT_df['CurrentCycleCount'] = raw_WOT_df['CurrentCycleCount'] + wot_cycle
            raw_WOT_summary_df['WOT_#'] = wot_cycle
            MasterTableCurrent = pd.concat([MasterTableCurrent, raw_WOT_df])
            MasterTableCurrentSummary = pd.concat([MasterTableCurrentSummary,raw_WOT_summary_df])
            wot_cycle = raw_WOT_df['CurrentCycleCount'].max()

        print(MasterTableCurrent)
        self.OutputToExcel(MasterTableCurrent,'combined_WOT_data.csv')
        self.OutputToExcel(MasterTableCurrentSummary,'Temp_cycle_summary.csv')

    def ReadFile(self, file):
        filename = self.root+r'/'+file
        df = pd.read_csv(filename)
        column_conversion = {'Timestamp':'Timestamp',
                             '9300 2:VOLTAGE':'Voltage',
                             '9300 2:CURRENT':'Current',
                             'LOWDAQ2:Vfuse1':'Vf1',
                             'LOWDAQ2:Vfuse2':'Vf2',
                             'LOWDAQ2:Vfuse3':'Vf3',
                             'LOWDAQ2:Vfuse4':'Vf4',
                             'LOWDAQ2:Vfuse5':'Vf5',
                             'LOWDAQ2:Vfuse6':'Vf6',
                             'LOWDAQ2:Vfuse7':'Vf7',
                             'LOWDAQ2:Vfuse8':'Vf8',
                             'LOWDAQ2:Vfuse9':'Vf9',
                             'LOWDAQ2:Vfuse10':'Vf10',
                             'LOWDAQ2:Vfuse11':'Vf11',
                             'LOWDAQ2:Vfuse12':'Vf12',
                             'LOWDAQ2:Vfuse13':'Vf13',
                             'LOWDAQ2:Vfuse14':'Vf14',
                             'LOWDAQ2:Vfuse15':'Vf15',
                             'LOWDAQ2:Vfuse16':'Vf16',
                             'LOWDAQ2:Vtotal':'Vtotal',
                             'LOWDAQ2:Tfuse1':'Tf1',
                             'LOWDAQ2:Tfuse2':'Tf2',
                             'LOWDAQ2:Tfuse3':'Tf3',
                             'LOWDAQ2:Tfuse4':'Tf4',
                             'LOWDAQ2:Tfuse5':'Tf5',
                             'LOWDAQ2:Tfuse6':'Tf6',
                             'LOWDAQ2:Tfuse7':'Tf7',
                             'LOWDAQ2:Tfuse8':'Tf8',
                             'LOWDAQ2:Tfuse9':'Tf9',
                             'LOWDAQ2:Tfuse10':'Tf10',
                             'LOWDAQ2:Tfuse11':'Tf11',
                             'LOWDAQ2:Tfuse12':'Tf12',
                             'LOWDAQ2:Tfuse13':'Tf13',
                             'LOWDAQ2:Tfuse14':'Tf14',
                             'LOWDAQ2:Tfuse15':'Tf15',
                             'LOWDAQ2:Tfuse16':'Tf16',
                             'LOWDAQ2:Tfuse#1 Terminal':'Tf1_Term',
                             'LOWDAQ2:Tfuse#3 Terminal':'Tf3_Term',
                             'LOWDAQ2:Tfuse#6 Terminal':'Tf6_Term',
                             'LOWDAQ2:Tfuse#11 Terminal':'Tf11_Term',
                             'LOWDAQ2:Tfuse#14 Terminal':'Tf14_Term',
                             'LOWDAQ2:Tfuse#16 Terminal':'Tf16_Term',
                             'LOWDAQ2:Tair middle':'Tair_Mid',
                             'LOWDAQ2:Tair edge':'Tair_Edge'}
        df.rename(columns=column_conversion,inplace=True)
        df['Timestamp'] = pd.TimedeltaIndex(df['Timestamp'], unit='d') + dt.datetime(1899, 12, 30)

        dfs = df.agg([min, max])

        df['CurrentCycleStart'] = np.where((df['Current'] > 10) & (df['Current'].shift(1) < 10), 1, 0)
        df['CurrentCycleCount'] = df['CurrentCycleStart'].cumsum()

        df = df[df['Current'] > 10]
        return df, dfs

    def OutputToExcel(self, df, filename):
        filename = self.summary_folder + '/' + filename
        df.reset_index(inplace=True, drop=True)
        df.to_csv(path_or_buf=filename)

if __name__ == "__main__":
    test = WOT_data()
