# /usr/local/bin/python3.9 /Users/jtbruschetto/GIT/JBPL/source/Aptiv/EOL_data_ingest.py

import os
import math
import pandas as pd
from pandas.api.types import is_numeric_dtype
import matplotlib.pyplot as plt
import numpy as np
import statistics
import logging
from openpyxl import load_workbook
from itertools import islice

class PTCE_data:
    NegHVDBdictionary = {
        'SN':'SN',
        'Data':'Date',
        'Hora':'Hour',
        'Status Final':'Final Status',
        'Step 5.2 Contactor Contact of current pulse primary contactor - Ciclo 1': 'PC_Pulse_1',
        'Step 17.3 Contactor Contact of current pulse DCFC contactor - Ciclo 1': 'DC_Pulse_1',
        'Step 5.2 Contactor Contact of current pulse primary contactor - Ciclo 2': 'PC_Pulse_2',
        'Step 17.3 Contactor Contact of current pulse DCFC contactor - Ciclo 2': 'DC_Pulse_2',
        'Step 5.2 Contactor Contact of current pulse primary contactor - Ciclo 3': 'PC_Pulse_3',
        'Step 17.3 Contactor Contact of current pulse DCFC contactor - Ciclo 3': 'DC_Pulse_3',
        'Step 5.2 Contactor Contact of current pulse primary contactor - Ciclo 4': 'PC_Pulse_4',
        'Step 17.3 Contactor Contact of current pulse DCFC contactor - Ciclo 4': 'DC_Pulse_4',
        'Step 5.2 Contactor Contact of current pulse primary contactor - Ciclo 5': 'PC_Pulse_5',
        'Step 17.3 Contactor Contact of current pulse DCFC contactor - Ciclo 5': 'DC_Pulse_5',
        'Step 12.2 Contactor Contact of current pulse secondary contactor - Ciclo 1': 'SC_Pulse_1',
        'Step 12.2 Contactor Contact of current pulse secundary contactor - Ciclo 2': 'SC_Pulse_2',
        'Step 12.2 Contactor Contact of current pulse primary contactor - Ciclo 3': 'SC_Pulse_3',
        'Step 12.2 Contactor Contact of current pulse secundary contactor - Ciclo 4': 'SC_Pulse_4',
        'Step 12.2 Contactor Contact of current pulse secundary contactor - Ciclo 5': 'SC_Pulse_5',
        'Contactor Contact Range Primary': 'PC_Range_Max',
        'Contactor Contact Range Secundary': 'SC_Range_Max',
        'Contactor Contact Range DCFC': 'DC_Range_Max'}
    # PosHVDBdictionary = {}
    trace_column_names = {'111prime':'PC_P1',
                          '111prime.1':'PC_P2',
                          '111prime.2':'PC_P3',
                          '111prime.3':'PC_P4',
                          '111prime.4':'PC_P5',
                          '111sec':'SC_P1',
                          '111sec.1':'SC_P2',
                          '111sec.2':'SC_P3',
                          '111sec.3':'SC_P4',
                          '111sec.4':'SC_P5',
                          '112': 'DC_P1',
                          '112.1': 'DC_P2',
                          '112.2': 'DC_P3',
                          '112.3': 'DC_P4',
                          '112.4': 'DC_P5',
                          '110prime': 'PC_P1',
                          '110prime.1': 'PC_P2',
                          '110prime.2': 'PC_P3',
                          '110prime.3': 'PC_P4',
                          '110prime.4': 'PC_P5',
                          '110sec': 'SC_P1',
                          '110sec.1': 'SC_P2',
                          '110sec.2': 'SC_P3',
                          '110sec.3': 'SC_P4',
                          '110sec.4': 'SC_P5',
                          '111': 'DC_P1',
                          '111.1': 'DC_P2',
                          '111.2': 'DC_P3',
                          '111.3': 'DC_P4',
                          '111.4': 'DC_P5'
                          }

    def __init__(self):
        os.chdir('/projects/Aptiv')
        logging.basicConfig(
                            format='%(asctime)s - %(levelname)s - %(message)s') #filename='PTC_Data.log',
        self.logger = logging.getLogger()
        self.logger.setLevel(logging.DEBUG)
        self.logger.info('---- Starting Aptiv PTC Data Ingest Process ----')
        data_folder = {'Neg': r"raw_EOL_data/LOG NEG pulse data/",
                       'Pos': r"raw_EOL_data/LOG POS pulse data/"}
        files = os.listdir(data_folder['Neg'])
        files = [k for k in files if '.csv' in k]

        self.Controller()



    def Controller(self):
        os.chdir('/projects/Aptiv')

        data_folder = {'Neg': r"raw_EOL_data/LOG NEG pulse data/",
                       'Pos': r"raw_EOL_data/LOG POS pulse data/"}

        files = os.listdir(data_folder['Neg'])
        files = [k for k in files if '.csv' in k]
        dfn = self.loadExcel(data_folder['Neg'],files)
        self.logger.info('Completed Loading')
        dfn['HVDB'] = "Neg"

        files = os.listdir(data_folder['Pos'])
        files = [k for k in files if '.csv' in k]
        dfp = self.loadExcel(data_folder['Pos'],files)
        self.logger.info('Completed Loading')
        dfp['HVDB'] = "Pos"

        df = dfn.append(dfp)

        print(df)

        HVDB = list(df.HVDB.unique())
        Contactor = list(df.Contactor.unique())
        PulseNumber = list(df.PulseNumber.unique())
        SN = list(df.SN.unique())
        deltalst = []
        for hvdb in HVDB:
            print(hvdb)
            for sn in SN:
                print(sn)
                for contactor in Contactor:
                    print(contactor)
                    for pn in PulseNumber:
                        try:
                            halftime = df.loc[(df['HVDB'] == hvdb)&(df['SN'] == sn)&(df['Contactor'] == contactor)&(df[ 'PulseNumber'] == pn)&(df['pulseID'] == 78)]['VoltageDrop'].values[0]
                            fulltime = df.loc[(df['HVDB'] == hvdb)&(df['SN'] == sn)&(df['Contactor'] == contactor)&(df[ 'PulseNumber'] == pn)&(df['pulseID'] == 146)]['VoltageDrop'].values[0]
                            delta = halftime - fulltime
                            deltalst.append(delta)
                        except:
                            print('error on {}-{}-{}-{}'.format(hvdb,sn,contactor,PulseNumber))

        print('Deltalst mean: {}'.format(statistics.mean(deltalst)))
        print('Deltalst std: {}'.format(statistics.stdev(deltalst)))
        print('Deltalst quantiles: {}'.format(statistics.quantiles(deltalst)))





        df.set_index(['HVDB','SN','Contactor','PulseNumber','pulseID'],inplace=True)
        df.sort_index(ascending=True,inplace=True)
        df['VoltageDropShift1pt'] = df.groupby(level=[0,1,2,3])['VoltageDrop'].shift(1)
        df['VoltageDropShift10pt'] = df.groupby(level=[0, 1, 2, 3])['VoltageDrop'].shift(10)
        df['VoltageDropShift25pt'] = df.groupby(level=[0, 1, 2, 3])['VoltageDrop'].shift(10)
        df['VoltageDropDelta1pt'] = df['VoltageDrop'] - df['VoltageDropShift1pt']
        df['VoltageDropDelta10pt'] = df['VoltageDrop'] - df['VoltageDropShift10pt']
        df['VoltageDropDelta25pt'] = df['VoltageDrop'] - df['VoltageDropShift25pt']
        print(df)







        self.OutputToExcel(df, r'diagnostic_data/EOL_test_trace_file.csv')


    def loadExcel(self,root,files):

        df = pd.DataFrame()
        files = sorted(files)
        print(files)
        for i in range(0, len(files)):
            self.logger.info('Starting: {}'.format(files[i]))
            ldf = pd.read_csv(root+files[i],
                              index_col=None,
                              header=0,
                              delimiter=';',
                              decimal=',',
                              low_memory=False)


            ldf['file'] = files[i]
            ldf.index = ldf.index.set_names(['pulseID'])
            ldf.reset_index(inplace=True)
            lst = list(ldf.columns.values)
            # print(lst)
            # print(ldf.info())
            df = df.append(ldf)
            del ldf

        Cols = df.columns.to_list()
        Cols = [x if x not in self.trace_column_names else self.trace_column_names[x] for x in Cols]
        df.columns = Cols
        df[['SN', 'SL']] = df.file.str.split('_',expand=True)
        dropCol = ['file','SL']
        df.drop(columns=dropCol,inplace=True)
        print(df.SN.unique())
        df.set_index(['SN','pulseID'],inplace=True)

        df = df.stack()
        # df.index = pd.MultiIndex.rename(level="None",names='cID_pulseN',inplace=True)
        df = df.to_frame(name='VoltageDrop')
        df.reset_index(inplace=True)
        print(df)
        df.rename(columns={'level_2': 'cID_pulseNUM'}, inplace=True)
        df[['Contactor', 'PulseNumber']] = df.cID_pulseNUM.str.split('_', expand=True)
        dropCol = ['cID_pulseNUM']
        df.drop(columns=dropCol, inplace=True)


        print(df.index)
        # df = df.groupby(['SN','pulseID']).sum()['Salary']
        # df = df.unstack()
        # df.reset_index(inplace=True)
        # print(df.info())

        return df

    def OutputToExcel(self, df, filename):
        # df.reset_index(inplace=True, drop=True)
        df.to_csv(path_or_buf=filename)

if __name__ == "__main__":
    test = PTCE_data()
