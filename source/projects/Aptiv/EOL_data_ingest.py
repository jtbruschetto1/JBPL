# /usr/local/bin/python3.9 /Users/jtbruschetto/GIT/JBPL/source/Aptiv/EOL_data_ingest.py

import os
import math
import pandas as pd
from pandas.api.types import is_numeric_dtype
import matplotlib.pyplot as plt
import numpy as np
import statistics
import logging
from openpyxl import load_workbook
from itertools import islice

class PTCE_data:
    NegHVDBdictionary = {
        'SN':'SN',
        'Data':'Date',
        'Hora':'Hour',
        'Status Final':'Final Status',
        'Step 5.2 Contactor Contact of current pulse primary contactor - Ciclo 1': 'PC_Pulse_1',
        'Step 17.3 Contactor Contact of current pulse DCFC contactor - Ciclo 1': 'DC_Pulse_1',
        'Step 5.2 Contactor Contact of current pulse primary contactor - Ciclo 2': 'PC_Pulse_2',
        'Step 17.3 Contactor Contact of current pulse DCFC contactor - Ciclo 2': 'DC_Pulse_2',
        'Step 5.2 Contactor Contact of current pulse primary contactor - Ciclo 3': 'PC_Pulse_3',
        'Step 17.3 Contactor Contact of current pulse DCFC contactor - Ciclo 3': 'DC_Pulse_3',
        'Step 5.2 Contactor Contact of current pulse primary contactor - Ciclo 4': 'PC_Pulse_4',
        'Step 17.3 Contactor Contact of current pulse DCFC contactor - Ciclo 4': 'DC_Pulse_4',
        'Step 5.2 Contactor Contact of current pulse primary contactor - Ciclo 5': 'PC_Pulse_5',
        'Step 17.3 Contactor Contact of current pulse DCFC contactor - Ciclo 5': 'DC_Pulse_5',
        'Step 12.2 Contactor Contact of current pulse secondary contactor - Ciclo 1': 'SC_Pulse_1',
        'Step 12.2 Contactor Contact of current pulse secundary contactor - Ciclo 2': 'SC_Pulse_2',
        'Step 12.2 Contactor Contact of current pulse primary contactor - Ciclo 3': 'SC_Pulse_3',
        'Step 12.2 Contactor Contact of current pulse secundary contactor - Ciclo 4': 'SC_Pulse_4',
        'Step 12.2 Contactor Contact of current pulse secundary contactor - Ciclo 5': 'SC_Pulse_5',
        'Contactor Contact Range Primary': 'PC_Range_Max',
        'Contactor Contact Range Secundary': 'SC_Range_Max',
        'Contactor Contact Range DCFC': 'DC_Range_Max'}
    # PosHVDBdictionary = {}
    def __init__(self):
        os.chdir('/projects/Aptiv')
        logging.basicConfig(
                            format='%(asctime)s - %(levelname)s - %(message)s') #filename='PTC_Data.log',
        self.logger = logging.getLogger()
        self.logger.setLevel(logging.DEBUG)
        self.logger.info('---- Starting Aptiv PTC Data Ingest Process ----')
        data_folder = r"raw_EOL_data/"
        files = os.listdir(data_folder)
        files = [k for k in files if '.csv' in k]

        self.Controller(files,data_folder)



    def Controller(self, files, root):
        load = 0
        os.chdir('/projects/Aptiv')

        self.logger.info('Loading Test File')
        df = self.loadExcel(root,files)
        self.logger.info('Completed Loading')
        Cols = list(df.columns)

        print(df.shape)
        for item in Cols:
            print('starting {}'.format(item))
            if 'Pulse' in item:
                print('... Starting String Split')
                print(df[[str(item)]])
                df[str(item)] = df[str(item)].str.replace('_', ' ')
                df[str(item)] = df[str(item)].str.replace(',', '.')
                try:
                    df[[str(item + '_status')]] = df[str(item)].str.split().str[0]
                    df[[str(item + '_Mean')]] = df[str(item)].str.split().str[3]
                    df[[str(item + '_Max')]] = df[str(item)].str.split().str[5]
                    # df[[str(item + '_Range')]] = df[str(item + '_Max')] - df[str(item + '_Mean')]
                except:
                    df[[str(item + '_status')]] = None
                    df[[str(item + '_Mean')]] = None
                    df[[str(item + '_Max')]] = None
                    # df[[str(item + '_Range')]] = None
                print(df[[str(item)]])
            if 'Range' in item:
                print('... Starting String Split')
                print(df[[str(item)]])
                df[str(item)] = df[str(item)].str.replace('_', ' ')
                df[str(item)] = df[str(item)].str.replace(',', '.')
                try:
                    df[[str(item + '_status')]] = df[str(item)].str.split().str[0]
                    df[[str(item + '_Points')]] = df[str(item)].str.split().str[3]
                    df[[str(item + '_MaxRange')]] = df[str(item)].str.split().str[5]

                except:
                    df[[str(item + '_status')]] = None
                    df[[str(item + '_Points')]] = None
                    df[[str(item + '_MaxRange')]] = None

                print(df[[str(item)]])

        df = df[df['PC_Pulse_1']!='---']
        print(df.shape)
        print(df)
        self.OutputToExcel(df, r'diagnostic_data/EOL_test_file.csv')



    def loadExcel(self,root,files):

        df = pd.DataFrame()
        files = sorted(files)
        print(files)
        for i in range(0, len(files)):
            self.logger.info('Starting: {}'.format(files[i]))
            ldf = pd.read_csv(root+files[i],
                              index_col=None,
                              header=0,
                              delimiter=',',
                              parse_dates=['Data', 'Hora'],
                              low_memory=False)

            lst = list(ldf.columns.values)
            # droplst = list(filter(lambda x: str(x)[-2:] == '.1', lst))
            # ldf.drop(columns=droplst, inplace=True)
            print(ldf.info())
            df = df.append(ldf)
            del ldf

        Cols = df.columns.to_list()
        Cols = [x if x not in self.NegHVDBdictionary else self.NegHVDBdictionary[x] for x in Cols]
        # print(Cols)
        df.columns = Cols
        df_Cols = list(self.NegHVDBdictionary.values())
        # print(df_Cols)
        ndf = df[df_Cols].copy()
        # print(ndf)

        return ndf

    def OutputToExcel(self, df, filename):
        df.reset_index(inplace=True, drop=True)
        df.to_csv(path_or_buf=filename)

if __name__ == "__main__":
    test = PTCE_data()
