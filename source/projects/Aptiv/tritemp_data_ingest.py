# /usr/local/bin/python3.9 /Users/jtbruschetto/GIT/JBPL/source/Aptiv/TriTemp_data_ingest.py

import os
import math
import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
import statistics
import logging
from openpyxl import load_workbook
from itertools import islice

class TriTemp_data:
    def __init__(self):
        os.chdir('/projects/Aptiv')
        logging.basicConfig(
                            format='%(asctime)s - %(levelname)s - %(message)s') #filename='TriTemp_Data.log',
        self.logger = logging.getLogger()
        self.logger.setLevel(logging.DEBUG)
        self.logger.info('---- Starting Aptiv TriTemp Data Ingest Process ----')
        data_folder = r"raw_TriTemp_data/"
        files = os.listdir(data_folder)
        files = [k for k in files if '.csv' in k]
        self.Controller(files,data_folder)

    def Controller(self, files, root):
        MasterTableTemp = pd.DataFrame()
        MasterTableCurrent = pd.DataFrame()
        load = 1
        os.chdir('/projects/Aptiv')
        if load == 1:
            self.logger.info('Loading All Test Files')
            df = self.loadExcel(root, files)
            self.OutputToExcel(df, r'joined_raw_TriTemp_data/Aptiv_TriTemp_Data.csv')
            self.logger.info('Completed File Loading')
        else:
            self.logger.info('Loading Joined Test File')
            df = pd.read_csv(r'joined_raw_TriTemp_data/Aptiv_TriTemp_Data.csv',
                             index_col=0,
                             header=0,
                             delimiter=',',
                             parse_dates=['Date', 'Time', 'Real Date','stepTime'],
                             infer_datetime_format=True,
                             low_memory=False)
            df['stepTime'] = pd.to_timedelta(df['stepTime'])
            print(df.stepTime.dtypes)
            self.logger.info('Completed Loading')

        colDF, SNlst = self.ParseSN(df)

        for sn in SNlst:
            self.logger.info('Starting SN: {} ({}/{})'.format(sn,SNlst.index(sn),len(SNlst)))
            snDF = colDF[colDF['SN'] == str(sn)]
            type = snDF.Polarity.unique().tolist()
            for Pol in type:
                self.logger.info('Starting Pol: {}'.format(Pol))
                snPDF = snDF[snDF['Polarity'] == Pol]
                # print(snPDF)
                if snPDF.empty == True:
                    continue
                snCOLS = snPDF['key'].tolist()
                snLEG = snPDF['Legend'].tolist()
                stdCOLS = ['Date','Time','stepTime','TempCycle','TempCycleCount','CurrentCycleCount','CurrentCycleIndicator','PowerCycleCount','PowerCycleIndicator','CURRENT','T_AMB_1','T_AMB_2']
                stdrnmCOLS = ['Date','Time','stepTime','TempCycle','TempCycleCount','CurrentCycleCount','CurrentCycleIndicator','PowerCycleCount','PowerCycleIndicator','CURRENT','T_AMB_1','T_AMB_2']
                fltCOLS = stdCOLS
                rnmCOLS = stdrnmCOLS
                fltCOLS.extend(snCOLS)
                rnmCOLS.extend(snLEG)
                snDATA = df[fltCOLS].copy()
                snDATA.columns = rnmCOLS
                CurrentSummaryTable = self.CurrentCycleVoltageData(snDATA.copy(), Pol, sn)

                Procdf, TempSummaryTable = self.TempCycleVoltageData(snDATA.copy(), CurrentSummaryTable.copy(), Pol,sn)

                TempSummaryTable['key'] = Pol + str('-') + sn
                CurrentSummaryTable['key'] = Pol + str('-') + sn
                MasterTableTemp = MasterTableTemp.append(TempSummaryTable)
                MasterTableCurrent = MasterTableCurrent.append(CurrentSummaryTable)

                self.OutputToExcel(Procdf, r'unit_data/Aptiv_TriTemp_Temp_Unit_Data_SN{}_Pol-{}.csv'.format(sn, Pol))
                self.OutputToExcel(TempSummaryTable, r'unit_summary_data/Aptiv_TriTemp_Temp_Summary_SN{}_Pol-{}.csv'.format(sn,Pol))


                del TempSummaryTable
                del Procdf
        #
        # self.PlotAllSamples(MasterTableTemp[['key','TempCycle','meanCurrent','meanDCFCv','meanDCFCcr','AccumDCBBdT','AccumDCFCI2RT']])
        # self.PlotAllSamples(MasterTableTemp[['key', 'TempCycle', 'meanDCFCcr', 'eSTDmCR', 'eSTDmCRs', 'rSTDmCR', 'rMEANmCR', 'MADeSTDmCR', 'MADemCR']])
        self.OutputToExcel(MasterTableTemp, r'summary_data/Aptiv_TriTemp_Temp_Summary.csv')
        self.OutputToExcel(MasterTableCurrent, r'summary_data/Aptiv_TriTemp_Current_Summary.csv')

    def loadExcel(self, root, files):
        # pd.set_option('display.max_rows', None, 'display.max_columns', 20)

        na_values = ['+OVER', '-OVER', '#NOME?']
        df = pd.DataFrame()
        files = sorted(files)
        print(files)
        for i in range(0, len(files)):
            self.logger.info('Starting: {}'.format(files[i]))
            if i == 0:
                ldf = pd.read_csv(root+files[i],
                                  index_col=None,
                                  header=0,
                                  delimiter=',',
                                  parse_dates=['Date', 'Time'],
                                  na_values=na_values,
                                  low_memory=False)

            elif i >= 1:
                ldf = pd.read_csv(root+files[i],
                                  index_col=None,
                                  header=0,
                                  delimiter=',',
                                  parse_dates=['Date', 'Time'],
                                  na_values=na_values,
                                  low_memory=False)
            lst = list(ldf.columns.values)
            droplst = list(filter(lambda x: str(x)[-2:] == '.1', lst))
            # droplst.extend(['BALANCE', 'EXT_SET1', 'EXT_SET2', 'EXT_SET3', 'EXT_SET4', 'EXT_SET5', 'EXT_SET6'])
            ldf.drop(columns=droplst, inplace=True)
            ldf['T_AMB_2'] = pd.to_numeric(ldf['T_AMB_2'], errors='coerce')
            ldf['CURRENT'] = pd.to_numeric(ldf['CURRENT'], errors='coerce')

            print(ldf.info())
            df = df.append(ldf)
            del ldf

        self.logger.info('Finished loading files')
        cols = list(df.columns.values)
        cols = cols[4:]
        print(cols)

        i = 0
        for col in cols:
            i += 1
            self.logger.info('Clean up column {} / {}'.format(i , len(cols)+1))
            df[col] = pd.to_numeric(df[col], errors='coerce')

        print(df)
        df = df.dropna(thresh=100)

        df['stepTime'] = df.Time.diff()
        df = df[(df.stepTime < np.timedelta64(5, 'm')) & (df.stepTime > - np.timedelta64(5, 'm'))]
        df['TempCycle'] = np.where((df.T_AMB_2 >= 0) & (df.T_AMB_2.shift(-100) > 0) & (df.T_AMB_2.shift(1) < 0) & (df.T_AMB_2.shift(2) < 0) & (df.T_AMB_2.shift(3) < 0) &(df.T_AMB_2.shift(100) < 0), 1, 0)
        df['TempCycleCount'] = df['TempCycle'].cumsum()
        df['CURRENT'] = df['CURRENT'].fillna(0)
        df.loc[(df['CURRENT'] < 250) & (df['CURRENT'] > -250), 'CURRENT'] = 0
        df.loc[df['T_AMB_2'] < -60, 'T_AMB_2'] = np.nan
        df.loc[df['T_AMB_1'] < -60, 'T_AMB_1'] = np.nan
        df['CurrentCycleStart'] = np.where((df['CURRENT'] > 1 ) & (df['CURRENT'].shift(1) == 0) & (df['CURRENT'].shift(-1) >= 1) & (df['CURRENT'].shift(-3) >= 1), 1, 0)
        df['CurrentCycleEnd'] = np.where((df['CURRENT'] == 0 ) & (df['CURRENT'].shift(1) > 1) & (df['CURRENT'].shift(-1) == 0) & (df['CURRENT'].shift(3) >= 1), 1, 0)
        df['CurrentCycleCount'] = df['CurrentCycleStart'].cumsum()
        df['CurrentCycleCompare'] = df['CurrentCycleEnd'].cumsum()
        df['CurrentCycleIndicator'] = df['CurrentCycleCount'] - df['CurrentCycleCompare']
        df['PowerCycleStart'] = np.where((df['CURRENT'] > 1) &
                                         (df['CURRENT'].shift(1) == 0) &
                                         (df['CURRENT'].shift(-1000).rolling(min_periods=1, window=1000).mean() >= 10) &
                                         (df['CURRENT'].shift(1).rolling(min_periods=10, window=1000).mean() <= 1)
                                         , 1, 0)
        df['PowerCycleRollingEval150'] = df['CURRENT'].shift(1).rolling(min_periods=1, window=150).mean()
        df['PowerCycleRollingEval300'] = df['CURRENT'].shift(1).rolling(min_periods=1, window=300).mean()
        df['PowerCycleRollingEval1000'] = df['CURRENT'].shift(1).rolling(min_periods=1, window=1000).mean()
        df['PowerCycleEnd'] = np.where((df['CURRENT'] == 0) &
                                       (df['CURRENT'].shift(1) > 1) &
                                       (df['CURRENT'].shift(1).rolling(min_periods=1, window=1000).mean() >= 10) &
                                       (df['CURRENT'].shift(-1001).rolling(min_periods=10, window=1000).mean() <= 1)
                                       , 1, 0)

        df['PowerCycleCount'] = df['PowerCycleStart'].cumsum()
        df['PowerCycleCompare'] = df['PowerCycleEnd'].cumsum()
        df['PowerCycleIndicator'] = df['PowerCycleCount'] - df['PowerCycleCompare']


        self.logger.info('Finished profile characterization')
        return df

    def ParseSN(self, df):
        cols = list(df.columns.values)
        colDF = pd.DataFrame(cols, columns=['key'])

        colDF['Pos'] = np.where(colDF['key'].str[:2] == "PB", 'Pos', '')
        colDF['Neg'] = np.where(colDF['key'].str[:2] == "NB", 'Neg', '')
        colDF['Polarity'] = colDF.Pos + colDF.Neg
        colDF['F1type'] = np.where((colDF['Pos'] == 1) & (colDF['key'].str[3] == '1'), "Eaton", '')
        colDF['F2type'] = np.where((colDF['Pos'] == 1) & (colDF['key'].str[3] == '2'), "Mersen", '')
        colDF['Ftype'] = colDF.F1type + colDF.F2type
        colDF['SN'] = np.where((colDF['Pos'] == 'Pos'), colDF['key'].str[5:7], colDF['key'].str[3:5])

        colDF['measT'] = np.where(colDF['key'].str[-2:-1] == 'T', 1, 0)
        colDF['T1'] = np.where(colDF['key'].str[-2:] == 'T1', 'Input_BB', '')
        colDF['T2'] = np.where(colDF['key'].str[-2:] == 'T2', 'Output_BB', '')
        colDF['T3'] = np.where(colDF['key'].str[-2:] == 'T3', 'DCFC_BB', '')
        colDF['T4'] = np.where(colDF['key'].str[-2:] == 'T4', 'Sec_Fuse_Body', '')
        colDF['T5'] = np.where(colDF['key'].str[-2:] == 'T5', 'Prim_Fuse_Body', '')
        colDF['T6'] = np.where(colDF['key'].str[-2:] == 'T6', 'Sec_Fuse_Term', '')
        colDF['T7'] = np.where(colDF['key'].str[-2:] == 'T7', 'Prim_Fuse_Term', '')

        colDF['IOV'] = np.where(colDF['key'].str[-7:] == 'INP_OUT', 'INP_OUT', '')
        colDF['ODCFCV'] = np.where(colDF['key'].str[-8:] == 'OUT_DCFC', 'OUT_DCFC', '')
        colDF['FPV'] = np.where(colDF['key'].str[-7:] == 'F_PRIME', 'F_', '')
        colDF['FSV'] = np.where(colDF['key'].str[-5:] == 'F_SEC', 'F_', '')
        colDF['PV'] = np.where(colDF['key'].str[-5:] == 'PRIME', 'PRIME', '')
        colDF['SV'] = np.where(colDF['key'].str[-3:] == 'SEC', 'SEC', '')
        colDF['ShuntV'] = np.where(colDF['key'].str[6:12] == 'SHUNT', 'SHUNT', '')
        colDF['ShuntI'] = np.where(colDF['key'].str[-13:] == 'SHUNT_CURRENT', 'SHUNT_CURRENT', '')
        colDF[
            'Legend'] = colDF.T1 + colDF.T2 + colDF.T3 + colDF.T4 + colDF.T5 + colDF.T6 + colDF.T7 + colDF.IOV + colDF.ODCFCV + colDF.FPV + colDF.FSV + colDF.PV + colDF.SV + colDF.ShuntV + colDF.ShuntI
        colDF.drop(columns=['Pos', 'Neg', 'F1type', 'F2type', 'measT', 'T1', 'T2', 'T3', 'T4', 'T5', 'T6', 'T7', 'IOV',
                            'ODCFCV', 'FPV', 'FSV', 'PV', 'SV', 'ShuntV', 'ShuntI'], inplace=True)
        self.logger.info('Finished Column DF manipulations')
        SNlst = colDF.SN.unique()
        SNlst = [x for x in SNlst if x.isdigit()]

        return colDF, SNlst

    def CurrentCycleData(self, df, pol, sn):
        CurrentCycleType = None
        Tlst = ['dT_Input_BB', 'dT_Output_BB', 'dT_DCFC_BB', 'dT_PriFuseBody', 'dT_SecFuseBody', 'dT_PvSBody',
                'dT_PriTerm', 'dT_SecTerm', 'dT_PvSTerm']
        if sn == '18' and pol == 'Pos':
            print('-------Fixing Sensor Data-------')
            for e in Tlst:
                df.loc[df['TempCycleCount'] == 218, e] = 0
        elif sn == '11' and pol == 'Pos':
            print('-------Fixing Sensor Data-------')
            for e in Tlst:
                df.loc[df['TempCycleCount'] == 116, e] = 0


        ''' DC Fast Charge Data '''
        df['stepTime'] = df['stepTime'].dt.total_seconds()
        df.loc[df['OUT_DCFC'] < 0, 'OUT_DCFC'] = 0
        df.loc[df['OUT_DCFC'] > .5, 'OUT_DCFC'] = 0
        df['IOVI2RT'] = df['INP_OUT'] * df['CURRENT'] * df['stepTime']
        df['DCFCI2RT'] = df['OUT_DCFC'] * df['CURRENT'] * df['stepTime']
        df['DCFCcr'] = df['OUT_DCFC'] / df['CURRENT']
        df.loc[df['DCFCcr'] == 0, 'DCFCcr'] = None
        df.loc[df['DCFCI2RT'] == 0, 'DCFCI2RT'] = None

        df.loc[df['Input_BB'] < -60, 'Input_BB'] = np.nan
        df['dT_Input_BB'] = df['Input_BB'] - df['T_AMB_2']
        df['dT_Output_BB'] = df['Output_BB'] - df['T_AMB_2']
        df['dT_DCFC_BB'] = df['DCFC_BB'] - df['T_AMB_2']

        ''' Primary and Secondary Data'''
        df['PCcrSplit'] = df['PRIME'] * 2 / df['CURRENT']
        if pol == 'Pos':
            df['SCcrSplit'] = df['SEC'] * 2 / df['CURRENT']
            df['dT_PriFuseBody'] = df['Prim_Fuse_Body'] - df['T_AMB_2']
            df['dT_SecFuseBody'] = df['Sec_Fuse_Body'] - df['T_AMB_2']
            df['dT_PvSBody'] = df['Prim_Fuse_Body'] - df['Sec_Fuse_Body']
            df['dT_PriTerm'] = df['Prim_Fuse_Term'] - df['T_AMB_2']
            df['dT_SecTerm'] = df['Sec_Fuse_Term'] - df['T_AMB_2']
            df['dT_PvSTerm'] = df['Prim_Fuse_Term'] - df['Sec_Fuse_Term']
            df['vPrime'] = df['PRIME'] + df['F_PRIME']
            df['vSec'] = df['SEC'] + df['F_SEC']
            df['vT_PaS'] = (df['vPrime'] + df['vSec']) / 2
            df['vTI2RT'] = df['vT_PaS'] * df['CURRENT'] * df['stepTime']

        CurrentSummaryTable = pd.DataFrame(
            columns=['CurrentCycle', 'CycleTime', 'TempCycle','CurrentCycleType','CurrentShareCycle', 'maxIBB',
                     'maxIBBdT', 'sumIBBdT', 'maxOBB', 'maxOBBdT', 'sumOBBdT', 'maxDCBB', 'maxDCBBdT', 'sumDCBBdT',
                     'sumDCFCI2RT', 'meanCurrent', 'meanDCFCcr', 'maxRangeDCFCcr',
                     'meanDCFCv', 'maxPFBdT', 'maxSFBdT',
                     'maxPFTdT', 'maxSFTdT', 'sumPvSBdT', 'sumPvSTdT', 'meanVP', 'meanVS', 'meanVT', 'sumVTI2RT',
                     'cntDCFCoos','meanVPc','meanVSc','meanVPf','meanVSf','cntPRIMEoos','cntSECoos','maxRangePCcr','maxRangeSCcr'])
        CurrentShareCycle = 0
        for i in range(1, df.CurrentCycleCount.max()):
            tdf = df.loc[(df['CurrentCycleCount'] == i) & (df['CURRENT'] != 0)]

            tdf.reset_index(inplace=True, drop=True)
            TempCycle = tdf['TempCycleCount'].max()
            time = tdf.Time.max() - tdf.Time.min()
            '''Temp Measurements'''
            maxIBB = tdf.Input_BB.max()
            maxIBBdT = tdf.dT_Input_BB.max()
            sumIBBdT = tdf.dT_Input_BB.sum()
            maxOBB = tdf.Output_BB.max()
            maxOBBdT = tdf.dT_Output_BB.max()
            sumOBBdT = tdf.dT_Output_BB.sum()
            maxDCBB = tdf.DCFC_BB.max()
            maxDCBBdT = tdf.dT_DCFC_BB.max()
            sumDCBBdT = tdf.dT_DCFC_BB.sum()
            meanVPc = df['PRIME'].mean()

            if pol == 'Pos':
                maxPFBdT = df['dT_PriFuseBody'].max()
                maxSFBdT = df['dT_SecFuseBody'].max()
                maxPFTdT = df['dT_PriTerm'].max()
                maxSFTdT = df['dT_SecTerm'].max()
                sumPvSBdT = df['dT_PvSBody'].sum()
                sumPvSTdT = df['dT_PvSTerm'].sum()
                meanVSc = df['SEC'].mean()
                meanVPf = df['F_PRIME'].mean()
                meanVSf = df['F_SEC'].mean()
                meanVP = df['vPrime'].mean()
                meanVS = df['vSec'].mean()
                meanVT = df['vT_PaS'].max()
                sumVTI2RT = df['vTI2RT'].sum()
            elif pol == 'Neg':
                maxPFBdT = None
                maxSFBdT = None
                maxPFTdT = None
                maxSFTdT = None
                sumPvSBdT = None
                sumPvSTdT = None
                meanVSc = None
                meanVPf = None
                meanVSf = None
                meanVP = None
                meanVS = None
                meanVT = None
                sumVTI2RT = None
                meanSCcrSplit  = None

            '''DCFC Measurements'''
            mVdf = tdf.loc[(tdf['OUT_DCFC'] > 0)]
            meanDCFCv = mVdf.OUT_DCFC.mean()
            spec = 0.0002

            meanDCFCcr = tdf.DCFCcr[2:-2].mean()
            meanPCcrSplit  = tdf.PCcrSplit[2:-2].mean()
            cntDCFCoos = 1 if meanDCFCcr > spec else 0
            cntPRIMEoos = 1 if meanPCcrSplit  > spec else 0
            maxRangeDCFCcr = tdf.DCFCcr[2:-2].max() - tdf.DCFCcr[2:-2].min()
            maxRangePCcr = tdf.PCcrSplit[2:-2].max() - tdf.PCcrSplit[2:-2].min()
            if pol == 'Pos':
                meanSCcrSplit  = tdf.PCcrSplit[2:-2].mean()
                cntSECoos = 1 if meanSCcrSplit  > spec else 0
                maxRangeSCcr = tdf.SCcrSplit[2:-2].max() - tdf.SCcrSplit[2:-2].min()

            # if meanDCFCcr > spec:
            #     print('OOS')
            #     cntDCFCoos = 1
            # elif meanDCFCcr <= spec:
            #     print('OK')
            #     cntDCFCoos = 0
            # else:
            #     print('Not Comparable - Cycle: {} / {}'.format(i , df.CurrentCycleCount.max()))


            meanAdjCurrent = tdf.CURRENT[2:-2].mean()
            sumDCFCI2RT = tdf.DCFCI2RT[2:-2].sum()

            if meanAdjCurrent > 1250:
                CurrentCycleType = 'WOT_Discharge'
            elif (1250 > meanAdjCurrent > 700) & (time.total_seconds() < 30):
                CurrentCycleType = 'WOT_DCFC'
            elif (1250 > meanAdjCurrent > 700) & (time.total_seconds() > 60):
                CurrentCycleType = 'CC_Discharge'
            elif (700 > meanAdjCurrent > 300) & (time.total_seconds() > 60):
                CurrentCycleType = 'CC_DCFC'
            elif (700 > meanAdjCurrent > 300) & (30 < time.total_seconds() < 60):
                CurrentShareCycle += 1
                if (pol == 'Pos'):
                    if (meanSCcrSplit  > meanPCcrSplit ):
                        CurrentCycleType = 'CS_Prime'
                    elif (meanSCcrSplit  < meanPCcrSplit ):
                        CurrentCycleType = 'CS_Sec'
                elif (pol == 'Neg'):
                    CurrentCycleType = 'CS'
            else:
                print('cannot recognize profile  {} / {}'.format(i,df.CurrentCycleCount.max()))

            row = [i, time, TempCycle, CurrentCycleType, CurrentShareCycle,maxIBB, maxIBBdT, sumIBBdT, maxOBB, maxOBBdT,
                   sumOBBdT, maxDCBB, maxDCBBdT,sumDCBBdT, sumDCFCI2RT, meanAdjCurrent, meanDCFCcr, maxRangeDCFCcr,
                   meanDCFCv, maxPFBdT, maxSFBdT, maxPFTdT, maxSFTdT,sumPvSBdT, sumPvSTdT, meanVP, meanVS, meanVT,
                   sumVTI2RT, cntDCFCoos,
                   meanVPc,meanVSc,meanVPf,meanVSf,cntPRIMEoos,cntSECoos,maxRangePCcr,maxRangeSCcr]
            CurrentSummaryTable = CurrentSummaryTable.append(pd.Series(row, index=CurrentSummaryTable.columns),
                                                       ignore_index=True)

        cols = ['TempCycle', 'TempCycle', 'sumDCFCI2RT', 'meanCurrent', 'meanDCFCcr']

        i = 0
        for col in cols:
            i += 1
            self.logger.info('Clean up Current Summary Column {} / {}'.format(i, len(cols) + 1))
            CurrentSummaryTable[col] = pd.to_numeric(CurrentSummaryTable[col], errors='coerce')

        CurrentSummaryTable['accumPvSBdT'] = CurrentSummaryTable.sumPvSBdT.cumsum()
        CurrentSummaryTable['accumPvSTdT'] = CurrentSummaryTable.sumPvSTdT.cumsum()
        CurrentSummaryTable['accumVTI2RT'] = CurrentSummaryTable.sumVTI2RT.cumsum()

        CurrentSummaryTable['AccumIBBdT'] = CurrentSummaryTable.sumIBBdT.cumsum()
        CurrentSummaryTable['AccumOBBdT'] = CurrentSummaryTable.sumOBBdT.cumsum()
        CurrentSummaryTable['AccumDCBBdT'] = CurrentSummaryTable.sumDCBBdT.cumsum()
        # CurrentSummaryTable['AccumIOVI2RT'] = CurrentSummaryTable.sumIOVI2RT.cumsum()
        CurrentSummaryTable['AccumDCFCI2RT'] = CurrentSummaryTable.sumDCFCI2RT.cumsum()
        CurrentSummaryTable['AccumDCFCoos'] = CurrentSummaryTable.cntDCFCoos.cumsum()
        CurrentSummaryTable['AccumPRIMEoos'] = CurrentSummaryTable.cntPRIMEoos.cumsum()
        CurrentSummaryTable['AccumSECoos'] = CurrentSummaryTable.cntSECoos.cumsum()
        # CurrentSummaryTable = CurrentSummaryTable[CurrentSummaryTable['CycleTime'] >= np.timedelta64(15, 'm')]

        CurrentSummaryTable['eSTDmCR'] = CurrentSummaryTable['meanDCFCcr'].expanding(min_periods=10).std()
        CurrentSummaryTable['rSTDmCR'] = CurrentSummaryTable['meanDCFCcr'].rolling(min_periods=10, window=50).std()
        CurrentSummaryTable['eSTDmCRs'] = CurrentSummaryTable['eSTDmCR'].diff(10)
        CurrentSummaryTable['MADeSTDmCR'] = abs(CurrentSummaryTable['eSTDmCR'] - CurrentSummaryTable['eSTDmCR'].
                                             expanding(min_periods=10).mean())
        CurrentSummaryTable['MADrSTDmCR'] = abs(CurrentSummaryTable['rSTDmCR'] - CurrentSummaryTable['rSTDmCR'].
                                             rolling(min_periods=10, window=50).mean())
        CurrentSummaryTable['MADemCR'] = abs(CurrentSummaryTable['meanDCFCcr'] - CurrentSummaryTable['meanDCFCcr'].
                                          expanding(min_periods=10).mean())
        CurrentSummaryTable['rSTDmCR'] = CurrentSummaryTable['meanDCFCcr'].rolling(min_periods=10, window=50).std()
        CurrentSummaryTable['rMEANmCR'] = CurrentSummaryTable['meanDCFCcr'].rolling(min_periods=10, window=50).mean()

        return CurrentSummaryTable

    def CurrentCycleVoltageData(self, df, pol, sn):

        ''' DC Fast Charge Data '''
        df['stepTime'] = df['stepTime'].dt.total_seconds()
        df['CURRENTSQ'] = df['CURRENT'].copy() ** 2
        df.loc[df['OUT_DCFC'] < 0, 'OUT_DCFC'] = 0
        df.loc[df['OUT_DCFC'] > .5, 'OUT_DCFC'] = 0
        df['DCFCI2RT'] = df['OUT_DCFC'] * df['CURRENT'] * df['stepTime']
        df['DCFCcr'] = df['OUT_DCFC'] / df['CURRENT']
        df.loc[df['DCFCcr'] == 0, 'DCFCcr'] = None
        df.loc[df['DCFCI2RT'] == 0, 'DCFCI2RT'] = None

        ''' Input Output Voltage '''
        df['IOVI2RT'] = df['INP_OUT'] * df['CURRENT'] * df['stepTime']

        ''' Primary and Secondary Data'''
        df['PCcrSplit'] = df['PRIME'] * 2 / df['CURRENT']
        df['PCcr'] = df['PRIME'] / df['CURRENT']

        if pol == 'Pos':
            df['SCcrSplit'] = df['SEC'] * 2 / df['CURRENT']
            df['SCcr'] = df['SEC'] / df['CURRENT']
            df['vPrime'] = df['PRIME'] + df['F_PRIME']
            df['vSec'] = df['SEC'] + df['F_SEC']
            df['vT_PaS'] = (df['vPrime'] + df['vSec']) / 2
            df['vTI2RT'] = df['vT_PaS'] * df['CURRENT'] * df['stepTime']
        if pol == 'Neg':
            df['vT_SaPC'] = df['SHUNT'] + df['PRIME']
            df['vTI2RT'] = df['vT_SaPC'] * df['CURRENT'] * df['stepTime']

        CurrentSummaryTable = pd.DataFrame(
            columns=['CurrentCycle', 'CycleTime', 'TempCycle','CurrentCycleType','CurrentShareCycle',
                     'sumDCFCI2RT', 'meanCurrent', 'meanDCFCcr', 'maxRangeDCFCcr',
                     'meanDCFCv', 'meanVP', 'meanVS', 'meanVT', 'sumVTI2RT',
                     'cntDCFCoos','meanVPc','meanVSc','meanVPf','meanVSf','cntPRIMEoos','cntSECoos','maxRangePCcr',
                     'maxRangeSCcr','meanPCcr','meanSCcr','meanPCcrSplit','meanSCcrSplit','ISQsum','rmsT',
                     'maxAmb', 'maxInput_BB', 'maxOutput_BB', 'maxDCFC_BB', 'maxPriFuseBody', 'maxSecFuseBody', 'maxPriTerm', 'maxSecTerm'])
        CurrentShareCycle = 0
        for i in range(1, df.CurrentCycleCount.max()):
            tdf = df.loc[(df['CurrentCycleCount'] == i) & (df['CURRENT'] != 0)]
            tdf.reset_index(inplace=True, drop=True)
            meanAdjCurrent = tdf.CURRENT[2:-2].mean()
            TempCycle = tdf['TempCycleCount'].max()
            time = tdf.Time.max() - tdf.Time.min()

            ''' Temp Max Value '''
            maxAmb = tdf['T_AMB_2'].max()
            maxInput_BB = tdf['Input_BB'].max()
            maxOutput_BB = tdf['Output_BB'].max()
            maxDCFC_BB = tdf['DCFC_BB'].max()

            if pol == 'Pos':
                maxPriFuseBody = tdf['Prim_Fuse_Body'].max()
                maxSecFuseBody = tdf['Sec_Fuse_Body'].max()
                maxPriTerm = tdf['Prim_Fuse_Term'].max()
                maxSecTerm = tdf['Sec_Fuse_Term'].max()
            elif pol == 'Neg':
                maxPriFuseBody = None
                maxSecFuseBody = None
                maxPriTerm = None
                maxSecTerm = None



            ''' Value Initialization '''
            spec = 0.0002
            CurrentCycleType = None
            CurrentShareCycle = 0
            sumDCFCI2RT = None
            meanDCFCcr = None
            maxRangeDCFCcr = None
            meanDCFCv = None
            meanVP = None
            meanVS = None
            meanVT = None
            sumVTI2RT = None
            cntDCFCoos = None
            meanVPc = None
            meanVSc = None
            meanVPf = None
            meanVSf = None
            cntPRIMEoos = None
            cntSECoos = None
            maxRangePCcr = None
            maxRangeSCcr = None
            meanPCcr = 0
            meanSCcr = 0
            meanPCcrSplit = None
            meanSCcrSplit = None
            rms = df.loc[(df['CurrentCycleCount'] == i)]
            ISQsum = rms['CURRENTSQ'].sum()
            rmsT = rms.Time.max() - rms.Time.min()

            ''' Current Cycle Evaluation'''
            if meanAdjCurrent > 1250:
                CurrentCycleType = 'WOT_Discharge'

            elif (1250 > meanAdjCurrent > 700) & (time.total_seconds() < 30):
                CurrentCycleType = 'WOT_DCFC'
            elif (1250 > meanAdjCurrent > 700) & (time.total_seconds() > 60):
                CurrentCycleType = 'CC_Discharge'
            elif (700 > meanAdjCurrent > 300) & (time.total_seconds() > 60):
                CurrentCycleType = 'CC_DCFC'
            elif (700 > meanAdjCurrent > 300) & (30 < time.total_seconds() < 60):
                CurrentShareCycle += 1
                CurrentCycleType = 'CS'
            else:
                print('cannot recognize profile  {} / {}'.format(i, df.CurrentCycleCount.max()))

            ''' Current Cycle Measurements '''
            if (CurrentCycleType == 'WOT_Discharge') or (CurrentCycleType == 'CC_Discharge'):
                meanVPc = tdf['PRIME'].mean()

                '''Prime'''
                meanPCcrSplit = tdf.PCcrSplit[2:-2].mean()
                cntPRIMEoos = 1 if meanPCcrSplit  > spec else 0
                maxRangePCcr = tdf.PCcrSplit[2:-2].max() - tdf.PCcrSplit[2:-2].min()
                if pol == 'Pos':
                    meanVSc = tdf['SEC'].mean()
                    meanVPf = tdf['F_PRIME'].mean()
                    meanVSf = tdf['F_SEC'].mean()
                    meanVP = tdf['vPrime'].mean()
                    meanVS = tdf['vSec'].mean()
                    meanVT = tdf['vT_PaS'].max()
                    sumVTI2RT = tdf['vTI2RT'].sum()
                    '''Secondary'''
                    meanSCcrSplit = tdf.SCcrSplit[2:-2].mean()
                    cntSECoos = 1 if meanSCcrSplit > spec else 0
                    maxRangeSCcr = tdf.SCcrSplit[2:-2].max() - tdf.SCcrSplit[2:-2].min()

            elif (CurrentCycleType == 'WOT_DCFC') or (CurrentCycleType == 'CC_DCFC'):
                meanDCFCv = tdf.OUT_DCFC.mean()
                meanDCFCcr = tdf.DCFCcr[2:-2].mean()
                cntDCFCoos = 1 if meanDCFCcr > spec else 0
                maxRangeDCFCcr = tdf.DCFCcr[2:-2].max() - tdf.DCFCcr[2:-2].min()
                sumDCFCI2RT = tdf.DCFCI2RT[2:-2].sum()

                meanVPc = tdf['PRIME'].mean()
                '''Prime'''
                meanPCcrSplit = tdf.PCcrSplit[2:-2].mean()
                cntPRIMEoos = 1 if meanPCcrSplit  > spec else 0
                maxRangePCcr = tdf.PCcrSplit[2:-2].max() - tdf.PCcrSplit[2:-2].min()
                if pol == 'Pos':
                    meanVSc = tdf['SEC'].mean()
                    meanVPf = tdf['F_PRIME'].mean()
                    meanVSf = tdf['F_SEC'].mean()
                    meanVP = tdf['vPrime'].mean()
                    meanVS = tdf['vSec'].mean()
                    meanVT = tdf['vT_PaS'].max()
                    sumVTI2RT = tdf['vTI2RT'].sum()
                    '''Secondary'''
                    meanSCcrSplit = tdf.PCcrSplit[2:-2].mean()
                    cntSECoos = 1 if meanSCcrSplit > spec else 0
                    maxRangeSCcr = tdf.SCcrSplit[2:-2].max() - tdf.SCcrSplit[2:-2].min()

            elif CurrentCycleType == 'CS':
                if pol == 'Pos':
                    meanVPc = tdf['PRIME'].mean()
                    meanVSc = tdf['SEC'].mean()
                    if meanVPc < meanVSc:
                        meanPCcr = tdf.PCcr[2:-2].mean()
                        CurrentCycleType = 'CS_Primary'
                    elif meanVPc > meanVSc:
                        meanSCcr = tdf.SCcr[2:-2].mean()
                        CurrentCycleType = 'CS_Secondary'
                elif pol == ' Neg':
                    meanVPc = tdf['PRIME'].mean()
                    meanPCcr = tdf.PCcr[2:-2].mean()
                    CurrentCycleType = 'CS_Unknown'

            row = [i, time, TempCycle, CurrentCycleType, CurrentShareCycle, sumDCFCI2RT, meanAdjCurrent, meanDCFCcr,
                   maxRangeDCFCcr,meanDCFCv, meanVP, meanVS, meanVT, sumVTI2RT, cntDCFCoos,
                   meanVPc,meanVSc,meanVPf,meanVSf,cntPRIMEoos,cntSECoos,maxRangePCcr,maxRangeSCcr,meanPCcr,meanSCcr,meanPCcrSplit,meanSCcrSplit,ISQsum,rmsT,
                   maxAmb, maxInput_BB, maxOutput_BB, maxDCFC_BB, maxPriFuseBody, maxSecFuseBody, maxPriTerm, maxSecTerm]

            CurrentSummaryTable = CurrentSummaryTable.append(pd.Series(row, index=CurrentSummaryTable.columns),
                                                       ignore_index=True)

        cols = ['TempCycle', 'TempCycle', 'sumDCFCI2RT', 'meanCurrent', 'meanDCFCcr']

        i = 0
        for col in cols:
            i += 1
            self.logger.info('Clean up Current Summary Column {} / {}'.format(i, len(cols) + 1))
            CurrentSummaryTable[col] = pd.to_numeric(CurrentSummaryTable[col], errors='coerce')

        CurrentSummaryTable['accumVTI2RT'] = CurrentSummaryTable.sumVTI2RT.cumsum()
        # CurrentSummaryTable['AccumIOVI2RT'] = CurrentSummaryTable.sumIOVI2RT.cumsum()
        CurrentSummaryTable['AccumDCFCI2RT'] = CurrentSummaryTable.sumDCFCI2RT.cumsum()
        CurrentSummaryTable['AccumDCFCoos'] = CurrentSummaryTable.cntDCFCoos.cumsum()
        CurrentSummaryTable['AccumPRIMEoos'] = CurrentSummaryTable.cntPRIMEoos.cumsum()
        CurrentSummaryTable['AccumSECoos'] = CurrentSummaryTable.cntSECoos.cumsum()
        # CurrentSummaryTable = CurrentSummaryTable[CurrentSummaryTable['CycleTime'] >= np.timedelta64(15, 'm')]

        # CurrentSummaryTable['eSTDmCR'] = CurrentSummaryTable['meanDCFCcr'].expanding(min_periods=10).std()
        # CurrentSummaryTable['rSTDmCR'] = CurrentSummaryTable['meanDCFCcr'].rolling(min_periods=10, window=50).std()
        # CurrentSummaryTable['eSTDmCRs'] = CurrentSummaryTable['eSTDmCR'].diff(10)
        # CurrentSummaryTable['MADeSTDmCR'] = abs(CurrentSummaryTable['eSTDmCR'] - CurrentSummaryTable['eSTDmCR'].
        #                                      expanding(min_periods=10).mean())
        # CurrentSummaryTable['MADrSTDmCR'] = abs(CurrentSummaryTable['rSTDmCR'] - CurrentSummaryTable['rSTDmCR'].
        #                                      rolling(min_periods=10, window=50).mean())
        # CurrentSummaryTable['MADemCR'] = abs(CurrentSummaryTable['meanDCFCcr'] - CurrentSummaryTable['meanDCFCcr'].
        #                                   expanding(min_periods=10).mean())
        # CurrentSummaryTable['rSTDmCR'] = CurrentSummaryTable['meanDCFCcr'].rolling(min_periods=10, window=50).std()
        # CurrentSummaryTable['rMEANmCR'] = CurrentSummaryTable['meanDCFCcr'].rolling(min_periods=10, window=50).mean()

        CurrentSummaryTable['CCIRMS'] = CurrentSummaryTable.ISQsum / CurrentSummaryTable.rmsT.dt.total_seconds()
        CurrentSummaryTable['CCIRMS'] = CurrentSummaryTable['CCIRMS'].pow(1. / 2)

        return CurrentSummaryTable

    def TempCycleData(self,df,pol,sn):

        ''' DC Fast Charge Data '''
        df['stepTime'] = df['stepTime'].dt.total_seconds()
        df.loc[df['OUT_DCFC'] < 0, 'OUT_DCFC'] = 0
        df.loc[df['OUT_DCFC'] > .5, 'OUT_DCFC'] = 0
        df['IOVI2RT'] = df['INP_OUT'] * df['CURRENT'] * df['stepTime']
        df['DCFCI2RT'] = df['OUT_DCFC'] * df['CURRENT'] * df['stepTime']
        df['DCFCcr'] = df['OUT_DCFC'] / df['CURRENT']
        df.loc[df['DCFCcr'] == 0, 'DCFCcr'] = None
        df.loc[df['DCFCI2RT'] == 0, 'DCFCI2RT'] = None

        df.loc[df['Input_BB'] < -60, 'Input_BB'] = np.nan
        df['dT_Input_BB'] = df['Input_BB'] - df['T_AMB_2']
        df['dT_Output_BB'] = df['Output_BB'] - df['T_AMB_2']
        df['dT_DCFC_BB'] = df['DCFC_BB'] - df['T_AMB_2']

        ''' Primary and Secondary Data'''
        if pol == 'Pos':
            df['dT_PriFuseBody'] = df['Prim_Fuse_Body'] - df['T_AMB_2']
            df['dT_SecFuseBody'] = df['Sec_Fuse_Body'] - df['T_AMB_2']
            df['dT_PvSBody'] = df['Prim_Fuse_Body'] - df['Sec_Fuse_Body']
            df['dT_PriTerm'] = df['Prim_Fuse_Term'] - df['T_AMB_2']
            df['dT_SecTerm'] = df['Sec_Fuse_Term'] - df['T_AMB_2']
            df['dT_PvSTerm'] = df['Prim_Fuse_Term'] - df['Sec_Fuse_Term']
            df['vPrime'] = df['PRIME'] + df['F_PRIME']
            df['vSec'] = df['SEC'] + df['F_SEC']
            df['vT_PaS'] = (df['vPrime'] + df['vSec'])/2
            df['vTI2RT'] = df['vT_PaS'] * df['CURRENT'] * df['stepTime']

        Tlst = ['dT_Input_BB','dT_Output_BB','dT_DCFC_BB','dT_PriFuseBody','dT_SecFuseBody','dT_PvSBody','dT_PriTerm','dT_SecTerm','dT_PvSTerm']
        if sn == '18' and pol == 'Pos':
            print('-------Fixing Sensor Data-------')
            for e in Tlst:
                df.loc[df['TempCycleCount'] == 218, e] = 0
        elif sn == '11' and pol == 'Pos':
            print('-------Fixing Sensor Data-------')
            for e in Tlst:
                df.loc[df['TempCycleCount'] == 116, e] = 0

        TempSummaryTable = pd.DataFrame(
            columns=['TempCycle', 'CycleTime', 'maxCurrentCycle', 'minCurrentCycle', 'rangeCurrentCycle','maxIBB',
                     'maxIBBdT','sumIBBdT','maxOBB','maxOBBdT','sumOBBdT','maxDCBB','maxDCBBdT','sumDCBBdT'
                     ,'sumIOVI2RT','sumDCFCI2RT','meanCurrent','meanDCFCcr','maxRangeDCFCcr','stdRangeDCFCcr','meanDCFCv','maxPFBdT','maxSFBdT',
                     'maxPFTdT','maxSFTdT','sumPvSBdT','sumPvSTdT','meanVP','meanVS','meanVT','sumVTI2RT','sumDCFCoos','IsumDCFC'])


        for i in range(1, df.TempCycleCount.max()):
            tdf = df.loc[df['TempCycleCount'] == i]
            tdf.reset_index(inplace=True, drop=True)
            time = tdf.Time.max()-tdf.Time.min()
            maxIBB = tdf.Input_BB.max()
            maxIBBdT = tdf.dT_Input_BB.max()
            sumIBBdT = tdf.dT_Input_BB.sum()
            maxOBB = tdf.Output_BB.max()
            maxOBBdT = tdf.dT_Output_BB.max()
            sumOBBdT = tdf.dT_Output_BB.sum()
            maxDCBB = tdf.DCFC_BB.max()
            maxDCBBdT = tdf.dT_DCFC_BB.max()
            sumDCBBdT = tdf.dT_DCFC_BB.sum()

            sumIOVI2RT = tdf.IOVI2RT.sum()
            # sumDCFCI2RT = tdf.DCFCI2RT.sum()

            maxCurrentCycle = tdf['CurrentCycleCount'].max()
            minCurrentCycle = tdf['CurrentCycleCount'].min()
            rangeCurrentCycle = maxCurrentCycle-minCurrentCycle

            mVdf = tdf.loc[(tdf['OUT_DCFC'] > 0)]
            meanDCFCv = mVdf.OUT_DCFC.mean()
            MDCFCcr = []
            OOSDCFCcr = []
            RDCFCcr = []
            AdjCurrent = []
            I2RT = []
            ISumDCFC = []
            ISumParallel = []
            spec = 0.0002

            if pol == 'Pos':
                maxPFBdT = df['dT_PriFuseBody'].max()
                maxSFBdT = df['dT_SecFuseBody'].max()
                maxPFTdT = df['dT_PriTerm'].max()
                maxSFTdT = df['dT_SecTerm'].max()
                sumPvSBdT = df['dT_PvSBody'].sum()
                sumPvSTdT = df['dT_PvSTerm'].sum()
                meanVP = df['vPrime'].max()
                meanVS = df['vSec'].max()
                meanVT = df['vT_PaS'].max()
                sumVTI2RT = df['vTI2RT'].sum()

            if pol == 'Neg':
                maxPFBdT = None
                maxSFBdT = None
                maxPFTdT = None
                maxSFTdT = None
                sumPvSBdT = None
                sumPvSTdT = None
                meanVP = None
                meanVS = None
                meanVT = None
                sumVTI2RT = None

            if (minCurrentCycle > 0.0) and (rangeCurrentCycle > 10.0):
                for j in range(int(minCurrentCycle), int(maxCurrentCycle)):
                    cdf = df.loc[(df['CurrentCycleCount'] == j) & (df['CURRENT'] != 0)]
                    cdf.reset_index(inplace=True, drop=True)
                    # crdf = cdf.loc[(cdf['DCFCcr'] > 0) & (cdf['DCFCcr'] < 10000)]
                    mDCFCcr = cdf.DCFCcr[2:-2].mean()
                    cdf['CURRENTSQ'] = cdf['CURRENT']**2
                    isumDCFC = cdf.CURRENTSQ[2:-2].sum()
                    if mDCFCcr > spec:
                        OOSDCFCcr.append(1)
                    elif mDCFCcr < spec:
                        OOSDCFCcr.append(0)
                    rDCFCcr = cdf.DCFCcr[2:-2].max() - cdf.DCFCcr[2:-2].min()
                    mAC = cdf.CURRENT[2:-2].mean()
                    mI2RT = cdf.DCFCI2RT[2:-2].sum()
                    # print(cdf.DCFCI2RT[1:-1])
                    RDCFCcr.append(rDCFCcr)
                    MDCFCcr.append(mDCFCcr)
                    AdjCurrent.append(mAC)
                    I2RT.append(mI2RT)
                    ISumDCFC.append(isumDCFC)

            try:

                MDCFCcr = [v for v in MDCFCcr if not math.isnan(v) and not math.isinf(v) ]
                AdjCurrent = [v for v in AdjCurrent if not math.isnan(v) and not math.isinf(v)]
                I2RT = [v for v in I2RT if not math.isnan(v) and not math.isinf(v)]
                meanAdjCurrent = statistics.mean(AdjCurrent)
                meanDCFCcr = statistics.mean(MDCFCcr)
                maxRangeDCFCcr = max(RDCFCcr)
                stdRangeDCFCcr = statistics.stdev(RDCFCcr)
                sumDCFCI2RT = sum(I2RT)
                sumDCFCoos = sum(OOSDCFCcr)
                IsumDCFC = sum(ISumDCFC)
            except:
                meanAdjCurrent = None
                meanDCFCcr = None
                sumDCFCI2RT = None
                sumDCFCoos = 0
                maxRangeDCFCcr = None
                stdRangeDCFCcr = None
                IsumDCFC = 0




            row = [i, time, maxCurrentCycle, minCurrentCycle,
                   rangeCurrentCycle,maxIBB,maxIBBdT,sumIBBdT,maxOBB,maxOBBdT,sumOBBdT,maxDCBB,maxDCBBdT,sumDCBBdT,
                   sumIOVI2RT,sumDCFCI2RT, meanAdjCurrent,meanDCFCcr,maxRangeDCFCcr,stdRangeDCFCcr, meanDCFCv, maxPFBdT, maxSFBdT, maxPFTdT, maxSFTdT,
                   sumPvSBdT, sumPvSTdT, meanVP, meanVS, meanVT, sumVTI2RT, sumDCFCoos,IsumDCFC]
            TempSummaryTable = TempSummaryTable.append(pd.Series(row, index=TempSummaryTable.columns),
                                                       ignore_index=True)

        cols = ['TempCycle','maxCurrentCycle','minCurrentCycle','rangeCurrentCycle','sumDCFCI2RT','meanCurrent','meanDCFCcr']

        i = 0
        for col in cols:
            i += 1
            self.logger.info('Clean up Temp Summary Column {} / {}'.format(i, len(cols) + 1))
            TempSummaryTable[col] = pd.to_numeric(TempSummaryTable[col], errors='coerce')

        TempSummaryTable['accumPvSBdT'] = TempSummaryTable.sumPvSBdT.cumsum()
        TempSummaryTable['accumPvSTdT'] = TempSummaryTable.sumPvSTdT.cumsum()
        TempSummaryTable['accumVTI2RT'] = TempSummaryTable.sumVTI2RT.cumsum()

        TempSummaryTable['AccumIBBdT'] = TempSummaryTable.sumIBBdT.cumsum()
        TempSummaryTable['AccumOBBdT'] = TempSummaryTable.sumOBBdT.cumsum()
        TempSummaryTable['AccumDCBBdT'] = TempSummaryTable.sumDCBBdT.cumsum()
        TempSummaryTable['AccumCurrentCount'] = TempSummaryTable.rangeCurrentCycle.cumsum()
        # TempSummaryTable['AccumIOVI2RT'] = TempSummaryTable.sumIOVI2RT.cumsum()
        TempSummaryTable['AccumDCFCI2RT'] = TempSummaryTable.sumDCFCI2RT.cumsum()
        TempSummaryTable['AccumDCFCoos'] = TempSummaryTable.sumDCFCoos.cumsum()
        TempSummaryTable = TempSummaryTable[TempSummaryTable['CycleTime'] >= np.timedelta64(15, 'm')]

        TempSummaryTable['eSTDmCR'] = TempSummaryTable['meanDCFCcr'].expanding(min_periods=10).std()
        TempSummaryTable['rSTDmCR'] = TempSummaryTable['meanDCFCcr'].rolling(min_periods=10, window=50).std()
        TempSummaryTable['eSTDmCRs'] = TempSummaryTable['eSTDmCR'].diff(10)
        TempSummaryTable['MADeSTDmCR'] = abs(TempSummaryTable['eSTDmCR']-TempSummaryTable['eSTDmCR'].
                                             expanding(min_periods=10).mean())
        TempSummaryTable['MADrSTDmCR'] = abs(TempSummaryTable['rSTDmCR'] - TempSummaryTable['rSTDmCR'].
                                             rolling(min_periods=10,window=50).mean())
        TempSummaryTable['MADemCR'] = abs(TempSummaryTable['meanDCFCcr'] - TempSummaryTable['meanDCFCcr'].
                                          expanding(min_periods=10).mean())
        TempSummaryTable['rSTDmCR'] = TempSummaryTable['meanDCFCcr'].rolling(min_periods=10, window=50).std()
        TempSummaryTable['rMEANmCR'] = TempSummaryTable['meanDCFCcr'].rolling(min_periods=10, window=50).mean()

        #
        # print('cycle time (): {}'.format(TempSummaryTable.CycleTime.dt.total_seconds()))
        # print('IsumDCFC {}'.format(TempSummaryTable.IsumDCFC))
        # TempSummaryTable['IRMSDCFCtemp'] = TempSummaryTable.IsumDCFC/ TempSummaryTable.CycleTime.dt.total_seconds()
        # TempSummaryTable['IRMSDCFCtemp'] = TempSummaryTable['IRMSDCFCtemp'].pow(1./2)

        return df, TempSummaryTable

    def TempCycleVoltageData(self,df,ccdf,pol,sn):

        ''' DC Fast Charge Data '''
        df['stepTime'] = df['stepTime'].dt.total_seconds()
        df['CURRENTSQ'] = df['CURRENT'].copy() ** 2
        df.loc[df['OUT_DCFC'] < 0, 'OUT_DCFC'] = 0
        df.loc[df['OUT_DCFC'] > .5, 'OUT_DCFC'] = 0
        # df['IOVI2RT'] = df['INP_OUT'] * df['CURRENT'] * df['stepTime']
        # df['DCFCI2RT'] = df['OUT_DCFC'] * df['CURRENT'] * df['stepTime']
        # df['DCFCcr'] = df['OUT_DCFC'] / df['CURRENT']
        # df.loc[df['DCFCcr'] == 0, 'DCFCcr'] = None
        # df.loc[df['DCFCI2RT'] == 0, 'DCFCI2RT'] = None
        #
        # ''' Primary and Secondary Data'''
        # if pol == 'Pos':
        #     df['vPrime'] = df['PRIME'] + df['F_PRIME']
        #     df['vSec'] = df['SEC'] + df['F_SEC']
        #     df['vT_PaS'] = (df['vPrime'] + df['vSec'])/2
        #     df['vTI2RT'] = df['vT_PaS'] * df['CURRENT'] * df['stepTime']

        TempSummaryTable = pd.DataFrame(
            columns=['TempCycle', 'CycleTime', 'maxCurrentCycle', 'minCurrentCycle', 'rangeCurrentCycle',
                     'maxAmb', 'maxInput_BB', 'maxOutput_BB', 'maxDCFC_BB', 'maxPriFuseBody', 'maxSecFuseBody', 'maxPriTerm','maxSecTerm',
                     'meanDCFCv', 'meanDCFCcr', 'RangeDCFCcr', 'stdDCFCcr', 'sumDCFCI2RT',
                     'meanVP', 'meanVPc', 'meanPCcr','meanPCcrSplit', 'RangePCcr', 'stdPCcr', 'meanVPf',
                     'meanVS', 'meanVSc', 'meanSCcr','meanSCcrSplit', 'RangeSCcr', 'stdSCcr', 'meanVSf',
                     'meanVT', 'sumVTI2RT',
                     'sumDCFCoos','sumPRIMEoos','sumSECoos']#,
                     # 'TC_I','TC_T',
                     # 'PC_I','PC_T',
                     # 'DCC_I','DCC_T',
                     # 'PSC_I','PSC_T']
                      )


        for i in range(1, df.TempCycleCount.max()):
            '''Local Variables'''
            spec = 0.0002

            '''Full Data Set'''
            tdf = df.loc[df['TempCycleCount'] == i]
            tdf.reset_index(inplace=True, drop=True)

            time = tdf.Time.max() - tdf.Time.min()
            maxCurrentCycle = tdf['CurrentCycleCount'].max()
            minCurrentCycle = tdf['CurrentCycleCount'].min()
            rangeCurrentCycle = maxCurrentCycle - minCurrentCycle

            ''' Temp Max Value '''
            maxAmb = tdf['T_AMB_2'].max()
            maxInput_BB = tdf['Input_BB'].max()
            maxOutput_BB = tdf['Output_BB'].max()
            maxDCFC_BB = tdf['DCFC_BB'].max()

            if pol == 'Pos':
                maxPriFuseBody = tdf['Prim_Fuse_Body'].max()
                maxSecFuseBody = tdf['Sec_Fuse_Body'].max()
                maxPriTerm = tdf['Prim_Fuse_Term'].max()
                maxSecTerm = tdf['Sec_Fuse_Term'].max()
            elif pol == 'Neg':
                maxPriFuseBody = None
                maxSecFuseBody = None
                maxPriTerm = None
                maxSecTerm = None


            '''Current Cycle Summary Data Set'''
            cctdf = ccdf.loc[ccdf['TempCycle'] == i]
            cctdf.reset_index(inplace=True, drop=True)

            '''DCFC'''
            sumDCFCI2RT = cctdf.sumDCFCI2RT.sum()
            meanDCFCv = cctdf.meanDCFCv.mean()
            meanDCFCcr = cctdf.meanDCFCcr.mean()
            RangeDCFCcr = cctdf.meanDCFCcr.max() - cctdf.meanDCFCcr.min()
            stdDCFCcr = cctdf.meanDCFCcr.std()

            '''Primary Leg'''
            meanVP = cctdf.meanVP.mean()
            meanVPc = cctdf.meanVPc.mean()
            meanPCcr = cctdf.meanPCcr.mean()
            meanPCcrSplit = cctdf.meanPCcrSplit.mean()
            RangePCcr = cctdf.meanPCcr.max() - cctdf.meanPCcr.min()
            stdPCcr = cctdf.meanPCcr.std()
            meanVPf = cctdf.meanVPf.mean()

            '''Secondary Leg '''
            meanVS = cctdf.meanVS.mean() if pol == 'Pos' else None
            meanVSc = cctdf.meanVSc.mean() if pol == 'Pos' else None
            meanSCcr = cctdf.meanSCcr.mean() if pol == 'Pos' else None
            meanSCcrSplit = cctdf.meanSCcrSplit.mean() if pol == 'Pos' else None
            RangeSCcr = cctdf.meanSCcr.max() - cctdf.meanSCcr.min() if pol == 'Pos' else None
            stdSCcr = cctdf.meanSCcr.std() if pol == 'Pos' else None
            meanVSf = cctdf.meanVSf.mean() if pol == 'Pos' else None

            '''Combined Circuit'''
            meanVT = cctdf.meanVT.mean() if pol == 'Pos' else None
            sumVTI2RT = cctdf.sumVTI2RT.sum() if pol == 'Pos' else None

            '''OOS Measurements'''
            sumDCFCoos = cctdf.cntDCFCoos.sum()
            sumPRIMEoos = cctdf.cntPRIMEoos.sum()
            sumSECoos = cctdf.cntSECoos.sum()

            # ''' Thermal Cycle RMS '''
            # TC_I = tdf.CURRENTSQ.sum()
            # TC_T = time
            #
            # ''' Power Cycle RMS'''
            # pcdf = tdf[tdf['PowerCycleIndicator'] >= 1]
            # # print(pdf)
            # PC_I = pcdf.CURRENTSQ.sum()
            # PC_T = pcdf.Time.max() - pcdf.Time.min()
            #
            # ''' DCFC RMS'''
            # dcfcdf = tdf[(tdf['PowerCycleIndicator'] == 1) & (tdf['OUT_DCFC'] > 0)]
            # DCC_I = dcfcdf.CURRENTSQ.sum()
            # DCC_T = dcfcdf.Time.max() - dcfcdf.Time.min()
            #
            # ''' PS RMS'''
            # PSC_I = pcdf.CURRENTSQ.sum()/4
            # PSC_T = pcdf.Time.max() - dcfcdf.Time.min()

            row = [i, time, maxCurrentCycle, minCurrentCycle, rangeCurrentCycle,
                   maxAmb, maxInput_BB, maxOutput_BB, maxDCFC_BB, maxPriFuseBody, maxSecFuseBody, maxPriTerm, maxSecTerm,
                   meanDCFCv, meanDCFCcr, RangeDCFCcr, stdDCFCcr, sumDCFCI2RT,
                   meanVP, meanVPc, meanPCcr,meanPCcrSplit, RangePCcr, stdPCcr, meanVPf,
                   meanVS, meanVSc, meanSCcr,meanSCcrSplit, RangeSCcr, stdSCcr, meanVSf,
                   meanVT, sumVTI2RT,
                   sumDCFCoos,sumPRIMEoos,sumSECoos]#,
                   # TC_I,TC_T,
                   # PC_I,PC_T,
                   # DCC_I,DCC_T,
                   # PSC_I,PSC_T]
            TempSummaryTable = TempSummaryTable.append(pd.Series(row, index=TempSummaryTable.columns),
                                                       ignore_index=True)

        # cols = ['TempCycle','maxCurrentCycle','minCurrentCycle','rangeCurrentCycle','sumDCFCI2RT','meanDCFCcr']
        #
        # i = 0
        # for col in cols:
        #     i += 1
        #     self.logger.info('Clean up Temp Summary Column {} / {}'.format(i, len(cols) + 1))
        #     TempSummaryTable[col] = pd.to_numeric(TempSummaryTable[col], errors='coerce')

        TempSummaryTable['AccumCurrentCount'] = TempSummaryTable.rangeCurrentCycle.cumsum()
        # TempSummaryTable['AccumIOVI2RT'] = TempSummaryTable.sumIOVI2RT.cumsum()
        TempSummaryTable['AccumDCFCI2RT'] = TempSummaryTable.sumDCFCI2RT.cumsum()
        TempSummaryTable['AccumDCFCoos'] = TempSummaryTable.sumDCFCoos.cumsum()
        TempSummaryTable['AccumPRIMEoos'] = TempSummaryTable.sumPRIMEoos.cumsum()
        TempSummaryTable['AccumSECoos'] = TempSummaryTable.sumSECoos.cumsum()

        TempSummaryTable = TempSummaryTable[TempSummaryTable['CycleTime'] >= np.timedelta64(15, 'm')]

        # TempSummaryTable['eSTDmCR'] = TempSummaryTable['meanDCFCcr'].expanding(min_periods=10).std()
        # TempSummaryTable['rSTDmCR'] = TempSummaryTable['meanDCFCcr'].rolling(min_periods=10, window=50).std()
        # TempSummaryTable['eSTDmCRs'] = TempSummaryTable['eSTDmCR'].diff(10)
        # TempSummaryTable['MADeSTDmCR'] = abs(TempSummaryTable['eSTDmCR']-TempSummaryTable['eSTDmCR'].
        #                                      expanding(min_periods=10).mean())
        # TempSummaryTable['MADrSTDmCR'] = abs(TempSummaryTable['rSTDmCR'] - TempSummaryTable['rSTDmCR'].
        #                                      rolling(min_periods=10,window=50).mean())
        # TempSummaryTable['MADemCR'] = abs(TempSummaryTable['meanDCFCcr'] - TempSummaryTable['meanDCFCcr'].
        #                                   expanding(min_periods=10).mean())
        # TempSummaryTable['rSTDmCR'] = TempSummaryTable['meanDCFCcr'].rolling(min_periods=10, window=50).std()
        TempSummaryTable['DCFCrMEANmCR'] = TempSummaryTable['meanDCFCcr'].rolling(min_periods=10, window=50).mean()
        TempSummaryTable['PRIMErMEANmCR'] = TempSummaryTable['meanPCcr'].rolling(min_periods=1, window=4).mean()
        TempSummaryTable['SECrMEANmCR'] = TempSummaryTable['meanSCcr'].rolling(min_periods=1, window=4).mean()
        TempSummaryTable['SplitPRIMErMEANmCR'] = TempSummaryTable['meanPCcrSplit'].rolling(min_periods=10, window=50).mean()
        TempSummaryTable['SplitSECrMEANmCR'] = TempSummaryTable['meanSCcrSplit'].rolling(min_periods=10, window=50).mean()
        # TempSummaryTable['rMEANmCR'] = TempSummaryTable['meanDCFCcr'].rolling(min_periods=10, window=50).mean()

        # TempSummaryTable['TC_T'] = TempSummaryTable['TC_T'].fillna(pd.Timedelta(seconds=0))
        # TempSummaryTable['PC_T'] = TempSummaryTable['PC_T'].fillna(pd.Timedelta(seconds=0))
        # TempSummaryTable['DCC_T'] = TempSummaryTable['DCC_T'].astype('timedelta64[ns]')
        # TempSummaryTable['DCC_T'] = TempSummaryTable['DCC_T'].fillna(pd.Timedelta(seconds=0))
        # TempSummaryTable['PSC_T'] = TempSummaryTable['PSC_T'].fillna(pd.Timedelta(seconds=0))
        #
        # ndf = TempSummaryTable[['TC_T','PC_T','DCC_T']].copy()
        # print(ndf,ndf.dtypes)
        #
        # TempSummaryTable['TempIRMS'] = TempSummaryTable.TC_I / TempSummaryTable.TC_T.dt.total_seconds()
        # TempSummaryTable['TempIRMS'] = TempSummaryTable['TempIRMS'].pow(1./2)
        #
        # TempSummaryTable['PowerIRMS'] = TempSummaryTable.PC_I / TempSummaryTable.PC_T.dt.total_seconds()
        # TempSummaryTable['PowerIRMS'] = TempSummaryTable['PowerIRMS'].pow(1. / 2)
        # TempSummaryTable['DCFCIRMS'] = TempSummaryTable.DCC_I / TempSummaryTable.DCC_T.dt.total_seconds()
        # TempSummaryTable['DCFCIRMS'] = TempSummaryTable['DCFCIRMS'].pow(1. / 2)
        # TempSummaryTable['PSIRMS'] = TempSummaryTable.PSC_I / TempSummaryTable.PSC_T.dt.total_seconds()
        # TempSummaryTable['PSIRMS'] = TempSummaryTable['PSIRMS'].pow(1. / 2)

        return df, TempSummaryTable

    def OutputToExcel(self, df, filename):
        df.reset_index(inplace=True, drop=True)
        df.to_csv(path_or_buf=filename)

    def PlotAllSamples(self, signals): #Input subset of data for Plotting
        samples = signals['key'].nunique()
        print(signals['key'])
        self.logger.info('Number of Samples {}'.format(samples))
        keys = signals.key.unique()
        keys = sorted(keys)
        row, col = signals.shape
        print(col-2, samples)
        fig, axs = plt.subplots(nrows=col-2, ncols=len(keys)+1, sharey='row')

        for i in range(0, len(keys)):
            print('i={}'.format(i))
            tdf = signals.loc[signals['key'] == keys[i]]
            t2df = tdf.drop('key', axis=1)
            for j in range(0, col-2):
                t2df.plot(x=0, y=j+1, style='.', legend=False, ax=axs[j, i])
                t2df.plot(x=0, y=j+1, style='.', legend=False, label=keys[i][-10:],
                          ax=axs[j, len(keys)])
                if j == 0:
                    axs[0, i].set_title("{}".format(keys[i][-10:]))
                if i == 0:
                    axs[j, 0].set_ylabel(t2df.columns[j+1])

        plt.show()


if __name__ == "__main__":
    test = TriTemp_data()
