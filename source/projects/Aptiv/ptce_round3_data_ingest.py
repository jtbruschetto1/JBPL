# /usr/local/bin/python3.9 /Users/jtbruschetto/GIT/JBPL/source/Aptiv/ptce_data_ingest.py

import os
import time
import math
import pandas as pd
from pandas.api.types import is_numeric_dtype
import matplotlib.pyplot as plt
import numpy as np
import statistics
import logging
from openpyxl import load_workbook
from itertools import islice

class PTCE_data:
    def __init__(self):
        ''' Initialization Process
            1) Change Local Directory
            2) Configure Logging
            3) Select Program Execution Variable
            4) Loop through different Validation Rounds
                a) List All Files
                b) Run controller
                c) Calculate Combined Life Temperature exposure DataFrame
                d) Calculate Combined Life Failure DataFrame
                e) Output Summaries to Excel
        '''
        os.chdir(r"/Users/jtbruschetto/Documents/OneDrive - Rivian Automotive, LLC/Projects/HVDB/Aptiv/Python Data") # /Users/jtbruschetto/GIT/JBPL/source/projects/Aptiv"
        logging.basicConfig(format='%(asctime)s - %(levelname)s - %(message)s') #filename='PTC_Data.log',
        self.logger = logging.getLogger()
        self.logger.setLevel(logging.INFO)
        self.logger.info('---- Starting Aptiv PTC Data Ingest Process ----')
        time.sleep(2)
        self.profile_Dict = {'R1':{ 'WOT_Discharge':1250,
                               'WOT_DCFC':[1250,700],
                               'CC_Discharge':[1250,700],
                               'CC_DCFC':[700,300],
                               'CS':[700,300] },
                        '350A':{'WOT_Discharge':800,
                                'WOT_DCFC':[800,400],
                                'CC_Discharge':[800,400],
                                'CC_DCFC':[400,200],
                                'CS':[400,200] }}
        self.round_Dict = {'C':'R1',
                      '2':'R1',
                      '3':'R1',
                      '4':'350A'}

        load = int(input('Select Level:\n  - 4 start from Raw Files\n  - 3 start from Joined File\n  - 2 Start from Split Files\n  - 1 Start from Summary Files\n  :'))
        CombinedLifeDF = pd.DataFrame(columns=['Pol','Round','TempProfile','Unit','Signal','95%','90%','75%','50%'])
        CombinedFailureDF = pd.DataFrame()
        for rnd in ['C','2','3','4']: #'C','2','3' / '3','2','C'
            self.round = rnd
            self.logger.info('---- Starting Round {} Data Analysis ----'.format(self.round))
            data_folder = r"raw_ptce_round{}_data/".format(self.round)
            files = os.listdir(data_folder)
            files = [k for k in files if '.csv' in k]
            LifeDF, FailDF = self.Controller(files,data_folder,load)
            CombinedLifeDF = CombinedLifeDF.append(LifeDF)
            CombinedFailureDF = CombinedFailureDF.append(FailDF)
        self.OutputToExcel(CombinedLifeDF, r'summary_data/Life_Temp_Summary.csv')
        self.OutputToExcel(CombinedFailureDF, r'summary_data/Component_Failure_Summary.csv')
        AssemblyFailureDF = self.AssemblyFailureSummary(CombinedFailureDF)
        self.OutputToExcel(AssemblyFailureDF, r'summary_data/Assembly_Failure_Summary.csv')

    def Controller(self, files, root, load):
        ''' Main Program controller
            Option: Run load cases
        '''

        if load == 4:
            ''' Load all Raw Files and Join '''
            self.logger.info('Loading Test File')
            df = self.loadExcel(root,files)
            self.logger.info('Completed Loading')
            self.OutputToExcel(df,'summary_data/Aptiv_PTC_Rd{}_Joined.csv'.format(self.round))

        if load >= 3:
            ''' Open Joined File and Split Into Files based on SN'''
            if load == 3:
                df = pd.read_csv('summary_data/Aptiv_PTC_Rd{}_Joined.csv'.format(self.round))
            colDF, SNlst = self.ParseSN(df)
            self.SplitRawFile(df,colDF,SNlst)

        if load >= 2:
            ''' Run Positive/Negative HVDB Evaluations and Create Current Cycle Summary Sheet '''
            posMasterDF = self.positive_HVDB_Eval()
            # negMasterDF = self.negative_HVDB_Eval()
            self.OutputToExcel(posMasterDF,'summary_data/Aptiv_PTC_Rd{}_Positive_CurrentSummary.csv'.format(self.round))
            # self.OutputToExcel(negMasterDF, 'summary_data/Aptiv_PTC_Rd{}_Negative_CurrentSummary.csv'.format(self.round))

        if load >= 1:
            ''' Evaluate Failures in the Current Summary Documents'''
            if load < 2:
                posMasterDF = pd.read_csv('summary_data/Aptiv_PTC_Rd{}_Positive_CurrentSummary.csv'.format(self.round))
                # negMasterDF = pd.read_csv('summary_data/Aptiv_PTC_Rd{}_Negative_CurrentSummary.csv'.format(self.round))

            pos_Component_Failure_Summary = self.ComponentFailureSummary(posMasterDF, 'Pos')
            # neg_Component_Failure_Summary = self.ComponentFailureSummary(negMasterDF, 'Neg')
            # pos_Component_Failure_Summary = pos_Component_Failure_Summary.append(neg_Component_Failure_Summary)

        posLifeDF = self.LifeTempSummary(posMasterDF, 'Pos')
        # negLifeDF = self.LifeTempSummary(negMasterDF, 'Neg')
        # posLifeDF = posLifeDF.append(negLifeDF)
        return posLifeDF, pos_Component_Failure_Summary

    def loadExcel(self,root,files):
        ''' Load Excel
            Note: Need to manually convert excel sheets from "'" delim to "."
            Drop all Duplicate Columns
            Establish Temp Cycles, Current Cycles, and Power Cycles
            Output results to Excel "Joined File"
        '''
        self.logger.info('~~~~ Start LoadExcel Process ~~~~')
        df = pd.DataFrame()
        files = sorted(files)
        print('Files to load: {}'.format(files))
        for i in range(0, len(files)):
            self.logger.info('Starting: {}'.format(files[i]))
            ldf = pd.read_csv(root+files[i],
                              index_col=None,
                              header=0,
                              delimiter=',',
                              parse_dates=['Date', 'Time'],
                              low_memory=False)

            lst = list(ldf.columns.values)
            droplst = list(filter(lambda x: str(x)[-2:] == '.1', lst))
            ldf.drop(columns=droplst, inplace=True)
            ldf.rename(columns={'CURRENT_CSU':'CURRENT'},inplace=True)
            df = df.append(ldf)
            del ldf


        dropCols = df.columns.to_list()
        dropCols[:] = [x for x in dropCols if any(y in x for y in ['.1'])]
        # dropCols.append('PB_2_556_V_PRIME')
        df = df.drop(columns=dropCols)

        Cols = df.columns.to_list()

        Cols.remove('Time')
        Cols.remove('Date')
        for col in Cols:
            if is_numeric_dtype(df[col]):
                continue
            else:
                df[col] = pd.to_numeric(df[col], errors='coerce', downcast="float")


        '''Temp Cleanup'''
        df.loc[df['T_AMB_1.2'] < -60, 'T_AMB_1'] = np.nan
        # df.loc[df['T_AMB_2.2'] < -60, 'T_AMB_1'] = np.nan

        '''Current Cleanup'''
        df['CURRENT'] = df['CURRENT'].fillna(0)
        df.loc[(df['CURRENT'] < 250) & (df['CURRENT'] > -250), 'CURRENT'] = 0

        df['stepTime'] = df.Time.diff()
        df = df[(df.stepTime < np.timedelta64(5, 'm')) & (df.stepTime > - np.timedelta64(5, 'm'))]
        df['stepTime'] = df['stepTime'].dt.total_seconds()
        df['TempCycle'] = np.where(
            (df['T_AMB_1.2'] >= 0) & (df['T_AMB_1.2'].shift(-100) > 0) & (df['T_AMB_1.2'].shift(1) < 0) & (df['T_AMB_1.2'].shift(2) < 0) & (
                        df['T_AMB_1.2'].shift(3) < 0) & (df['T_AMB_1.2'].shift(100) < 0), 1, 0)
        df['TempCycleCount'] = df['TempCycle'].cumsum()
        df['CurrentCycleStart'] = np.where(
            (df['CURRENT'] > 1) & (df['CURRENT'].shift(1) == 0) & (df['CURRENT'].shift(-1) >= 1) & (
                        df['CURRENT'].shift(-3) >= 1), 1, 0)
        df['CurrentCycleEnd'] = np.where(
            (df['CURRENT'] == 0) & (df['CURRENT'].shift(1) > 1) & (df['CURRENT'].shift(-1) == 0) & (
                        df['CURRENT'].shift(3) >= 1), 1, 0)
        df['CurrentCycleCount'] = df['CurrentCycleStart'].cumsum()
        df['CurrentCycleCompare'] = df['CurrentCycleEnd'].cumsum()
        df['CurrentCycleIndicator'] = df['CurrentCycleCount'] - df['CurrentCycleCompare']
        df['PowerCycleStart'] = np.where((df['CURRENT'] > 1) &
                                         (df['CURRENT'].shift(1) == 0) &
                                         (df['CURRENT'].shift(-1000).rolling(min_periods=1, window=1000).mean() >= 10) &
                                         (df['CURRENT'].shift(1).rolling(min_periods=10, window=1000).mean() <= 1)
                                         , 1, 0)
        df['PowerCycleRollingEval150'] = df['CURRENT'].shift(1).rolling(min_periods=1, window=150).mean()
        df['PowerCycleRollingEval300'] = df['CURRENT'].shift(1).rolling(min_periods=1, window=300).mean()
        df['PowerCycleRollingEval1000'] = df['CURRENT'].shift(1).rolling(min_periods=1, window=1000).mean()
        df['PowerCycleEnd'] = np.where((df['CURRENT'] == 0) &
                                       (df['CURRENT'].shift(1) > 1) &
                                       (df['CURRENT'].shift(1).rolling(min_periods=1, window=1000).mean() >= 10) &
                                       (df['CURRENT'].shift(-1001).rolling(min_periods=10, window=1000).mean() <= 1)
                                       , 1, 0)

        df['PowerCycleCount'] = df['PowerCycleStart'].cumsum()
        df['PowerCycleCompare'] = df['PowerCycleEnd'].cumsum()
        df['PowerCycleIndicator'] = df['PowerCycleCount'] - df['PowerCycleCompare']
        df['CURRENT^2'] = df['CURRENT']**2

        # df['TempCycleDelta'] = df['TempCycle'] - df['TempCycle'].shift(-1)
        # a = df.TempCycleDelta < 0
        # df['CurrentCycleID'] = a.cumsum() - a.cumsum().where(~a).ffill().fillna(0).astype(int)
        df['CurrentCycleID'] = np.nan
        for i in range(0,df.TempCycleCount.max()):
            minCycle = df[df['TempCycleCount']==i]['CurrentCycleCount'].min()
            df['CurrentCycleID'] = np.where((df['TempCycleCount']== i),df.CurrentCycleCount - minCycle,df['CurrentCycleID'])
        # df['stepTime'] = pd.to_timedelta(df['stepTime'])


        self.OutputToExcel(df, r'diagnostic_data/Cleaned_Characterization_TestFile.csv')
        return df

    def ParseSN(self, df):
        ''' Split Joined file into Serial Number Specific Files
            Signal Decipher / Conversion
        '''
        self.logger.info('~~~~ Start ParseSN Process ~~~~')
        cols = list(df.columns.values)
        colDF = pd.DataFrame(cols, columns=['key'])

        colDF['Pos'] = np.where(colDF['key'].str[:2] == "PB", 'Pos', '')
        colDF['Neg'] = np.where(colDF['key'].str[:2] == "NB", 'Neg', '')

        colDF['F1type'] = np.where(((colDF['key'].str[:2] == "PB") & (colDF['key'].str[3] == '1')), "Eaton", '')
        colDF['F2type'] = np.where(((colDF['key'].str[:2] == "PB") & (colDF['key'].str[3] == '2')), "Mersen", '')
        colDF['Ftype'] = colDF.F1type + colDF.F2type
        colDF['SN'] = np.where((colDF['Pos'] == 'Pos'), colDF['key'].str.split(pat='_',expand=True)[2], colDF['key'].str.split(pat='_',expand=True)[1])

        colDF['measT'] = np.where(colDF['key'].str[-2:-1] == 'T', 1, 0)
        '''Positive TC Locations'''
        colDF['T1P'] = np.where(((colDF['key'].str[:2] == "PB") & (colDF['key'].str[-2:] == 'T1')), 'Input_BB', '')
        colDF['T2P'] = np.where(((colDF['key'].str[:2] == "PB") & (colDF['key'].str[-2:] == 'T2')), 'Output_BB', '')
        colDF['T3P'] = np.where(((colDF['key'].str[:2] == "PB") & (colDF['key'].str[-2:] == 'T3')), 'DCFC_BB', '')
        colDF['T4P'] = np.where(((colDF['key'].str[:2] == "PB") & (colDF['key'].str[-2:] == 'T4')), 'Sec_Fuse_Body', '')
        colDF['T5'] = np.where(colDF['key'].str[-2:] == 'T5', 'Prim_Fuse_Body', '')
        colDF['T6'] = np.where(colDF['key'].str[-2:] == 'T6', 'Sec_Cont_Term', '')
        colDF['T7'] = np.where(colDF['key'].str[-2:] == 'T7', 'Prim_Cont_Term', '')
        '''Negative TC Locations'''
        colDF['T1N'] = np.where(((colDF['key'].str[:2] == "NB") & (colDF['key'].str[-2:] == 'T1')), 'Shunt_BB', '')
        colDF['T2N'] = np.where(((colDF['key'].str[:2] == "NB") & (colDF['key'].str[-2:] == 'T2')), 'Prim_Cont_Term', '')
        colDF['T3N'] = np.where(((colDF['key'].str[:2] == "NB") & (colDF['key'].str[-2:] == 'T3')), 'DCFC_BB', '')
        colDF['T4N'] = np.where(((colDF['key'].str[:2] == "NB") & (colDF['key'].str[-2:] == 'T4')), 'Sec_Cont_Term', '')

        ''' Terminal Temp'''
        # colDF['TPT'] = np.where(colDF['key'].str[-8:] == '_T_PRIME', 'T_', '')
        # colDF['TST'] = np.where(colDF['key'].str[-6:] == '_T_SEC', 'T_', '')
        # colDF['TDT'] = np.where(colDF['key'].str[-7:] == '_T_DCFC', 'T_', '')
        # colDF['TPTa'] = np.where(colDF['key'].str[-8:] == '_T_PRIME', '_A1', '')
        # colDF['TSTa'] = np.where(colDF['key'].str[-6:] == '_T_SEC', '_A1', '')
        # colDF['TDTa'] = np.where(colDF['key'].str[-7:] == '_T_DCFC', '_A1', '')
        colDF['TPT'] = np.where(colDF['key'].str[-11:] == '_T_PRIME_A1', 'T_PRIME_A1', '')
        colDF['TST'] = np.where(colDF['key'].str[-9:] == '_T_SEC_A1', 'T_SEC_A1', '')
        colDF['TDT'] = np.where(colDF['key'].str[-10:] == '_T_DCFC_A1', 'T_DCFC_A1', '')
        colDF['TPT'] = np.where(colDF['key'].str[-11:] == '_T_PRIME_A2', 'T_PRIME_A2', '')
        colDF['TST'] = np.where(colDF['key'].str[-9:] == '_T_SEC_A2', 'T_SEC_A2', '')
        colDF['TDT'] = np.where(colDF['key'].str[-10:] == '_T_DCFC_A2', 'T_DCFC_A2', '')

        '''DUT Voltage'''
        colDF['IOV'] = np.where(colDF['key'].str[-7:] == 'INP_OUT', 'INP_OUT', '')
        colDF['ODCFCV'] = np.where(colDF['key'].str[-8:] == 'OUT_DCFC', 'OUT_DCFC', '')
        colDF['FPV'] = np.where(colDF['key'].str[-7:] == 'F_PRIME', 'F_', '')
        colDF['PCV'] = np.where(colDF['key'].str[-7:] == 'V_PRIME', 'V_', '')
        colDF['FSV'] = np.where(colDF['key'].str[-5:] == 'F_SEC', 'F_', '')
        colDF['PV'] = np.where(colDF['key'].str[-5:] == 'PRIME', 'PRIME', '')
        colDF['SV'] = np.where(colDF['key'].str[-3:] == 'SEC', 'SEC', '')
        colDF['ShuntV'] = np.where(colDF['key'].str[-5:] == 'SHUNT', 'ShuntV', '')
        # colDF['ShuntI'] = np.where(colDF['key'].str[-13:] == 'SHUNT_CURRENT', 'SHUNT_CURRENT', '')

        ''' Coil Current'''
        colDF['PCC'] = np.where(colDF['key'].str[-15:] == 'CUUR_COIL_PRIME', 'CURR_', '')
        colDF['SCC'] = np.where(colDF['key'].str[-13:] == 'CUUR_COIL_SEC', 'CURR_', '')
        colDF['DCC'] = np.where(colDF['key'].str[-14:] == 'CUUR_COIL_DCFC', 'CURR_DCFC', '')

        ''' AUX Signal'''
        colDF['Paux'] = np.where(colDF['key'].str[-9:] == 'AUX_PRIME', 'AUX_', '')
        colDF['Saux'] = np.where(colDF['key'].str[-7:] == 'AUX_SEC', 'AUX_', '')
        colDF['Daux'] = np.where(colDF['key'].str[-8:] == 'AUX_DCFC', 'AUX_DCFC', '')

        '''External Contactor'''
        colDF['EPos'] = np.where(colDF['key'].str[:4] == "T_PB", 'Ext_Pos', '')
        colDF['ENeg'] = np.where(colDF['key'].str[:4] == "T_NB", 'Ext_Neg', '')
        colDF['TExt'] = np.where(colDF['key'].str[:5] == "T_EXT", 'Ext', '')
        colDF['Ext'] = np.where(colDF['key'].str[:3] == "EXT", 'Ext', '')
        colDF['ECC'] = np.where(colDF['key'].str[-8:] == 'EXT_CURR', 'EXT_CURR', '')
        colDF['EXTV'] = np.where(colDF['key'].str[-3:] == 'EXT', 'EXT_Volt', '')
        colDF['T8'] = np.where(colDF['key'].str[-8:] == 'T_PB_OUT', 'HVDB_Discharge_BB', '')
        colDF['T9'] = np.where(colDF['key'].str[-9:] == 'T_EXT_POS', 'EXT_CONT_Term', '')
        colDF['T10'] = np.where(colDF['key'].str[-9:] == 'T_EXT_NEG', 'EXT_CONT_Term', '')
        colDF['T11'] = np.where(colDF['key'].str[-8:] == 'T_PB_OUT', 'HVDB_Discharge_BB', '')


        colDF['Polarity'] = colDF.Pos + colDF.Neg + colDF.Ext + colDF.EPos + colDF.ENeg + colDF.TExt
        colDF['T1'] = colDF.T1P + colDF.T1N
        colDF['T2'] = colDF.T2P + colDF.T2N
        colDF['T3'] = colDF.T3P + colDF.T3N
        colDF['T4'] = colDF.T4P + colDF.T4N


        colDF['Legend'] = colDF.T1 + colDF.T2 + colDF.T3 + colDF.T4 + colDF.T5 + colDF.T6 + colDF.T7 + colDF.T8 + \
                          colDF.T9 + colDF.T10 + colDF.T11 + colDF.IOV + colDF.ECC + colDF.EXTV +\
                          colDF.ODCFCV + colDF.FPV + colDF.PCV + colDF.FSV + colDF.PCC + colDF.SCC + colDF.DCC + colDF.Paux + \
                          colDF.Saux + colDF.Daux + colDF.TPT + colDF.TST + colDF.TDT + colDF.PV + colDF.SV + \
                          colDF.ShuntV #+ colDF.TPTa + colDF.TSTa + colDF.TDTa

        colDF.drop(columns=['Pos', 'Neg','EPos', 'ENeg', 'F1type', 'F2type', 'measT', 'T1', 'T2', 'T3', 'T4', 'T5', 'T6', 'T7','T8','T9', 'T10','T11', 'IOV',
                            'T1P', 'T2P', 'T3P', 'T4P','T1N', 'T2N', 'T3N', 'T4N',
                            'ODCFCV', 'FPV','PCV' , 'FSV', 'PV', 'SV', 'ShuntV', 'PCC','SCC','DCC','Paux','Saux','Daux','TPT','TST','TDT','ECC','EXTV'], inplace=True) #,'TPTa','TSTa','TDTa'
        self.logger.info('Finished Column DF manipulations')
        SNlst = colDF.SN.unique()
        SNlst = SNlst[SNlst != np.array(None)]
        SNlst = [x for x in SNlst if x.isdigit()]

        return colDF, SNlst

    def SplitRawFile(self,df,colDF,SNlst):
        ''' Use Parse Data to Split Files'''
        self.logger.info('~~~~ Start SplitRawFile Process ~~~~')
        badSNs = ['171','172','173']
        for x in badSNs:
            if x in SNlst: SNlst.remove(x)

        self.logger.info('Serial List: {}'.format(SNlst))
        for sn in SNlst:
            self.logger.info('Starting SN: {} ({}/{})'.format(sn, SNlst.index(sn), len(SNlst)))
            snDF = colDF[colDF['SN'] == str(sn)]
            type = snDF.Polarity.unique().tolist()
            # print(type)
            for Pol in type:
                self.logger.info('Starting Pol: {}'.format(Pol))
                snPDF = snDF[snDF['Polarity'] == Pol]
                if snPDF.empty == True:
                    continue
                snCOLS = snPDF['key'].tolist()
                snLEG = snPDF['Legend'].tolist()
                fusedic = {'Eaton':'1','Mersen':'2'}
                if Pol == "Pos":
                    PolStr = {'Pos':'PB_'+fusedic[colDF[colDF['SN'] == str(sn)]['Ftype'].values[0]]+'_'+sn+'_',
                          'Neg':'NB_'+sn+'_'}
                else:
                    PolStr = {'Pos': 'nan',
                              'Neg': 'NB_' + sn + '_'}


                posCOLs = [PolStr['Pos']+'T_DCFC_A1',PolStr['Pos']+'T_DCFC_A2',
                           PolStr['Pos']+'T_PRIME_A1',PolStr['Pos']+'T_PRIME_A2',
                           PolStr['Pos']+'T_SEC_A1',PolStr['Pos']+'T_SEC_A2']
                negCOLs = [PolStr['Neg']+'T_DCFC_A1',PolStr['Neg']+'T_DCFC_A2',
                           PolStr['Neg']+'T_PRIME_A1',PolStr['Neg']+'T_PRIME_A2',
                           PolStr['Neg']+'T_SEC_A1',PolStr['Neg']+'T_SEC_A2']

                stdCOLS = ['Date', 'Time', 'stepTime', 'TempCycle', 'TempCycleCount', 'CurrentCycleCount','CurrentCycleID',
                           'CurrentCycleIndicator', 'PowerCycleCount', 'PowerCycleIndicator', 'CURRENT','CURRENT^2', 'T_AMB_1.2']
                stdrnmCOLS = ['Date', 'Time', 'stepTime', 'TempCycle', 'TempCycleCount', 'CurrentCycleCount','CurrentCycleID',
                              'CurrentCycleIndicator', 'PowerCycleCount', 'PowerCycleIndicator', 'CURRENT', 'T_AMB_1.2']
                if Pol == 'Pos':
                    stdCOLS.extend(posCOLs)
                elif Pol == "Neg":
                    stdCOLS.extend(negCOLs)

                fltCOLS = stdCOLS.copy()
                rnmCOLS = stdCOLS.copy()
                fltCOLS.extend(snCOLS)
                rnmCOLS.extend(snLEG)
                # snDATA = df[df.columns.intersection(fltCOLS)].copy()
                snDATA = df[fltCOLS].copy()
                snDATA.columns = rnmCOLS

                
                if Pol == 'Pos':
                    ''' Common'''
                    snDATA['dT_Input_BB'] = snDATA['Input_BB'] - snDATA['T_AMB_1.2']
                    snDATA['dT_Output_BB'] = snDATA['Output_BB'] - snDATA['T_AMB_1.2']

                    ''' Primary Contactor'''
                    snDATA['dT_Primary_Terminal'] = snDATA['Prim_Cont_Term'] - snDATA['T_AMB_1.2']
                    snDATA['dT_Primary_Stationary_1'] = snDATA[PolStr['Pos']+'T_PRIME_A1'] - snDATA['T_AMB_1.2']
                    snDATA['dT_Primary_Stationary_2'] = snDATA[PolStr['Pos']+'T_PRIME_A2'] - snDATA['T_AMB_1.2']
                    snDATA['dT_Primary_Stationary_Across'] = snDATA[PolStr['Pos']+'T_PRIME_A1']-snDATA[PolStr['Pos']+'T_PRIME_A2']
                    if sn == '847':
                        snDATA['PRIME'] = snDATA['PRIME'].astype(float)

                    try: snDATA['PCcrSplit'] = snDATA['PRIME'] * 2 / snDATA['CURRENT']
                    except:
                        self.logger.error('Current or Prime have multiple columns')
                        print(snDATA.CURRENT)
                        print(snDATA.PRIME)


                    ''' Secondary Contactor'''
                    snDATA['dT_Secondary_Terminal'] = snDATA['Sec_Cont_Term'] - snDATA['T_AMB_1.2']
                    snDATA['dT_Secondary_Stationary_1'] = snDATA[PolStr['Pos']+'T_SEC_A1'] - snDATA['T_AMB_1.2']
                    snDATA['dT_Secondary_Stationary_2'] = snDATA[PolStr['Pos']+'T_SEC_A2'] - snDATA['T_AMB_1.2']
                    snDATA['dT_Secondary_Stationary_Across'] = snDATA[PolStr['Pos']+'T_SEC_A1']-snDATA[PolStr['Pos']+'T_SEC_A2']
                    snDATA['SCcrSplit'] = snDATA['SEC'] * 2 / snDATA['CURRENT']

                    ''' DCFC Contactor'''
                    snDATA.loc[snDATA['OUT_DCFC'] < 0, 'OUT_DCFC'] = 0
                    snDATA.loc[snDATA['OUT_DCFC'] > .5, 'OUT_DCFC'] = 0
                    snDATA['DCFCcr'] = snDATA['OUT_DCFC'] / snDATA['CURRENT']
                    snDATA.loc[snDATA['DCFCcr'] == 0, 'DCFCcr'] = None
                    snDATA['dT_DCFC_BB'] = snDATA['DCFC_BB'] - snDATA['T_AMB_1.2']
                    snDATA['dT_DCFC_Stationary_1'] = snDATA[PolStr['Pos']+'T_DCFC_A1'] - snDATA['T_AMB_1.2']
                    snDATA['dT_DCFC_Stationary_2'] = snDATA[PolStr['Pos']+'T_DCFC_A2'] - snDATA['T_AMB_1.2']
                    snDATA['dT_DCFC_Stationary_Across'] = snDATA[PolStr['Pos']+'T_DCFC_A1']-snDATA[PolStr['Pos']+'T_DCFC_A2']

                    ''' Primary Fuse'''
                    snDATA['dT_Primary_Fuse_Body'] = snDATA['Prim_Fuse_Body'] - snDATA['T_AMB_1.2']
                    snDATA['PFrSplit'] = snDATA['F_PRIME'] * 2 / snDATA['CURRENT']
                    snDATA['PFi2rt'] = snDATA['F_PRIME'] * (snDATA['CURRENT']/2) * snDATA['stepTime'] #.dt.total_seconds()

                    ''' Secondary Fuse'''
                    snDATA['dT_Seconary_Fuse_Body'] = snDATA['Sec_Fuse_Body'] - snDATA['T_AMB_1.2']
                    snDATA['SFrSplit'] = snDATA['F_SEC'] * 2 / snDATA['CURRENT']
                    snDATA['SFi2rt'] = snDATA['F_SEC'] * (snDATA['CURRENT']/2) * snDATA['stepTime'] #.dt.total_seconds()

                elif Pol == "Neg":
                    ''' Common'''

                    ''' Primary Contactor'''
                    snDATA['dT_Primary_Terminal'] = snDATA['Prim_Cont_Term'] - snDATA['T_AMB_1.2']
                    snDATA['dT_Primary_Stationary_1'] = snDATA[PolStr['Neg']+'T_PRIME_A1'] - snDATA['T_AMB_1.2']
                    snDATA['dT_Primary_Stationary_2'] = snDATA[PolStr['Neg']+'T_PRIME_A2'] - snDATA['T_AMB_1.2']
                    snDATA['dT_Primary_Stationary_Across'] = snDATA[PolStr['Neg']+'T_PRIME_A1']-snDATA[PolStr['Neg']+'T_PRIME_A2']
                    snDATA['PCcrSplit'] = snDATA['PRIME'] * 2 / snDATA['CURRENT']

                    ''' Secondary Contactor'''
                    snDATA['dT_Secondary_Terminal'] = snDATA['Sec_Cont_Term'] - snDATA['T_AMB_1.2']
                    snDATA['dT_Secondary_Stationary_1'] = snDATA[PolStr['Neg']+'T_SEC_A1'] - snDATA['T_AMB_1.2']
                    snDATA['dT_Secondary_Stationary_2'] = snDATA[PolStr['Neg']+'T_SEC_A2'] - snDATA['T_AMB_1.2']
                    snDATA['dT_Secondary_Stationary_Across'] = snDATA[PolStr['Neg']+'T_SEC_A1']-snDATA[PolStr['Neg']+'T_SEC_A2']
                    snDATA['SCcrSplit'] = snDATA['SEC'] * 2 / snDATA['CURRENT']

                    ''' DCFC Contactor'''
                    snDATA.loc[snDATA['OUT_DCFC'] < 0, 'OUT_DCFC'] = 0
                    snDATA.loc[snDATA['OUT_DCFC'] > .5, 'OUT_DCFC'] = 0
                    snDATA['DCFCcr'] = snDATA['OUT_DCFC'] / snDATA['CURRENT']
                    snDATA.loc[snDATA['DCFCcr'] == 0, 'DCFCcr'] = None
                    snDATA['dT_DCFC_BB'] = snDATA['DCFC_BB'] - snDATA['T_AMB_1.2']
                    snDATA['dT_DCFC_Stationary_1'] = snDATA[PolStr['Neg']+'T_DCFC_A1'] - snDATA['T_AMB_1.2']
                    snDATA['dT_DCFC_Stationary_2'] = snDATA[PolStr['Neg']+'T_DCFC_A2'] - snDATA['T_AMB_1.2']
                    snDATA['dT_DCFC_Stationary_Across'] = snDATA[PolStr['Neg']+'T_DCFC_A1']-snDATA[PolStr['Neg']+'T_DCFC_A2']

                    ''' Shunt '''
                    snDATA['ShuntR'] = snDATA['ShuntV'] / snDATA['CURRENT']
                    snDATA.loc[snDATA['ShuntR'] == 0, 'ShuntR'] = None

                self.OutputToExcel(snDATA,r'unit_data/ptc_round_{}/{}/Aptiv_PTC_Modified_Temp_Unit_Data_SN{}_Pol-{}.csv'.format(self.round,Pol, sn, Pol))

    def positive_HVDB_Eval(self):
        ''' Positive HVDB data evaluation
            1) Convert column headers to standard names
            2) Set Device Specific Columns for Evaluation
            3) Loop through Current Cycles
                a) Evaluate Current Cycle Temperature
                b) Evaluate Current Cycle Current signal
                    i)Determine cycle type
                    ii)Run device specific evaluation based on current profile
            4) Combine all device and current cycle results into master sheet
            5) Output to excel
        '''
        self.logger.info('~~~~ Start positive_HVDB_Eval Process ~~~~')
        data_folder = r"unit_data/ptc_round_{}/Pos/".format(self.round)
        files = os.listdir(data_folder)
        files = [k for k in files if '.csv' in k]
        JoinedMasterCurrentSummary = pd.DataFrame()

        for i in range(0, len(files)):
            self.logger.info('Starting: {}'.format(files[i]))
            df = pd.read_csv(data_folder+files[i],
                              index_col=None,
                              header=0,
                              delimiter=',',
                              parse_dates=['Date', 'Time'],
                              low_memory=False)

            cols = list(df.columns)
            cols = [str for str in cols if any(sub in str for sub in ['PB_'])]
            serialdata = cols[0].split('_') # 2 - SN / 1 - FT
            colconvsiondict = {'PB_'+serialdata[1]+'_'+serialdata[2]+'_T_PRIME_A1':'PB_T_PRIME_A1',
                               'PB_'+serialdata[1]+'_'+serialdata[2]+'_T_PRIME_A2':'PB_T_PRIME_A2',
                               'PB_'+serialdata[1]+'_'+serialdata[2]+'_T_SEC_A1':'PB_T_SEC_A1',
                               'PB_'+serialdata[1]+'_'+serialdata[2]+'_T_SEC_A2':'PB_T_SEC_A2',
                               'PB_'+serialdata[1]+'_'+serialdata[2]+'_T_DCFC_A1':'PB_T_DCFC_A1',
                               'PB_'+serialdata[1]+'_'+serialdata[2]+'_T_DCFC_A2':'PB_T_DCFC_A2'}
            df = df.rename(columns=colconvsiondict)

            for value in colconvsiondict.values():
                df[df[value] > 200] = None

            ''' Common'''
            signal_lst = ['Date','Time','stepTime','TempCycleCount','CurrentCycleCount','CURRENT','T_AMB_1.2']

            ''' Primary Contactor'''
            PC_signal_lst = signal_lst.copy()
            PC_signal_lst.extend(['PRIME','Prim_Cont_Term','PB_T_PRIME_A1','PB_T_PRIME_A2','PCcrSplit'])

            ''' Secondary Contactor'''
            SC_signal_lst = signal_lst.copy()
            SC_signal_lst.extend(['SEC','Sec_Cont_Term','PB_T_SEC_A1','PB_T_SEC_A2','SCcrSplit'])

            ''' DCFC Contactor'''
            DC_signal_lst = signal_lst.copy()
            DC_signal_lst.extend(['OUT_DCFC','DCFC_BB','PB_T_DCFC_A1','PB_T_DCFC_A2','DCFCcr'])

            ''' Primary Fuse'''
            PF_signal_lst = signal_lst.copy()
            PF_signal_lst.extend(['F_PRIME','Prim_Fuse_Body','PFrSplit','PFi2rt'])

            ''' Secondary Fuse'''
            SF_signal_lst = signal_lst.copy()
            SF_signal_lst.extend(['F_SEC','Sec_Fuse_Body','SFrSplit','SFi2rt'])

            columns = ['CurrentCycle','TempCycle','CycleTime','meanCurrent','cntCurrent','CurrentCycleType','TempProfile']

            cont_columns=['CurrentCycle','meanCR','stdCR','maxCR','minCR','rangeCR','maxTermTemp','minTermTemp','maxDeltaTermTemp','rangeTermTemp',
                          'maxSA1Temp','minSA1Temp','maxDeltaSA1Temp','rangeSA1Temp','maxSA2Temp','minSA2Temp','maxDeltaSA2Temp','rangeSA2Temp','deltaStationary']

            fuse_columns=['CurrentCycle','meanR','stdR','maxR','minR','rangeR','maxBodyTemp','rangeBodyTemp','i2rt']

            cdf = pd.DataFrame(columns=columns)
            pdf = pd.DataFrame(columns=cont_columns)
            sdf = pd.DataFrame(columns=cont_columns)
            ddf = pd.DataFrame(columns=cont_columns)
            pfdf = pd.DataFrame(columns=fuse_columns)
            sfdf = pd.DataFrame(columns=fuse_columns)

            for i in range(1, int(df.CurrentCycleCount.max())):
                tdf = df.loc[(df['CurrentCycleCount'] == i) & (df['CURRENT'] != 0)]
                time = tdf.Time.max() - tdf.Time.min()
                TempCycle = tdf.TempCycleCount.max()

                maxAmb = tdf['T_AMB_1.2'].max()
                if maxAmb > 55:
                    tempProfile = "60C"
                else:
                    tempProfile = '0C'

                meanCurrent = tdf.CURRENT[2:-2].mean()
                cntCurrent = len(tdf.CURRENT[2:-2])

                # print(self.round)
                # print(self.round_Dict[self.round])
                # print(self.profile_Dict[self.round_Dict[self.round]])
                # print(self.profile_Dict[self.round_Dict[self.round]]['WOT_Discharge'])
                # print(self.profile_Dict[self.round_Dict[self.round]]['WOT_DCFC'][0])
                # input('Continue?')

                if meanCurrent > self.profile_Dict[self.round_Dict[self.round]]['WOT_Discharge']:
                    CurrentCycleType = 'WOT_Discharge'
                elif (self.profile_Dict[self.round_Dict[self.round]]['WOT_DCFC'][0]> meanCurrent > self.profile_Dict[self.round_Dict[self.round]]['WOT_DCFC'][1]) & (time.total_seconds() < 30):
                    CurrentCycleType = 'WOT_DCFC'
                    DCFC_Cont_CC = self.contactor_current_cycle(i, tdf[DC_signal_lst])
                    ddf = ddf.append(pd.Series(DCFC_Cont_CC, index=ddf.columns), ignore_index=True)
                elif (self.profile_Dict[self.round_Dict[self.round]]['CC_Discharge'][0]> meanCurrent > self.profile_Dict[self.round_Dict[self.round]]['CC_Discharge'][1]) & (time.total_seconds() > 60):
                    CurrentCycleType = 'CC_Discharge'
                elif (self.profile_Dict[self.round_Dict[self.round]]['CC_DCFC'][0]> meanCurrent > self.profile_Dict[self.round_Dict[self.round]]['CC_DCFC'][1]) & (time.total_seconds() > 60):
                    CurrentCycleType = 'CC_DCFC'
                elif (self.profile_Dict[self.round_Dict[self.round]]['CS'][0]> meanCurrent > self.profile_Dict[self.round_Dict[self.round]]['CS'][1]) & (30 < time.total_seconds() < 60):
                    if tdf.PFrSplit[2:-2].mean() > 0.0001:
                        CurrentCycleType = 'CS - Primary'
                        Primary_Cont_CC = self.contactor_current_share_cycle(i, tdf[PC_signal_lst])
                        Primary_Term_CC = self.fuse_current_share_cycle(i, tdf[PF_signal_lst])
                        pdf = pdf.append(pd.Series(Primary_Cont_CC, index=pdf.columns), ignore_index=True)
                        pfdf = pfdf.append(pd.Series(Primary_Term_CC, index=pfdf.columns), ignore_index=True)
                    elif tdf.SFrSplit[2:-2].mean() > 0.0001:
                        CurrentCycleType = 'CS - Secondary'
                        Secondary_Cont_CC = self.contactor_current_share_cycle(i, tdf[SC_signal_lst])
                        Secondary_Term_CC = self.fuse_current_share_cycle(i, tdf[SF_signal_lst])
                        sdf = sdf.append(pd.Series(Secondary_Cont_CC, index=sdf.columns), ignore_index=True)
                        sfdf = sfdf.append(pd.Series(Secondary_Term_CC, index=sfdf.columns), ignore_index=True)
                    else:
                        CurrentCycleType = 'CS - Error'

                else:
                    CurrentCycleType = 'Unknown'

                if CurrentCycleType in ['WOT_Discharge','CC_Discharge']:
                    Primary_Cont_CC = self.contactor_current_cycle(i, tdf[PC_signal_lst])
                    Secondary_Cont_CC = self.contactor_current_cycle(i, tdf[SC_signal_lst])
                    Primary_Term_CC = self.fuse_current_cycle(i, tdf[PF_signal_lst])
                    Secondary_Term_CC = self.fuse_current_cycle(i, tdf[SF_signal_lst])
                    pdf = pdf.append(pd.Series(Primary_Cont_CC, index=pdf.columns), ignore_index=True)
                    sdf = sdf.append(pd.Series(Secondary_Cont_CC, index=sdf.columns), ignore_index=True)
                    pfdf = pfdf.append(pd.Series(Primary_Term_CC, index=pfdf.columns), ignore_index=True)
                    sfdf = sfdf.append(pd.Series(Secondary_Term_CC, index=sfdf.columns), ignore_index=True)
                if CurrentCycleType in ['WOT_DCFC','CC_DCFC']:
                    Primary_Cont_CC = self.contactor_current_cycle(i, tdf[PC_signal_lst])
                    Secondary_Cont_CC = self.contactor_current_cycle(i, tdf[SC_signal_lst])
                    Primary_Term_CC = self.fuse_current_cycle(i, tdf[PF_signal_lst])
                    Secondary_Term_CC = self.fuse_current_cycle(i, tdf[SF_signal_lst])
                    DCFC_Cont_CC = self.contactor_current_cycle(i, tdf[DC_signal_lst])
                    ddf = ddf.append(pd.Series(DCFC_Cont_CC, index=ddf.columns), ignore_index=True)
                    pdf = pdf.append(pd.Series(Primary_Cont_CC, index=pdf.columns), ignore_index=True)
                    sdf = sdf.append(pd.Series(Secondary_Cont_CC, index=sdf.columns), ignore_index=True)
                    pfdf = pfdf.append(pd.Series(Primary_Term_CC, index=pfdf.columns), ignore_index=True)
                    sfdf = sfdf.append(pd.Series(Secondary_Term_CC, index=sfdf.columns), ignore_index=True)

                Common_CC = [i,TempCycle,time,meanCurrent,cntCurrent,CurrentCycleType,tempProfile]
                cdf = cdf.append(pd.Series(Common_CC, index=cdf.columns),ignore_index=True)


            pdf = self.current_cycle_summary_calc(pdf)
            sdf = self.current_cycle_summary_calc(sdf)
            ddf = self.current_cycle_summary_calc(ddf)
            pfdf = self.current_cycle_summary_calc(pfdf)
            sfdf = self.current_cycle_summary_calc(sfdf)

            pdf.set_index('CurrentCycle',drop=True,inplace=True)
            pdf.columns = pdf.columns.map(lambda x: str(x) + '_PC')
            sdf.set_index('CurrentCycle', drop=True, inplace=True)
            sdf.columns = sdf.columns.map(lambda x: str(x) + '_SC')
            ddf.set_index('CurrentCycle', drop=True, inplace=True)
            ddf.columns = ddf.columns.map(lambda x: str(x) + '_DC')
            pfdf.set_index('CurrentCycle', drop=True, inplace=True)
            pfdf.columns = pfdf.columns.map(lambda x: str(x) + '_PF')
            sfdf.set_index('CurrentCycle', drop=True, inplace=True)
            sfdf.columns = sfdf.columns.map(lambda x: str(x) + '_SF')

            MasterCurrentSummary = cdf.join(pdf,on='CurrentCycle')
            MasterCurrentSummary = MasterCurrentSummary.join(sdf, on='CurrentCycle')
            MasterCurrentSummary = MasterCurrentSummary.join(ddf, on='CurrentCycle')
            MasterCurrentSummary = MasterCurrentSummary.join(pfdf, on='CurrentCycle')
            MasterCurrentSummary = MasterCurrentSummary.join(sfdf, on='CurrentCycle')

            MasterCurrentSummary.drop_duplicates(subset=['CurrentCycle'], keep='first',inplace=True)

            MasterCurrentSummary['TempCycleDelta'] = MasterCurrentSummary['TempCycle'] - MasterCurrentSummary['TempCycle'].shift(-1)
            a = MasterCurrentSummary.TempCycleDelta > -1
            MasterCurrentSummary['CurrentCycleID'] = a.cumsum() - a.cumsum().where(~a).ffill().fillna(0).astype(int)

            MasterCurrentSummary['AccumI2RT_PF'] = MasterCurrentSummary['i2rt_PF'].cumsum()
            MasterCurrentSummary['AccumI2RT_SF'] = MasterCurrentSummary['i2rt_SF'].cumsum()
            MasterCurrentSummary['CS_MaxTempDelta_PCTerm'] = (MasterCurrentSummary.minTermTemp_PC - MasterCurrentSummary.maxTermTemp_PC.shift(1))*-1
            MasterCurrentSummary['CS_MaxTempDelta_SCTerm'] = (MasterCurrentSummary.minTermTemp_SC - MasterCurrentSummary.maxTermTemp_SC.shift(1))*-1
            MasterCurrentSummary['CS_MaxTempDelta_DCTerm'] = (MasterCurrentSummary.minTermTemp_DC - MasterCurrentSummary.maxTermTemp_DC.shift(1))*-1
            MasterCurrentSummary['CS_MaxTempDelta_PCA1'] = (MasterCurrentSummary.minSA1Temp_PC - MasterCurrentSummary.maxSA1Temp_PC.shift(1))*-1
            MasterCurrentSummary['CS_MaxTempDelta_SCA1'] = (MasterCurrentSummary.minSA1Temp_SC - MasterCurrentSummary.maxSA1Temp_SC.shift(1))*-1
            MasterCurrentSummary['CS_MaxTempDelta_DCA1'] = (MasterCurrentSummary.minSA1Temp_DC - MasterCurrentSummary.maxSA1Temp_DC.shift(1))*-1
            MasterCurrentSummary['CS_MaxTempDelta_PCA2'] = (MasterCurrentSummary.minSA2Temp_PC - MasterCurrentSummary.maxSA2Temp_PC.shift(1))*-1
            MasterCurrentSummary['CS_MaxTempDelta_SCA2'] = (MasterCurrentSummary.minSA2Temp_SC - MasterCurrentSummary.maxSA2Temp_SC.shift(1))*-1
            MasterCurrentSummary['CS_MaxTempDelta_DCA2'] = (MasterCurrentSummary.minSA2Temp_DC - MasterCurrentSummary.maxSA2Temp_DC.shift(1))*-1
            MasterCurrentSummary['ParallelResistance'] = ((MasterCurrentSummary.meanCR_PC + MasterCurrentSummary.meanR_PF) *
                                                          (MasterCurrentSummary.meanCR_SC + MasterCurrentSummary.meanR_SF)) / \
                                                         ((MasterCurrentSummary.meanCR_PC + MasterCurrentSummary.meanR_PF) +
                                                          (MasterCurrentSummary.meanCR_SC + MasterCurrentSummary.meanR_SF))
            MasterCurrentSummary['ParallelResistanceRollingMean'] = MasterCurrentSummary['ParallelResistance'].rolling(min_periods=10, window=50).mean()

            for item in ['CS_MaxTempDelta_PCTerm','CS_MaxTempDelta_SCTerm','CS_MaxTempDelta_DCTerm',
                         'CS_MaxTempDelta_PCA1','CS_MaxTempDelta_SCA1','CS_MaxTempDelta_DCA1',
                         'CS_MaxTempDelta_PCA2','CS_MaxTempDelta_SCA2','CS_MaxTempDelta_DCA2']:
                MasterCurrentSummary.loc[MasterCurrentSummary.CurrentCycleID == 1, item] = 0

            MasterCurrentSummary['SN'] = serialdata[2]
            MasterCurrentSummary['FuseType'] = serialdata[1]

            self.OutputToExcel(MasterCurrentSummary, r'summary_data/ptc_round{}_unitsummary/Round{}_{}_{}_Master_Current_Summary_Pos.csv'.format(self.round,self.round,serialdata[2],serialdata[1]))
            JoinedMasterCurrentSummary = JoinedMasterCurrentSummary.append(MasterCurrentSummary)
            # self.OutputToExcel(pdf, r'diagnostic_data/Master_Current_Summary_PC.csv')
            # self.OutputToExcel(sdf, r'diagnostic_data/Master_Current_Summary_SC.csv')
            # self.OutputToExcel(ddf, r'diagnostic_data/Master_Current_Summary_DC.csv')
            # self.OutputToExcel(pfdf, r'diagnostic_data/Master_Current_Summary_PF.csv')
            # self.OutputToExcel(sfdf, r'diagnostic_data/Master_Current_Summary_SF.csv')


        # self.OutputToExcel(LifeTempSummary,r'summary_data/ptc_round{}_lifeTempsummary/Round{}_Life_Temp_Summary_Pos.csv'.format(self.round,self.round))
        return JoinedMasterCurrentSummary

    def negative_HVDB_Eval(self):
        ''' Negative HVDB data evaluation
            1) Convert column headers to standard names
            2) Set Device Specific Columns for Evaluation
            3) Loop through Current Cycles
                a) Evaluate Current Cycle Temperature
                b) Evaluate Current Cycle Current signal
                    i)Determine cycle type
                    ii)Run device specific evaluation based on current profile
            4) Combine all device and current cycle results into master sheet
            5) Output to excel
        '''
        self.logger.info('~~~~ Start negative_HVDB_Eval Process ~~~~')
        data_folder = r"unit_data/ptc_round_{}/Neg/".format(self.round)
        files = os.listdir(data_folder)
        files = [k for k in files if '.csv' in k]
        JoinedMasterCurrentSummary = pd.DataFrame()
        for i in range(0, len(files)):
            self.logger.info('Starting: {}'.format(files[i]))
            df = pd.read_csv(data_folder + files[i],
                             index_col=None,
                             header=0,
                             delimiter=',',
                             parse_dates=['Date', 'Time'],
                             low_memory=False)
            #TODO Use the change serial columns to std columns "PB_#_SN_SIGNAL" to 'PB_SIGNAL'
            cols = list(df.columns)
            cols = [str for str in cols if any(sub in str for sub in ['NB_'])]
            serialdata = cols[0].split('_')

            colconvsiondict = {'NB_'+serialdata[1]+'_T_PRIME_A1':'NB_T_PRIME_A1',
                               'NB_'+serialdata[1]+'_T_PRIME_A2':'NB_T_PRIME_A2',
                               'NB_'+serialdata[1]+'_T_SEC_A1':'NB_T_SEC_A1',
                               'NB_'+serialdata[1]+'_T_SEC_A2':'NB_T_SEC_A2',
                               'NB_'+serialdata[1]+'_T_DCFC_A1':'NB_T_DCFC_A1',
                               'NB_'+serialdata[1]+'_T_DCFC_A2':'NB_T_DCFC_A2'}
            df = df.rename(columns=colconvsiondict)
            for value in colconvsiondict.values():
                df[df[value] > 200] = None
            ''' Common'''
            signal_lst = ['Date', 'Time', 'stepTime', 'TempCycleCount', 'CurrentCycleCount', 'CURRENT', 'T_AMB_1.2',
                          'T_AMB_2.2']

            ''' Primary Contactor'''
            PC_signal_lst = signal_lst.copy()
            PC_signal_lst.extend(
                [ 'PRIME', 'Prim_Cont_Term', 'NB_T_PRIME_A1', 'NB_T_PRIME_A2',
                 'PCcrSplit'])

            ''' Secondary Contactor'''
            SC_signal_lst = signal_lst.copy()
            SC_signal_lst.extend(
                [ 'SEC', 'Sec_Cont_Term', 'NB_T_SEC_A1', 'NB_T_SEC_A2', 'SCcrSplit'])

            ''' DCFC Contactor'''
            DC_signal_lst = signal_lst.copy()
            DC_signal_lst.extend(
                [ 'OUT_DCFC', 'DCFC_BB', 'NB_T_DCFC_A1', 'NB_T_DCFC_A2', 'DCFCcr'])

            ''' Shunt '''
            ST_signal_lst = signal_lst.copy()
            ST_signal_lst.extend(['ShuntR'])

            columns = ['CurrentCycle', 'TempCycle', 'CycleTime', 'meanCurrent', 'cntCurrent', 'CurrentCycleType',
                       'TempProfile']
            cont_columns=['CurrentCycle','meanCR','stdCR','maxCR','minCR','rangeCR','maxTermTemp','minTermTemp','maxDeltaTermTemp','rangeTermTemp',
                          'maxSA1Temp','minSA1Temp','maxDeltaSA1Temp','rangeSA1Temp','maxSA2Temp','minSA2Temp','maxDeltaSA2Temp','rangeSA2Temp','deltaStationary']
            shunt_columns=['CurrentCycle','meanR','stdR','maxR','minR','rangeR']

            cdf = pd.DataFrame(columns=columns)
            pdf = pd.DataFrame(columns=cont_columns)
            sdf = pd.DataFrame(columns=cont_columns)
            ddf = pd.DataFrame(columns=cont_columns)
            stdf = pd.DataFrame(columns=shunt_columns)


            for i in range(0, int(df.CurrentCycleCount.max())):
                tdf = df.loc[(df['CurrentCycleCount'] == i) & (df['CURRENT'] != 0)]
                time = tdf.Time.max() - tdf.Time.min()
                TempCycle = tdf.TempCycleCount.max()

                maxAmb = tdf['T_AMB_1.2'].max()
                if maxAmb > 55:
                    tempProfile = "60C"
                else:
                    tempProfile = '0C'

                meanCurrent = tdf.CURRENT[2:-2].mean()
                cntCurrent = len(tdf.CURRENT[2:-2])

                # if tdf.CurrentCycleID.max()- tdf.CurrentCycleID.min() > 0:
                #     print(tdf.CurrentCycleID.max(),tdf.CurrentCycleID.min())
                #     input('coninue?')

                if meanCurrent > 1250:
                    CurrentCycleType = 'WOT_Discharge'
                elif (1250 > meanCurrent > 700) & (time.total_seconds() < 30):
                    CurrentCycleType = 'WOT_DCFC'
                    DCFC_Cont_CC = self.contactor_current_cycle(i, tdf[DC_signal_lst])
                    ddf = ddf.append(pd.Series(DCFC_Cont_CC, index=ddf.columns), ignore_index=True)
                elif (1250 > meanCurrent > 700) & (time.total_seconds() > 60):
                    CurrentCycleType = 'CC_Discharge'
                elif (700 > meanCurrent > 300) & (time.total_seconds() > 60):
                    CurrentCycleType = 'CC_DCFC'
                elif (700 > meanCurrent > 300) & (30 < time.total_seconds() < 60):
                    # print(tdf.CurrentCycleID)
                    if tdf.CurrentCycleID.max() % 2 == 0:
                        CurrentCycleType = 'CS - Primary'
                        Primary_Cont_CC = self.contactor_current_share_cycle(i, tdf[PC_signal_lst])
                        pdf = pdf.append(pd.Series(Primary_Cont_CC, index=pdf.columns), ignore_index=True)
                    elif tdf.CurrentCycleID.max() % 2 != 0:
                        CurrentCycleType = 'CS - Secondary'
                        Secondary_Cont_CC = self.contactor_current_share_cycle(i, tdf[SC_signal_lst])
                        sdf = sdf.append(pd.Series(Secondary_Cont_CC, index=sdf.columns), ignore_index=True)
                    else:
                        print('CS Error')
                        CurrentCycleType = 'CS - Error'

                else:
                    # print('cannot recognize profile  {} / {}'.format(i, df.CurrentCycleCount.max()))
                    CurrentCycleType = 'Unknown'

                if CurrentCycleType in ['WOT_Discharge','WOT_DCFC','CC_Discharge','CC_DCFC']:
                    Primary_Cont_CC = self.contactor_current_cycle(i, tdf[PC_signal_lst])
                    Secondary_Cont_CC = self.contactor_current_cycle(i, tdf[SC_signal_lst])
                    pdf = pdf.append(pd.Series(Primary_Cont_CC, index=pdf.columns), ignore_index=True)
                    sdf = sdf.append(pd.Series(Secondary_Cont_CC, index=sdf.columns), ignore_index=True)
                if CurrentCycleType in ['WOT_DCFC','CC_DCFC']:
                    DCFC_Cont_CC = self.contactor_current_cycle(i, tdf[DC_signal_lst])
                    ddf = ddf.append(pd.Series(DCFC_Cont_CC, index=ddf.columns), ignore_index=True)

                Shunt_CC = self.shunt_current_cycle(i,tdf[ST_signal_lst])
                stdf = stdf.append(pd.Series(Shunt_CC,index=stdf.columns),ignore_index = True)
                Common_CC = [i,TempCycle,time,meanCurrent,cntCurrent,CurrentCycleType,tempProfile]
                cdf = cdf.append(pd.Series(Common_CC, index=cdf.columns),ignore_index=True)

            pdf = self.current_cycle_summary_calc(pdf)
            sdf = self.current_cycle_summary_calc(sdf)
            ddf = self.current_cycle_summary_calc(ddf)
            stdf = self.current_cycle_summary_calc(stdf)

            pdf.set_index('CurrentCycle', drop=True, inplace=True)
            pdf.columns = pdf.columns.map(lambda x: str(x) + '_PC')
            sdf.set_index('CurrentCycle', drop=True, inplace=True)
            sdf.columns = sdf.columns.map(lambda x: str(x) + '_SC')
            ddf.set_index('CurrentCycle', drop=True, inplace=True)
            ddf.columns = ddf.columns.map(lambda x: str(x) + '_DC')
            stdf.set_index('CurrentCycle', drop=True, inplace=True)
            stdf.columns = stdf.columns.map(lambda x: str(x) + '_ST')


            MasterCurrentSummary = cdf.join(pdf, on='CurrentCycle')
            MasterCurrentSummary = MasterCurrentSummary.join(sdf, on='CurrentCycle')
            MasterCurrentSummary = MasterCurrentSummary.join(ddf, on='CurrentCycle')
            MasterCurrentSummary = MasterCurrentSummary.join(stdf, on='CurrentCycle')
            MasterCurrentSummary.drop_duplicates(subset=['CurrentCycle'], keep='first', inplace=True)

            MasterCurrentSummary['TempCycleDelta'] = MasterCurrentSummary['TempCycle'] - MasterCurrentSummary[
                'TempCycle'].shift(-1)
            a = MasterCurrentSummary.TempCycleDelta > -1
            MasterCurrentSummary['CurrentCycleID'] = a.cumsum() - a.cumsum().where(~a).ffill().fillna(0).astype(int)

            MasterCurrentSummary['CS_MaxTempDelta_PCTerm'] = MasterCurrentSummary.minTermTemp_PC - MasterCurrentSummary.maxTermTemp_PC.shift(1)
            MasterCurrentSummary['CS_MaxTempDelta_SCTerm'] = MasterCurrentSummary.minTermTemp_SC - MasterCurrentSummary.maxTermTemp_SC.shift(1)
            MasterCurrentSummary['CS_MaxTempDelta_DCTerm'] = MasterCurrentSummary.minTermTemp_DC - MasterCurrentSummary.maxTermTemp_DC.shift(1)
            MasterCurrentSummary['CS_MaxTempDelta_PCA1'] = MasterCurrentSummary.minSA1Temp_PC - MasterCurrentSummary.maxSA1Temp_PC.shift(1)
            MasterCurrentSummary['CS_MaxTempDelta_SCA1'] = MasterCurrentSummary.minSA1Temp_SC - MasterCurrentSummary.maxSA1Temp_SC.shift(1)
            MasterCurrentSummary['CS_MaxTempDelta_DCA1'] = MasterCurrentSummary.minSA1Temp_DC - MasterCurrentSummary.maxSA1Temp_DC.shift(1)
            MasterCurrentSummary['CS_MaxTempDelta_PCA2'] = MasterCurrentSummary.minSA2Temp_PC - MasterCurrentSummary.maxSA2Temp_PC.shift(1)
            MasterCurrentSummary['CS_MaxTempDelta_SCA2'] = MasterCurrentSummary.minSA2Temp_SC - MasterCurrentSummary.maxSA2Temp_SC.shift(1)
            MasterCurrentSummary['CS_MaxTempDelta_DCA2'] = MasterCurrentSummary.minSA2Temp_DC - MasterCurrentSummary.maxSA2Temp_DC.shift(1)


            for item in ['CS_MaxTempDelta_PCTerm','CS_MaxTempDelta_SCTerm','CS_MaxTempDelta_DCTerm',
                         'CS_MaxTempDelta_PCA1','CS_MaxTempDelta_SCA1','CS_MaxTempDelta_DCA1',
                         'CS_MaxTempDelta_PCA2','CS_MaxTempDelta_SCA2','CS_MaxTempDelta_DCA2']:
                MasterCurrentSummary.loc[MasterCurrentSummary.CurrentCycleID == 1, item] = 0

            MasterCurrentSummary['SN'] = serialdata[1]

            self.OutputToExcel(MasterCurrentSummary, r'summary_data/ptc_round{}_unitsummary/Round{}_{}_Master_Current_Summary_Neg.csv'.format(self.round,self.round,serialdata[1]))
            JoinedMasterCurrentSummary = JoinedMasterCurrentSummary.append(MasterCurrentSummary)
            # self.OutputToExcel(pdf, r'diagnostic_data/Master_Current_Summary_PC.csv')
            # self.OutputToExcel(sdf, r'diagnostic_data/Master_Current_Summary_SC.csv')
            # self.OutputToExcel(ddf, r'diagnostic_data/Master_Current_Summary_DC.csv')
            # self.OutputToExcel(pfdf, r'diagnostic_data/Master_Current_Summary_PF.csv')
            # self.OutputToExcel(sfdf, r'diagnostic_data/Master_Current_Summary_SF.csv')

        return JoinedMasterCurrentSummary

    def contactor_current_cycle(self,i,df):
        ''' Contactor Current Cycle Evaluation
            Drop first 2 and last 2 current data points and evaluate CR
            Calculate Temperatures on stationaries and busbars
        '''
        self.logger.debug('~~~~ Start contactor_current_cycle Process ~~~~')
        df.columns = ['Date','Time','stepTime','TempCycleCount','CurrentCycleCount','CURRENT','T_AMB_1.2',
                      'Voltage_Drop','Busbar_Temp','Contactor_Terminal_Temp_A1',
                      'Contactor_Terminal_Temp_A2','ContactResistance']

        # print(i)
        # print(df.CurrentCycleCount)
        # print(df.ContactResistance)
        # plt.plot(df.ContactResistance)
        # fig, axs = plt.subplots(2, 1, figsize=(10, 8))
        # ax0 = axs[0].twinx()
        # axs[0].set_xlabel('Time(s)')
        # axs[0].set_ylabel('Current', color='r')
        # ax0.set_ylabel('Resistance', color='c')
        # axs[0].set_title("Current Cycle")
        # axs[0].plot(df.index[2:-2],df.CURRENT[2:-2], color='red', linewidth=2, markersize=6, label='Current')
        # ax0.plot(df.index[2:-2],df.ContactResistance[2:-2], color='c',linewidth=2, markersize=6, label='CR')
        # axs[0].legend(loc='center left', bbox_to_anchor=(0, 0.5))
        # ax0.legend(loc='center left', bbox_to_anchor=(0, 0.3))
        #
        # ax1 = axs[1].twinx()
        # axs[1].set_xlabel('Time(s)')
        # axs[1].set_ylabel('T', color='r')
        # ax1.set_ylabel('T', color='c')
        # axs[1].set_title("Terminal Temp")
        # axs[1].plot(df.index,df.Contactor_Terminal_Temp_A1, color='red', linewidth=2, markersize=6, label='A1 Temp')
        # ax1.plot(df.index,df.Contactor_Terminal_Temp_A2, color='c',linewidth=2, markersize=6, label='A2 Temp')
        # axs[1].legend(loc='center left', bbox_to_anchor=(0, 0.5))
        # ax1.legend(loc='center left', bbox_to_anchor=(0, 0.3))
        # plt.show()
        # input('continue?')

        meanContactResistance = df.ContactResistance[2:-2].mean()
        stdContactResistance = df.ContactResistance[2:-2].std()
        maxContactResistance = df.ContactResistance[2:-2].max()
        minContactResistance = df.ContactResistance[2:-2].min()
        rangeContactResistance = maxContactResistance - minContactResistance


        maxTerm = df.Busbar_Temp.max()
        minTerm = df.Busbar_Temp.min()
        maxDeltaTerm = (df.Busbar_Temp-df['T_AMB_1.2']).max()
        rangeTerm = df.Busbar_Temp.max() - df.Busbar_Temp.min()
        maxStationaryA1 = df.Contactor_Terminal_Temp_A1.max()
        minStationaryA1 = df.Contactor_Terminal_Temp_A1.min()
        maxDeltaStationaryA1 = (df.Contactor_Terminal_Temp_A1-df['T_AMB_1.2']).max()
        rangeStationaryA1 = df.Contactor_Terminal_Temp_A1.max() - df.Contactor_Terminal_Temp_A1.min()
        maxStationaryA2 = df.Contactor_Terminal_Temp_A2.max()
        minStationaryA2 = df.Contactor_Terminal_Temp_A2.min()
        maxDeltaStationaryA2 = (df.Contactor_Terminal_Temp_A2-df['T_AMB_1.2']).max()
        rangeStationaryA2 = df.Contactor_Terminal_Temp_A2.max() - df.Contactor_Terminal_Temp_A2.min()
        deltaStationary = df.Contactor_Terminal_Temp_A1.max() - df.Contactor_Terminal_Temp_A2.max()

        results = [i,meanContactResistance,stdContactResistance,maxContactResistance,minContactResistance,rangeContactResistance,
                maxTerm,minTerm,maxDeltaTerm,rangeTerm,maxStationaryA1,minStationaryA1,maxDeltaStationaryA1,rangeStationaryA1,maxStationaryA2,minStationaryA2,maxDeltaStationaryA2,rangeStationaryA2,deltaStationary]
        return results

    def contactor_current_share_cycle(self,i,df):
        ''' Contactor Current Share Cycle Evaluation
            Drop first 2 and last 2 current data points and evaluate CR (divide by two for current adjustment)
            Calculate Temperatures on stationaries and busbars
        '''
        self.logger.debug('~~~~ Start contactor_current_share_cycle Process ~~~~')
        df.columns = ['Date', 'Time', 'stepTime', 'TempCycleCount', 'CurrentCycleCount', 'CURRENT', 'T_AMB_1.2',
                      'Voltage_Drop', 'Busbar_Temp', 'Contactor_Terminal_Temp_A1',
                      'Contactor_Terminal_Temp_A2', 'ContactResistance']

        meanContactResistance = df.ContactResistance[2:-2].mean()/2
        stdContactResistance = df.ContactResistance[2:-2].std()
        maxContactResistance = df.ContactResistance[2:-2].max()/2
        minContactResistance = df.ContactResistance[2:-2].min()/2
        rangeContactResistance = maxContactResistance - minContactResistance

        maxTerm = df.Busbar_Temp.max()
        minTerm = df.Busbar_Temp.min()
        maxDeltaTerm = (df.Busbar_Temp-df['T_AMB_1.2']).max()
        rangeTerm = df.Busbar_Temp.max() - df.Busbar_Temp.min()
        maxStationaryA1 = df.Contactor_Terminal_Temp_A1.max()
        minStationaryA1 = df.Contactor_Terminal_Temp_A1.min()
        maxDeltaStationaryA1 = (df.Contactor_Terminal_Temp_A1-df['T_AMB_1.2']).max()
        rangeStationaryA1 = df.Contactor_Terminal_Temp_A1.max() - df.Contactor_Terminal_Temp_A1.min()
        maxStationaryA2 = df.Contactor_Terminal_Temp_A2.max()
        minStationaryA2 = df.Contactor_Terminal_Temp_A2.min()
        maxDeltaStationaryA2 = (df.Contactor_Terminal_Temp_A2-df['T_AMB_1.2']).max()
        rangeStationaryA2 = df.Contactor_Terminal_Temp_A2.max() - df.Contactor_Terminal_Temp_A2.min()
        deltaStationary = df.Contactor_Terminal_Temp_A1.max() - df.Contactor_Terminal_Temp_A2.max()

        results = [i, meanContactResistance, stdContactResistance, maxContactResistance, minContactResistance,
                   rangeContactResistance,
                   maxTerm, minTerm, maxDeltaTerm, rangeTerm, maxStationaryA1, minStationaryA1, maxDeltaStationaryA1,
                   rangeStationaryA1, maxStationaryA2, minStationaryA2, maxDeltaStationaryA2, rangeStationaryA2,
                   deltaStationary]
        return results

    def fuse_current_cycle(self, i, df):
        ''' Fuse Current Cycle Evaluation
            Drop first 2 and last 2 current data points and evaluate CR
            Calculate Temperatures on stationaries and busbars
        '''
        self.logger.debug('~~~~ Start fuse_current_cycle Process ~~~~')
        df.columns = ['Date', 'Time', 'stepTime', 'TempCycleCount', 'CurrentCycleCount', 'CURRENT', 'T_AMB_1.2',
                      'Voltage_Drop', 'Body_Temp','Resistance','i2rt']

        meanResistance = df.Resistance[2:-2].mean()
        stdResistance = df.Resistance[2:-2].std()
        maxResistance = df.Resistance[2:-2].max()
        minResistance = df.Resistance[2:-2].min()
        rangeResistance = maxResistance - minResistance
        i2rt = df.i2rt[2:-2].sum()


        maxBody = df.Body_Temp.max()
        rangeBody = df.Body_Temp.max() - df.Body_Temp.min()
        results = [i, meanResistance, stdResistance, maxResistance, minResistance,
                   rangeResistance,maxBody, rangeBody, i2rt]
        return results

    def fuse_current_share_cycle(self, i, df):
        ''' Fuse Current Share Cycle Evaluation
            Drop first 2 and last 2 current data points and evaluate CR (divide by two for current adjustment)
            Calculate Temperatures on stationaries and busbars
        '''
        self.logger.debug('~~~~ Start fuse_current_share_cycle Process ~~~~')
        df.columns = ['Date', 'Time', 'stepTime', 'TempCycleCount', 'CurrentCycleCount', 'CURRENT', 'T_AMB_1.2',
                      'Voltage_Drop', 'Body_Temp','Resistance','i2rt']

        meanResistance = df.Resistance[2:-2].mean()/2
        stdResistance = df.Resistance[2:-2].std()
        maxResistance = df.Resistance[2:-2].max()/2
        minResistance = df.Resistance[2:-2].min()/2
        rangeResistance = maxResistance - minResistance
        i2rt = df.i2rt[2:-2].sum()

        maxBody = df.Body_Temp.max()
        rangeBody = df.Body_Temp.max() - df.Body_Temp.min()
        results = [i, meanResistance, stdResistance, maxResistance, minResistance,
                   rangeResistance,maxBody, rangeBody, i2rt]
        return results

    def shunt_current_cycle(self, i, df):
        ''' Shunt Current Cycle Evaluation
            Drop first 2 and last 2 current data points and evaluate CR
            Calculate Temperatures on stationaries and busbars
        '''
        self.logger.debug('~~~~ Start shunt_current_cycle Process ~~~~')
        df.columns = ['Date', 'Time', 'stepTime', 'TempCycleCount', 'CurrentCycleCount', 'CURRENT', 'T_AMB_1.2',
                      'Resistance']

        meanResistance = df.Resistance[2:-2].mean()
        stdResistance = df.Resistance[2:-2].std()
        maxResistance = df.Resistance[2:-2].max()
        minResistance = df.Resistance[2:-2].min()
        rangeResistance = maxResistance - minResistance

        results = [i, meanResistance, stdResistance, maxResistance, minResistance, rangeResistance]
        return results

    def current_cycle_summary_calc(self,df):
        ''' Summary Calculations
            1) eSTDmCR - expanding window standard deviation of current cycle mean contact resistance (Exploratory Measurement)
            2) rSTDmCR - rolling window standard deviation of current cycle mean contact resistance (Exploratory Measurement)
            3) eSTDmCRs - Slope of the expanding window standard deviation of current cycle mean contact resistance (Exploratory Measurement)
            4) MADeSTDmCR - Mean Adjusted expanding window standard deviation of current cycle mean contact resistance (Exploratory Measurement)
            5) MADrSTDmCR - Mean Adjusted rolling window standard deviation of current cycle mean contact resistance (Exploratory Measurement)
            6) MADemCR - Mean Adjusted expanding rolling window mean of current cycle mean contact resistance (Exploratory Measurement)
            7) rMEANmCR - rolling window mean of current cycle mean contact resistance (Primary Measurement)
        '''
        self.logger.debug('~~~~ Start current_cycle_summary_calc Process ~~~~')
        try:
            df['eSTDmCR'] = df['meanCR'].expanding(min_periods=10).std()
            df['rSTDmCR'] = df['meanCR'].rolling(min_periods=10, window=50).std()
            df['eSTDmCRs'] = df['eSTDmCR'].diff(10)
            df['MADeSTDmCR'] = abs(df['eSTDmCR'] - df['eSTDmCR'].
                                                 expanding(min_periods=10).mean())
            df['MADrSTDmCR'] = abs(df['rSTDmCR'] - df['rSTDmCR'].
                                                 rolling(min_periods=10, window=50).mean())
            df['MADemCR'] = abs(df['meanCR'] - df['meanCR'].
                                              expanding(min_periods=10).mean())
            df['rMEANmCR'] = df['meanCR'].rolling(min_periods=10, window=50).mean()
        except:
            df['eSTDmR'] = df['meanR'].expanding(min_periods=10).std()
            df['rSTDmR'] = df['meanR'].rolling(min_periods=10, window=50).std()
            df['eSTDmRs'] = df['eSTDmR'].diff(10)
            df['MADeSTDmR'] = abs(df['eSTDmR'] - df['eSTDmR'].
                                                 expanding(min_periods=10).mean())
            df['MADrSTDmR'] = abs(df['rSTDmR'] - df['rSTDmR'].
                                                 rolling(min_periods=10, window=50).mean())
            df['MADemR'] = abs(df['meanR'] - df['meanR'].
                                              expanding(min_periods=10).mean())
            df['rSTDmR'] = df['meanR'].rolling(min_periods=10, window=50).std()
            df['rMEANmR'] = df['meanR'].rolling(min_periods=10, window=50).mean()

        return df

    def ComponentFailureSummary(self, CSdf, Pol):
        '''
        Take in summary data table and output the failures when the unit fails the 0.2mOhm Mean CR spec
        Get List of serials
        Loop through and look for condition per serial number
        add row information to DF
            Serial number
            Current Cycle it failed
            Temp Cycle it failed
            Count of range failures prior to failure
            Max temp in failure temp cycle
            Max temp in entire test
        '''
        if Pol == 'Pos':
            unitlst = ['PC','SC','DC','PF','SF']
        elif Pol == 'Neg':
            unitlst = ['PC', 'SC', 'DC'] # 'ST'
            fusetype=None

        SNlst = sorted(CSdf.SN.unique().tolist())
        Failure_Summary = pd.DataFrame(
            columns=['Serial','FuseType','Device','Type', 'PTC Batch','Status', 'Last_Current_Cycle', 'Failed_Current_Cycle', 'Wiebull_Current_Cycle',
                     'Weibull_Status','Weibull_Temp_Cycle', 'Temp_Cycle', 'Failed_Cycle_Temp', 'Max_Test_Temp', 'Range_OOS_BF',
                     'Range_OOS_Total', 'Final_Rolling_CR', 'Max_Rolling_CR'])
        fusedic = {1:'Eaton',2:'Mersen','1':'Eaton','2':'Mersen'}
        for Serial in SNlst:
            for unit in unitlst:
                self.logger.info('Starting Component Analysis: {}/{}'.format(unit, Serial))
                sndf = CSdf.loc[(CSdf.SN == Serial)]
                if unit in ['PC', 'SC', 'DC']:
                    R = 'CR'
                    T = 'maxTermTemp'
                    Type = 'Contactor'
                    fusetype = None
                    limit = 0.00024
                elif unit in ['PF','SF']:
                    R = 'R'
                    T = 'maxBodyTemp'
                    Type = 'Fuse'
                    fusetype =  fusedic[sndf['FuseType'].iat[0]]
                    limit = 0.0004
                elif unit in ['ST']:
                    R = 'R'
                    T = None
                    Type = 'Shunt'
                    fusetype = None
                    limit = 0.0004
                rMean = 'rMEANm{}_{}'.format(R,unit)
                range = 'range{}_{}'.format(R,unit)
                if unit in ['PC','SC','DC','PF','SF']:
                    temp =  '{}_{}'.format(T,unit)
                Failure_Row = sndf[sndf[rMean] > limit]
                if Failure_Row.empty:
                    Failure_Status = 'Pass'
                    Weibull_Status = 'S'
                    Last_Current_Cycle = sndf['CurrentCycle'].max()
                    Failed_Current_Cycle = None
                    Wiebull_Current_Cycle = Last_Current_Cycle
                    Failed_Temp_Cycle = sndf['TempCycle'].max()
                    Failed_Cycle_Max_Temp = None
                    Weibull_Temp_Cycle = Failed_Temp_Cycle
                    if unit in ['PC', 'SC', 'DC', 'PF', 'SF']:
                        Max_Temp = sndf[temp].max()
                    else:
                        Max_Temp=None
                    Count_BF_Range_OOS = None

                else:
                    Failure_Status = 'Fail'
                    Weibull_Status = 'F'
                    Last_Current_Cycle = sndf['CurrentCycle'].max()
                    Failed_Current_Cycle = Failure_Row['CurrentCycle'].iat[0]
                    Wiebull_Current_Cycle = Failed_Current_Cycle
                    Failed_Temp_Cycle = Failure_Row['TempCycle'].iat[0]
                    Failed_Cycle_Max_Temp = sndf.loc[sndf.TempCycle == Failed_Temp_Cycle][temp].max()
                    Weibull_Temp_Cycle = Failed_Temp_Cycle
                    if unit in ['PC', 'SC', 'DC', 'PF', 'SF']:
                        Max_Temp = sndf[temp].max()
                    else:
                        Max_Temp=None
                    Count_BF_Range_OOS = len(
                        sndf[(sndf['CurrentCycle'] < Failed_Current_Cycle) & (sndf[range] > 0.1)])


                Count_Total_Range_OOS = len(sndf[sndf[range] > 0.1])
                Final_Rolling_CR = sndf.loc[sndf[rMean].last_valid_index(), rMean]
                Max_Rolling_CR = sndf[rMean].max()
                row = [Serial, fusetype,unit,Type, self.round, Failure_Status, Last_Current_Cycle, Failed_Current_Cycle, Wiebull_Current_Cycle,
                       Weibull_Status, Weibull_Temp_Cycle, Failed_Temp_Cycle, Failed_Cycle_Max_Temp, Max_Temp, Count_BF_Range_OOS,
                       Count_Total_Range_OOS, Final_Rolling_CR, Max_Rolling_CR]
                Failure_Summary = Failure_Summary.append(pd.Series(row, index=Failure_Summary.columns), ignore_index=True)
        Failure_Summary['Polarity'] = Pol

        return Failure_Summary

    def AssemblyFailureSummary(self, CSdf):
        '''
        Take in Component failure summary
        Get List of serials
        Loop through and look for condition per serial number
            Serial
            Polarity
            Fuse type
            Status
            Weibull Temp Cycle
            Weibull Current Cycle
            Failure Cause
            PTC batch

        '''

        SNlst = sorted(CSdf.Serial.unique().tolist())
        Failure_Summary = pd.DataFrame(
            columns=['Serial','Polarity','FuseType','Status','Weibull_Status','Weibull_Current_Cycle'
                     ,'Weibull_Temp_Cycle','Failure_Cause','PTC_Batch'])
        for Serial in SNlst:
            self.logger.info('Starting Assembly Analysis: {}'.format(Serial))
            sndf = CSdf.loc[(CSdf.Serial == Serial)]
            Pol = sndf['Polarity'].iat[0]
            PTC_Batch = sndf['PTC Batch'].iat[0]
            Weibull_Current_Cycle = sndf['Wiebull_Current_Cycle'].min()
            Weibull_Temp_Cycle = sndf['Weibull_Temp_Cycle'].min()

            if Pol == 'Pos':
                fusetype = sndf[sndf['Type']=='Fuse']['FuseType'].iat[0]
            else:
                fusetype = None

            if sndf[sndf['Status']=='Fail']['Status'].size >= 1:
                Failure_Status = 'Fail'
                Weibull_Status = 'F'
                Failure_Cause = sndf[sndf['Status']=='Fail']['Device'].tolist()
            else:
                Failure_Status = 'Pass'
                Weibull_Status = 'S'
                Failure_Cause = None

            row = [Serial, Pol, fusetype,Failure_Status, Weibull_Status, Weibull_Current_Cycle,
                    Weibull_Temp_Cycle, Failure_Cause, PTC_Batch]
            Failure_Summary = Failure_Summary.append(pd.Series(row, index=Failure_Summary.columns), ignore_index=True)


        return Failure_Summary

    def LifeTempSummary(self, df,pol):
        ''' Life Temp Summary
            Calculate temp percentiles per unit for post test life calculations
        '''
        LifeTempSummary = pd.DataFrame(columns=['TempProfile', 'Signal', '95%', '90%', '75%', '50%'])
        for temp in ['0C', '60C']:
            sigdic = {'Pos':['maxTermTemp_PC', 'maxTermTemp_SC', 'maxTermTemp_DC',
                         'maxSA1Temp_PC', 'maxSA1Temp_SC', 'maxSA1Temp_DC',
                         'maxSA2Temp_PC', 'maxSA2Temp_SC', 'maxSA2Temp_DC',
                         'CS_MaxTempDelta_PCTerm', 'CS_MaxTempDelta_SCTerm', 'CS_MaxTempDelta_DCTerm',
                         'CS_MaxTempDelta_PCA1', 'CS_MaxTempDelta_SCA1', 'CS_MaxTempDelta_DCA1',
                         'CS_MaxTempDelta_PCA2', 'CS_MaxTempDelta_SCA2', 'CS_MaxTempDelta_DCA2',
                         'maxBodyTemp_PF', 'maxBodyTemp_SF'],
                      'Neg': ['maxTermTemp_PC', 'maxTermTemp_SC', 'maxTermTemp_DC',
                              'maxSA1Temp_PC', 'maxSA1Temp_SC', 'maxSA1Temp_DC',
                              'maxSA2Temp_PC', 'maxSA2Temp_SC', 'maxSA2Temp_DC',
                              'CS_MaxTempDelta_PCTerm', 'CS_MaxTempDelta_SCTerm', 'CS_MaxTempDelta_DCTerm',
                              'CS_MaxTempDelta_PCA1', 'CS_MaxTempDelta_SCA1', 'CS_MaxTempDelta_DCA1',
                              'CS_MaxTempDelta_PCA2', 'CS_MaxTempDelta_SCA2', 'CS_MaxTempDelta_DCA2']}

            for item in sigdic[pol]:
                tdf = df[(df['TempProfile'] == temp) & (df['TempCycle'] <= 30)]
                x = tdf[[item]].to_numpy()
                x = x[np.logical_not(np.isnan(x))]
                try: percentile95 = np.percentile(x, q=95)
                except: percentile95 = np.nan
                try: percentile90 = np.percentile(x, q=90)
                except: percentile90 = np.nan
                try: percentile75 = np.percentile(x, q=75)
                except: percentile75 = np.nan
                try: percentile50 = np.percentile(x, q=50)
                except: percentile50 = np.nan
                row = [temp, item, percentile95, percentile90, percentile75, percentile50]
                LifeTempSummary = LifeTempSummary.append(pd.Series(row, index=LifeTempSummary.columns),
                                                         ignore_index=True)

        LifeTempSummary['Round'] = self.round
        if pol == 'Pos':
            LifeTempSummary['Pol'] = 'Positive'
            LifeTempSummary.loc[LifeTempSummary['Signal'].str.contains('PF'), 'Unit'] = 'PF'
            LifeTempSummary.loc[LifeTempSummary['Signal'].str.contains('SF'), 'Unit'] = 'SF'
        else:
            LifeTempSummary['Pol'] = 'Negative'
        LifeTempSummary.loc[LifeTempSummary['Signal'].str.contains('PC'), 'Unit'] = 'PC'
        LifeTempSummary.loc[LifeTempSummary['Signal'].str.contains('SC'), 'Unit'] = 'SC'
        LifeTempSummary.loc[LifeTempSummary['Signal'].str.contains('DC'), 'Unit'] = 'DC'
        return LifeTempSummary

    ''' OLD FUNCTIONS '''

    def CurrentCycleData(self, df, pol, sn):
        '''
        Obsolete Do Not Use
        '''
        CurrentCycleType = None
        Tlst = ['dT_Input_BB', 'dT_Output_BB', 'dT_DCFC_BB', 'dT_Primary_Fuse_Body', 'dT_Seconary_Fuse_Body', 'dT_PvSBody',
                'dT_Primary_Terminal', 'dT_Secondary_Terminal', 'dT_PvSTerm']

        ''' DC Fast Charge Data '''

        df.loc[df['OUT_DCFC'] < 0, 'OUT_DCFC'] = 0
        df.loc[df['OUT_DCFC'] > .5, 'OUT_DCFC'] = 0
        df['IOVI2RT'] = df['INP_OUT'] * df['CURRENT'] * df['stepTime']
        df['DCFCI2RT'] = df['OUT_DCFC'] * df['CURRENT'] * df['stepTime']
        df['DCFCR'] = df['OUT_DCFC'] / df['CURRENT']
        df.loc[df['DCFCcr'] == 0, 'DCFCcr'] = None
        df.loc[df['DCFCI2RT'] == 0, 'DCFCI2RT'] = None

        df.loc[df['Input_BB'] < -60, 'Input_BB'] = np.nan
        df['dT_Input_BB'] = df['Input_BB'] - df['T_AMB_1.2']
        df['dT_Output_BB'] = df['Output_BB'] - df['T_AMB_1.2']
        df['dT_DCFC_BB'] = df['DCFC_BB'] - df['T_AMB_1.2']

        ''' Primary and Secondary Data'''
        df['PCcrSplit'] = df['PRIME'] * 2 / df['CURRENT']
        if pol == 'Pos':
            df['SCcrSplit'] = df['SEC'] * 2 / df['CURRENT']
            df['dT_Primary_Fuse_Body'] = df['Prim_Fuse_Body'] - df['T_AMB_1.2']
            df['dT_Seconary_Fuse_Body'] = df['Sec_Fuse_Body'] - df['T_AMB_1.2']
            df['dT_PvSBody'] = df['Prim_Fuse_Body'] - df['Sec_Fuse_Body']
            df['dT_Primary_Terminal'] = df['Prim_Cont_Term'] - df['T_AMB_1.2']
            df['dT_Secondary_Terminal'] = df['Sec_Cont_Term'] - df['T_AMB_1.2']
            df['dT_PvSTerm'] = df['Prim_Cont_Term'] - df['Sec_Cont_Term']
            df['vPrime'] = df['PRIME'] + df['F_PRIME']
            df['vSec'] = df['SEC'] + df['F_SEC']
            df['vT_PaS'] = (df['vPrime'] + df['vSec']) / 2
            df['vTI2RT'] = df['vT_PaS'] * df['CURRENT'] * df['stepTime']

        CurrentSummaryTable = pd.DataFrame(
            columns=['CurrentCycle', 'CycleTime', 'TempCycle','CurrentCycleType','CurrentShareCycle', 'maxIBB',
                     'maxIBBdT', 'sumIBBdT', 'maxOBB', 'maxOBBdT', 'sumOBBdT', 'maxDCBB', 'maxDCBBdT', 'sumDCBBdT',
                     'sumDCFCI2RT', 'meanCurrent', 'meanDCFCcr', 'maxRangeDCFCcr',
                     'meanDCFCv', 'maxPFBdT', 'maxSFBdT',
                     'maxPFTdT', 'maxSFTdT', 'sumPvSBdT', 'sumPvSTdT', 'meanVP', 'meanVS', 'meanVT', 'sumVTI2RT',
                     'cntDCFCoos','meanVPc','meanVSc','meanVPf','meanVSf','cntPRIMEoos','cntSECoos','maxRangePCcr','maxRangeSCcr'])
        CurrentShareCycle = 0
        for i in range(1, int(df.CurrentCycleCount.max())):
            tdf = df.loc[(df['CurrentCycleCount'] == i) & (df['CURRENT'] != 0)]

            tdf.reset_index(inplace=True, drop=True)
            TempCycle = tdf['TempCycleCount'].max()
            time = tdf.Time.max() - tdf.Time.min()
            '''Temp Measurements'''
            maxIBB = tdf.Input_BB.max()
            maxIBBdT = tdf.dT_Input_BB.max()
            sumIBBdT = tdf.dT_Input_BB.sum()
            maxOBB = tdf.Output_BB.max()
            maxOBBdT = tdf.dT_Output_BB.max()
            sumOBBdT = tdf.dT_Output_BB.sum()
            maxDCBB = tdf.DCFC_BB.max()
            maxDCBBdT = tdf.dT_DCFC_BB.max()
            sumDCBBdT = tdf.dT_DCFC_BB.sum()
            meanVPc = df['PRIME'].mean()

            if pol == 'Pos':
                maxPFBdT = df['dT_Primary_Fuse_Body'].max()
                maxSFBdT = df['dT_Seconary_Fuse_Body'].max()
                maxPFTdT = df['dT_Primary_Terminal'].max()
                maxSFTdT = df['dT_Secondary_Terminal'].max()
                sumPvSBdT = df['dT_PvSBody'].sum()
                sumPvSTdT = df['dT_PvSTerm'].sum()
                meanVSc = df['SEC'].mean()
                meanVPf = df['F_PRIME'].mean()
                meanVSf = df['F_SEC'].mean()
                meanVP = df['vPrime'].mean()
                meanVS = df['vSec'].mean()
                meanVT = df['vT_PaS'].max()
                sumVTI2RT = df['vTI2RT'].sum()
            elif pol == 'Neg':
                maxPFBdT = None
                maxSFBdT = None
                maxPFTdT = None
                maxSFTdT = None
                sumPvSBdT = None
                sumPvSTdT = None
                meanVSc = None
                meanVPf = None
                meanVSf = None
                meanVP = None
                meanVS = None
                meanVT = None
                sumVTI2RT = None
                meanSCcrSplit  = None

            '''DCFC Measurements'''
            mVdf = tdf.loc[(tdf['OUT_DCFC'] > 0)]
            meanDCFCv = mVdf.OUT_DCFC.mean()
            spec = 0.0002

            meanDCFCcr = tdf.DCFCcr[2:-2].mean()
            meanPCcrSplit  = tdf.PCcrSplit[2:-2].mean()
            cntDCFCoos = 1 if meanDCFCcr > spec else 0
            cntPRIMEoos = 1 if meanPCcrSplit  > spec else 0
            maxRangeDCFCcr = tdf.DCFCcr[2:-2].max() - tdf.DCFCcr[2:-2].min()
            maxRangePCcr = tdf.PCcrSplit[2:-2].max() - tdf.PCcrSplit[2:-2].min()
            if pol == 'Pos':
                meanSCcrSplit  = tdf.PCcrSplit[2:-2].mean()
                cntSECoos = 1 if meanSCcrSplit  > spec else 0
                maxRangeSCcr = tdf.SCcrSplit[2:-2].max() - tdf.SCcrSplit[2:-2].min()

            # if meanDCFCcr > spec:
            #     print('OOS')
            #     cntDCFCoos = 1
            # elif meanDCFCcr <= spec:
            #     print('OK')
            #     cntDCFCoos = 0
            # else:
            #     print('Not Comparable - Cycle: {} / {}'.format(i , df.CurrentCycleCount.max()))


            meanAdjCurrent = tdf.CURRENT[2:-2].mean()
            sumDCFCI2RT = tdf.DCFCI2RT[2:-2].sum()

            if meanAdjCurrent > 1250:
                CurrentCycleType = 'WOT_Discharge'
            elif (1250 > meanAdjCurrent > 700) & (time.total_seconds() < 30):
                CurrentCycleType = 'WOT_DCFC'
            elif (1250 > meanAdjCurrent > 700) & (time.total_seconds() > 60):
                CurrentCycleType = 'CC_Discharge'
            elif (700 > meanAdjCurrent > 300) & (time.total_seconds() > 60):
                CurrentCycleType = 'CC_DCFC'
            elif (700 > meanAdjCurrent > 300) & (30 < time.total_seconds() < 60):
                CurrentShareCycle += 1
                if (pol == 'Pos'):
                    if (meanSCcrSplit  > meanPCcrSplit ):
                        CurrentCycleType = 'CS_Prime'
                    elif (meanSCcrSplit  < meanPCcrSplit ):
                        CurrentCycleType = 'CS_Sec'
                elif (pol == 'Neg'):
                    CurrentCycleType = 'CS'
            else:
                print('cannot recognize profile  {} / {}'.format(i,df.CurrentCycleCount.max()))

            row = [i, time, TempCycle, CurrentCycleType, CurrentShareCycle,maxIBB, maxIBBdT, sumIBBdT, maxOBB, maxOBBdT,
                   sumOBBdT, maxDCBB, maxDCBBdT,sumDCBBdT, sumDCFCI2RT, meanAdjCurrent, meanDCFCcr, maxRangeDCFCcr,
                   meanDCFCv, maxPFBdT, maxSFBdT, maxPFTdT, maxSFTdT,sumPvSBdT, sumPvSTdT, meanVP, meanVS, meanVT,
                   sumVTI2RT, cntDCFCoos,
                   meanVPc,meanVSc,meanVPf,meanVSf,cntPRIMEoos,cntSECoos,maxRangePCcr,maxRangeSCcr]
            CurrentSummaryTable = CurrentSummaryTable.append(pd.Series(row, index=CurrentSummaryTable.columns),
                                                       ignore_index=True)

        cols = ['TempCycle', 'TempCycle', 'sumDCFCI2RT', 'meanCurrent', 'meanDCFCcr']

        i = 0
        for col in cols:
            i += 1
            self.logger.info('Clean up Current Summary Column {} / {}'.format(i, len(cols) + 1))
            CurrentSummaryTable[col] = pd.to_numeric(CurrentSummaryTable[col], errors='coerce')

        CurrentSummaryTable['accumPvSBdT'] = CurrentSummaryTable.sumPvSBdT.cumsum()
        CurrentSummaryTable['accumPvSTdT'] = CurrentSummaryTable.sumPvSTdT.cumsum()
        CurrentSummaryTable['accumVTI2RT'] = CurrentSummaryTable.sumVTI2RT.cumsum()

        CurrentSummaryTable['AccumIBBdT'] = CurrentSummaryTable.sumIBBdT.cumsum()
        CurrentSummaryTable['AccumOBBdT'] = CurrentSummaryTable.sumOBBdT.cumsum()
        CurrentSummaryTable['AccumDCBBdT'] = CurrentSummaryTable.sumDCBBdT.cumsum()
        # CurrentSummaryTable['AccumIOVI2RT'] = CurrentSummaryTable.sumIOVI2RT.cumsum()
        CurrentSummaryTable['AccumDCFCI2RT'] = CurrentSummaryTable.sumDCFCI2RT.cumsum()
        CurrentSummaryTable['AccumDCFCoos'] = CurrentSummaryTable.cntDCFCoos.cumsum()
        CurrentSummaryTable['AccumPRIMEoos'] = CurrentSummaryTable.cntPRIMEoos.cumsum()
        CurrentSummaryTable['AccumSECoos'] = CurrentSummaryTable.cntSECoos.cumsum()
        # CurrentSummaryTable = CurrentSummaryTable[CurrentSummaryTable['CycleTime'] >= np.timedelta64(15, 'm')]

        CurrentSummaryTable['eSTDmCR'] = CurrentSummaryTable['meanDCFCcr'].expanding(min_periods=10).std()
        CurrentSummaryTable['rSTDmCR'] = CurrentSummaryTable['meanDCFCcr'].rolling(min_periods=10, window=50).std()
        CurrentSummaryTable['eSTDmCRs'] = CurrentSummaryTable['eSTDmCR'].diff(10)
        CurrentSummaryTable['MADeSTDmCR'] = abs(CurrentSummaryTable['eSTDmCR'] - CurrentSummaryTable['eSTDmCR'].
                                             expanding(min_periods=10).mean())
        CurrentSummaryTable['MADrSTDmCR'] = abs(CurrentSummaryTable['rSTDmCR'] - CurrentSummaryTable['rSTDmCR'].
                                             rolling(min_periods=10, window=50).mean())
        CurrentSummaryTable['MADemCR'] = abs(CurrentSummaryTable['meanDCFCcr'] - CurrentSummaryTable['meanDCFCcr'].
                                          expanding(min_periods=10).mean())
        CurrentSummaryTable['rSTDmCR'] = CurrentSummaryTable['meanDCFCcr'].rolling(min_periods=10, window=50).std()
        CurrentSummaryTable['rMEANmCR'] = CurrentSummaryTable['meanDCFCcr'].rolling(min_periods=10, window=50).mean()

        return CurrentSummaryTable

    def CurrentCycleVoltageData(self, df, pol, sn):
        '''
        Obsolete Do Not Use
        '''

        ''' DC Fast Charge Data '''
        df['stepTime'] = df['stepTime'] #.dt.total_seconds()
        df['CURRENTSQ'] = df['CURRENT'].copy() ** 2
        df.loc[df['OUT_DCFC'] < 0, 'OUT_DCFC'] = 0
        df.loc[df['OUT_DCFC'] > .5, 'OUT_DCFC'] = 0
        df['DCFCI2RT'] = df['OUT_DCFC'] * df['CURRENT'] * df['stepTime']
        df['DCFCcr'] = df['OUT_DCFC'] / df['CURRENT']
        df.loc[df['DCFCcr'] == 0, 'DCFCcr'] = None
        df.loc[df['DCFCI2RT'] == 0, 'DCFCI2RT'] = None

        ''' Input Output Voltage '''
        df['IOVI2RT'] = df['INP_OUT'] * df['CURRENT'] * df['stepTime']

        ''' Primary and Secondary Data'''
        df['PCcrSplit'] = df['PRIME'] * 2 / df['CURRENT']
        df['PCcr'] = df['PRIME'] / df['CURRENT']

        if pol == 'Pos':
            df['SCcrSplit'] = df['SEC'] * 2 / df['CURRENT']
            df['SCcr'] = df['SEC'] / df['CURRENT']
            df['vPrime'] = df['PRIME'] + df['F_PRIME']
            df['vSec'] = df['SEC'] + df['F_SEC']
            df['vT_PaS'] = (df['vPrime'] + df['vSec']) / 2
            df['vTI2RT'] = df['vT_PaS'] * df['CURRENT'] * df['stepTime']
        if pol == 'Neg':
            df['vT_SaPC'] = df['SHUNT'] + df['PRIME']
            df['vTI2RT'] = df['vT_SaPC'] * df['CURRENT'] * df['stepTime']

        CurrentSummaryTable = pd.DataFrame(
            columns=['CurrentCycle', 'CycleTime', 'TempCycle','CurrentCycleType','CurrentShareCycle',
                     'sumDCFCI2RT', 'meanCurrent', 'meanDCFCcr', 'maxRangeDCFCcr',
                     'meanDCFCv', 'meanVP', 'meanVS', 'meanVT', 'sumVTI2RT',
                     'cntDCFCoos','meanVPc','meanVSc','meanVPf','meanVSf','cntPRIMEoos','cntSECoos','maxRangePCcr',
                     'maxRangeSCcr','meanPCcr','meanSCcr','meanPCcrSplit','meanSCcrSplit','ISQsum','rmsT',
                     'maxAmb', 'maxInput_BB', 'maxOutput_BB', 'maxDCFC_BB', 'maxPriFuseBody', 'maxSecFuseBody', 'maxPriTerm', 'maxSecTerm'])
        CurrentShareCycle = 0
        for i in range(1, df.CurrentCycleCount.max()):
            tdf = df.loc[(df['CurrentCycleCount'] == i) & (df['CURRENT'] != 0)]
            tdf.reset_index(inplace=True, drop=True)
            meanAdjCurrent = tdf.CURRENT[2:-2].mean()
            TempCycle = tdf['TempCycleCount'].max()
            time = tdf.Time.max() - tdf.Time.min()

            ''' Temp Max Value '''
            maxAmb = tdf['T_AMB_1.2'].max()
            maxInput_BB = tdf['Input_BB'].max()
            maxOutput_BB = tdf['Output_BB'].max()
            maxDCFC_BB = tdf['DCFC_BB'].max()

            if pol == 'Pos':
                maxPriFuseBody = tdf['Prim_Fuse_Body'].max()
                maxSecFuseBody = tdf['Sec_Fuse_Body'].max()
                maxPriTerm = tdf['Prim_Cont_Term'].max()
                maxSecTerm = tdf['Sec_Cont_Term'].max()
            elif pol == 'Neg':
                maxPriFuseBody = None
                maxSecFuseBody = None
                maxPriTerm = None
                maxSecTerm = None



            ''' Value Initialization '''
            spec = 0.0002
            CurrentCycleType = None
            CurrentShareCycle = 0
            sumDCFCI2RT = None
            meanDCFCcr = None
            maxRangeDCFCcr = None
            meanDCFCv = None
            meanVP = None
            meanVS = None
            meanVT = None
            sumVTI2RT = None
            cntDCFCoos = None
            meanVPc = None
            meanVSc = None
            meanVPf = None
            meanVSf = None
            cntPRIMEoos = None
            cntSECoos = None
            maxRangePCcr = None
            maxRangeSCcr = None
            meanPCcr = 0
            meanSCcr = 0
            meanPCcrSplit = None
            meanSCcrSplit = None
            rms = df.loc[(df['CurrentCycleCount'] == i)]
            ISQsum = rms['CURRENTSQ'].sum()
            rmsT = rms.Time.max() - rms.Time.min()

            ''' Current Cycle Evaluation'''
            if meanAdjCurrent > 1250:
                CurrentCycleType = 'WOT_Discharge'

            elif (1250 > meanAdjCurrent > 700) & (time.total_seconds() < 30):
                CurrentCycleType = 'WOT_DCFC'
            elif (1250 > meanAdjCurrent > 700) & (time.total_seconds() > 60):
                CurrentCycleType = 'CC_Discharge'
            elif (700 > meanAdjCurrent > 300) & (time.total_seconds() > 60):
                CurrentCycleType = 'CC_DCFC'
            elif (700 > meanAdjCurrent > 300) & (30 < time.total_seconds() < 60):
                CurrentShareCycle += 1
                CurrentCycleType = 'CS'
            else:
                print('cannot recognize profile  {} / {}'.format(i, df.CurrentCycleCount.max()))

            ''' Current Cycle Measurements '''
            if (CurrentCycleType == 'WOT_Discharge') or (CurrentCycleType == 'CC_Discharge'):
                meanVPc = tdf['PRIME'].mean()

                '''Prime'''
                meanPCcrSplit = tdf.PCcrSplit[2:-2].mean()
                cntPRIMEoos = 1 if meanPCcrSplit  > spec else 0
                maxRangePCcr = tdf.PCcrSplit[2:-2].max() - tdf.PCcrSplit[2:-2].min()
                if pol == 'Pos':
                    meanVSc = tdf['SEC'].mean()
                    meanVPf = tdf['F_PRIME'].mean()
                    meanVSf = tdf['F_SEC'].mean()
                    meanVP = tdf['vPrime'].mean()
                    meanVS = tdf['vSec'].mean()
                    meanVT = tdf['vT_PaS'].max()
                    sumVTI2RT = tdf['vTI2RT'].sum()
                    '''Secondary'''
                    meanSCcrSplit = tdf.SCcrSplit[2:-2].mean()
                    cntSECoos = 1 if meanSCcrSplit > spec else 0
                    maxRangeSCcr = tdf.SCcrSplit[2:-2].max() - tdf.SCcrSplit[2:-2].min()

            elif (CurrentCycleType == 'WOT_DCFC') or (CurrentCycleType == 'CC_DCFC'):
                meanDCFCv = tdf.OUT_DCFC.mean()
                meanDCFCcr = tdf.DCFCcr[2:-2].mean()
                cntDCFCoos = 1 if meanDCFCcr > spec else 0
                maxRangeDCFCcr = tdf.DCFCcr[2:-2].max() - tdf.DCFCcr[2:-2].min()
                sumDCFCI2RT = tdf.DCFCI2RT[2:-2].sum()

                meanVPc = tdf['PRIME'].mean()
                '''Prime'''
                meanPCcrSplit = tdf.PCcrSplit[2:-2].mean()
                cntPRIMEoos = 1 if meanPCcrSplit  > spec else 0
                maxRangePCcr = tdf.PCcrSplit[2:-2].max() - tdf.PCcrSplit[2:-2].min()
                if pol == 'Pos':
                    meanVSc = tdf['SEC'].mean()
                    meanVPf = tdf['F_PRIME'].mean()
                    meanVSf = tdf['F_SEC'].mean()
                    meanVP = tdf['vPrime'].mean()
                    meanVS = tdf['vSec'].mean()
                    meanVT = tdf['vT_PaS'].max()
                    sumVTI2RT = tdf['vTI2RT'].sum()
                    '''Secondary'''
                    meanSCcrSplit = tdf.PCcrSplit[2:-2].mean()
                    cntSECoos = 1 if meanSCcrSplit > spec else 0
                    maxRangeSCcr = tdf.SCcrSplit[2:-2].max() - tdf.SCcrSplit[2:-2].min()

            elif CurrentCycleType == 'CS':
                if pol == 'Pos':
                    meanVPc = tdf['PRIME'].mean()
                    meanVSc = tdf['SEC'].mean()
                    if meanVPc < meanVSc:
                        meanPCcr = tdf.PCcr[2:-2].mean()
                        CurrentCycleType = 'CS_Primary'
                    elif meanVPc > meanVSc:
                        meanSCcr = tdf.SCcr[2:-2].mean()
                        CurrentCycleType = 'CS_Secondary'
                elif pol == ' Neg':
                    meanVPc = tdf['PRIME'].mean()
                    meanPCcr = tdf.PCcr[2:-2].mean()
                    CurrentCycleType = 'CS_Unknown'

            row = [i, time, TempCycle, CurrentCycleType, CurrentShareCycle, sumDCFCI2RT, meanAdjCurrent, meanDCFCcr,
                   maxRangeDCFCcr,meanDCFCv, meanVP, meanVS, meanVT, sumVTI2RT, cntDCFCoos,
                   meanVPc,meanVSc,meanVPf,meanVSf,cntPRIMEoos,cntSECoos,maxRangePCcr,maxRangeSCcr,meanPCcr,meanSCcr,meanPCcrSplit,meanSCcrSplit,ISQsum,rmsT,
                   maxAmb, maxInput_BB, maxOutput_BB, maxDCFC_BB, maxPriFuseBody, maxSecFuseBody, maxPriTerm, maxSecTerm]

            CurrentSummaryTable = CurrentSummaryTable.append(pd.Series(row, index=CurrentSummaryTable.columns),
                                                       ignore_index=True)

        cols = ['TempCycle', 'TempCycle', 'sumDCFCI2RT', 'meanCurrent', 'meanDCFCcr']

        i = 0
        for col in cols:
            i += 1
            self.logger.info('Clean up Current Summary Column {} / {}'.format(i, len(cols) + 1))
            CurrentSummaryTable[col] = pd.to_numeric(CurrentSummaryTable[col], errors='coerce')

        CurrentSummaryTable['accumVTI2RT'] = CurrentSummaryTable.sumVTI2RT.cumsum()
        # CurrentSummaryTable['AccumIOVI2RT'] = CurrentSummaryTable.sumIOVI2RT.cumsum()
        CurrentSummaryTable['AccumDCFCI2RT'] = CurrentSummaryTable.sumDCFCI2RT.cumsum()
        CurrentSummaryTable['AccumDCFCoos'] = CurrentSummaryTable.cntDCFCoos.cumsum()
        CurrentSummaryTable['AccumPRIMEoos'] = CurrentSummaryTable.cntPRIMEoos.cumsum()
        CurrentSummaryTable['AccumSECoos'] = CurrentSummaryTable.cntSECoos.cumsum()
        # CurrentSummaryTable = CurrentSummaryTable[CurrentSummaryTable['CycleTime'] >= np.timedelta64(15, 'm')]

        # CurrentSummaryTable['eSTDmCR'] = CurrentSummaryTable['meanDCFCcr'].expanding(min_periods=10).std()
        # CurrentSummaryTable['rSTDmCR'] = CurrentSummaryTable['meanDCFCcr'].rolling(min_periods=10, window=50).std()
        # CurrentSummaryTable['eSTDmCRs'] = CurrentSummaryTable['eSTDmCR'].diff(10)
        # CurrentSummaryTable['MADeSTDmCR'] = abs(CurrentSummaryTable['eSTDmCR'] - CurrentSummaryTable['eSTDmCR'].
        #                                      expanding(min_periods=10).mean())
        # CurrentSummaryTable['MADrSTDmCR'] = abs(CurrentSummaryTable['rSTDmCR'] - CurrentSummaryTable['rSTDmCR'].
        #                                      rolling(min_periods=10, window=50).mean())
        # CurrentSummaryTable['MADemCR'] = abs(CurrentSummaryTable['meanDCFCcr'] - CurrentSummaryTable['meanDCFCcr'].
        #                                   expanding(min_periods=10).mean())
        # CurrentSummaryTable['rSTDmCR'] = CurrentSummaryTable['meanDCFCcr'].rolling(min_periods=10, window=50).std()
        # CurrentSummaryTable['rMEANmCR'] = CurrentSummaryTable['meanDCFCcr'].rolling(min_periods=10, window=50).mean()

        CurrentSummaryTable['CCIRMS'] = CurrentSummaryTable.ISQsum / CurrentSummaryTable.rmsT.dt.total_seconds()
        CurrentSummaryTable['CCIRMS'] = CurrentSummaryTable['CCIRMS'].pow(1. / 2)

        return CurrentSummaryTable

    def TempCycleData(self,df,pol,sn):
        '''
        Obsolete Do Not Use
        '''
        ''' DC Fast Charge Data '''
        df['stepTime'] = df['stepTime'].dt.total_seconds()
        df.loc[df['OUT_DCFC'] < 0, 'OUT_DCFC'] = 0
        df.loc[df['OUT_DCFC'] > .5, 'OUT_DCFC'] = 0
        df['IOVI2RT'] = df['INP_OUT'] * df['CURRENT'] * df['stepTime']
        df['DCFCI2RT'] = df['OUT_DCFC'] * df['CURRENT'] * df['stepTime']
        df['DCFCcr'] = df['OUT_DCFC'] / df['CURRENT']
        df.loc[df['DCFCcr'] == 0, 'DCFCcr'] = None
        df.loc[df['DCFCI2RT'] == 0, 'DCFCI2RT'] = None

        df.loc[df['Input_BB'] < -60, 'Input_BB'] = np.nan
        df['dT_Input_BB'] = df['Input_BB'] - df['T_AMB_1.2']
        df['dT_Output_BB'] = df['Output_BB'] - df['T_AMB_1.2']
        df['dT_DCFC_BB'] = df['DCFC_BB'] - df['T_AMB_1.2']

        ''' Primary and Secondary Data'''
        if pol == 'Pos':
            df['dT_Primary_Fuse_Body'] = df['Prim_Fuse_Body'] - df['T_AMB_1.2']
            df['dT_Seconary_Fuse_Body'] = df['Sec_Fuse_Body'] - df['T_AMB_1.2']
            df['dT_PvSBody'] = df['Prim_Fuse_Body'] - df['Sec_Fuse_Body']
            df['dT_Primary_Terminal'] = df['Prim_Cont_Term'] - df['T_AMB_1.2']
            df['dT_Secondary_Terminal'] = df['Sec_Cont_Term'] - df['T_AMB_1.2']
            df['dT_PvSTerm'] = df['Prim_Cont_Term'] - df['Sec_Cont_Term']
            df['vPrime'] = df['PRIME'] + df['F_PRIME']
            df['vSec'] = df['SEC'] + df['F_SEC']
            df['vT_PaS'] = (df['vPrime'] + df['vSec'])/2
            df['vTI2RT'] = df['vT_PaS'] * df['CURRENT'] * df['stepTime']

        Tlst = ['dT_Input_BB','dT_Output_BB','dT_DCFC_BB','dT_Primary_Fuse_Body','dT_Seconary_Fuse_Body','dT_PvSBody','dT_Primary_Terminal','dT_Secondary_Terminal','dT_PvSTerm']
        if sn == '18' and pol == 'Pos':
            print('-------Fixing Sensor Data-------')
            for e in Tlst:
                df.loc[df['TempCycleCount'] == 218, e] = 0
        elif sn == '11' and pol == 'Pos':
            print('-------Fixing Sensor Data-------')
            for e in Tlst:
                df.loc[df['TempCycleCount'] == 116, e] = 0

        TempSummaryTable = pd.DataFrame(
            columns=['TempCycle', 'CycleTime', 'maxCurrentCycle', 'minCurrentCycle', 'rangeCurrentCycle','maxIBB',
                     'maxIBBdT','sumIBBdT','maxOBB','maxOBBdT','sumOBBdT','maxDCBB','maxDCBBdT','sumDCBBdT'
                     ,'sumIOVI2RT','sumDCFCI2RT','meanCurrent','meanDCFCcr','maxRangeDCFCcr','stdRangeDCFCcr','meanDCFCv','maxPFBdT','maxSFBdT',
                     'maxPFTdT','maxSFTdT','sumPvSBdT','sumPvSTdT','meanVP','meanVS','meanVT','sumVTI2RT','sumDCFCoos','IsumDCFC'])


        for i in range(1, df.TempCycleCount.max()):
            tdf = df.loc[df['TempCycleCount'] == i]
            tdf.reset_index(inplace=True, drop=True)
            time = tdf.Time.max()-tdf.Time.min()
            maxIBB = tdf.Input_BB.max()
            maxIBBdT = tdf.dT_Input_BB.max()
            sumIBBdT = tdf.dT_Input_BB.sum()
            maxOBB = tdf.Output_BB.max()
            maxOBBdT = tdf.dT_Output_BB.max()
            sumOBBdT = tdf.dT_Output_BB.sum()
            maxDCBB = tdf.DCFC_BB.max()
            maxDCBBdT = tdf.dT_DCFC_BB.max()
            sumDCBBdT = tdf.dT_DCFC_BB.sum()

            sumIOVI2RT = tdf.IOVI2RT.sum()
            # sumDCFCI2RT = tdf.DCFCI2RT.sum()

            maxCurrentCycle = tdf['CurrentCycleCount'].max()
            minCurrentCycle = tdf['CurrentCycleCount'].min()
            rangeCurrentCycle = maxCurrentCycle-minCurrentCycle

            mVdf = tdf.loc[(tdf['OUT_DCFC'] > 0)]
            meanDCFCv = mVdf.OUT_DCFC.mean()
            MDCFCcr = []
            OOSDCFCcr = []
            RDCFCcr = []
            AdjCurrent = []
            I2RT = []
            ISumDCFC = []
            ISumParallel = []
            spec = 0.0002

            if pol == 'Pos':
                maxPFBdT = df['dT_Primary_Fuse_Body'].max()
                maxSFBdT = df['dT_Seconary_Fuse_Body'].max()
                maxPFTdT = df['dT_Primary_Terminal'].max()
                maxSFTdT = df['dT_Secondary_Terminal'].max()
                sumPvSBdT = df['dT_PvSBody'].sum()
                sumPvSTdT = df['dT_PvSTerm'].sum()
                meanVP = df['vPrime'].max()
                meanVS = df['vSec'].max()
                meanVT = df['vT_PaS'].max()
                sumVTI2RT = df['vTI2RT'].sum()

            if pol == 'Neg':
                maxPFBdT = None
                maxSFBdT = None
                maxPFTdT = None
                maxSFTdT = None
                sumPvSBdT = None
                sumPvSTdT = None
                meanVP = None
                meanVS = None
                meanVT = None
                sumVTI2RT = None

            if (minCurrentCycle > 0.0) and (rangeCurrentCycle > 10.0):
                for j in range(int(minCurrentCycle), int(maxCurrentCycle)):
                    cdf = df.loc[(df['CurrentCycleCount'] == j) & (df['CURRENT'] != 0)]
                    cdf.reset_index(inplace=True, drop=True)
                    # crdf = cdf.loc[(cdf['DCFCcr'] > 0) & (cdf['DCFCcr'] < 10000)]
                    mDCFCcr = cdf.DCFCcr[2:-2].mean()
                    cdf['CURRENTSQ'] = cdf['CURRENT']**2
                    isumDCFC = cdf.CURRENTSQ[2:-2].sum()
                    if mDCFCcr > spec:
                        OOSDCFCcr.append(1)
                    elif mDCFCcr < spec:
                        OOSDCFCcr.append(0)
                    rDCFCcr = cdf.DCFCcr[2:-2].max() - cdf.DCFCcr[2:-2].min()
                    mAC = cdf.CURRENT[2:-2].mean()
                    mI2RT = cdf.DCFCI2RT[2:-2].sum()
                    # print(cdf.DCFCI2RT[1:-1])
                    RDCFCcr.append(rDCFCcr)
                    MDCFCcr.append(mDCFCcr)
                    AdjCurrent.append(mAC)
                    I2RT.append(mI2RT)
                    ISumDCFC.append(isumDCFC)

            try:

                MDCFCcr = [v for v in MDCFCcr if not math.isnan(v) and not math.isinf(v) ]
                AdjCurrent = [v for v in AdjCurrent if not math.isnan(v) and not math.isinf(v)]
                I2RT = [v for v in I2RT if not math.isnan(v) and not math.isinf(v)]
                meanAdjCurrent = statistics.mean(AdjCurrent)
                meanDCFCcr = statistics.mean(MDCFCcr)
                maxRangeDCFCcr = max(RDCFCcr)
                stdRangeDCFCcr = statistics.stdev(RDCFCcr)
                sumDCFCI2RT = sum(I2RT)
                sumDCFCoos = sum(OOSDCFCcr)
                IsumDCFC = sum(ISumDCFC)
            except:
                meanAdjCurrent = None
                meanDCFCcr = None
                sumDCFCI2RT = None
                sumDCFCoos = 0
                maxRangeDCFCcr = None
                stdRangeDCFCcr = None
                IsumDCFC = 0




            row = [i, time, maxCurrentCycle, minCurrentCycle,
                   rangeCurrentCycle,maxIBB,maxIBBdT,sumIBBdT,maxOBB,maxOBBdT,sumOBBdT,maxDCBB,maxDCBBdT,sumDCBBdT,
                   sumIOVI2RT,sumDCFCI2RT, meanAdjCurrent,meanDCFCcr,maxRangeDCFCcr,stdRangeDCFCcr, meanDCFCv, maxPFBdT, maxSFBdT, maxPFTdT, maxSFTdT,
                   sumPvSBdT, sumPvSTdT, meanVP, meanVS, meanVT, sumVTI2RT, sumDCFCoos,IsumDCFC]
            TempSummaryTable = TempSummaryTable.append(pd.Series(row, index=TempSummaryTable.columns),
                                                       ignore_index=True)

        cols = ['TempCycle','maxCurrentCycle','minCurrentCycle','rangeCurrentCycle','sumDCFCI2RT','meanCurrent','meanDCFCcr']

        i = 0
        for col in cols:
            i += 1
            self.logger.info('Clean up Temp Summary Column {} / {}'.format(i, len(cols) + 1))
            TempSummaryTable[col] = pd.to_numeric(TempSummaryTable[col], errors='coerce')

        TempSummaryTable['accumPvSBdT'] = TempSummaryTable.sumPvSBdT.cumsum()
        TempSummaryTable['accumPvSTdT'] = TempSummaryTable.sumPvSTdT.cumsum()
        TempSummaryTable['accumVTI2RT'] = TempSummaryTable.sumVTI2RT.cumsum()

        TempSummaryTable['AccumIBBdT'] = TempSummaryTable.sumIBBdT.cumsum()
        TempSummaryTable['AccumOBBdT'] = TempSummaryTable.sumOBBdT.cumsum()
        TempSummaryTable['AccumDCBBdT'] = TempSummaryTable.sumDCBBdT.cumsum()
        TempSummaryTable['AccumCurrentCount'] = TempSummaryTable.rangeCurrentCycle.cumsum()
        # TempSummaryTable['AccumIOVI2RT'] = TempSummaryTable.sumIOVI2RT.cumsum()
        TempSummaryTable['AccumDCFCI2RT'] = TempSummaryTable.sumDCFCI2RT.cumsum()
        TempSummaryTable['AccumDCFCoos'] = TempSummaryTable.sumDCFCoos.cumsum()
        TempSummaryTable = TempSummaryTable[TempSummaryTable['CycleTime'] >= np.timedelta64(15, 'm')]

        TempSummaryTable['eSTDmCR'] = TempSummaryTable['meanDCFCcr'].expanding(min_periods=10).std()
        TempSummaryTable['rSTDmCR'] = TempSummaryTable['meanDCFCcr'].rolling(min_periods=10, window=50).std()
        TempSummaryTable['eSTDmCRs'] = TempSummaryTable['eSTDmCR'].diff(10)
        TempSummaryTable['MADeSTDmCR'] = abs(TempSummaryTable['eSTDmCR']-TempSummaryTable['eSTDmCR'].
                                             expanding(min_periods=10).mean())
        TempSummaryTable['MADrSTDmCR'] = abs(TempSummaryTable['rSTDmCR'] - TempSummaryTable['rSTDmCR'].
                                             rolling(min_periods=10,window=50).mean())
        TempSummaryTable['MADemCR'] = abs(TempSummaryTable['meanDCFCcr'] - TempSummaryTable['meanDCFCcr'].
                                          expanding(min_periods=10).mean())
        TempSummaryTable['rSTDmCR'] = TempSummaryTable['meanDCFCcr'].rolling(min_periods=10, window=50).std()
        TempSummaryTable['rMEANmCR'] = TempSummaryTable['meanDCFCcr'].rolling(min_periods=10, window=50).mean()


        print('cycle time (): {}'.format(TempSummaryTable.CycleTime.dt.total_seconds()))
        print('IsumDCFC {}'.format(TempSummaryTable.IsumDCFC))
        TempSummaryTable['IRMSDCFCtemp'] = TempSummaryTable.IsumDCFC/ TempSummaryTable.CycleTime.dt.total_seconds()
        TempSummaryTable['IRMSDCFCtemp'] = TempSummaryTable['IRMSDCFCtemp'].pow(1./2)

        return df, TempSummaryTable

    def TempCycleVoltageData(self,df,ccdf,pol,sn):
        '''
        Obsolete Do Not Use
        '''
        ''' DC Fast Charge Data '''
        df['stepTime'] = df['stepTime'].dt.total_seconds()
        df['CURRENTSQ'] = df['CURRENT'].copy() ** 2
        df.loc[df['OUT_DCFC'] < 0, 'OUT_DCFC'] = 0
        df.loc[df['OUT_DCFC'] > .5, 'OUT_DCFC'] = 0
        # df['IOVI2RT'] = df['INP_OUT'] * df['CURRENT'] * df['stepTime']
        # df['DCFCI2RT'] = df['OUT_DCFC'] * df['CURRENT'] * df['stepTime']
        # df['DCFCcr'] = df['OUT_DCFC'] / df['CURRENT']
        # df.loc[df['DCFCcr'] == 0, 'DCFCcr'] = None
        # df.loc[df['DCFCI2RT'] == 0, 'DCFCI2RT'] = None
        #
        # ''' Primary and Secondary Data'''
        # if pol == 'Pos':
        #     df['vPrime'] = df['PRIME'] + df['F_PRIME']
        #     df['vSec'] = df['SEC'] + df['F_SEC']
        #     df['vT_PaS'] = (df['vPrime'] + df['vSec'])/2
        #     df['vTI2RT'] = df['vT_PaS'] * df['CURRENT'] * df['stepTime']

        TempSummaryTable = pd.DataFrame(
            columns=['TempCycle', 'CycleTime', 'maxCurrentCycle', 'minCurrentCycle', 'rangeCurrentCycle',
                     'maxAmb', 'maxInput_BB', 'maxOutput_BB', 'maxDCFC_BB', 'maxPriFuseBody', 'maxSecFuseBody', 'maxPriTerm','maxSecTerm',
                     'meanDCFCv', 'meanDCFCcr', 'RangeDCFCcr', 'stdDCFCcr', 'sumDCFCI2RT',
                     'meanVP', 'meanVPc', 'meanPCcr','meanPCcrSplit', 'RangePCcr', 'stdPCcr', 'meanVPf',
                     'meanVS', 'meanVSc', 'meanSCcr','meanSCcrSplit', 'RangeSCcr', 'stdSCcr', 'meanVSf',
                     'meanVT', 'sumVTI2RT',
                     'sumDCFCoos','sumPRIMEoos','sumSECoos',
                     'TC_I','TC_T',
                     'PC_I','PC_T',
                     'DCC_I','DCC_T',
                     'PSC_I','PSC_T'])


        for i in range(1, df.TempCycleCount.max()):
            '''Local Variables'''
            spec = 0.0002

            '''Full Data Set'''
            tdf = df.loc[df['TempCycleCount'] == i]
            tdf.reset_index(inplace=True, drop=True)

            time = tdf.Time.max() - tdf.Time.min()
            maxCurrentCycle = tdf['CurrentCycleCount'].max()
            minCurrentCycle = tdf['CurrentCycleCount'].min()
            rangeCurrentCycle = maxCurrentCycle - minCurrentCycle

            ''' Temp Max Value '''
            maxAmb = tdf['T_AMB_1.2'].max()
            maxInput_BB = tdf['Input_BB'].max()
            maxOutput_BB = tdf['Output_BB'].max()
            maxDCFC_BB = tdf['DCFC_BB'].max()

            if pol == 'Pos':
                maxPriFuseBody = tdf['Prim_Fuse_Body'].max()
                maxSecFuseBody = tdf['Sec_Fuse_Body'].max()
                maxPriTerm = tdf['Prim_Cont_Term'].max()
                maxSecTerm = tdf['Sec_Cont_Term'].max()
            elif pol == 'Neg':
                maxPriFuseBody = None
                maxSecFuseBody = None
                maxPriTerm = None
                maxSecTerm = None


            '''Current Cycle Summary Data Set'''
            cctdf = ccdf.loc[ccdf['TempCycle'] == i]
            cctdf.reset_index(inplace=True, drop=True)

            '''DCFC'''
            sumDCFCI2RT = cctdf.sumDCFCI2RT.sum()
            meanDCFCv = cctdf.meanDCFCv.mean()
            meanDCFCcr = cctdf.meanDCFCcr.mean()
            RangeDCFCcr = cctdf.meanDCFCcr.max() - cctdf.meanDCFCcr.min()
            stdDCFCcr = cctdf.meanDCFCcr.std()

            '''Primary Leg'''
            meanVP = cctdf.meanVP.mean()
            meanVPc = cctdf.meanVPc.mean()
            meanPCcr = cctdf.meanPCcr.mean()
            meanPCcrSplit = cctdf.meanPCcrSplit.mean()
            RangePCcr = cctdf.meanPCcr.max() - cctdf.meanPCcr.min()
            stdPCcr = cctdf.meanPCcr.std()
            meanVPf = cctdf.meanVPf.mean()

            '''Secondary Leg '''
            meanVS = cctdf.meanVS.mean() if pol == 'Pos' else None
            meanVSc = cctdf.meanVSc.mean() if pol == 'Pos' else None
            meanSCcr = cctdf.meanSCcr.mean() if pol == 'Pos' else None
            meanSCcrSplit = cctdf.meanSCcrSplit.mean() if pol == 'Pos' else None
            RangeSCcr = cctdf.meanSCcr.max() - cctdf.meanSCcr.min() if pol == 'Pos' else None
            stdSCcr = cctdf.meanSCcr.std() if pol == 'Pos' else None
            meanVSf = cctdf.meanVSf.mean() if pol == 'Pos' else None

            '''Combined Circuit'''
            meanVT = cctdf.meanVT.mean() if pol == 'Pos' else None
            sumVTI2RT = cctdf.sumVTI2RT.sum() if pol == 'Pos' else None

            '''OOS Measurements'''
            sumDCFCoos = cctdf.cntDCFCoos.sum()
            sumPRIMEoos = cctdf.cntPRIMEoos.sum()
            sumSECoos = cctdf.cntSECoos.sum()

            ''' Thermal Cycle RMS '''
            TC_I = tdf.CURRENTSQ.sum()
            TC_T = time

            ''' Power Cycle RMS'''
            pcdf = tdf[tdf['PowerCycleIndicator'] >= 1]
            # print(pdf)
            PC_I = pcdf.CURRENTSQ.sum()
            PC_T = pcdf.Time.max() - pcdf.Time.min()

            ''' DCFC RMS'''
            dcfcdf = tdf[(tdf['PowerCycleIndicator'] == 1) & (tdf['OUT_DCFC'] > 0)]
            DCC_I = dcfcdf.CURRENTSQ.sum()
            DCC_T = dcfcdf.Time.max() - dcfcdf.Time.min()

            ''' PS RMS'''
            PSC_I = pcdf.CURRENTSQ.sum()/4
            PSC_T = pcdf.Time.max() - dcfcdf.Time.min()

            row = [i, time, maxCurrentCycle, minCurrentCycle, rangeCurrentCycle,
                   maxAmb, maxInput_BB, maxOutput_BB, maxDCFC_BB, maxPriFuseBody, maxSecFuseBody, maxPriTerm, maxSecTerm,
                   meanDCFCv, meanDCFCcr, RangeDCFCcr, stdDCFCcr, sumDCFCI2RT,
                   meanVP, meanVPc, meanPCcr,meanPCcrSplit, RangePCcr, stdPCcr, meanVPf,
                   meanVS, meanVSc, meanSCcr,meanSCcrSplit, RangeSCcr, stdSCcr, meanVSf,
                   meanVT, sumVTI2RT,
                   sumDCFCoos,sumPRIMEoos,sumSECoos,
                   TC_I,TC_T,
                   PC_I,PC_T,
                   DCC_I,DCC_T,
                   PSC_I,PSC_T]
            TempSummaryTable = TempSummaryTable.append(pd.Series(row, index=TempSummaryTable.columns),
                                                       ignore_index=True)

        # cols = ['TempCycle','maxCurrentCycle','minCurrentCycle','rangeCurrentCycle','sumDCFCI2RT','meanDCFCcr']
        #
        # i = 0
        # for col in cols:
        #     i += 1
        #     self.logger.info('Clean up Temp Summary Column {} / {}'.format(i, len(cols) + 1))
        #     TempSummaryTable[col] = pd.to_numeric(TempSummaryTable[col], errors='coerce')

        TempSummaryTable['AccumCurrentCount'] = TempSummaryTable.rangeCurrentCycle.cumsum()
        # TempSummaryTable['AccumIOVI2RT'] = TempSummaryTable.sumIOVI2RT.cumsum()
        TempSummaryTable['AccumDCFCI2RT'] = TempSummaryTable.sumDCFCI2RT.cumsum()
        TempSummaryTable['AccumDCFCoos'] = TempSummaryTable.sumDCFCoos.cumsum()
        TempSummaryTable['AccumPRIMEoos'] = TempSummaryTable.sumPRIMEoos.cumsum()
        TempSummaryTable['AccumSECoos'] = TempSummaryTable.sumSECoos.cumsum()

        TempSummaryTable = TempSummaryTable[TempSummaryTable['CycleTime'] >= np.timedelta64(15, 'm')]

        # TempSummaryTable['eSTDmCR'] = TempSummaryTable['meanDCFCcr'].expanding(min_periods=10).std()
        # TempSummaryTable['rSTDmCR'] = TempSummaryTable['meanDCFCcr'].rolling(min_periods=10, window=50).std()
        # TempSummaryTable['eSTDmCRs'] = TempSummaryTable['eSTDmCR'].diff(10)
        # TempSummaryTable['MADeSTDmCR'] = abs(TempSummaryTable['eSTDmCR']-TempSummaryTable['eSTDmCR'].
        #                                      expanding(min_periods=10).mean())
        # TempSummaryTable['MADrSTDmCR'] = abs(TempSummaryTable['rSTDmCR'] - TempSummaryTable['rSTDmCR'].
        #                                      rolling(min_periods=10,window=50).mean())
        # TempSummaryTable['MADemCR'] = abs(TempSummaryTable['meanDCFCcr'] - TempSummaryTable['meanDCFCcr'].
        #                                   expanding(min_periods=10).mean())
        # TempSummaryTable['rSTDmCR'] = TempSummaryTable['meanDCFCcr'].rolling(min_periods=10, window=50).std()
        TempSummaryTable['DCFCrMEANmCR'] = TempSummaryTable['meanDCFCcr'].rolling(min_periods=10, window=50).mean()
        TempSummaryTable['PRIMErMEANmCR'] = TempSummaryTable['meanPCcr'].rolling(min_periods=1, window=4).mean()
        TempSummaryTable['SECrMEANmCR'] = TempSummaryTable['meanSCcr'].rolling(min_periods=1, window=4).mean()
        TempSummaryTable['SplitPRIMErMEANmCR'] = TempSummaryTable['meanPCcrSplit'].rolling(min_periods=10, window=50).mean()
        TempSummaryTable['SplitSECrMEANmCR'] = TempSummaryTable['meanSCcrSplit'].rolling(min_periods=10, window=50).mean()
        # TempSummaryTable['rMEANmCR'] = TempSummaryTable['meanDCFCcr'].rolling(min_periods=10, window=50).mean()

        TempSummaryTable['TC_T'] = TempSummaryTable['TC_T'].fillna(pd.Timedelta(seconds=0))
        TempSummaryTable['PC_T'] = TempSummaryTable['PC_T'].fillna(pd.Timedelta(seconds=0))
        TempSummaryTable['DCC_T'] = TempSummaryTable['DCC_T'].astype('timedelta64[ns]')
        TempSummaryTable['DCC_T'] = TempSummaryTable['DCC_T'].fillna(pd.Timedelta(seconds=0))
        TempSummaryTable['PSC_T'] = TempSummaryTable['PSC_T'].fillna(pd.Timedelta(seconds=0))

        ndf = TempSummaryTable[['TC_T','PC_T','DCC_T']].copy()
        print(ndf,ndf.dtypes)

        TempSummaryTable['TempIRMS'] = TempSummaryTable.TC_I / TempSummaryTable.TC_T.dt.total_seconds()
        TempSummaryTable['TempIRMS'] = TempSummaryTable['TempIRMS'].pow(1./2)

        TempSummaryTable['PowerIRMS'] = TempSummaryTable.PC_I / TempSummaryTable.PC_T.dt.total_seconds()
        TempSummaryTable['PowerIRMS'] = TempSummaryTable['PowerIRMS'].pow(1. / 2)
        TempSummaryTable['DCFCIRMS'] = TempSummaryTable.DCC_I / TempSummaryTable.DCC_T.dt.total_seconds()
        TempSummaryTable['DCFCIRMS'] = TempSummaryTable['DCFCIRMS'].pow(1. / 2)
        TempSummaryTable['PSIRMS'] = TempSummaryTable.PSC_I / TempSummaryTable.PSC_T.dt.total_seconds()
        TempSummaryTable['PSIRMS'] = TempSummaryTable['PSIRMS'].pow(1. / 2)

        return df, TempSummaryTable

    def OutputToExcel(self, df, filename):
        outdir = filename.split('/')
        loc = ''
        for i in range(0, len(outdir) - 1):
            loc = os.path.join(loc, outdir[i])
            if not os.path.exists(loc):
                os.mkdir(loc)
        df.to_csv(path_or_buf=filename,index=False)

    def PlotAllSamples(self, signals): #Input subset of data for Plotting
        samples = signals['key'].nunique()
        print(signals['key'])
        self.logger.info('Number of Samples {}'.format(samples))
        keys = signals.key.unique()
        keys = sorted(keys)
        row, col = signals.shape
        print(col-2, samples)
        fig, axs = plt.subplots(nrows=col-2, ncols=len(keys)+1, sharey='row')

        for i in range(0, len(keys)):
            print('i={}'.format(i))
            tdf = signals.loc[signals['key'] == keys[i]]
            t2df = tdf.drop('key', axis=1)
            for j in range(0, col-2):
                t2df.plot(x=0, y=j+1, style='.', legend=False, ax=axs[j, i])
                t2df.plot(x=0, y=j+1, style='.', legend=False, label=keys[i][-10:],
                          ax=axs[j, len(keys)])
                if j == 0:
                    axs[0, i].set_title("{}".format(keys[i][-10:]))
                if i == 0:
                    axs[j, 0].set_ylabel(t2df.columns[j+1])

        plt.show()


if __name__ == "__main__":
    test = PTCE_data()
