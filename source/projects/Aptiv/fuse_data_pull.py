import os
import time
import math
import pandas as pd
from pandas.api.types import is_numeric_dtype
import matplotlib.pyplot as plt
import numpy as np
import statistics
import logging
from openpyxl import load_workbook
from itertools import islice

class raw_data_ingest:
    def __init__(self):
        self.root = r"/Users/jtbruschetto/Documents/OneDrive - Rivian Automotive, LLC/Projects/HVDB/Aptiv/Python Data/unit_data/ptc_round_4/Pos/"
        os.chdir(self.root) # /Users/jtbruschetto/GIT/JBPL/source/projects/Aptiv"
        logging.basicConfig(format='%(asctime)s - %(levelname)s - %(message)s') #filename='PTC_Data.log',
        self.logger = logging.getLogger()
        self.logger.setLevel(logging.INFO)
        self.logger.info('---- Starting raw Data Ingest Process ----')
        time.sleep(2)

        self.ColumnDict = {'T_AMB_1.2':'Ambient Temperature',
                           'Prim_Fuse_Body':'Primary Fuse Body Temperature',
                           'Sec_Fuse_Body':'Secondary Fuse Body Temperature',
                           'PFrSplit':'Primary Fuse Resistance (assuming 50/50 Current Split)',
                           'SFrSplit':'Secondary Fuse Resistance (assuming 50/50 Current Split)',
                           'F_PRIME':'Primary Fuse Voltage Drop',
                           'F_SEC':'Secondary Fuse Voltage Drop',
                           'CURRENT':'HVDB Current',
                           'PFi2rt':'Primary Fuse Instantaneous i^2xRxT',
                           'SFi2rt': 'Secondary Fuse Instantaneous i^2xRxT',
                           'TempCycleCount':'Temperature Cycle ID'
                           }
        self.Controller()

    def Controller(self):
        self.logger.info('Starting Controller')
        files = os.listdir()
        files = [k for k in files if '.csv' in k]
        files = sorted(files)
        for file in files:
            print(file)
            df = self.loadExcel(self.root,file)
            print(self.ColumnDict.keys())
            df = df[self.ColumnDict.keys()]
            df.reset_index(inplace=True,drop=False)
            df = df[(df['TempCycleCount']<=7) & (df['TempCycleCount']>=5)]
            print(df.columns)
            df.rename(columns=self.ColumnDict,inplace=True)
            print(df.columns)

            self.OutputToExcel(df,file)

    def loadExcel(self,root,file):
        self.logger.info('Starting Load Excel')
        df = pd.read_csv(root + file,
                         index_col=None,
                         header=0,
                         delimiter=',',
                         low_memory=False)
        return df

    def OutputToExcel(self, df, file):
        os.chdir(r'/Users/jtbruschetto/Desktop/Fuse PTC Data/')
        df.to_csv(path_or_buf=file,index=True)
        os.chdir(self.root)

if __name__ == "__main__":
    test = raw_data_ingest()
