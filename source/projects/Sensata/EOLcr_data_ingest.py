import os
import math
import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
import statistics

class EOL_data:
    def __init__(self):
        os.chdir(r"/Users/jtbruschetto/Documents/OneDrive - Rivian Automotive, LLC/Projects/HVDB/Sensata/Python Data")
        # pd.set_option('display.max_columns', None)
        data_folder = r"raw_EOLcr_data/"
        files = os.listdir(data_folder)
        files [:] = [x for x in files if any(y in x for y in ['.csv'])]
        self.Controller(files,data_folder)
        # self.FailureReport()
        # self.RawData(files,data_folder)

    def Controller(self, files, root):
        JoinedRawData = pd.DataFrame()  # columns=['CR', 'SN', 'CR_Delta', 'Cycle_indicator', 'Cycle_Count', 'Cycle_Current_ID']
        CurrentSummaryData = pd.DataFrame(
            columns=['SN', 'Cycle_Count', 'maxCR', 'minCR', 'rangeCR', 'meanCR', 'lastCR','status','type'])  #
        UnitSummaryData = pd.DataFrame(
            columns=['SN', 'maxCR', 'minCR', 'rangeAllCR', 'maxRangeCR', 'meanCR', 'meanLastCR','residualLastCR', 'status','cntBoth','cntLast','cntRange'])
        for i in range(0, len(files)):
            print('{}/{} Starting: {}'.format(i,len(files),files[i]))
            df = self.loadExcel(root + files[i])
            serials = df.columns.tolist()

            if files[i] == 'CR Results White dots 06-03-2021.csv':
                dot = ['WhiteDot']*len(serials)
                whitedot = dict(zip(serials,dot))

            if files[i] == 'Green dots data 06-07-2021.csv':
                dot = ['GreenDot'] * len(serials)
                greendot = dict(zip(serials, dot))

            '''Serial Number Drop List'''
            drop_sn = ['1','GV354RPB:210521-363950','GV354RPB:210524-368238','GV354RPB:210524-368212','GV354RPB:210524-368201','GV354RPB:210524-368218']
            for dsn in drop_sn:
                print('Drop SN: {}'.format(dsn))
                try:
                    serials.remove(dsn)
                except:
                    pass

            for sn in serials:
                print('Starting SN: {}'.format(sn))
                SRdf, cycles = self.SplitRawFile(sn,df)
                JoinedRawData = JoinedRawData.append(SRdf)
                for cycle in cycles:
                    print('Starting SN: {} Cycle: {}'.format(sn,cycle))
                    header = [sn,cycle]
                    row = self.CurrentCycleSummary(cycle,SRdf)
                    header.extend(row)
                    CurrentSummaryData = CurrentSummaryData.append(pd.Series(header,index=CurrentSummaryData.columns),ignore_index=True)

                header = [sn]
                row = self.UnitSummary(sn,CurrentSummaryData)
                header.extend(row)
                UnitSummaryData = UnitSummaryData.append(pd.Series(header, index=UnitSummaryData.columns),ignore_index=True)



        grouping = {'GV354RPB:210503-329957':	'ATM',
                    'GV354RPB:210503-329933':	'ATM',
                    'GV354RPB:210503-329952':	'ATM',
                    'GV354RPB:210503-329935':	'ATM',
                    'GV354RPB:210503-329936':	'5 psig',
                    'GV354RPB:210503-329932':	'5 psig',
                    'GV354RPB:210503-329925':	'5 psig',
                    'GV354RPB:210503-329940':	'5 psig',
                    'GV354RPB:210503-329954':	'10 psig',
                    'GV354RPB:210503-329922':	'10 psig',
                    'GV354RPB:210503-329946':	'10 psig',
                    'GV354RPB:210503-329956':	'10 psig',
                    'GV354RPB:210503-329948':	'15 psig',
                    'GV354RPB:210503-329927':	'15 psig',
                    'GV354RPB:210503-329963':	'15 psig',
                    'GV354RPB:210503-329924':	'15 psig',
                    'GV354RPB:210503-329955':	'20 psig',
                    'GV354RPB:210503-329930':	'20 psig',
                    'GV354RPB:210503-329926':	'20 psig',
                    'GV354RPB:210503-329947':	'20 psig',
                    'GV354RPB:210503-329929':	'unpinched',
                    'GV354RPB:210503-329949':	'unpinched'}
        print(grouping)
        grouping = {**grouping, **whitedot, **greendot}
        print(grouping)

        overall_status = pd.Series(UnitSummaryData.status.values,index=UnitSummaryData.SN).to_dict()

        JoinedRawData['Group'] = JoinedRawData['SN']
        JoinedRawData['overall_status'] = JoinedRawData['SN']
        JoinedRawData.replace({'Group':grouping,'overall_status':overall_status},inplace=True)
        CurrentSummaryData['Group'] = CurrentSummaryData['SN']
        CurrentSummaryData['overall_status'] = CurrentSummaryData['SN']
        CurrentSummaryData.replace({'Group': grouping,'overall_status':overall_status},inplace=True)
        UnitSummaryData['Group'] = UnitSummaryData['SN']
        UnitSummaryData.replace({'Group': grouping},inplace=True)

        self.OutputToExcel(JoinedRawData, 'diagnostic/EOL_Data/JoinedRawData.csv')
        self.OutputToExcel(CurrentSummaryData, 'diagnostic/EOL_Data/CurrentSummaryData.csv')
        self.OutputToExcel(UnitSummaryData, 'diagnostic/EOL_Data/UnitSummaryData.csv')

    def loadExcel(self, file):
        df = pd.read_csv(file)

        if file in [r'raw_EOLcr_data/CR Results White dots 06-03-2021.csv',r'raw_EOLcr_data/Green dots data 06-07-2021.csv']:
            print('df modification')
            df.drop(columns=['CR Min','CR Max', 'CR Delta','Sample Rate'],inplace=True)
            # df.reset_index(drop=True,inplace=True)
            df = df.T
            print(df)
            df.columns = df.iloc[0]
            df.drop(df.index[0],inplace=True)
            df.reset_index(drop=True, inplace=True)

        return df

    def SplitRawFile(self,sn,df):
        sndf = df[[sn]].copy()
        sndf['SN'] = sn
        try:
            sndf.columns = ['CR', 'SN']
        except:
            print(sndf.columns)
            input('continue?')
            exit()

        sndf =sndf.dropna()

        total_window = []
        cycle_window = int(len(sndf)/5)

        if int(len(sndf)/5) < 40:
            cycle_window = int(len(sndf)/3)

        window = list(range(0,20))
        for i in range(1,5):
            estimate = int(cycle_window * i)-10
            x = [y + estimate for y in window]
            total_window.extend(x)

        sndf['CR_Delta'] = sndf.CR - sndf.CR.shift(1)
        sndf['Cycle_Windows'] = np.where(sndf.index.isin(total_window),1,0)
        # print(cycle_window,len(sndf))
        # print(sndf[sndf['Cycle_Windows']==1])
        # input('continue?')
        sndf['Cycle_indicator_1'] = np.where((sndf['Cycle_Windows'] == 1 ) &(sndf['CR_Delta'] > 0.002) & (sndf['CR_Delta'].shift(1).rolling(min_periods=20, window=25).max() < 0.002) & (sndf['CR_Delta'].shift(-26).rolling(min_periods=20, window=25).max() < 0.01),1,0)
        sndf['Cycle_indicator_2'] = np.where((sndf['Cycle_Windows'] == 1 ) &(sndf['CR_Delta'] < -0.005) & (sndf['CR_Delta'].shift(1).rolling(min_periods=20, window=25).std() < 0.005)& (sndf['Cycle_indicator_1'].shift(-5).rolling(min_periods=10, window=10).sum() == 0), 1, 0)
        sndf['Cycle_indicator'] = sndf['Cycle_indicator_1'] + sndf['Cycle_indicator_2']
        sndf['Cycle_Count'] = sndf.Cycle_indicator.cumsum()
        a = sndf.Cycle_indicator < 1
        sndf['Cycle_Current_ID'] = a.cumsum() - a.cumsum().where(~a).ffill().fillna(0).astype(int)

        cycles = sndf['Cycle_Count'].unique()
        cycles = cycles.tolist()

        for cycle in cycles:
            tdf = sndf.loc[(sndf['Cycle_Count'] == cycle)]
            max_cycle = int(tdf['Cycle_Current_ID'].max())

            if (cycle_window + 10) < max_cycle < (cycle_window*2 + 20):
                sndf['Cycle_indicator_3'] = np.where((sndf['Cycle_Count'] == cycle) & (sndf['Cycle_Current_ID'] == int(max_cycle/2)-2), 1, 0)
                sndf['Cycle_indicator'] = sndf['Cycle_indicator_1'] + sndf['Cycle_indicator_2'] + sndf['Cycle_indicator_3']
                sndf['Cycle_Count'] = sndf.Cycle_indicator.cumsum()
                a = sndf.Cycle_indicator < 1
                sndf['Cycle_Current_ID'] = a.cumsum() - a.cumsum().where(~a).ffill().fillna(0).astype(int)
                sndf.drop(columns=['Cycle_indicator_3'],inplace=True)
            elif max_cycle > (cycle_window*2 + 20):
                sndf['Cycle_indicator_3'] = np.where((sndf['Cycle_Count'] == cycle) & (sndf['Cycle_Current_ID'] == int(max_cycle/3-1)), 1, 0)
                sndf['Cycle_indicator_4'] = np.where((sndf['Cycle_Count'] == cycle) & (sndf['Cycle_Current_ID'] == int(max_cycle/3)*2-1), 1, 0)
                sndf['Cycle_indicator'] = sndf['Cycle_indicator_1'] + sndf['Cycle_indicator_2'] + sndf['Cycle_indicator_3'] + sndf['Cycle_indicator_4']
                sndf['Cycle_Count'] = sndf.Cycle_indicator.cumsum()
                a = sndf.Cycle_indicator < 1
                sndf['Cycle_Current_ID'] = a.cumsum() - a.cumsum().where(~a).ffill().fillna(0).astype(int)
                # sndf.drop(columns=['Cycle_indicator_3'],inplace=True)


        # sndf.drop(columns=['Cycle_indicator_1','Cycle_indicator_2'],inplace=True)
        sndf.dropna(subset=["CR"], inplace=True)

        sndf['minAdjCR'] = 0
        sndf['10ptRollingDiff'] = 0

        for cycle in cycles:
            tdf = sndf.loc[(sndf['Cycle_Count'] == cycle)]
            CycleminCR = int(tdf['CR'].min())
            '''
            min adjusted
            10point difference
            '''
            # sndf.loc[sndf['Cycle_Count']==cycle]['minAdjCR'] = sndf['CR'] - CycleminCR
            # sndf.loc[sndf['Cycle_Count'] == cycle]['10ptRollingDiff'] = sndf['CR']-sndf['CR'].shift(10)
            sndf.loc[sndf['Cycle_Count'] == cycle]['minAdjCR'] = sndf['CR'] - CycleminCR
            sndf.loc[sndf['Cycle_Count'] == cycle]['10ptRollingDiff'] = sndf['CR'] - sndf['CR'].shift(10)




            # print(sndf.columns.tolist())
        return sndf, cycles

    def CurrentCycleSummary(self,cycle,df):
        # print(df.columns.tolist())
        CCdf = df.loc[df['Cycle_Count'] == cycle].copy()
        maxCR = CCdf.CR.max()
        minCR = CCdf.CR.min()
        rangeCR = maxCR - minCR
        meanCR = CCdf.CR.mean()
        lastCR = CCdf.CR.iloc[-1]

        if (lastCR > 0.2) | (rangeCR > 0.1):
            status = 'Fail'
            if (lastCR > 0.2) & (rangeCR > 0.1):
                type = 'Both'
            elif (lastCR > 0.2):
                type = 'Last'
            elif (rangeCR > 0.1):
                type = 'Range'
            else:
                type = 'Strange'
        else:
            status = 'Pass'
            type = None

        row = [maxCR,minCR,rangeCR,meanCR,lastCR,status,type]
        return row

    def UnitSummary(self,sn,df):
        # print(df.columns.tolist())
        CCdf = df.loc[df['SN'] == sn].copy()
        # print(CCdf)
        maxCR = CCdf.maxCR.max()
        minCR = CCdf.minCR.min()
        maxRangeCR = CCdf.rangeCR.max()
        rangeAllCR = maxCR - minCR
        meanCR = CCdf.meanCR.mean()
        meanLastCR = CCdf.lastCR.mean()
        CCdf['lCR-mCR'] = (CCdf.lastCR - meanLastCR).abs()
        residualLastCR = CCdf['lCR-mCR'].sum()

        if (meanLastCR > 0.2) | (maxRangeCR > 0.1):
            status = 'Fail'
        else:
            status = 'Pass'

        '''cycle failures'''
        cntBoth = CCdf[CCdf['type']=='Both']['SN'].count()
        cntLast = CCdf[CCdf['type']=='Last']['SN'].count()
        cntRange = CCdf[CCdf['type']=='Range']['SN'].count()

        row = [maxCR,minCR,rangeAllCR,maxRangeCR,meanCR,meanLastCR,residualLastCR ,status,cntBoth,cntLast,cntRange]
        return row

    def OutputToExcel(self, df, filename):
        df.reset_index(inplace=True, drop=True)
        df.to_csv(path_or_buf=filename)



if __name__ == "__main__":
    test = EOL_data()
