import os
# import math
import time
import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
import statistics
import logging
import file_parser


class PTCE_data:
    def __init__(self):
        os.chdir(r"/Users/jtbruschetto/Documents/OneDrive - Rivian Automotive, LLC/Projects/HVDB/Sensata/Python Data")
        logging.basicConfig(
            format='%(asctime)s - %(levelname)s - %(message)s')  # filename='PTC_Data.log',
        self.logger = logging.getLogger()
        self.logger.setLevel(logging.DEBUG)
        self.logger.info('---- Starting Sensata PTC Data Ingest Process ----')

        self.LOC_Map = {'CR1': [4, 1],
                        'CR2': [3, 1],
                        'CR3': [2, 1],
                        'CR4': [1, 1],
                        'CR5': [1, 2],
                        'CR6': [2, 2],
                        'CR7': [3, 2],
                        'CR8': [4, 2],
                        'CR9': [4, 3],
                        'CR10': [3, 3],
                        'CR11': [2, 3],
                        'CR12': [1, 3],
                        'CR13': [1, 4],
                        'CR14': [2, 4],
                        'CR15': [3, 4],
                        'CR16': [4, 4],
                        'CR17': [4, 5],
                        'CR18': [3, 5],
                        'CR19': [2, 5],
                        'CR20': [1, 5],
                        'CR21': [1, 6],
                        'CR22': [2, 6],
                        'CR23': [3, 6],
                        'CR24': [4, 6]}  # CR : [X , Y] Left to right top to bottom
        self.GDstart = {'CR1': 'GV354RPB:210602-383470',
                        'CR2': 'GV354RPB:210602-383238',
                        'CR3': 'GV354RPB:210602-383380',
                        'CR4': 'GV354RPB:210602-382344',
                        'CR5': 'GV354RPB:210602-382947',
                        'CR6': 'GV354RPB:210603-384206',
                        'CR7': 'GV354RPB:210602-382944',
                        'CR8': 'GV354RPB:210602-382392',
                        'CR9': 'GV354RPB:210602-382273',
                        'CR10': 'GV354RPB:210602-382309',
                        'CR11': 'GV354RPB:210602-382394',
                        'CR12': 'GV354RPB:210602-382705',
                        'CR13': 'GV354RPB:210602-383045',
                        'CR14': 'GV354RPB:210603-384309',
                        'CR15': 'GV354RPB:210602-383497',
                        'CR16': 'GV354RPB:210602-382602',
                        'CR17': 'GV354RPB:210602-383035',
                        'CR18': 'GV354RPB:210602-383804',
                        'CR19': 'GV354RPB:210602-383714',
                        'CR20': 'GV354RPB:210602-383326',
                        'CR21': 'GV354RPB:210602-382331',
                        'CR22': 'GV354RPB:210602-383028',
                        'CR23': 'GV354RPB:210603-384346',
                        'CR24': 'GV354RPB:210602-382978'}
        self.GDunitswap = {'CR4': [77, 'GV354RPB:210602-382344', 'GV354RPB:210602-382970'],
                           'CR5': [77, 'GV354RPB:210602-382947', 'GV354RPB:210602-383391'],
                           'CR8': [77, 'GV354RPB:210602-382392', 'GV354RPB:210602-382322'],
                           'CR11': [77, 'GV354RPB:210602-382394', 'GV354RPB:210602-383582'],
                           'CR17': [111, 'GV354RPB:210602-383035', 'GV354RPB:210602-382995'],
                           'CR22': [111, 'GV354RPB:210602-383028', 'GV354RPB:210602-383535'],
                           'CR3': [143, 'GV354RPB:210602-383380', 'GV354RPB:210602-382271'],
                           'CR21': [143, 'GV354RPB:210602-382331', 'GV354RPB:210602-383832']
                           }
        self.ProfileDict = {}
        self.GroupDict = {}
        data_folder = r"raw_ptce_data"

        load = 1
        while load > 0:
            profiles = [name for name in os.listdir(data_folder) if os.path.isdir(os.path.join(data_folder, name))]
            for i in profiles:
                localDir = data_folder + '/' + i
                groups = [name for name in os.listdir(localDir) if os.path.isdir(os.path.join(localDir, name))]
                self.ProfileDict[i] = groups
                for j in groups:
                    localDir = data_folder + '/' + i + '/' + j
                    files = os.listdir(localDir)
                    files[:] = [x for x in files if any(y in x for y in ['.csv'])]
                    self.GroupDict[j] = files
            time.sleep(1)

            load = int(input(
                'Program Options:\n  - 5 Parse Files \n  - 4 Run All \n  - 3 Run Profile \n  - 2 Run Group \n  - 1 '
                'Run Summary \n  - 0 Exit \nSelection:'))

            if load == 5:
                file_parser.fileParser()

            if load == 4:
                self.logger.info('Option: "Run All" has been selected')
                for profile in self.ProfileDict.keys():
                    self.logger.info("Starting Profile: {}".format(profile))
                    for group in self.ProfileDict[profile]:
                        self.logger.info("-- Starting Group: {}".format(group))
                        files = self.GroupDict[group]
                        localDir = data_folder + '/' + profile + '/' + group + '/'
                        self.Controller(files, profile, group, localDir)
            if load == 3:
                self.logger.info('Option: "Run Profile" has been selected')
                time.sleep(1)
                profile_selection = int(input(
                    'Profile Options: {} \n Select the index between 0 & {}\nSelection:'
                    .format(self.ProfileDict.keys(), len(self.ProfileDict.keys()) - 1)))
                profile_selected = list(self.ProfileDict)[profile_selection]
                self.logger.info('Profile: {} has been selected'.format(profile_selected))
                for group in self.ProfileDict[profile_selected]:
                    self.logger.info("-- Starting Group: {}".format(group))
                    files = self.GroupDict[group]
                    localDir = data_folder + '/' + profile_selected + '/' + group + '/'
                    self.Controller(files, profile_selected, group, localDir)
            if load == 2:
                self.logger.info('Option: "Run Group" has been selected')
                time.sleep(1)
                profile_selection = int(input(
                    'Profile Options: {} \n Select the index between 0 & {}\nSelection:'
                    .format(self.ProfileDict.keys(), len(self.ProfileDict.keys())-1)))
                profile_selected = list(self.ProfileDict)[profile_selection]
                self.logger.info('Profile: {} has been selected'.format(profile_selected))
                time.sleep(1)
                group_selection = int(input('Group Options: {} \n Select the index between 0 & {}\nSelection:'.format(
                    self.ProfileDict[profile_selected],
                    len(self.ProfileDict[profile_selected])-1)))
                group_selected = list(self.ProfileDict[profile_selected])[group_selection]
                self.logger.info('Group: {} has been selected'.format(group_selected))
                files = self.GroupDict[group_selected]
                localDir = data_folder + '/' + profile_selected + '/' + group_selected + '/'
                self.Controller(files, profile_selected, group_selected, localDir)

            if load == 1:
                self.logger.info('Option: "Run Summary" has been selected')
                CurrentSummary = pd.DataFrame()
                FailureSummary = pd.DataFrame()
                lifeSummary = pd.DataFrame()
                TempSummary = pd.DataFrame()
                self.logger.info('Start Loading Summary Data')
                for profile in self.ProfileDict.keys():
                    self.logger.info("Starting Profile: {}".format(profile))
                    for group in self.ProfileDict[profile]:
                        self.logger.info("-- Starting Group: {}".format(group))
                        localDir = r'summary_data' + '/' + profile + '/' + group + '/'
                        current_File = pd.read_csv(localDir + 'Current_Summary.csv')
                        failure_File = pd.read_csv(localDir + 'Failure_Summary.csv')
                        life_File = pd.read_csv(localDir + 'life_Summary.csv')
                        temp_File = pd.read_csv(localDir + 'Temp_Summary.csv')
                        CurrentSummary = pd.concat([CurrentSummary, current_File])
                        FailureSummary = pd.concat([FailureSummary, failure_File])
                        lifeSummary = pd.concat([lifeSummary, life_File])
                        TempSummary = pd.concat([TempSummary, temp_File])
                self.logger.info('Completed Loading Summary Data')
                self.OutputToExcel(CurrentSummary, r'summary_data/Sensata_PTC_Current_Summary.csv')
                self.OutputToExcel(FailureSummary, r'summary_data/Sensata_PTC_Failure_Summary.csv')
                self.OutputToExcel(lifeSummary, r'summary_data/Sensata_PTC_life_Summary.csv')
                self.OutputToExcel(TempSummary, r'summary_data/Sensata_PTC_Temp_Summary.csv')
                self.logger.info('Saved Combined Summary Data')

        self.logger.info('Option: "Exit" has been selected')
        # self.FailureReport()
        # self.RawData(files,data_folder)

    def Controller(self, files, profile, group, root):
        MasterTableCurrent = pd.DataFrame()
        MasterTableTemp = pd.DataFrame()
        self.logger.info('Starting Data Summarization')
        for i in range(0, len(files)):
            self.logger.debug('{}/{} Data Summarization - Starting File: {}'.format(i, len(files), files[i]))
            df = self.loadExcel(root + files[i])
            CurrentSummaryTable = self.CurrentCycleData(df)
            CurrentSummaryTable['key'] = files[i]
            TempSummaryTable = self.TempCycleData(df)
            TempSummaryTable['key'] = files[i]

            MasterTableCurrent = pd.concat([MasterTableCurrent, CurrentSummaryTable])
            MasterTableTemp = MasterTableTemp.append(TempSummaryTable)
        self.logger.info('Completed Data Summarization')

        MasterTableCurrent.reset_index(drop=True, inplace=True)
        MasterTableTemp.reset_index(drop=True, inplace=True)
        MasterTableTemp['Profile'] = profile
        MasterTableTemp['Group'] = group
        MasterTableTemp['LOC'] = MasterTableTemp.key.str.split('_', n=2, expand=True)[2].str[:-4]

        MasterTableCurrent['Profile'] = profile
        MasterTableCurrent['Group'] = group
        MasterTableCurrent['LOC'] = MasterTableCurrent.key.str.split('_', n=2, expand=True)[2].str[:-4]


        MasterTableCurrent['SN'] = np.nan
        MasterTableTemp['SN'] = np.nan

        MasterTableCurrent['Rig_Loc'] = MasterTableCurrent["LOC"].apply(lambda x: self.LOC_Map.get(x))
        MasterTableCurrent[['Rig_X','Rig_Y']] = pd.DataFrame(MasterTableCurrent['Rig_Loc'].values.tolist())
        MasterTableTemp['Rig_Loc'] = MasterTableTemp["LOC"].apply(lambda x: self.LOC_Map.get(x))
        MasterTableTemp[['Rig_X', 'Rig_Y']] = pd.DataFrame(MasterTableTemp['Rig_Loc'].values.tolist())

        for key in self.GDstart.keys():
            MasterTableCurrent['SN'] = np.where(
                ((MasterTableCurrent['Group'] == "GDL1") & (MasterTableCurrent['LOC'] == key)), self.GDstart[key],
                MasterTableCurrent['SN'])
            MasterTableTemp['SN'] = np.where(((MasterTableTemp['Group'] == "GDL1") & (MasterTableTemp['LOC'] == key)),
                                             self.GDstart[key], MasterTableTemp['SN'])

        for key in self.GDunitswap.keys():
            MasterTableCurrent['SN'] = np.where(((MasterTableCurrent['Group'] == "GDL1") & (
                    MasterTableCurrent['LOC'] == key) & (MasterTableCurrent['TempCycle'] >= self.GDunitswap[key][
                        0])), self.GDunitswap[key][2], MasterTableCurrent['SN'])
            MasterTableCurrent['LOC'] = np.where((MasterTableCurrent['SN'] == self.GDunitswap[key][2]),
                                                 MasterTableCurrent['LOC'] + "_R", MasterTableCurrent['LOC'])
            MasterTableTemp['SN'] = np.where(((MasterTableTemp['Group'] == "GDL1") & (MasterTableTemp['LOC'] == key) & (
                    MasterTableTemp['TempCycle'] >= self.GDunitswap[key][0])), self.GDunitswap[key][2],
                                             MasterTableTemp['SN'])
            MasterTableTemp['LOC'] = np.where((MasterTableTemp['SN'] == self.GDunitswap[key][2]),
                                              MasterTableTemp['LOC'] + "_R", MasterTableTemp['LOC'])
            minCurrentCycle = MasterTableCurrent[MasterTableCurrent['SN'] == self.GDunitswap[key][2]][
                'CurrentCycle'].min()
            minTempCycle = MasterTableCurrent[MasterTableCurrent['SN'] == self.GDunitswap[key][2]]['TempCycle'].min()
            MasterTableCurrent['CurrentCycle'] = np.where(MasterTableCurrent['SN'] == self.GDunitswap[key][2],
                                                          MasterTableCurrent['CurrentCycle'] - minCurrentCycle,
                                                          MasterTableCurrent['CurrentCycle'])
            MasterTableCurrent['TempCycle'] = np.where(MasterTableCurrent['SN'] == self.GDunitswap[key][2],
                                                       MasterTableCurrent['TempCycle'] - minTempCycle,
                                                       MasterTableCurrent['TempCycle'])
            MasterTableTemp['TempCycle'] = np.where(MasterTableTemp['SN'] == self.GDunitswap[key][2],
                                                    MasterTableTemp['TempCycle'] - minTempCycle,
                                                    MasterTableTemp['TempCycle'])
            MasterTableCurrent['rMEANmCR'] = np.where(
                (MasterTableCurrent['SN'] == self.GDunitswap[key][2]) & (MasterTableCurrent['TempCycle'] <= 3), np.nan,
                MasterTableCurrent['rMEANmCR'])
            MasterTableTemp['rMEANmCR'] = np.where(
                (MasterTableTemp['SN'] == self.GDunitswap[key][2]) & (MasterTableTemp['TempCycle'] <= 3), np.nan,
                MasterTableTemp['rMEANmCR'])



        # SteadyStateTable = MasterTableTemp.loc[MasterTableTemp['TempCycle'] >= 40]
        # self.PlotAllSamples(SteadyStateTable[['key', 'TempCycle', 'meanContactResistance', 'eSTDmCR', 'eSTDmCRs', 'rSTDmCR', 'rMEANmCR', 'MADeSTDmCR', 'MADemCR']],
        #                     'CR Analysis.png')
        # self.PlotAllSamples(SteadyStateTable[['key', 'TempCycle','AccumMeasI2RT','measI2RT','AccumDeltaT','AccumCurrentCount']],'Profile Analysis.png')
        # self.PlotEverySamples(MasterTableTemp[['key','TempCycle','meanAdjCurrent','meanContactResistance','eSTDmCR', 'rMEANmCR', 'MADMeasI2RT']], r'partCharts/')
        self.PlotAllSamples(MasterTableCurrent,['LOC', 'CurrentCycle','rMEANmCR'],profile,group,'Current_Cycle_Summary_{}_{}.jpg'.format(profile,group))

        self.OutputToExcel(MasterTableCurrent, r'summary_data/{}/{}/Current_Summary.csv'.format(profile, group))
        self.OutputToExcel(MasterTableTemp, r'summary_data/{}/{}/Temp_Summary.csv'.format(profile, group))

        lifedf = self.LifeTempSummary(MasterTableCurrent)
        self.OutputToExcel(lifedf, r'summary_data/{}/{}/life_Summary.csv'.format(profile, group))

        Failure_Summary = self.FailureSummary(MasterTableCurrent)
        self.OutputToExcel(Failure_Summary, r'summary_data/{}/{}/Failure_Summary.csv'.format(profile, group))



    def FailureReport(self):
        MasterTableCurrent = pd.read_csv(r'summary_data/Sensata_PTC_Current_Summary.csv')
        Failure_Summary = self.FailureSummary(MasterTableCurrent)
        self.OutputToExcel(Failure_Summary, r'summary_data/Sensata_PTC_Failure_Summary.csv')

    def RawData(self, files, root):
        for i in range(0, len(files)):
            self.logger.info('{}/{} Starting: {}'.format(i, len(files), files[i]))
            df = self.loadExcel(root + files[i])
            self.OutputToExcel(df, r'diagnostic/Sensata_PTC_Current_RAW_{}.csv'.format(files[i]))

    def loadExcel(self, file):
        df = pd.read_csv(file) #usecols=[0, 1, 2, 3, 4]
        df = df.apply(pd.to_numeric)
        # df.columns = ['Time', 'Current', 'ContactResistance', 'AmbientTemp', 'DutTemp']
        # df['Time'] = df['Time'] * 60 * 60
        # df['stepTime'] = df.Time.diff()
        # df['TempCycle'] = np.where((df.AmbientTemp >= 0) & (df.AmbientTemp.shift(1) < 0), 1, 0)
        # df['TempCycleCount'] = df['TempCycle'].cumsum()
        # df['CurrentCycleStart'] = np.where((df['Current'] > 10) & (df['Current'].shift(1) < 10), 1, 0)
        # df['CurrentCycleEnd'] = np.where((df['Current'] < 0) & (df['Current'].shift(1) > 10), 1, 0)
        # df['CurrentCycleCount'] = df['CurrentCycleStart'].cumsum()
        # df['CurrentCycleCompare'] = df['CurrentCycleEnd'].cumsum()
        # df['CurrentCycleIndicator'] = df['CurrentCycleCount'] - df['CurrentCycleCompare']
        # a = df.CurrentCycleIndicator != 0
        # df['CurrentCycleID'] = a.cumsum() - a.cumsum().where(~a).ffill().fillna(0).astype(int)
        # df['VoltageDrop'] = df['Current'] * df['ContactResistance']
        # df['TempDelta'] = df['DutTemp'] - df['AmbientTemp']
        # df['I2RT'] = df['Current'] * df['Current'] * df['stepTime'] * df['ContactResistance'] / 1000
        # df['I2RT/MC'] = df['I2RT'] / 21.9065
        # df['isumsq'] = df['Current'] ** 2
        # # df['rmsCR'] = math.sqrt()
        return df

    def CurrentCycleData(self, df):
        df = df[['Time', 'Current', 'AmbientTemp', 'DutTemp', 'TempDelta', 'ContactResistance', 'VoltageDrop',
                 'TempCycleCount', 'CurrentCycleCount', 'CurrentCycleIndicator', 'CurrentCycleID', 'I2RT']]
        df = df[(df[['CurrentCycleIndicator']] != 0).all(axis=1)]
        CurrentSummaryTable = pd.DataFrame(
            columns=['CurrentCycle', 'CurrentCycleProfile', 'CycleDataPoints', 'TempCycle', 'TempCycleProfile',
                     'CycleTime', 'minTime', 'maxTime', 'meanCurrent', 'meanAdjCurrent', 'meanContactResistance',
                     'stdContactResistance',
                     'maxContactResistance', 'minContactResistance', 'rangeContactResistance', 'meanVoltageDrop',
                     'stdVoltageDrop', 'measI2RT', 'meanI2RT', 'maxAmb', 'minDUT', 'maxDUT', 'rangeDUT', 'maxDelta'])
        # print(df.CurrentCycleCount.max())
        for i in range(1, df.CurrentCycleCount.max()):

            tdf = df.loc[(df['CurrentCycleCount'] == i) & (df['Current'] > 100)]
            # print(tdf.shape)
            tdf.reset_index(inplace=True, drop=True)
            TempCycle = tdf.TempCycleCount.max()
            minTime = tdf.Time.min()
            maxTime = tdf.Time.max()
            time = tdf.Time.max() - tdf.Time.min()
            meanCurrent = tdf.Current.mean()
            meanAdjCurrent = tdf.Current[1:-1].mean()
            cntAdjCurrent = len(tdf.Current[1:-1])
            if meanAdjCurrent > 1000:
                currentProfile = '70%WOT'
            elif meanAdjCurrent < 600:
                currentProfile = '400ACC'
            else:
                currentProfile = '60%WOT'
            meanContactResistance = tdf.ContactResistance[1:-1].mean()
            stdContactResistance = tdf.ContactResistance[1:-1].std()
            maxContactResistance = tdf.ContactResistance[1:-1].max()
            minContactResistance = tdf.ContactResistance[1:-1].min()
            rangeContactResistance = maxContactResistance - minContactResistance
            meanVoltageDrop = tdf.VoltageDrop.mean()
            stdVoltageDrop = tdf.VoltageDrop.std()
            measI2RT = tdf.I2RT.sum()
            meanI2RT = meanCurrent * meanCurrent * time * meanContactResistance

            maxAmb = tdf.AmbientTemp.max()
            if maxAmb > 55:
                tempProfile = "60C"
            else:
                tempProfile = '0C'
            maxDUT = tdf.DutTemp.max()
            minDUT = tdf.DutTemp.min()
            rangeDUT = tdf.DutTemp.max() - tdf.DutTemp.min()
            maxDelta = tdf.TempDelta.max()

            row = [i, currentProfile, cntAdjCurrent, TempCycle, tempProfile, time, minTime, minTime, meanCurrent,
                   meanAdjCurrent, meanContactResistance, stdContactResistance, maxContactResistance,
                   minContactResistance, rangeContactResistance, meanVoltageDrop, stdVoltageDrop, measI2RT, meanI2RT,
                   maxAmb, minDUT, maxDUT, rangeDUT, maxDelta]
            # print(row)

            CurrentSummaryTable = CurrentSummaryTable.append(pd.Series(row, index=CurrentSummaryTable.columns),
                                                             ignore_index=True)

        CurrentSummaryTable['TbC'] = CurrentSummaryTable.minTime - CurrentSummaryTable.maxTime.shift(-1)

        CurrentSummaryTable['TempCycleDelta'] = CurrentSummaryTable['TempCycle'] - CurrentSummaryTable[
            'TempCycle'].shift(-1)
        a = CurrentSummaryTable.TempCycleDelta > -1

        CurrentSummaryTable['CurrentCycleID'] = a.cumsum() - a.cumsum().where(~a).ffill().fillna(0).astype(int)

        CurrentSummaryTable['DUTdeltaTemp'] = CurrentSummaryTable.minDUT - CurrentSummaryTable.maxDUT.shift(1)
        CurrentSummaryTable.loc[CurrentSummaryTable.CurrentCycleID == 1, 'DUTdeltaTemp'] = 0
        CurrentSummaryTable['AMBdeltaTemp'] = CurrentSummaryTable.maxAmb - CurrentSummaryTable.maxAmb.shift(1)
        CurrentSummaryTable.loc[CurrentSummaryTable.CurrentCycleID == 1, 'AMBdeltaTemp'] = 0
        CurrentSummaryTable['TbCsignal'] = np.where((CurrentSummaryTable.TbC <= -.2), 1, 0)
        CurrentSummaryTable['AccumCurrentTime'] = CurrentSummaryTable.CycleTime.cumsum()
        CurrentSummaryTable['AccumMeanI2RT'] = CurrentSummaryTable.meanI2RT.cumsum()
        CurrentSummaryTable['AccumMeasI2RT'] = CurrentSummaryTable.measI2RT.cumsum()
        CurrentSummaryTable['rSTDmCR'] = CurrentSummaryTable['meanContactResistance'].rolling(min_periods=20,
                                                                                              window=50).std()
        CurrentSummaryTable['rMEANmCR'] = CurrentSummaryTable['meanContactResistance'].rolling(min_periods=20,
                                                                                               window=50).mean()

        CurrentSummaryTable['CS_MaxTempDelta'] = CurrentSummaryTable.maxDUT - CurrentSummaryTable.minDUT.shift(1)

        return CurrentSummaryTable

    def TempCycleData(self, df):
        df = df[
            ['Time', 'AmbientTemp', 'DutTemp', 'TempCycleCount', 'CurrentCycleCount', 'TempDelta', 'Current', 'isumsq',
             'ContactResistance', 'VoltageDrop', 'CurrentCycleIndicator', 'I2RT']]
        TempSummaryTable = pd.DataFrame(
            columns=['TempCycle', 'CycleTime', 'maxDUT', 'minDUT', 'meanDUT', 'rangeDUT',
                     'maxDelta', 'minDelta', 'sumDelta', 'maxCurrentCycle', 'minCurrentCycle', 'rangeCurrentCycle',
                     'meanCurrent', 'meanAdjCurrent', 'meanContactResistance', 'stdContactResistance',
                     'maxContactResistance',
                     'minContactResistance', 'rangeContactResistance', 'meanVoltageDrop', 'stdVoltageDrop', 'measI2RT',
                     'Isum'])
        for i in range(1, df.TempCycleCount.max()):
            tdf = df.loc[(df['TempCycleCount'] == i)]
            tdf.reset_index(inplace=True, drop=True)
            cdf = tdf.loc[tdf['Current'] > 100]
            time = tdf.Time.max() - tdf.Time.min()
            # print(time)
            maxDUT = cdf.DutTemp.max()
            minDUT = cdf.DutTemp.min()
            meanDUT = cdf.DutTemp.mean()
            rangeDUT = maxDUT - minDUT
            maxDelta = tdf.TempDelta.max()
            minDelta = tdf.TempDelta.min()
            sumDelta = tdf.TempDelta.sum()
            maxCurrentCycle = tdf['CurrentCycleCount'].max()
            minCurrentCycle = tdf['CurrentCycleCount'].min()
            rangeCurrentCycle = maxCurrentCycle - minCurrentCycle
            AdjCurrent = []
            I2RT = []
            Isum = []
            for j in range(minCurrentCycle, maxCurrentCycle):
                cdf = df.loc[df['CurrentCycleCount'] == j]
                cdf.reset_index(inplace=True, drop=True)
                isum = cdf.isumsq[1:-1].sum()
                mAC = cdf.Current[1:-1].mean()
                mI2RT = cdf.I2RT[1:-1].sum()
                AdjCurrent.append(mAC)
                I2RT.append(mI2RT)
                Isum.append(isum)
            try:
                meanAdjCurrent = statistics.mean(AdjCurrent)
            except:
                meanAdjCurrent = None
            meanCurrent = tdf.Current.mean()
            meanContactResistance = tdf.ContactResistance.mean()
            stdContactResistance = tdf.ContactResistance.std()
            maxContactResistance = tdf.ContactResistance.max()
            minContactResistance = tdf.ContactResistance.min()
            rangeContactResistance = maxContactResistance - minContactResistance
            meanVoltageDrop = tdf.VoltageDrop.mean()
            stdVoltageDrop = tdf.VoltageDrop.std()
            measI2RT = sum(I2RT)
            Isum = sum(Isum)

            row = [i, time, maxDUT, minDUT, meanDUT, rangeDUT,
                   maxDelta, minDelta, sumDelta, maxCurrentCycle, minCurrentCycle,
                   rangeCurrentCycle, meanCurrent, meanAdjCurrent, meanContactResistance, stdContactResistance,
                   maxContactResistance, minContactResistance, rangeContactResistance, meanVoltageDrop, stdVoltageDrop,
                   measI2RT, Isum]
            TempSummaryTable = TempSummaryTable.append(pd.Series(row, index=TempSummaryTable.columns),
                                                       ignore_index=True)

        TempSummaryTable['AccumDeltaT'] = TempSummaryTable.sumDelta.cumsum()
        TempSummaryTable['AccumCurrentCount'] = TempSummaryTable.rangeCurrentCycle.cumsum()
        TempSummaryTable['AccumMeasI2RT'] = TempSummaryTable.measI2RT.cumsum()
        TempSummaryTable['MADMeasI2RT'] = abs(TempSummaryTable['measI2RT'] - TempSummaryTable['measI2RT'].
                                              expanding(min_periods=10).mean())  # ABS(Measurement - Mean)
        TempSummaryTable['eSTDmCR'] = TempSummaryTable['meanContactResistance'].expanding(min_periods=40).std()
        TempSummaryTable['eSTDmCRs'] = TempSummaryTable['eSTDmCR'].diff(10)
        TempSummaryTable['MADeSTDmCR'] = abs(TempSummaryTable['eSTDmCR'] - TempSummaryTable['eSTDmCR'].
                                             expanding(min_periods=10).mean())
        TempSummaryTable['MADemCR'] = abs(
            TempSummaryTable['meanContactResistance'] - TempSummaryTable['meanContactResistance'].
            expanding(min_periods=10).mean())
        TempSummaryTable['rSTDmCR'] = TempSummaryTable['meanContactResistance'].rolling(min_periods=40, window=50).std()
        TempSummaryTable['rMEANmCR'] = TempSummaryTable['meanContactResistance'].rolling(min_periods=40,
                                                                                         window=50).mean()
        TempSummaryTable['IRMS'] = TempSummaryTable.Isum / TempSummaryTable.CycleTime
        TempSummaryTable['IRMS'] = TempSummaryTable['IRMS'].pow(1. / 2)
        return TempSummaryTable

    def FailureSummary(self, CSdf):
        '''
        Take in summary data table and output the failures when the unit fails the 0.2mOhm Mean CR spec
        Get List of serials
        Loop through and look for condition per serial number
        add row information to DF
            Serial number
            Current Cycle it failed
            Temp Cycle it failed
            Count of range failures prior to failure
            Max temp in failure temp cycle
            Max temp in entire test
        '''

        SNlst = sorted(CSdf.key.unique().tolist())
        Failure_Summary = pd.DataFrame(
            columns=['Key', 'Status', 'Weibull_Status', 'Last_Current_Cycle', 'Failed_Current_Cycle',
                     'Wiebull_Current_Cycle', 'Weibull_Temp_Cycle', 'Temp_Cycle', 'Failed_Cycle_Temp', 'Max_Test_Temp',
                     'Range_OOS_BF', 'Range_OOS_Total', 'Final_Rolling_CR', 'Max_Rolling_CR', 'FailureCycleType'])
        self.logger.info('Starting Failure Summary')
        for Serial in SNlst:
            self.logger.debug(
                '{}/{} Failure Summary - Starting Serial: {}'.format(SNlst.index(Serial) + 1, len(SNlst), Serial))
            if Serial[:4] == 'PTC1':
                sndf = CSdf.loc[(CSdf.key == Serial)]
            else:
                sndf = CSdf.loc[(CSdf.key == Serial) & (CSdf.CurrentCycleProfile == '60%WOT')]
            Failure_Row = sndf[sndf.rMEANmCR > 0.25]
            if Failure_Row.empty:
                Failure_Status = 'Pass'
                Failure_Status_Weibull = 'S'
                Last_Current_Cycle = sndf['CurrentCycle'].max()
                Failed_Current_Cycle = None
                Wiebull_Current_Cycle = Last_Current_Cycle
                Failed_Temp_Cycle = sndf.loc[sndf.CurrentCycleProfile == '60%WOT']['TempCycle'].max()
                Failed_Cycle_Max_Temp = None
                Weibull_Temp_Cycle = Failed_Temp_Cycle
                Max_Temp = sndf.loc[sndf.CurrentCycleProfile == '60%WOT']['maxDUT'].max()
                Count_BF_Range_OOS = None
                FailureCycleType = None
            else:
                Failure_Status = 'Fail'
                Failure_Status_Weibull = 'F'
                Last_Current_Cycle = sndf['CurrentCycle'].max()
                Failed_Current_Cycle = Failure_Row['CurrentCycle'].iat[0]
                Wiebull_Current_Cycle = Failed_Current_Cycle
                Failed_Temp_Cycle = Failure_Row['TempCycle'].iat[0]
                Failed_Cycle_Max_Temp = sndf.loc[sndf.TempCycle == Failed_Temp_Cycle]['maxDUT'].max()
                Weibull_Temp_Cycle = Failed_Temp_Cycle
                Max_Temp = sndf['maxDUT'].max()
                Count_BF_Range_OOS = len(
                    sndf[(sndf['CurrentCycle'] < Failed_Current_Cycle) & (sndf['rangeContactResistance'] > 0.1)])
                FailureCycleType = sndf.loc[sndf.TempCycle == Failed_Temp_Cycle]['CurrentCycleProfile'].iat[0]

            Count_Total_Range_OOS = len(sndf[sndf['rangeContactResistance'] > 0.1])
            Final_Rolling_CR = sndf.loc[sndf['rMEANmCR'].last_valid_index(), 'rMEANmCR']
            Max_Rolling_CR = sndf['rMEANmCR'].max()
            row = [Serial, Failure_Status, Failure_Status_Weibull, Last_Current_Cycle, Failed_Current_Cycle,
                   Wiebull_Current_Cycle, Weibull_Temp_Cycle, Failed_Temp_Cycle, Failed_Cycle_Max_Temp, Max_Temp,
                   Count_BF_Range_OOS, Count_Total_Range_OOS, Final_Rolling_CR, Max_Rolling_CR, FailureCycleType]
            Failure_Summary = Failure_Summary.append(pd.Series(row, index=Failure_Summary.columns), ignore_index=True)

        Failure_Summary['Profile'] = Failure_Summary.Key.str.split('_', n=1, expand=True)[0]
        Failure_Summary['UNIT'] = Failure_Summary.Key.str.split('_', n=1, expand=True)[1]
        Failure_Summary['LOC'] = Failure_Summary.Key.str.split('_', n=2, expand=True)[2].str[:-4]
        Failure_Summary['Group'] = Failure_Summary.Key.str.split('_', n=2, expand=True)[1]
        self.logger.info('Completed Failure Summary')
        return Failure_Summary

    def LifeTempSummary(self, df):
        LifeTempSummary = pd.DataFrame(columns=['TempProfile', 'Signal', '95%', '90%', '75%', '50%'])
        for temp in ['0C', '60C']:
            sigdic = {'Pos': ['maxDUT', 'CS_MaxTempDelta'],
                      'Neg': ['maxTermTemp_PC', 'maxTermTemp_SC', 'maxTermTemp_DC',
                              'maxSA1Temp_PC', 'maxSA1Temp_SC', 'maxSA1Temp_DC',
                              'maxSA2Temp_PC', 'maxSA2Temp_SC', 'maxSA2Temp_DC',
                              'CS_MaxTempDelta_PCTerm', 'CS_MaxTempDelta_SCTerm', 'CS_MaxTempDelta_DCTerm',
                              'CS_MaxTempDelta_PCA1', 'CS_MaxTempDelta_SCA1', 'CS_MaxTempDelta_DCA1',
                              'CS_MaxTempDelta_PCA2', 'CS_MaxTempDelta_SCA2', 'CS_MaxTempDelta_DCA2']}

            for item in sigdic["Pos"]:
                tdf = df[(df['TempCycleProfile'] == temp) & (df['TempCycle'] <= 30)]
                x = tdf[[item]].to_numpy()
                x = x[np.logical_not(np.isnan(x))]
                try:
                    percentile95 = np.percentile(x, q=95)
                except:
                    percentile95 = np.nan
                try:
                    percentile90 = np.percentile(x, q=90)
                except:
                    percentile90 = np.nan
                try:
                    percentile75 = np.percentile(x, q=75)
                except:
                    percentile75 = np.nan
                try:
                    percentile50 = np.percentile(x, q=50)
                except:
                    percentile50 = np.nan
                row = [temp, item, percentile95, percentile90, percentile75, percentile50]
                LifeTempSummary = LifeTempSummary.append(pd.Series(row, index=LifeTempSummary.columns),
                                                         ignore_index=True)

        return LifeTempSummary

    def OutputToExcel(self, df, filename):
        outdir = filename.split('/')
        loc = ''
        for i in range(0, len(outdir) - 1):
            loc = os.path.join(loc, outdir[i])
            if not os.path.exists(loc):
                os.mkdir(loc)
        df.to_csv(path_or_buf=filename,index=False)

    def PlotAllSamples(self, df, signals, profile, group, filename):  # Input subset of data for Plotting
        pdf = df.pivot(index=signals[1],columns=signals[0],values=signals[2])
        ax = pdf.plot(figsize=(12,7),title=filename[:-5],sort_columns=True,xlable=signals[1],ylable=signals[2])
        ax.axhline(y=0.25,linestyle='--',color='r')
        ax.axhline(y=0.2,linestyle='--',color='k')
        fig = ax.get_figure()
        # samples = signals['key'].nunique()
        # # print(signals['key'])
        # self.logger.info('Number of Samples {}'.format(samples))
        # keys = signals.key.unique()
        # row, col = signals.shape
        # # print(col - 2, samples)
        # for pType in ['New Profile', 'Old Profile']:
        #     filteredKeys = list(filter(lambda k: pType in k, keys))
        #     fig, axs = plt.subplots(nrows=col - 2, ncols=len(filteredKeys) + 1, sharey='row')
        #     # fig.canvas.set_window_title('Test')
        #     for i in range(0, len(filteredKeys)):
        #         print('i={}'.format(i))
        #         tdf = signals.loc[signals['key'] == filteredKeys[i]]
        #         t2df = tdf.drop('key', axis=1)
        #         for j in range(0, col - 2):
        #             t2df.plot(x=0, y=j + 1, style='.', legend=False, ax=axs[j, i])
        #             t2df.plot(x=0, y=j + 1, style='.', legend=False, label=filteredKeys[i][-10:],
        #                       ax=axs[j, len(filteredKeys)])
        #             if j == 0:
        #                 axs[0, i].set_title("{}".format(filteredKeys[i][-10:]))
        #             if i == 0:
        #                 axs[j, 0].set_ylabel(t2df.columns[j + 1])
        #         # plt.title(keys[i])
        folder = r'charts/{}/{}/'.format(profile, group)
        outdir = folder.split('/')
        loc = ''
        for i in range(0, len(outdir) - 1):
            loc = os.path.join(loc, outdir[i])
            if not os.path.exists(loc):
                os.mkdir(loc)
        fig.savefig(folder + filename,bbox_inches='tight')

    def PlotEverySamples(self, signals, folder):  # Input subset of data for Plotting
        samples = signals['key'].nunique()
        # print(signals['key'])
        self.logger.info('Number of Samples {}'.format(samples))
        keys = signals.key.unique()
        row, col = signals.shape

        x = col - 2
        if x <= 3:
            r = 1
            c = x
        else:
            c = 3
            r = 1
            while x > 3:
                x -= 3
                r += 1

        print(r, c)
        for i in keys:
            fig, axs = plt.subplots(nrows=r, ncols=c, constrained_layout=True)
            print('i= {}'.format(i))
            tdf = signals.loc[signals['key'] == i]
            t2df = tdf.drop('key', axis=1)
            print(t2df)
            for rr in range(0, r):
                print('rr = {}'.format(rr))
                for cc in range(0, c):
                    if (rr == 0) & (cc == 0):
                        plt.title(i)
                    print('cc = {}'.format(cc))
                    y = (rr * 3) + cc + 1
                    print('sample = {}'.format(y))
                    try:
                        t2df.plot(x=0, y=y, style='.', legend=False, ax=axs[rr, cc])
                        axs[rr, cc].set_ylabel(t2df.columns[y])
                    except:
                        print('Out of Range')

            plt.title(i)
            plt.savefig(folder + i[:-4] + '.png')


if __name__ == "__main__":
    test = PTCE_data()
