import pandas as pd
import numpy as np
import rainflow
import matplotlib.pyplot as plt
import math
import os
from collections import defaultdict


def extract_cycles_binning(series: pd.Series, binsize: float = None) -> pd.DataFrame:
    """
    Extracts rainflow cycles from time series data

        Arguments:
            series (pd.Series): time series data
            binsize (float): bin size (default None)
        Returns:
            pd.DataFrame
    """

    vals = {"rng": [], "mean": [], "count": []}
    if binsize is not None:
        for rng, mean, count, _, _ in rainflow.extract_cycles(series):
            vals["rng"].append(math.ceil(rng / binsize) * binsize)
            vals["mean"].append(math.ceil(mean / binsize) * binsize)
            vals["count"].append(count)
    else:
        for rng, mean, count, _, _ in rainflow.extract_cycles(series):
            vals["rng"].append(rng)
            vals["mean"].append(mean)
            vals["count"].append(count)

    return pd.DataFrame(vals)


def count_cycles_binning(series: pd.Series, binsize: float = None) -> pd.DataFrame:
    """
    Extracts binned rainflow cycles from time series data

        Arguments:
            series (pd.Series): time series data
            binsize (float): bin size (default None)
        Returns:
            pd.DataFrame
    """

    cycle_counts = defaultdict(float)
    if binsize is not None:
        for rng, _, count, _, _ in rainflow.extract_cycles(series):
            cycle_counts[math.ceil(rng / binsize) * binsize] += count
    else:
        for rng, _, count, _, _ in rainflow.extract_cycles(series):
            cycles_counts[rng] += count
    rng = []
    count = []
    for cycle in sorted(cycle_counts.items()):
        rng.append(cycle[0])
        count.append(cycle[1])
    return pd.DataFrame({"range": rng, "count": count})

if __name__ == "__main__":
    df = pd.read_csv(r"XXX")
    counts_drive = pd.read_csv(r"diagnostic/Attributes_drive_cycles/Counts_Drive.csv")
    print(df.head(15))

    Tmax = df.T13.max()
    Tmin = df.T13.min()
    Trange = Tmax - Tmin

    temp = count_cycles_binning(df["T13"], 0.5)

    print(Tmax,Tmin,Trange)

    print(temp)

    print(temp["count"].sum())

    fig = plt.figure(figsize=(20, 10))
    ax = fig.add_subplot(111)
    ax.set_yscale("log")


    ax.bar(temp["range"], temp["count"], width=0.25)

    ax.set_xlabel("Range (˚C)")
    ax.set_ylabel("Count")
    plt.title("Contactor Terminal Temperature delta_T histogram over life for drive cycles")


    temp['Mult'] = temp['count']*temp['range']
    Tsum = temp[temp['range'] >= 1.0]['Mult'].sum()

    for x in temp['range']:
        print(x)
        a = x/counts_drive['range']
        if x == .5:
            lst = a
        else:
            zipped_lists = zip(lst, a)
            lst = [x + y for (x, y) in zipped_lists]
    print(lst)

    lst_sum = sum(lst[1:])
    print('List Sum: {}:'.format(lst_sum))
    print('List Sum: {}:'.format(counts_drive['total_count'].sum()))
    ratio = counts_drive['total_count'].sum()/lst_sum
    print ('ratio: {}'.format(ratio))

    # plt.show()
    # fig.savefig("histogram.png", transparent=False)

