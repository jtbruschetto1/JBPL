import os
import math
import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
import statistics

class fileParser:
    def __init__(self):
        os.chdir(r"/Users/jtbruschetto/Documents/OneDrive - Rivian Automotive, LLC/Projects/HVDB/Sensata/Python Data")
        data_folder = r"grouped_ptce_data/"
        files = os.listdir(data_folder)
        self.Controller(files,data_folder)

    def Controller(self, files, root):
        print('Starting Controller')
        # print(files)
        appendFiles = {1:['PTC2_DVR3p1.csv','PTC2_DVR3p2.csv'],
                       2:['PTC3_GDL1.csv','PTC3_GDL2.csv'],
                       3:['PTC3_WDL1.csv','PTC3_WDL2.csv','PTC3_WDL3.csv','PTC3_WDL4.csv']}
        # print(appendFiles)
        # if appendFiles in files:
        # print('enter IF statement')
        load = 1
        while load > 0:
            load = int(input(
                'Program Options:\n  - 3 Run All \n  - 2 Run Append Files \n  - 1 Run Single File \n  - 0 Exit \nSelection:'))
            if load >= 2:
                dropFiles = []
                for key in appendFiles.keys():
                    dropFiles.extend(appendFiles[key])
                    print('Starting Key: {}'.format(key))
                    print('-- Including {}'.format(appendFiles[key]))
                    self.appendExcel(appendFiles[key],root)
                files = [x for x in files if x not in dropFiles]
            files = [k for k in files if '.csv' in k]
            if load == 3:
                for i in range(0, len(files)):
                    print('Starting: {}'.format(files[i]))
                    self.splitExcel(root + files[i], files[i])
            if load == 1:
                k1 = range(0, len(files)+1, 1)
                d1 = dict(zip(k1,files))
                file_selection = int(input(
                    'file Options: {} \n Select the index of the file you would like to run\nSelection:'
                        .format(d1)))
                print('Starting: {}'.format(files[file_selection]))
                self.splitExcel(root + files[file_selection], files[file_selection])

    def appendExcel(self, keys, root):
        df = pd.DataFrame()
        for i in keys:
            Profile = i.split('_')[0]
            Group = i.split('_')[1][:-6]
            # if '.csv' in Group:
            #     Group = Group[:-5]
            print('Profile: {} / Group: {}'.format(Profile,Group))
            dft = pd.read_csv(root + i, dtype='float64')
            row, col = dft.shape
            print('{} size: {}/{}'.format(i,row, col))
            df = pd.concat([df,dft])
        row, col = df.shape
        print('Dataframe size: {}/{}'.format(row, col))
        samples = int((col - 3) / 2)
        for i in range(0, samples):
            print('---- Sample {}/{}'.format(i+1,samples))
            tdf = df.iloc[:, [0, 1, i + 2, 2 + samples, 3 + samples + i]]
            self.ProcessData(tdf,keys[0],Profile,Group)

    def splitExcel(self, file,files):
        Profile = files.split('_')[0]
        Group = files.split('_')[1][:-4]
        df = pd.read_csv(file, dtype='float64')
        row, col = df.shape
        print(row,col)
        samples = int((col - 3)/2)
        for i in range(0, samples):
            tdf = df.iloc[:, [0, 1, i+2, 2+samples, 3+samples+i]].copy()
            self.ProcessData(tdf,files,Profile,Group)

    def ProcessData(self,df,files,Profile,Group):
        tdf = df.copy()
        Loc = tdf.columns[2]
        tdf.columns = ['Time', 'Current', 'ContactResistance', 'AmbientTemp', 'DutTemp']
        tdf['Time'] = tdf['Time'] * 60 * 60
        tdf['stepTime'] = tdf.Time.diff()
        tdf['TempCycle'] = np.where((tdf.AmbientTemp >= 0) & (tdf.AmbientTemp.shift(1) < 0), 1, 0)
        tdf['TempCycleCount'] = tdf['TempCycle'].cumsum()
        tdf['CurrentCycleStart'] = np.where((tdf['Current'] > 10) & (tdf['Current'].shift(1) < 10), 1, 0)
        tdf['CurrentCycleEnd'] = np.where((tdf['Current'] < 0) & (tdf['Current'].shift(1) > 10), 1, 0)
        tdf['CurrentCycleCount'] = tdf['CurrentCycleStart'].cumsum()
        tdf['CurrentCycleCompare'] = tdf['CurrentCycleEnd'].cumsum()
        tdf['CurrentCycleIndicator'] = tdf['CurrentCycleCount'] - tdf['CurrentCycleCompare']
        a = tdf.CurrentCycleIndicator != 0
        tdf['CurrentCycleID'] = a.cumsum() - a.cumsum().where(~a).ffill().fillna(0).astype(int)
        tdf['VoltageDrop'] = tdf['Current'] * tdf['ContactResistance']
        tdf['TempDelta'] = tdf['DutTemp'] - tdf['AmbientTemp']
        tdf['I2RT'] = tdf['Current'] * tdf['Current'] * tdf['stepTime'] * tdf['ContactResistance'] / 1000
        tdf['I2RT/MC'] = tdf['I2RT'] / 21.9065
        tdf['isumsq'] = tdf['Current'] ** 2
        self.OutputToExcel(tdf,'raw_ptce_data/{}/{}/{}_{}.csv'.format(Profile,Group,files[:-4], Loc))

    def OutputToExcel(self,df,filename):
        outdir = filename.split('/')
        loc = ''
        for i in range(0, len(outdir) - 1):
            loc = os.path.join(loc, outdir[i])
            if not os.path.exists(loc):
                os.mkdir(loc)
        print(filename)
        df.to_csv(path_or_buf=filename,index=False)


if __name__ == "__main__":
    test = fileParser()
