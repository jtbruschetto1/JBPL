import math
import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
import statistics


'''
Calculations gathered from https://www.grc.nasa.gov/www/k-12/Numbers/Math/Mathematical_Thinking/sun12.htm
Weather Data https://www.ncdc.noaa.gov/ghcn/comparative-climatic-data
    ASCII Data https://www1.ncdc.noaa.gov/pub/data/ccd-data/
UVA Studies from: https://journals.ametsoc.org/view/journals/atot/22/12/jtech1823_1.xml

solar spectral radiance data: https://lasp.colorado.edu/home/sorce/data/ssi-data/
    from orbit, need to add ozone absorbtion factor

department of energy: https://www.energy.gov/eere/solar/solar-radiation-basics

Ozone - newport solar - https://www.newport.com/n/solar-uv-and-ozone-layer#:~:text=These%20gases%20block%20all%20short,to%20the%20production%20of%20ozone. 
    heat maps: https://maps.nrel.gov/nsrdb-viewer/?aL=x8CI3i%255Bv%255D%3Dt%26VRLt_G%255Bv%255D%3Dt%26VRLt_G%255Bd%255D%3D1%26ozt_aP%255Bv%255D%3Dt%26ozt_aP%255Bd%255D%3D2&bL=clight&cE=0&lR=0&mC=20.385825381874263%2C-68.994140625&zL=4
    
Internal Weathering RTS Spec: https://rivian.sharepoint.com/:w:/r/sites/pdstandards/_layouts/15/Doc.aspx?sourcedoc=%7B8a7aa537-b03b-47cd-86ed-de036da5972a%7D&action=view&wdAccPdf=1
    a. For samples above the beltline, complete exposure at 1240.8 kJ/m2
    b. For samples at the beltline, complete exposure at 601.6 kJ/m2
    c. For samples below the beltline, complete exposure at 225.6 kJ/m2
    d. For all High Duty Samples, complete exposure at 1504 kJ/m2

Industry Standards
SAE J2412 - Accelerated Exposure of Automotive Interior Trim Components Using a Controlled Irradiance Xenon-Arc Apparatus
    https://saemobilus.sae.org/content/J2412_201508/
    Controls	            Dark Cycle		        Light Cycle	
                            Target	Tolerance	    Target	Tolerance
    Automatic Irradiance	None		            0.55W/m^2	± 0.02 Wm2 nm−1
    Black Panel Temperature	38 °C	± 2.5 °C	    89 °C	± 2.5 °C
    Dry Bulb Temperature	38 °C	± 3 °C	        62 °C	± 2 °C
    Relative Humidity	    95%	± 10%	            50%	± 10%
    Radiant Exposure	    Not applicable		    Contractual Agreement	
    Cycle Duration	        1 hour ± 6 minutes	    3.8 hours ± 6 minutes

ASTM G113 Natural and Artificial Weathering Tests - https://ewb.ihs.com/#/document/YMQWSFAAAAAAAAAA?qid=637478997328262824&sr=re-1-10&kbid=4%7C20027&docid=943279406#h65e9aee7
    
GMW GMW14444 - Material Related Interior Part Performance - Issue 9; English
    https://ewb.ihs.com/#/document/ZGOBRGAAAAAAAAAA?sr=dv-dl&fdocid=ULLVCGAAAAAAAAAA&fdocpagenum=2#hfd4bb983
    Code 1 Horizontal surface, up to 105C   -- 115C and 26MJ/m^2 (visible parts) or 1240kJ/m^2 (customers feet)
    Code 2 Direct sunlight not in Code 1    -- 100C and 40MJ/m^2 (contacting body) or 601kJ/m^2 (customers feet)
    Code 3 No Direct sunlight               -- 100C and 22MJ/m^2 (contacting body) or 263kJ/m^2 (customers feet)
    
GMW GMW3417 - Natural Weathering Exposure Tests for Interior Trims/Materials
    https://ewb.ihs.com/#/document/JVQAGGAAAAAAAAAA?sr=dv-dl&fdocid=ZGOBRGAAAAAAAAAA&fdocpagenum=5#ha828f458
    This is an outdoor test without filtering 
    Energy includes wavelengths greater than 400nm
    test takes several months

Ultra accelerated weathering reference:  https://www.atlas-mts.com/-/media/ametekatlas/files/knowledgecenter/sunspots/volume-issue/2010-03_atlas_sunspots_v0140issue88.pdf?dmc=1&la=en&revision=53b2d543-4378-4723-8449-b2876d3d04a5

    
Day is offset by april 21 vernal equinox

'''


class SolarEnergy:
    def __init__(self):
        # Solar Constant 1370 #W/m^2 Broad Spectrum Light
        # self.solarconst = 60.5 # W/m^2 UVA Radiation
        self.solarconst = 60.7
        self.countries = pd.read_csv(r"reference/Countries_Altitude.csv")
        self.countries.sort_values(by="Adjusted Latitude", inplace=True)

        self.testClass = {}
        self.testClass['A'] = 1240.8
        self.testClass['B'] = 601.6
        self.testClass['C'] = 225.6
        self.testClass['D'] = 1504

        self.lightCycle = 3.8 * 60 * 60  # hr * min/hr * sec/min
        self.darkCycle = 1 * 60 * 60  # hr * min/hr * sec/min
        self.totalCycle = (self.lightCycle + self.darkCycle) / 60 / 60  # in Hours
        self.lightEnergy = 0.55 * self.lightCycle  # W/m2 * s = J/m2

        # 37.6 kJ/m2 per 24-hour day
        self.dailyCycles = 37.6 * 1000 / 7524
        self.dailyRunTime = self.totalCycle * self.dailyCycles
        self.dailyEnergy = self.lightEnergy * self.dailyCycles / 1000
        self.testDuration = self.testClass['A'] / self.dailyEnergy  # Class A 1240.8 kJ/m^2

        self.lightEnergyEquivalent = 60.5 * self.lightCycle / 1000  # kW/m2 * s  = kJ/m2
        self.dailyEnergyEquivalent = self.lightEnergyEquivalent * self.dailyCycles / 1000
        self.testEnergyEquivalent = self.dailyEnergyEquivalent * self.testDuration

    def print_test_requirements(self):
        print("Cycle Details\n   Light Cycle {:.2f}s\n   Dark Cycle {:.2f}s\n   Total Cycle Time {:.2f}hr\n"
              "   Light Energy per Cycle {:.2f}J/m^2\n"
              .format(self.lightCycle, self.darkCycle, self.totalCycle, self.lightEnergy))
        print("Test Details\n   Cycles Needed Daily {:.2f}\n   Daily Run Time {:.2f}hr\n"
              "   Daily Energy {:.2f}kJ/m^2\n   Test Duration {:.2f} days\n"
              .format(self.dailyCycles, self.dailyRunTime, self.dailyEnergy, self.testDuration))
        print("Test Equivalency Details\n   Light Energy Equivalency {:.2f}kJ/m2\n"
              "   Daily Energy Equivalency {:.2f}MJ/m2\n   Test Energy Equivalency {:.2f}MJ/m^2\n"
              .format(self.lightEnergyEquivalent, self.dailyEnergyEquivalent, self.testEnergyEquivalent))
    def sun_position(self, day):
        day = day - 21
        theta = 23.5 * math.sin((day/365)*2*math.pi)
        return theta

    def energy_per_day(self, theta, latitude):
        theta = theta * math.pi / 180
        latitude = latitude * math.pi / 180
        solarconst_for_day = self.solarconst * math.cos(latitude - theta)
        length_day = (24 / math.pi) * math.acos((-1 * math.tan(theta))*(math.tan(latitude)))  # Hour
        energy_day = solarconst_for_day * 3.6 * length_day / math.pi  # w/m^2 * min * sec * kW / w  * hr / 3.14
        return energy_day

    def energy_per_year(self, latitude):
        annual_energy = 0
        mean_daily_energy = []
        for x in range(1, 366):
            theta = self.sun_position(x)
            energy = self.energy_per_day(theta, latitude)  # kJ/m2
            mean_daily_energy.append(energy)
            annual_energy = annual_energy + energy  # kJ/m2

        mean_daily_energy = statistics.mean(mean_daily_energy)
        print("mean daily energy {:.2f}".format(mean_daily_energy))
        return annual_energy, mean_daily_energy  # kJ/m2

    def lifetime_energy(self, latitude):
        annual_energy , mdl = self.energy_per_year(latitude)
        total_energy = annual_energy * 10  # 10 Year life # kJ/m2
        return total_energy # kJ/m2

    def energy_per_Country(self):
        Country_energy = pd.DataFrame(columns=["Country", "Annual Energy"])
        for index, row in self.countries.iterrows():
            annual_energy, mdr = self.energy_per_year(row['Adjusted Latitude'])
            Country_energy = Country_energy.append([{"Country": row["Country"],"Annual Energy": annual_energy}])
        # Country_energy.set_index(["Country"], inplace=True)
        print(Country_energy)
        newlist = self.countries.set_index("Country").join(Country_energy.set_index("Country"))
        print(newlist)

        fig, axes = plt.subplots(1,2)
        newlist.plot.scatter(x='Adjusted Latitude', y='Annual Energy', ax=axes[0], grid=True)
        y = [2, 8, 20, 40, 100, 130, 145, 160, 171, 185, 193, 200]
        x = 1
        for index, row in newlist.iterrows():
            if x in y:
                axes[0].annotate(index, (row['Adjusted Latitude'], row['Annual Energy']))
            x += 1
        newlist['Adjusted Latitude'].plot.hist(ax=axes[1], grid=True, legend=True)

    def load_spectral_data(self):
        spectraldata = pd.read_csv(r"reference/spectraldatacolaradoedu.txt",delim_whitespace=True, skiprows=73,)
        spectraldata.columns = ['Date', 'Julian Date', 'Min Wavelength (nm)', 'Max Wavelength', 'Mode', 'version',
                                     'Irradiance (W/m2)', 'Irradiance uncertanty', 'quality']
        spectraldata['DateTime'] = pd.to_datetime(spectraldata['Date'].astype(str), format='%Y%m%d')
        spectraldata.drop(columns=['Date', 'Julian Date', 'version'])
        spectraldata = spectraldata[spectraldata['quality'] != 1]
        spectraldata.convert_dtypes()
        atm_data = self.load_atm_transmissivity()
        return spectraldata, atm_data

    def spectral_data_for_day(self,spectraldata, atm_data, day):  # YYYY-MM-DD format
        spectral_day = spectraldata[spectraldata['DateTime'] == day]
        print(spectral_day)


        spectral_day.plot(x='Min Wavelength (nm)', y='Irradiance (W/m2)', grid=True)
        plt.title('Spectral Data for {}'.format(day))
        plt.legend()
        # plt.figure()

    def spectral_power_density(self,spectraldata, atm_data, day, MinWave, MaxWave):
        spectral_pd = spectraldata[spectraldata['DateTime'] == day]
        spectral_pd = spectral_pd[(spectral_pd['Min Wavelength (nm)'] <= MaxWave) &
                                  (spectral_pd['Min Wavelength (nm)'] >= MinWave)]
        spectral_merge_pd = spectral_pd[['Min Wavelength (nm)','Irradiance (W/m2)']]
        # print(spectral_merge_pd)
        atm_merge_data = atm_data[['Wave Length', 'Transmission']]
        # print(atm_merge_data)
        merge_data = pd.merge_asof(spectral_merge_pd,atm_merge_data, left_on='Min Wavelength (nm)', right_on='Wave Length',direction='nearest')
        merge_data['Escape Irradiance (W/m2)'] = merge_data['Irradiance (W/m2)'] * merge_data['Transmission']
        # print(merge_data)

        powerDensity = merge_data['Escape Irradiance (W/m2)'].sum()

        # print('Total Power Density from {}-{}nm: {:.2f}W/m2'.
        #       format(MinWave, MaxWave,powerDensity ))

        # spectral_pd.plot(x='Min Wavelength (nm)', y='Irradiance (W/m2)', label='Irradiance', grid=True)
        # merge_data.plot(x='Wave Length', y='Escape Irradiance (W/m2)', label='Escape Irradiance')
        # plt.plot(atm_data['Wave Length'], atm_data['ETR'], label='ETR')
        # plt.plot(atm_data['Wave Length'], atm_data['Global Tilt'], label='Tilt')
        # plt.plot(atm_data['Wave Length'], atm_data['Circumsolar'], label='Circumsolar')
        # plt.title('UV Power Density {}'.format(day))
        # plt.legend()
        # plt.figure()
        return powerDensity

    def spectral_power_density_range(self,spectraldata,atm_data,MinWave,MaxWave):
        dates = spectraldata['DateTime'].unique()
        powerDensity = []
        x = 0
        for day in dates[:2000]:
            print('Percent Complete {:.2f}%'.format(x/2000*100))
            pd = self.spectral_power_density(spectraldata, atm_data, day, MinWave, MaxWave)
            powerDensity.append(pd)
            x += 1

        powerDensityMean = statistics.mean(powerDensity)
        print("Solar Mean Power Density Value {:.2f}W/m2 as Measured at Sea Level".format(powerDensityMean))

        plt.plot(powerDensity)
        plt.legend()
        plt.title('Power Density Average')
        # plt.figure()
        return powerDensityMean

    def load_atm_transmissivity(self):
        atm_data = pd.read_csv(r"reference/astmg173.csv")
        atm_data.columns = ['Wave Length', 'ETR', 'Global Tilt', 'Circumsolar', 'Transmission']
        atm_data.drop(columns=['Transmission'])
        atm_filtered_data = atm_data[(atm_data['Wave Length'] <= 400) &
                                  (atm_data['Wave Length'] >= 270)]
        atm_filtered_data['Transmission'] = atm_filtered_data['Circumsolar']/atm_filtered_data['ETR']

        print(atm_filtered_data)
        plt.plot(atm_filtered_data['Wave Length'], atm_filtered_data['ETR'], label='Orbital Intensity')
        plt.plot(atm_filtered_data['Wave Length'], atm_filtered_data['Global Tilt'], label='Global Tilt Intensity')
        plt.plot(atm_filtered_data['Wave Length'], atm_filtered_data['Circumsolar'], label='Direct and Circumsolar intensity')
        plt.title('UV intensity per measurement type ')
        plt.legend()
        plt.figure()
        plt.plot(atm_filtered_data['Wave Length'], atm_filtered_data['Transmission'], label='Transmission %')
        plt.title('UV Transmission Percent per Wavelength')
        plt.legend()
        plt.title('UV Transmission Percent per Wavelength')
        # plt.legend()
        return atm_filtered_data

if __name__ == "__main__":
    plt.close("all")
    # # Comparison to reference
    # test = SolarEnergy()
    # energy = test.energy_per_day(23.5, 42)
    # print("{:e} J/m^2".format(energy))

    # #Annual Energy Test
    # test2 = SolarEnergy()
    # annual_energy, mean_daily_energy = test2.energy_per_year(64.8)
    # print("Annual {:e} J/m^2 | Mean Daily {:e} J/m^2 ".format(annual_energy, mean_daily_energy))

    # Annual Energy Test per country
    # test3 = SolarEnergy()
    # test3.energy_per_Country()

    # Life Time Energy Requirement
    test4 = SolarEnergy()
    # spectraldata, atm_data = test4.load_spectral_data()
    # test4.spectral_data_for_day(spectraldata, atm_data, '2003-04-14')
    # test4.spectral_power_density(spectraldata, atm_data, '2003-04-14', 270, 400.0)
    # powerDensityMean = test4.spectral_power_density_range(spectraldata, atm_data, 270, 400)
    # test4.solarconst = 60.7
    test4.print_test_requirements()
    total_energy = test4.lifetime_energy(33)
    print("Warranty Life Energy {:.2f} MJ/m^2 ".format(total_energy / 1000))  # {:e}

    plt.show()

