import math
import pandas as pd
import matplotlib.pyplot as plt
from scipy import stats
import numpy as np
import statistics


if __name__ == "__main__":
    data = pd.read_csv(r"reference/Cloud Coverage Data US.csv", header=[0, 1]).drop('YRS', axis=1, level=0)\
        .reorder_levels([1, 0], axis=1).set_index('State').reset_index()\
        # \
        # .stack(level=0).sort_index(level=1)

    print(data)
    cols = ['JAN', 'FEB', 'MAR', 'APR', 'MAY', 'JUN', 'JUL', 'AUG', 'SEP', 'NOV', 'DEC', 'Annual']
    data = data[cols]

    # data.plot.bar(y='Annual', rot=90)

    cl_data = data[data.index.isin(['CL'], level=1)]
    cl_annual_data = cl_data['Annual'].div(365)
    cl_annual_data.plot(kind='hist', density=True)
    print(cl_annual_data)
    mean = cl_annual_data.mean()
    print(mean)

    plt.axvline(x=mean,color='r')
    plt.figure()
    pc_data = data[data.index.isin(['PC'], level=1)]
    pc_annual_data = pc_data['Annual'].div(365)
    pc_annual_data.plot(kind='hist', density=True)
    mean = cl_annual_data.mean()
    print(mean)

    plt.axvline(x=mean, color='r')
    plt.show()




