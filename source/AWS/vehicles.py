
import awswrangler as wr
import pandas as pd
from datetime import datetime
import boto3
import os
import matplotlib.pyplot as plt
#matplotlib inline

''' 
Log into AWS SSO 
aws sso login --profile AthenaQuery-786034295618
'''

os.environ["AWS_PROFILE"] = "AthenaQuery-786034295618"
boto3.setup_default_session(region_name="us-west-2")

query = """
SELECT DISTINCT vehicle_name 
FROM "prototype_can_data_v1"."signals"
WHERE 
    year=2021 
    AND month=03
"""
df = wr.athena.read_sql_query(query, database="prototype_can_data_v1", ctas_approach=False).reset_index()

print(df)
now = datetime.now()
df.to_csv(path_or_buf='VINs_{}.csv'.format(now.strftime("%Y_%m_%d_%H_%M_%S")))


# data = (
#     df.sort_index()
#     .loc[(df.timestamp >= "2020-11-11 00:00:00") & (df.timestamp <= "2020-11-11 00:08:23")]
#     .drop_duplicates(subset=["timestamp", "signal_name", "signal_value"])
#     .pivot_table(index="timestamp", columns="signal_name", values="signal_value")
# #     .ffill()
# )
# print(data.head())
#
# plt.figure()
# data.plot()
# plt.show()