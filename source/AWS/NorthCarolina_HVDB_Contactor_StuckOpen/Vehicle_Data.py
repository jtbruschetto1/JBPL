import awswrangler as wr
import pandas as pd
from datetime import datetime
import boto3
import os
import matplotlib.pyplot as plt
#matplotlib inline

os.environ["AWS_PROFILE"] = "AthenaQuery-786034295618"
boto3.setup_default_session(region_name="us-west-2")

query = """
SELECT 
    timestamp, 
    signal_name, 
    signal_value
FROM 
    "prototype_can_data_v1"."signals" 
WHERE 
    vehicle_name='VP2-068 North Carolina'
    AND year=2021 
    AND month=03
    AND day = 24
    AND signal_name IN (
        'I_contactor_dcfc_low',
        'I_contactor_dcfc_high',
        'I_contactor_primary_low',
        'I_contactor_primary_high',
        'I_contactor_secondary_low',
        'I_contactor_secondary_high',
        'DCDC_P_CurrentHV',
        'DCDC_P_CurrentLV',
        'DCDC_S_CurrentHV',
        'DCDC_S_CurrentLV',
        'BMS_P_PackIManCurrent_Slow',
        'I_pack_high_accuracy',
        'I_pack_high_speed',
        'BMS_P_PackIManCurrent_Fast',
        'BMS_P_BatteryStatus', 'BMS_OpState', 'BattSts',
        'BMS_P_SafeState', 
        'BMSSafeState',
        'BMS_P_ModeManDCFCContactorFeedback',
        'BMS_P_ModeManPrechargeStateFail',
        'BMS_P_ModeManBattContCtrlSts',
        'BMS_P_ModeManContactorControlAct',
        'DCDC_P_Mode',
        'DCDC_S_Mode',
        'BMS_P_CustSoc', 'User_SOC', 'UserSoc',
        'I_contactor_DCFC_negative',
        'I_contactor_DCFC_positive',
        'I_contactor_precharge',
        'I_contactor_primary_negative',
        'I_contactor_primary_positive',
        'I_contactor_secondary_negative',
        'I_contactor_secondary_positive',
        'I_DCDC_primary_HV',
        'I_DCDC_primary_LV',
        'I_DCDC_secondary_HV',
        'I_DCDC_secondary_LV',
        'N_BMS_state',
        'N_BMS_state_safe',
        'N_contactor_DCFC_state',
        'N_contactor_main_state',
        'N_pack_HVIL_state',
        'rp_SOC_pack_user',
        'rp_SOC_pack_user_max',
        'rp_SOC_pack_user_min',
        'T_busbar_DCFC_negative',
        'T_busbar_DCFC_positive',
        'T_busbar_main_positive',
        'T_shunt',
        'T_shunt_air',
        'T_shunt_estimated',
        'V_contactor_!_auxiliary_negative',
        'V_contactor_!_auxiliary_positive',
        'V_contactor_rail_!',
        'Ver_HVDB_negative_serial_number',
        'Ver_HVDB_positive_serial_number',
        'BMS_P_CPU#_Load_Current',
        'BMS_P_CPU#_Load_Average',
        'BMS_P_CPU#_Load_Max',
        'BMS_P_CPU#_Stack_Max',
        'BMS_P_CPU#_Interrupt_Stack_Max'
    )
LIMIT 1000000 ;
"""

# AND
# signal_name
# IN(

# )


df = wr.athena.read_sql_query(query, database="prototype_can_data_v1", ctas_approach=False).reset_index()
print(df.shape)
data = (
    df.sort_index()
    # .loc[(df.timestamp >= "2020-11-11 00:00:00") & (df.timestamp <= "2020-11-11 00:08:23")]
    .drop_duplicates(subset=["timestamp", "signal_name", "signal_value"])
    .pivot_table(index="timestamp", columns="signal_name", values="signal_value")
#     .ffill()
)
print(data.head())


now = datetime.now()
data.to_csv(path_or_buf='Data/NorthCarolina_Vehicle_data_{}.csv'.format(now.strftime("%Y_%m_%d_%H_%M_%S")))

# plt.figure()
# data.plot()
# plt.show()