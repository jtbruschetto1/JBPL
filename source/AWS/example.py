import awswrangler as wr
import pandas as pd
import boto3
import os
import matplotlib.pyplot as plt
#matplotlib inline

os.environ["AWS_PROFILE"] = "AthenaQuery-786034295618"
boto3.setup_default_session(region_name="us-west-2")

query = """
SELECT 
    timestamp, 
    signal_name, 
    signal_value
FROM 
    "prototype_can_data_v1"."signals" 
WHERE 
    vehicle_name='VP1-003 Avocado'
    AND year=2020 
    AND month=11
    AND day=11
    AND signal_name IN (
        'ESP_Wheel_Speed_Left_Front',
        'ESP_Vehicle_Speed'
    )
    AND filepath='VP1-003 Avocado/Driving 2020-11-10 23-59-47-119016 Partition 0.vsb.zip'
"""
df = wr.athena.read_sql_query(query, database="prototype_can_data_v1", ctas_approach=False).reset_index()

data = (
    df.sort_index()
    .loc[(df.timestamp >= "2020-11-11 00:00:00") & (df.timestamp <= "2020-11-11 00:08:23")]
    .drop_duplicates(subset=["timestamp", "signal_name", "signal_value"])
    .pivot_table(index="timestamp", columns="signal_name", values="signal_value")
#     .ffill()
)
print(data.head())

plt.figure()
data.plot()
plt.show()