
import applescript

script = """
			tell application "Microsoft Outlook"
				set theContent to "Mail Content"
				-- substitute 'plain text content' for 'content' if you don't want to use HTML
				set theMessage to make new outgoing message with properties {subject:"Mail Subject", content:theContent}
				make new recipient with properties {email address:{address:"user_a@example.com"}} at end of to recipients of theMessage
				-- recipient type can be 'to recipient', 'cc recipient' or 'bcc recipient'
				make new recipient with properties {email address:{address:"user_b@example.com"}} at end of cc recipients of theMessage
				-- optionally open the message instead...
				-- open theMessage
				send theMessage
			end tell
  	"""

applescript.run(script)

