import os
import math
import time
import pandas as pd
import datetime as dt
import matplotlib.pyplot as plt
import matplotlib.ticker as mticker
import numpy as np
import statistics
from scipy.stats.distributions import chi2
import rainflow
import logging
from configparser import ConfigParser



class DriveCycles(object):
    def __init__(self,config_file):
        os.chdir('/tools/reliability')
        logging.basicConfig(format='%(asctime)s - %(levelname)s - %(message)s')
        self.logger = logging.getLogger()
        self.logger.setLevel(logging.INFO)

        self.logger.info('Loading Configuration')
        self.config = ConfigParser()
        print(config_file)
        self.config.read(config_file)
        self.Basic = dict(self.config.items('Basic'))
        self.Basic['project_dir'] = os.path.join(self.Basic['project_root'],self.Basic['project_name'])
        self.project_dir()

        self.controller()

    '''Controller'''
    def controller(self):
        '''
        Steps:
        1) Get Duty Cycle Information
        2) Get Thermal Test Results
            Per thermal test
            a)Calculate Delta Ts
            b)Calculate Time at Temp
        3) Run Thermal Test Results against Duty Cycles
            Multiply thermal results by:
            a) drive cycles (days / year)
            b) test cycles per drive cycle (cycles / day)
        4) Sum results from all drive cycles
        5) Damage Model Calculation
            a)Coffin Manson for PTC / TS
            b)Arrhenius  for HTOE

        :return:
        '''
        self.logger.info('Starting Controller Process')
        self.get_duty_cycles()
        self.get_Rel_information()
        thermal_simulation_df, max_run_temp = self.get_thermal_simulations()
        print(max_run_temp)
        self.Test_list = thermal_simulation_df['Test'].unique()
        RF_counts = pd.DataFrame()
        TT_hist = pd.DataFrame()
        for Test in self.Test_list:
            if Test == '110C_ON':
                div = 2
            else:
                div = 5
            Run = thermal_simulation_df[thermal_simulation_df['Test']==Test]
            sub_run = Run[['time', 'dt_time', 'chipM']]
            counts = self.rain_flow(sub_run['chipM'])
            hist = self.time_at_temp(sub_run)
            self.OutputToExcel(counts, 'SimStress_{}_RainflowCounts.'.format(Test))
            self.OutputChart(counts, 'range', 'counts','bar', 'SimStress_{}_RainFlowCounts.jpeg'.format(Test))
            self.OutputToExcel(hist, 'SimStress_{}_TimeAtTemp.csv'.format(Test))
            self.OutputChart(hist, 'chipM', 'dt_time', 'bar','SimStress_{}_TimeAtTemp.jpeg'.format(Test))

            for cycle in self.DutyCycle.values():
                cycle_counts = counts.copy()
                cycle_hist = hist.copy()
                cycle_counts['counts']=cycle_counts['counts'] * int(self.CycleInfo[cycle][Test.lower()]) * int(self.CycleInfo[cycle]['mult']) / div
                cycle_hist['dt_time'] = cycle_hist['dt_time'] * int(self.CycleInfo[cycle][Test.lower()]) * int(self.CycleInfo[cycle]['mult']) / div
                RF_counts = RF_counts.append(cycle_counts)
                TT_hist = TT_hist.append(cycle_hist)

        self.logger.info('Starting Field Stress Calculation')
        RF_counts = RF_counts.groupby('range')['counts'].sum().reset_index()
        TT_hist = TT_hist.groupby('chipM')['dt_time'].sum().reset_index()
        # total_counts = RF_counts.counts.sum()
        # print('Total cycles = {:.2f} cycles'.format(total_counts))
        # total_cycle_perday =RF_counts.counts.sum()/ 6 / 52 / 10
        # print('Average cycles per day = {:.2f} cycles / day'.format(total_cycle_perday))
        #
        # total_time = TT_hist.dt_time.sum()/60/60
        # print('Total Time at Temp = {:.2f}hr'.format(total_time))
        # total_time_percent = TT_hist.dt_time.sum() / 60 / 60 / 24 / 6 / 52 / 10 * 100
        # print('Total Time at Temp = {:.2f}% / 10yrs'.format(total_time_percent))
        # print(TT_hist.at[6,'dt_time'])
        # TT_hist.at[6,'dt_time']=TT_hist.at[40,'dt_time']+84510000
        # print(TT_hist.at[6, 'dt_time'])
        self.OutputToExcel(RF_counts,'FieldStress_RainflowCounts.csv')
        self.OutputChart(RF_counts,'range','counts','bar','FieldStress_RainFlowCounts.jpeg')
        self.OutputToExcel(TT_hist, 'FieldStress_TimeAtTemp.csv')
        self.OutputChart(TT_hist, 'chipM', 'dt_time', 'bar','FieldStress_TimeAtTemp.jpeg')

        self.logger.info('Starting Test Stress Correlation')
        ptc_cycles, field_cycle, ptc_AF = self.CoffinManson(RF_counts, 'PTC')
        ptc_dem_rel = self.Demonstrated_Reliability('PTC',ptc_AF)
        ts_cycles, field_cycle, ts_AF  = self.CoffinManson(RF_counts, 'TS')
        ts_dem_rel = self.Demonstrated_Reliability('TS',ts_AF)
        htoe_hours, field_hours, htoe_AF = self.Arrhenius(TT_hist)
        htoe_dem_rel = self.Demonstrated_Reliability('HTOE',htoe_AF)

        dem_rel = [['Demonstrated Reliability','PTC',ptc_dem_rel],['Demonstrated Reliability','TS',ts_dem_rel],['Demonstrated Reliability','HTOE',htoe_dem_rel]]
        results = [['1x Life','PTC_cycles',ptc_cycles],['1x Life','TS_cycles', ts_cycles],['1x Life','HTOE_hours', htoe_hours]]
        assumptions = [['Constants','CM',self.TestStress['cm']],['Constants','Ea',self.TestStress['ea']]] #,['Test Parameter','PTC AF',ptc_AF],['Test Parameter','TS AF',ts_AF],['Test Parameter','HTOE AF',htoe_AF]]
        duty_cycle = []
        for cycle in self.DutyCycle.values():
            time_balancing = self.DutyCycleTimeCalc(cycle)
            results.append(['Balancing Time/Day (hrs)',cycle, time_balancing])
            for field in self.CycleInfo[cycle].keys():
                duty_cycle.append([cycle,field,self.CycleInfo[cycle][field]])
        results= results + max_run_temp + assumptions + duty_cycle + dem_rel
        self.results_df = pd.DataFrame(results,columns=['Context','ID','Value'])
        self.results_df =  self.results_df.T
        self.results_df.columns = pd.MultiIndex.from_arrays([ self.results_df.iloc[0], self.results_df.iloc[1]])
        self.results_df.drop(['Context','ID'],axis=0,inplace=True)

        self.OutputToExcel( self.results_df,'Test_Summary.csv')
        plt.close()

    '''Main Functions'''
    def project_dir(self):
        '''Check/Create Location, Check/Create Sub Dir'''
        self.logger.info('Checking Project Directory')
        root_status = os.path.isdir(self.Basic['project_dir'])
        if root_status == False:
            self.logger.info('Creating Project Directory')
            os.mkdir(self.Basic['project_dir'])
        os.chdir(self.Basic['project_dir'])
        sub_dir = os.listdir()
        if len(sub_dir) < 1:
            self.logger.info('Create Sub Directories')
            os.mkdir('temperature_files')
            os.mkdir('summary_files')
            os.mkdir('charts')

    def get_drive_cycles(self):
        self.logger.info('Collecting Drive Cycles')

    def get_duty_cycles(self):
        self.logger.info('Collecting Duty Cycles')
        self.DutyCycle = dict(self.config.items('Duty_Cycles'))
        self.CycleInfo = \
            {
                self.DutyCycle['profile1']: dict(self.config.items(self.DutyCycle['profile1'])),
                self.DutyCycle['profile2']: dict(self.config.items(self.DutyCycle['profile2'])),
                self.DutyCycle['profile3']: dict(self.config.items(self.DutyCycle['profile3'])),
            }
        self.TestStress = dict(self.config.items('Internal_TestStress'))

    def get_thermal_simulations(self):
        self.logger.info('Collecting Thermal simulations')
        os.chdir(self.Basic['drive_location'])
        files = os.listdir()
        files = [k for k in files if '.xlsx' in k]
        files = [k for k in files if '~$' not in k]
        files = sorted(files)
        Sim_df = pd.DataFrame()
        max_run_temp = []
        for file in files:
            title = file.split('(')[1][:-6]
            df = pd.read_excel(self.Basic['drive_location'] + file,sheet_name='ADBMS6830',usecols='A:E')
            cols = ['time', 'chip1', 'chip2', 'chip3', 'chip4']
            df.columns = cols
            try:
                df['time'] = pd.to_datetime(df['time'], format='%H:%M:%S')
                df['dt_time'] = df['time'] - df['time'].shift()
                df['dt_time'] = df.dt_time.astype('timedelta64[s]')
            except:
                df['dt_time'] = df['time'] - df['time'].shift()

            df['chipM'] = df[['chip1', 'chip2', 'chip3', 'chip4']].max(axis=1)
            df['Test'] = title
            self.OutputChart(df,'time','chipM','line','ThermalSim_{}_result.jpeg'.format(title))
            max_run_temp.append(['Sim Profile Max Temp',title,df['chipM'].max()])
            Sim_df = Sim_df.append(df)
        return Sim_df,max_run_temp

    def plot_thermal_simulation_results(self):
        self.logger.info('Plotting Thermal Sim Results')

    def calculate_temp_summary(self,df):
        self.logger.info('Plotting Thermal Sim Results')
        temps = pd.DataFrame()
        temps['max'] = df.groupby('run')['temperature'].max()
        temps['min'] = df.groupby('run')['temperature'].min()
        temps['delta'] = temps['max'] - temps['min']
        temps.reset_index()
        # temps = temps.join(t.set_index("run"), how="inner", on="run")
        temps['degC_per_min'] = temps['delta'] / (temps['cutoff_time'] / 60)
        temps = temps.reset_index()
        # temps.to_csv('min_max_temps.csv', index=False)

    def rain_flow(self,df):
        self.logger.debug('Starting Rain Flow Counts')
        counts = rainflow.count_cycles(df,binsize=1)
        counts = pd.DataFrame(counts, columns=['range', 'counts'])
        # counts.plot.bar(x='range',y='count')
        # plt.show()
        return counts

    def time_at_temp(self,df):
        self.logger.debug('Starting Time at Temp Calculation')
        tdf = df.copy()
        # df.plot.scatter(x="time", y='chipM')
        # plt.show()
        # df['chipM'].plot.hist()
        # plt.show()
        tdf['chipM'] = tdf.chipM.mul(2).round().div(2)
        df2 = tdf.groupby('chipM')['dt_time'].sum().reset_index()
        # df2.plot.scatter(x='chipM',y='dt_time')
        # plt.show()
        return df2

    def CoffinManson(self,df,type):
        self.logger.debug('Starting Coffin Manson Equation')
        time.sleep(2)
        df['total_cycles'] = df['counts'] / (float(self.TestStress[type.lower()])/df['range'])**float(self.TestStress['cm'])
        total_cycles = df['total_cycles'].sum()
        field_cycles = df['counts'].sum()
        AF = field_cycles/total_cycles
        print('Total Number of Cycles Needed for 1x Life of {} : {:.2f} '.format(type,total_cycles))
        return total_cycles, field_cycles, AF

    def Arrhenius(self,df):
        self.logger.debug('Starting Arrhenius Equation')
        time.sleep(2)
        test_temp = 1/(float(self.TestStress['htoe'])+float(self.TestStress['kc']))
        calc_constant = float(self.TestStress['ea'])/float(self.TestStress['ac'])
        df['AF'] = np.exp(calc_constant*((1/(df['chipM']+float(self.TestStress['kc'])))-test_temp))
        df['Test_Equivalent'] = df['dt_time']/df['AF']
        total_HTOE_hours = df['Test_Equivalent'].sum()/60/60
        field_hours = df['dt_time'].sum()
        AF = field_hours/total_HTOE_hours
        print('Total Number of HTOE hours Needed for 1x Life: {:.2f}'.format(total_HTOE_hours))
        return total_HTOE_hours, field_hours, AF

    def DutyCycleTimeCalc(self,cycle):
        self.logger.debug('Calculating Balancing time for {} Cycle'.format(cycle))
        time=0
        for test in self.Test_list:
            if test == '110C_ON':
                duration = 90
            else:
                duration = 5
            minutes = int(self.CycleInfo[cycle][test.lower()])*duration
            time += minutes
        hours = time / 60
        return hours

    def get_Rel_information(self):
        self.Rel_items = dict(self.config.items('Reliability_Test_Setup'))

    def Demonstrated_Reliability(self,test,AF):
        self.logger.debug('Calculating Demonstrated Reliabilty for {}'.format(test))
        # =EXP(0 - CHISQ.INV(C4, 2) / (2 * C9 * C11 ^ C5))
        numerator = chi2.ppf(float(self.Rel_items['alpha']), df=2)
        denominator = 2 * int(self.Rel_items['n']) * float(self.Rel_items['lf'])**float(self.Rel_items['beta'])
        dem_rel = math.exp(0-(numerator/denominator))
        return dem_rel

    '''Support Functions'''

    def OutputToExcel(self, df, filename):
        self.logger.debug('Output {} to Excel'.format(filename))
        filepath = os.path.join(self.Basic['project_dir'], 'summary_files/', filename)
        df.to_csv(path_or_buf=filepath,index=False)

    def OutputChart(self,df,x,y,kind,filename):
        self.logger.debug('Output {} Chart'.format(filename))
        filepath = os.path.join(self.Basic['project_dir'], 'charts/', filename)
        fig, ax = plt.subplots()

        if kind == 'bar':
            ax.bar(df[x], df[y])
            plt.yscale("log")
            ax.yaxis.set_major_formatter(mticker.ScalarFormatter())
        else:
            ax.plot(df[x], df[y])

        plt.savefig(filepath)
        fig.clf()
        plt.close()

if __name__ == "__main__":
    file = 'Gotion_BVT_100mA.ini'
    row = DriveCycles(file)
    print(row.results_df)