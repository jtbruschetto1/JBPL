import os
import pandas as pd
import Gotion_FieldStress

if __name__ == "__main__":
    Master_results = pd.DataFrame()
    files = os.listdir()
    files = [k for k in files if '.ini' in k]
    files = [k for k in files if 'Gotion' in k]
    files = sorted(files)
    print(files)
    for file in files:
        A = Gotion_FieldStress.DriveCycles(file)
        A.results_df['File'] = file
        print(A.results_df)
        Master_results=Master_results.append(A.results_df)
    print(Master_results)
    Master_results.set_index('File',drop=True,inplace=True)
    filepath = os.path.join('/Users/jtbruschetto/Documents/OneDrive - Rivian Automotive, LLC/Projects/ESS/Gotion/BVT/python/', 'summary_files/', 'Combined_Summary_Results.csv')
    Master_results.to_csv(path_or_buf=filepath, index=True)

