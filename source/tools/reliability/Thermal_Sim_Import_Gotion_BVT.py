import os
import math
import time
import pandas as pd
import datetime as dt
import matplotlib as mpl
import matplotlib.pyplot as plt
import matplotlib.ticker as mticker
import numpy as np
import statistics
from scipy.stats.distributions import chi2
import rainflow
import logging
from configparser import ConfigParser
from mpl_toolkits.axes_grid1 import make_axes_locatable

class Thermal_Sim_Import(object):
    def __init__(self, config_file):
        os.chdir('/Users/jtbruschetto/GIT/JBPL/source/tools/reliability')
        logging.basicConfig(format='%(asctime)s - %(levelname)s - %(message)s')
        self.logger = logging.getLogger()
        self.logger.setLevel(logging.INFO)

        self.logger.info('Loading Configuration')
        self.config = ConfigParser()
        print(config_file)
        self.config.read(config_file)
        self.get_damage_model_inputs()
        self.project_dir()
        self.Convert_thermal_Sim_Data()


    '''Main Functions'''
    def Convert_thermal_Sim_Data(self):
        self.logger.info('Collecting Thermal simulations')
        os.chdir(self.Basic['drive_location'])
        files = os.listdir()
        files = [k for k in files if '.xlsx' in k]
        files = [k for k in files if '~$' not in k]
        files = sorted(files)
        Sim_df = pd.DataFrame()
        max_run_temp = []
        for file in files:
            title = file.split('(')[1][:-6]
            df = pd.read_excel(self.Basic['drive_location'] + file, sheet_name='ADBMS6830', usecols='A:E')
            cols = ['time', 'chip1', 'chip2', 'chip3', 'chip4']
            df.columns = cols
            try:
                df['time'] = pd.to_datetime(df['time'], format='%H:%M:%S')
                df['dt_time'] = df['time'] - df['time'].shift()
                df['dt_time'] = df.dt_time.astype('timedelta64[s]')
            except:
                df['dt_time'] = df['time'] - df['time'].shift()

            df['Temp'] = df[['chip1', 'chip2', 'chip3', 'chip4']].max(axis=1)
            df['Test'] = title
            df = df[['time','dt_time','Temp','Test']]
            self.OutputChart(df, 'time', 'Temp', 'line', 'ThermalSim_{}_result.jpeg'.format(title))
            max_run_temp.append(['Sim Profile Max Temp', title, df['Temp'].max()])
            Sim_df = Sim_df.append(df)
        self.OutputToExcel(Sim_df, 'temperature_files', 'ThermalSim_Dataframe.csv')
        max_run_temp_df = pd.DataFrame(max_run_temp, columns=['ID', 'Title', 'Max Temp'])
        self.OutputToExcel(max_run_temp_df, 'temperature_files', 'ThermalSim_MaxTemp_Dataframe.csv')

    '''Support Functions'''
    def project_dir(self):
        '''Check/Create Location, Check/Create Sub Dir'''
        self.logger.info('Checking Project Directory')
        root_status = os.path.isdir(self.Basic['project_dir'])
        if root_status == False:
            self.logger.info('Creating Project Directory')
            os.mkdir(self.Basic['project_dir'])
        os.chdir(self.Basic['project_dir'])
        sub_dir = os.listdir()
        if len(sub_dir) < 1:
            self.logger.info('Create Sub Directories')
            os.mkdir('damage_model_files')
            os.mkdir('temperature_files')
            os.mkdir('simulation_results')
            os.mkdir('duty_cycle_results')
            os.mkdir('summary_results')
            os.mkdir('charts')
            os.mkdir('ini_files')

    def get_damage_model_inputs(self):
        self.Basic = dict(self.config.items('Basic'))
        self.Basic['project_dir'] = os.path.join(self.Basic['project_root'], self.Basic['project_name'])
        self.DutyCycleFiles = dict(self.config.items('Duty_Cycles'))
        self.TestStress = dict(self.config.items('Test_Stress'))
        self.TestSetup = dict(self.config.items('Test_Setup'))

    def OutputToExcel(self, df, loc , filename):
        self.logger.debug('Output {} to Excel'.format(filename))
        filepath = os.path.join(self.Basic['project_dir'], loc , filename)
        df.to_csv(path_or_buf=filepath,index=False)

    def OutputChart(self,df,x,y,kind,filename):
        self.logger.debug('Output {} Chart'.format(filename))
        filepath = os.path.join(self.Basic['project_dir'], 'charts/', filename)
        fig, ax = plt.subplots()

        if kind == 'bar':
            ax.bar(df[x], df[y])
            plt.yscale("log")
            ax.yaxis.set_major_formatter(mticker.ScalarFormatter())
        else:
            ax.plot(df[x], df[y])

        plt.savefig(filepath,bbox_inches='tight')
        fig.clf()
        plt.close()


if __name__ == "__main__":
    file = 'Damage_Model_Gotion_BVT_50mA.ini'
    run = Thermal_Sim_Import(file)

