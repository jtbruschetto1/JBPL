import os
import math
import time
import pandas as pd
import datetime as dt
import matplotlib as mpl
import matplotlib.pyplot as plt
import matplotlib.ticker as mticker
import numpy as np
import statistics
from scipy.stats.distributions import chi2
import rainflow
import logging
from configparser import ConfigParser
from mpl_toolkits.axes_grid1 import make_axes_locatable



class DamageModel(object):
    def __init__(self,config_file):
        os.chdir('/Users/jtbruschetto/GIT/JBPL/source/tools/reliability')
        logging.basicConfig(format='%(asctime)s - %(levelname)s - %(message)s')
        self.logger = logging.getLogger()
        self.logger.setLevel(logging.INFO)

        self.logger.info('Loading Configuration')
        self.config = ConfigParser()
        print(config_file)
        self.config.read(config_file)
        self.get_damage_model_inputs()
        self.project_dir()

        self.controller()

    '''Controller'''
    def controller(self):
        self.logger.info('Starting Controller Process')
        time.sleep(1)


        program_mode = int(input('Select Program Mode: 0 - Full Run | 1 - Load Thermal Sim Results | 2 - Custom Run | Answer: '))
        if program_mode == 0:
            CycleInfo_df, TestStress_df = self.full_run()
            summary_name = 'Complete_Summary.csv'
        elif program_mode == 1:
            CycleInfo_df, TestStress_df = self.full_run(load=True)
            summary_name = 'Complete_Summary.csv'
        elif program_mode == 2:
            self.logger.info('Starting Custom Run Process')
            self.logger.warning('Select Duty Cycle from files below:')
            time.sleep(1)
            print(self.DutyCycleFiles)
            file_id = str(input('selection: '))
            self.logger.warning('Select Test Stress Cycle from files below:')
            time.sleep(1)
            print(self.TestStress)
            stress_id = str(input('selection: '))
            CycleInfo_df, TestStress_df = self.custom_run(self.DutyCycleFiles[file_id],self.TestStress[stress_id],load=True,)
            summary_name = 'Custom_summary_{}_{}.csv'.format(self.DutyCycleFiles[file_id],self.TestStress[stress_id])

        self.summary_df = self.summary_df.join(CycleInfo_df.set_index('Duty Cycle File'), on='DutyCycleFile')
        self.summary_df = self.summary_df.join(TestStress_df.set_index('Stress File'), on='TestStressFile')
        self.summary_df = self.summary_df.T.reset_index(drop=False)
        self.OutputToExcel(self.summary_df, 'damage_model_files', summary_name)

        dem_rel_mode = int(input('Select Demonstrated Rel Calc: 0 - Run All | 1 - Run Single File | 2 - Skip | Answer: '))
        if dem_rel_mode == 0:
            self.logger.info('Starting Run All  Options')
            Rel_items_df = self.dem_all_options()
            Rel_name = 'Complete_Demonstarted_Reliability_Summary.csv'
            Rel_items_df.set_index('Setup File', inplace=True, drop=True)
            Rel_items_df = Rel_items_df.T
            Rel_items_df.reset_index(inplace=True, drop=False)
            self.OutputToExcel(Rel_items_df, 'damage_model_files', Rel_name)

        elif dem_rel_mode == 1:
            self.logger.info('Starting Samples to Demonstrate')
            self.logger.warning('Select Test Setup from files below:')
            time.sleep(1)
            print(self.TestSetup)
            test_id = str(input('selection: '))
            Rel_items_df = self.dem_single_option(self.TestSetup[test_id])
            Rel_items_df = Rel_items_df.to_frame()
            Rel_items_df.columns = [self.TestSetup[test_id][:-4]]
            Rel_items_df.reset_index(inplace=True, drop=False)
            Rel_name = 'Demonstarted_Reliability_Summary_{}.csv'.format(self.TestSetup[test_id][:-4])
            self.OutputToExcel(Rel_items_df, 'damage_model_files', Rel_name)



    ''' Damage Program Modes'''
    def full_run(self,load=False):
        self.logger.info('Starting Full Run Process')
        counts_dict, hist_dict = self.run_thermal_sim(load)

        CycleInfo_df = pd.DataFrame()
        for file in self.DutyCycleFiles.values():
            CycleInfo_tdf = self.run_duty_cycle_file(counts_dict,hist_dict,file)
            CycleInfo_df = CycleInfo_df.append(CycleInfo_tdf)

        CycleInfo_df.reset_index(inplace=True,drop=False)
        CycleInfo_df.rename(columns={'index':'Sim Profile'},inplace=True)
        CycleInfo_df = CycleInfo_df.set_index(['Duty Cycle File', 'Sim Profile']).unstack().reset_index()
        CycleInfo_df.columns = [' '.join(col).strip() for col in CycleInfo_df.columns.values]

        self.summary_df = pd.DataFrame(columns=['DutyCycleFile','TestStressFile', 'PTC Cycles', 'TS Cycles', 'HTOE Hours'])
        TestStress_df = pd.DataFrame()
        for stress in self.TestStress.values():
           TestStress_tdf = self.run_stress_file(stress)
           TestStress_df = TestStress_df.append(TestStress_tdf, ignore_index=True)

        return CycleInfo_df, TestStress_df

    def custom_run(self,file,stress,load=False):
        self.logger.info('Starting Custom Run Process')
        counts_dict, hist_dict = self.run_thermal_sim(load)

        CycleInfo_df = pd.DataFrame()
        CycleInfo_tdf = self.run_duty_cycle_file(counts_dict, hist_dict, file)
        CycleInfo_df = CycleInfo_df.append(CycleInfo_tdf)

        CycleInfo_df.reset_index(inplace=True, drop=False)
        CycleInfo_df.rename(columns={'index': 'Sim Profile'}, inplace=True)
        CycleInfo_df = CycleInfo_df.set_index(['Duty Cycle File', 'Sim Profile']).unstack().reset_index()
        CycleInfo_df.columns = [' '.join(col).strip() for col in CycleInfo_df.columns.values]

        self.summary_df = pd.DataFrame(columns=['DutyCycleFile', 'TestStressFile', 'PTC Cycles', 'TS Cycles', 'HTOE Hours'])
        TestStress_df = pd.DataFrame()
        TestStress_tdf = self.run_stress_file(stress,custom=True,c_file=file)
        TestStress_df = TestStress_df.append(TestStress_tdf, ignore_index=True)

        return CycleInfo_df, TestStress_df

    ''' Demonstrated Program Modes'''
    def dem_all_options(self):
        Rel_items_df = pd.DataFrame()
        for file in self.TestSetup.values():
            Rel_items_tdf = self.dem_single_option(file)
            Rel_items_df = Rel_items_df.append(Rel_items_tdf,ignore_index=True)
        return Rel_items_df

    def dem_single_option(self,file):
        Rel_items = self.get_Rel_information(file)
        Rel_items_tdf = pd.Series(Rel_items.values(), index=Rel_items.keys())
        Rel_items_tdf = pd.to_numeric(Rel_items_tdf)
        Rel_items_tdf['Setup File'] = file
        Demonstated = self.Demonstrated_Reliability(file, Rel_items)
        samples = self.samples_to_demonstrate(file,Rel_items)
        lives = self.lives_to_demonstrate(file,Rel_items)
        Rel_items_tdf['Demonstration'] = Demonstated
        Rel_items_tdf['Additional Samples needed'] = round(samples - Rel_items_tdf['n'])
        Rel_items_tdf['Additional Lives needed'] = round((lives - Rel_items_tdf['lf'])*100)/100
        Rel_items_tdf['Delta to Target'] = Rel_items_tdf['Demonstration'] - Rel_items_tdf['target']

        max_samples = 20 if int(Rel_items['n'])<=15 else (int(Rel_items['n']) +5)
        max_lives = 10 if float(Rel_items['lf'])<= 7 else (float(Rel_items['lf']) +2)
        max_failures = 10 if int(Rel_items['af']) <= 7 else (int(Rel_items['af']) + 2)

        self.dem_options_table(max_sample=max_samples, max_lives=max_lives, Rel_items=Rel_items,file=file)
        self.demfail_options_table(max_sample=max_samples, max_failures=max_failures, Rel_items=Rel_items,file=file)
        return Rel_items_tdf


    ''' Mode Sub Functions'''
    def run_thermal_sim(self,load=False):
        counts_dict = {}
        hist_dict = {}
        if load == False:
            self.logger.info('Starting Thermal Sim Evaluation')
            Sim_df, MaxT_df = self.get_thermal_simulations()
            Test_list = Sim_df['Test'].unique()
            for Test in Test_list:
                Run = Sim_df[Sim_df['Test'] == Test]
                sub_run = Run[['time', 'dt_time', 'Temp']]
                counts = self.rain_flow(sub_run['Temp'])
                hist = self.time_at_temp(sub_run)
                self.OutputToExcel(counts,'simulation_results', 'SimStress_{}_RainflowCounts.csv'.format(Test))
                self.OutputChart(counts, 'range', 'counts','bar', 'SimStress_{}_RainFlowCounts.jpeg'.format(Test))
                self.OutputToExcel(hist,'simulation_results','SimStress_{}_TimeAtTemp.csv'.format(Test))
                self.OutputChart(hist, 'Temp', 'dt_time', 'bar','SimStress_{}_TimeAtTemp.jpeg'.format(Test))
                counts_dict.update({Test: counts})
                hist_dict.update({Test: hist})
        else:
            self.logger.info('Loading Thermal Sim Evaluation')
            os.chdir(os.path.join(self.Basic['project_dir'], 'simulation_results'))
            files = os.listdir()
            count_files = [k for k in files if 'RainflowCounts.csv' in k]
            count_names = [k.split('_')[1:3] for k in count_files]
            count_names = [k[0]+'_'+k[1] for k in count_names]
            for i in range(len(count_files)):
                df = pd.read_csv(count_files[i])
                counts_dict.update({count_names[i]: df})
            hist_files = [k for k in files if 'TimeAtTemp.csv' in k]
            hist_names = [k.split('_')[1:3] for k in hist_files]
            hist_names = [k[0] + '_' + k[1] for k in hist_names]
            for i in range(len(hist_files)):
                df = pd.read_csv(hist_files[i])
                hist_dict.update({hist_names[i]: df})

        return counts_dict, hist_dict

    def run_duty_cycle_file(self,counts_dict,hist_dict, file):
        self.logger.info('Starting Duty Cycle Calculations: {}'.format(file))
        DutyCycle, Division, CycleInfo = self.get_duty_cycles(file)
        RF_counts = pd.DataFrame()
        TT_hist = pd.DataFrame()
        # print(counts_dict.keys())
        # input('contineu?')
        # print(DutyCycle.values())
        # input('contineu?')
        for key in counts_dict.keys():
            for cycle in DutyCycle.values():
                try:
                    cycle_counts = counts_dict[key].copy()
                    cycle_hist = hist_dict[key].copy()
                    cycle_counts['counts'] = cycle_counts['counts'] * float(CycleInfo[cycle][key.lower()]) * int(
                        CycleInfo[cycle]['mult']) / int(Division[key.lower()])
                    cycle_hist['dt_time'] = cycle_hist['dt_time'] * float(CycleInfo[cycle][key.lower()]) * int(
                        CycleInfo[cycle]['mult']) / int(Division[key.lower()])
                    RF_counts = RF_counts.append(cycle_counts)
                    TT_hist = TT_hist.append(cycle_hist)
                except:
                    self.logger.warning('Key: {}, not present in {}'.format(key,cycle))

        RF_counts = RF_counts.groupby('range')['counts'].sum().reset_index()
        TT_hist = TT_hist.groupby('Temp')['dt_time'].sum().reset_index()
        CycleInfo_tdf = pd.DataFrame.from_dict(CycleInfo)
        CycleInfo_tdf['Duty Cycle File'] = file

        self.OutputToExcel(RF_counts, 'duty_cycle_results', 'FieldStress_RainFlowCounts_{}.csv'.format(file))
        self.OutputToExcel(TT_hist, 'duty_cycle_results', 'FieldStress_TimeAtTemp_{}.csv'.format(file))
        self.OutputChart(RF_counts, 'range', 'counts', 'bar', 'FieldStress_RainFlowCounts_{}.jpeg'.format(file))
        self.OutputChart(TT_hist, 'Temp', 'dt_time', 'bar', 'FieldStress_TimeAtTemp_{}.jpeg'.format(file))
        return CycleInfo_tdf

    def run_stress_file(self,stress,custom=False,c_file=''):
        if custom == False:
            self.logger.info('Starting Damage Model Calculations: {}'.format(stress))
            TestStress = self.get_test_stress(stress)
            TestStress_tdf = pd.Series(TestStress.values(), index=TestStress.keys())
            TestStress_tdf['Stress File'] = stress
            for file in self.DutyCycleFiles.values():
                self.run_damage_calculation(file,stress,TestStress)
        else:
            self.logger.info('Starting Damage Model Calculations: {}'.format(stress))
            TestStress = self.get_test_stress(stress)
            TestStress_tdf = pd.Series(TestStress.values(), index=TestStress.keys())
            TestStress_tdf['Stress File'] = stress
            self.run_damage_calculation(c_file,stress,TestStress)
        return TestStress_tdf

    def run_damage_calculation(self,file,stress,TestStress):
        RF_counts = pd.read_csv(os.path.join(self.Basic['project_dir'], 'duty_cycle_results',
                                             'FieldStress_RainFlowCounts_{}.csv'.format(file)))
        ptc_cycles, field_cycle, ptc_AF = self.CoffinManson(RF_counts, TestStress, 'PTC')
        ts_cycles, field_cycle, ts_AF = self.CoffinManson(RF_counts, TestStress, 'TS')
        TT_hist = pd.read_csv(os.path.join(self.Basic['project_dir'], 'duty_cycle_results',
                                           'FieldStress_TimeAtTemp_{}.csv'.format(file)))
        htoe_hours, field_hours, htoe_AF = self.Arrhenius(TT_hist, TestStress)
        result = [file, stress, ptc_cycles, ts_cycles, htoe_hours]
        result = pd.Series(result,index=self.summary_df.columns)
        self.summary_df = self.summary_df.append(result, ignore_index=True)

    '''Demonstrated Rel Functions'''
    def get_Rel_information(self,file):
        os.chdir(os.path.join(self.Basic['project_dir'],'ini_files' ))
        Rel_config = ConfigParser()
        Rel_config.read(file)
        Rel_items = dict(Rel_config.items('Reliability_Test_Setup'))
        return Rel_items

    def Demonstrated_Reliability(self,file,Rel_items):
        self.logger.debug('Calculating Demonstrated Reliabilty for {}'.format(file))
        # =EXP(0 - CHISQ.INV(C4, 2) / (2 * C9 * C11 ^ C5))
        numerator = chi2.ppf(float(Rel_items['alpha']), df=(2*int(Rel_items['af'])+2))
        denominator = 2 * int(Rel_items['n']) * float(Rel_items['lf'])**float(Rel_items['beta'])
        dem_rel = math.exp(0-(numerator/denominator))
        return dem_rel

    def samples_to_demonstrate(self,file,Rel_items):
        self.logger.debug('Calculating samples needed to Demonstrated Reliabilty for {}'.format(file))
        numerator = chi2.ppf(float(Rel_items['alpha']), df=(2*int(Rel_items['af'])+2))
        samples = (numerator/(0-math.log(float(Rel_items['target']))))/(2 * float(Rel_items['lf'])**float(Rel_items['beta']))
        return samples

    def lives_to_demonstrate(self,file,Rel_items):
        self.logger.debug('Calculating lives needed to Demonstrated Reliabilty for {}'.format(file))
        numerator = chi2.ppf(float(Rel_items['alpha']), df=(2*int(Rel_items['af'])+2))
        lives = ((numerator / (0 - math.log(float(Rel_items['target'])))) / (2 * int(Rel_items['n'])))**(1/float(Rel_items['beta']))
        return lives

    def dem_options_calc(self,X,Y,Rel_items):
        numerator = chi2.ppf(float(Rel_items['alpha']), df=(2*int(Rel_items['af'])+2))
        denominator = 2 * X * Y ** float(Rel_items['beta'])
        dem_rel = np.exp(0 - (numerator / denominator))
        return dem_rel

    def dem_options_table(self,max_sample,max_lives,Rel_items,file):
        options_df = pd.DataFrame(columns=['# Samples','# Lives','Demonstrated Reliability'])
        x = np.linspace(0.1, max_sample, 10*max_sample)
        y = np.linspace(0.1, max_lives, 10*max_lives)
        X, Y = np.meshgrid(x, y)
        Z = self.dem_options_calc(X, Y, Rel_items)

        contours = plt.contour(X, Y, Z, [.5, .75, 0.9, 0.95, float(Rel_items['target'])-.02, float(Rel_items['target'])], colors='black')
        plt.clabel(contours, inline=True, fontsize=8)

        plt.imshow(Z, extent=[0, max_sample, 0, max_lives], origin='lower',
                   cmap='RdYlGn', alpha=0.5,vmin=.75, vmax=1.025)

        cbar = plt.colorbar(orientation='horizontal')
        cbar.ax.set_xlabel('% Demonstrated Reliability')
        plt.xlabel('Number of Samples Tested')
        plt.ylabel('Testing to X Number of Lives')
        plt.title('Demonstrated Reliability: Samples vs Lives')
        plt.plot(int(Rel_items['n']), float(Rel_items['lf']), 'k+')
        plt.annotate('Proposed Test Plan',  # this is the text
                     (int(Rel_items['n']), float(Rel_items['lf'])),  # these are the coordinates to position the label
                     textcoords="offset points",  # how to position the text
                     xytext=(0, 10),  # distance from text to points (x,y)
                     ha='center',
                     bbox=dict(boxstyle="round", fc="w"))
        figure = plt.gcf()
        figure.set_size_inches(15, 15)
        filepath = os.path.join(self.Basic['project_dir'], 'charts/', 'Sample vs Life Chart based on {}.png'.format(file[:-4]))
        plt.savefig(filepath, bbox_inches='tight')
        plt.close()

    def demfail_options_calc(self,X,Y,Rel_items):
        numerator = chi2.ppf(float(Rel_items['alpha']), df=(2*Y+2))
        denominator = 2 * X * float(Rel_items['lf']) ** float(Rel_items['beta'])
        dem_rel = np.exp(0 - (numerator / denominator))
        return dem_rel

    def demfail_options_table(self,max_sample,max_failures,Rel_items,file):
        options_df = pd.DataFrame(columns=['# Samples','# Lives','Demonstrated Reliability'])
        x = np.linspace(0.1, max_sample, 10*max_sample)
        y = np.linspace(0.1, max_failures, 10*max_failures)
        X, Y = np.meshgrid(x, y)
        Z = self.demfail_options_calc(X, Y, Rel_items)

        contours = plt.contour(X, Y, Z, [.5, .75, 0.9, 0.95, float(Rel_items['target'])-.02, float(Rel_items['target'])], colors='black')
        plt.clabel(contours, inline=True, fontsize=8)

        plt.imshow(Z, extent=[0, max_sample, 0, max_failures], origin='lower',
                   cmap='RdYlGn', alpha=0.5,vmin=.75, vmax=1.025)

        cbar = plt.colorbar(orientation='horizontal')
        cbar.ax.set_xlabel('% Demonstrated Reliability')
        plt.xlabel('Number of Samples Tested')
        plt.ylabel('# of Failures Observed')
        plt.title('Demonstrated Reliability: Samples vs Failures')
        plt.plot(int(Rel_items['n']), int(Rel_items['af']), 'k+')
        plt.annotate('Proposed Test Plan',  # this is the text
                     (int(Rel_items['n']), int(Rel_items['af'])),  # these are the coordinates to position the label
                     textcoords="offset points",  # how to position the text
                     xytext=(0, 10),  # distance from text to points (x,y)
                     ha='center',
                     bbox=dict(boxstyle="round", fc="w"))
        figure = plt.gcf()
        figure.set_size_inches(15, 15)
        filepath = os.path.join(self.Basic['project_dir'], 'charts/', 'Sample vs Allowable Failures Chart based on {}.png'.format(file[:-4]))
        plt.savefig(filepath, bbox_inches='tight')
        plt.close()

    '''Main Functions'''
    def get_thermal_simulations(self):
        self.logger.info('Collecting Thermal simulations')
        # files = os.listdir()
        # files = [k for k in files if '.xlsx' in k]
        # files = [k for k in files if '~$' not in k]
        # files = sorted(files)
        # Sim_df = pd.DataFrame()
        # max_run_temp = []
        # for file in files:
        #     title = file.split('(')[1][:-6]
        #     df = pd.read_excel(self.Basic['drive_location'] + file,sheet_name='ADBMS6830',usecols='A:E')
        #     cols = ['time', 'chip1', 'chip2', 'chip3', 'chip4']
        #     df.columns = cols
        #     try:
        #         df['time'] = pd.to_datetime(df['time'], format='%H:%M:%S')
        #         df['dt_time'] = df['time'] - df['time'].shift()
        #         df['dt_time'] = df.dt_time.astype('timedelta64[s]')
        #     except:
        #         df['dt_time'] = df['time'] - df['time'].shift()
        #
        #     df['Temp'] = df[['chip1', 'chip2', 'chip3', 'chip4']].max(axis=1)
        #     df['Test'] = title
        #     self.OutputChart(df,'time','Temp','line','ThermalSim_{}_result.jpeg'.format(title))
        #     max_run_temp.append(['Sim Profile Max Temp',title,df['Temp'].max()])
        #     Sim_df = Sim_df.append(df)
        # self.OutputToExcel(Sim_df,'temperature_files','ThermalSim_Dataframe.csv')
        # max_run_temp_df = pd.DataFrame(max_run_temp, columns=['ID','Title','Max Temp'])
        # self.OutputToExcel(max_run_temp_df, 'temperature_files', 'ThermalSim_MaxTemp_Dataframe.csv')
        os.chdir(os.path.join(self.Basic['project_dir'], 'temperature_files'))
        Sim_df = pd.read_csv('ThermalSim_Dataframe.csv')
        max_run_temp_df = pd.read_csv('ThermalSim_MaxTemp_Dataframe.csv')
        return Sim_df,max_run_temp_df

    def get_duty_cycles(self,file):
        self.logger.info('Collecting Duty Cycles')
        os.chdir(os.path.join(self.Basic['project_dir'],'ini_files' ))
        DC_config = ConfigParser()
        DC_config.read(file)
        DutyCycle = dict(DC_config.items('Profile'))
        Division = dict(DC_config.items('Division'))
        CycleInfo = {}
        for key in DutyCycle.keys():
            CycleInfo.update({DutyCycle[key]:dict(DC_config.items(DutyCycle[key]))})
        return DutyCycle, Division, CycleInfo

    def get_test_stress(self,file):
        self.logger.info('Collecting Test Stress')
        os.chdir(os.path.join(self.Basic['project_dir'],'ini_files' ))
        TS_config = ConfigParser()
        TS_config.read(file)
        TestStress = dict(TS_config.items('TestStress'))
        return TestStress

    def plot_thermal_simulation_results(self):
        self.logger.info('Plotting Thermal Sim Results')

    def calculate_temp_summary(self,df):
        self.logger.info('Plotting Thermal Sim Results')
        temps = pd.DataFrame()
        temps['max'] = df.groupby('run')['temperature'].max()
        temps['min'] = df.groupby('run')['temperature'].min()
        temps['delta'] = temps['max'] - temps['min']
        temps.reset_index()
        # temps = temps.join(t.set_index("run"), how="inner", on="run")
        temps['degC_per_min'] = temps['delta'] / (temps['cutoff_time'] / 60)
        temps = temps.reset_index()
        # temps.to_csv('min_max_temps.csv', index=False)

    def rain_flow(self,df):
        self.logger.debug('Starting Rain Flow Counts')
        counts = rainflow.count_cycles(df,binsize=1)
        counts = pd.DataFrame(counts, columns=['range', 'counts'])
        # counts.plot.bar(x='range',y='count')
        # plt.show()
        return counts

    def time_at_temp(self,df):
        self.logger.debug('Starting Time at Temp Calculation')
        tdf = df.copy()
        # df.plot.scatter(x="time", y='Temp')
        # plt.show()
        # df['Temp'].plot.hist()
        # plt.show()
        tdf['Temp'] = tdf.Temp.mul(2).round().div(2)
        df2 = tdf.groupby('Temp')['dt_time'].sum().reset_index()
        # df2.plot.scatter(x='Temp',y='dt_time')
        # plt.show()
        return df2

    def CoffinManson(self,df,TestStress,type):
        self.logger.debug('Starting Coffin Manson Equation')
        df['total_cycles'] = df['counts'] / (float(TestStress[type.lower()])/df['range'])**float(TestStress['cm'])
        total_cycles = df['total_cycles'].sum()
        field_cycles = df['counts'].sum()
        AF = field_cycles/total_cycles
        # print('Total Number of Cycles Needed for 1x Life of {} : {:.2f} '.format(type,total_cycles))
        return total_cycles, field_cycles, AF

    def Arrhenius(self,df,TestStress):
        self.logger.debug('Starting Arrhenius Equation')
        kc = 273.15  # Conversion from Celsius to Kelvin
        ac = 0.00008617  # Boltzmann constant eV⋅K−1
        test_temp = 1/(float(TestStress['htoe'])+kc)
        calc_constant = float(TestStress['ea'])/ac
        df['AF'] = np.exp(calc_constant*((1/(df['Temp']+kc))-test_temp))
        df['Test_Equivalent'] = df['dt_time']/df['AF']
        total_HTOE_hours = df['Test_Equivalent'].sum()/60/60
        field_hours = df['dt_time'].sum()
        AF = field_hours/total_HTOE_hours
        # print('Total Number of HTOE hours Needed for 1x Life: {:.2f}'.format(total_HTOE_hours))
        return total_HTOE_hours, field_hours, AF

    def DutyCycleTimeCalc(self,cycle):
        self.logger.debug('Calculating Balancing time for {} Cycle'.format(cycle))
        time=0
        for test in self.Test_list:
            if test == '110C_ON':
                duration = 90
            else:
                duration = 5
            minutes = int(self.CycleInfo[cycle][test.lower()])*duration
            time += minutes
        hours = time / 60
        return hours

    '''Support Functions'''
    def project_dir(self):
        '''Check/Create Location, Check/Create Sub Dir'''
        self.logger.info('Checking Project Directory')
        root_status = os.path.isdir(self.Basic['project_dir'])
        if root_status == False:
            self.logger.info('Creating Project Directory')
            os.mkdir(self.Basic['project_dir'])
        os.chdir(self.Basic['project_dir'])
        sub_dir = os.listdir()
        if len(sub_dir) < 1:
            self.logger.info('Create Sub Directories')
            os.mkdir('damage_model_files')
            os.mkdir('temperature_files')
            os.mkdir('simulation_results')
            os.mkdir('duty_cycle_results')
            os.mkdir('summary_results')
            os.mkdir('charts')
            os.mkdir('ini_files')

    def get_damage_model_inputs(self):
        self.Basic = dict(self.config.items('Basic'))
        self.Basic['project_dir'] = os.path.join(self.Basic['project_root'],self.Basic['project_name'])
        self.DutyCycleFiles = dict(self.config.items('Duty_Cycles'))
        self.TestStress = dict(self.config.items('Test_Stress'))
        self.TestSetup = dict(self.config.items('Test_Setup'))

    def OutputToExcel(self, df, loc , filename):
        self.logger.debug('Output {} to Excel'.format(filename))
        filepath = os.path.join(self.Basic['project_dir'], loc , filename)
        df.to_csv(path_or_buf=filepath,index=False)

    def OutputChart(self,df,x,y,kind,filename):
        self.logger.debug('Output {} Chart'.format(filename))
        filepath = os.path.join(self.Basic['project_dir'], 'charts/', filename)
        fig, ax = plt.subplots()

        if kind == 'bar':
            ax.bar(df[x], df[y])
            plt.yscale("log")
            ax.yaxis.set_major_formatter(mticker.ScalarFormatter())
        else:
            ax.plot(df[x], df[y])

        plt.savefig(filepath,bbox_inches='tight')
        fig.clf()
        plt.close()

if __name__ == "__main__":
    file = 'Damage_Model_Gotion_BVT_50mA.ini'
    # file = 'Damage_Model_Sensata_Contactor.ini'
    # file = 'Damage_Model_Aptiv_Contactor.ini'
    row = DamageModel(file)
    # print(row.results_df)