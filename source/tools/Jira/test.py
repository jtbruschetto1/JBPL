from jira import JIRA
from pyra import PYRA

access = PYRA()

jira = JIRA(access.dict['options'], basic_auth=(access.dict['user'], access.dict['token']))

# Fetch all fields
allfields = jira.fields()
# Make a map from field name -> field id
nameMap = {field['name']:field['id'] for field in allfields}
# Fetch an issue
issue = jira.issue('RC-2097')
# You can now look up custom fields by name using the map
supporting_department = getattr(issue.fields, nameMap['Supporting Department'])[0]
print(supporting_department)
reliability_team = getattr(issue.fields, nameMap['Reliability Team'])[0]
print(reliability_team)


# projects = jira.projects()
#
# search = """project = RC
# AND issuetype = Concern
# AND resolution = unresolved
# AND Severity in ("5 - Critical", "4 - Severe")
# AND "[Affected Program Population][Select List (multiple choices)]" in (R1T)
# AND ("Target Production Effectivity Date[Date]" < 2021-12-25 OR "Target Production Effectivity Date[Date]" is EMPTY)
# AND ("Planned Implementation Date[Date]" > 2021-08-16 OR "Planned Implementation Date[Date]" is EMPTY)
# AND ("[temp] Containment Saleability[Dropdown]" != "Manager-Approved Saleability" OR "[temp] Containment Saleability[Dropdown]" is EMPTY)
# AND "[Department][Dropdown]" in ("Engineering-Energy Storage System")
# AND (labels not in (aspendale) or labels is empty)
# ORDER BY cf[11878] ASC, cf[11260] DESC, created DESC]"""
#
# search = ' AND '.join(search)
#
# issues = jira.search_issues(search)
#
# print(issues)
#
# for issue in issues:
#  print(issue)
#  issue_id = jira.issue(issue)
#  title = issue_id.fields.summary
#  link = issue_id.self
#  print(title,link)



