import os
from jira import JIRA
import requests
from requests.auth import HTTPBasicAuth
from configparser import ConfigParser
import logging
import time
import json


class RCR:
    def __init__(self):
        logging.basicConfig(format='%(asctime)s - %(levelname)s - %(message)s')  # filename='RCR.log',
        self.logger = logging.getLogger()
        self.logger.setLevel(logging.INFO)

        self.root = r"/Users/jtbruschetto/GIT/JBPL/source/tools/Jira"
        os.chdir(self.root)

        self.logger.info('Loading Configuration')
        self.config = ConfigParser()
        self.config.read('rcr.ini')
        self.Access  = dict(self.config.items('Jira_Access'))
        self.User = dict(self.config.items('User'))
        self.Departments = dict(self.config.items('Departments'))


        self.logger.info('Connecting to Jira')
        self.jira = JIRA({'server': self.Access['server']},basic_auth=(self.Access['user'], self.Access['token']))
        allfields = self.jira.fields()
        self.nameMap = {field['name']: field['id'] for field in allfields}

    """ Main Functions """
    def rcr_review_triage(self,search_file_name):
        self.logger.info('Starting RCR Review Triage: {}'.format(search_file_name))
        search_file = open('jira_search/'+search_file_name,"r")
        search = search_file.readlines()


        for key in self.Departments:
            self.logger.info('Starting department: {}'.format(key))
            search[8]=self.Departments[key]
            jira_search = ' AND '.join(search)
            jira_search = jira_search + ' ORDER BY "Severity[Dropdown]" DESC'
            list_issues = self.jira.search_issues(jira_search)
            self.logger.info('Number of Issues: {}'.format(len(list_issues)))

            for issue in list_issues:
                self.get_issue_details(issue)
                self.issue_reaction_list(issue)

    def rcr_triage(self,search_file_name):
        self.logger.info('Starting RCR Triage: {}'.format(search_file_name))
        list_issues = self.issue_search(search_file_name)
        self.logger.info('Number of Issues: {}'.format(len(list_issues)))

        for issue in list_issues:
            self.get_issue_details(issue)
            self.issue_reaction_list(issue)

    def issue_review_loop(self):
        while True:
            ans = str(input('Input RCR Number (example "RC-XXXXX" / "Exit" to break)\nInput: '))
            if ans == "Exit":
                break
            else:
                self.get_issue_details(ans)
                # self.set_all_rel_fields(ans)
                self.issue_reaction_list(ans)


    """ Supporting Functions """
    def create_rc(self):
        self.logger.info('Starting RCR Create Process')
        # TODO
        '''
        fields: a dict containing field names and the values to use. If present, all other keyword arguments
            will be ignored

            Issue Types 
            Build - This is a Concern for any issues identified during the physical building of products. Build issues also include any related to physical supplier-related product quality.
            Business - This is a Concern for any Rivian internal process improvements or issues not directly impacting the build, testing, or development of a program/product build.
            Development - This is a Concern that was found any time during product development prior to the production going into full series production and out to customers.  
            Field Issue - These are Concerns created only from the Field Triage Project. This selection should NOT be made directly from the Concerns Registry.
            Validation - This is a Concern that is found during an official validation test of some kind.
            Virtual - This is a Concern that was found during a virtual validation (such as CAE) or virtual build event.
            
            Required Create Fields:
                Key - Auto Generated
                Status - Auto Generated
                Created - Auto Generated
                Updated - Auto Generated
                Project - RC
                Issue Type - Concern
                Summary - SN - Description
                Concern Register Issue Type - listed above
                Severity
                Reporter
                Description
                [Department]
                [Program]
                [Program Build Event]
                Date Found
                Manufacturing Shop/Line
                [Vehicle Name]
                Is this a software issue?
                [Product Line]
                [Program Variant]
                Vehicle Software
                Affected Program Population
        '''
        fields = {
            'project': {'key': 'RC'},
            'issuetype': {'name': ''},
            'summary': '',
            'description': '',
            self.nameMap['Severity']: '',
            self.nameMap['Date Found']: '',
            self.nameMap['[Concern Register Issue Type]']: '',
            self.nameMap['Assignee']: '',
            self.nameMap['Department']: '',
            self.nameMap['Supporting Department']: '',
            self.nameMap['Is this a software issue?']: '',
            self.nameMap['Supporting Department']: '',
            self.nameMap['Supporting Department']: ''
        }
        self.jira.create_issue(fields=fields)

    def add_comment(self, issue, comment):
        self.logger.info('Starting Add Comment Process')
        self.jira.add_comment(issue,comment)
        #TODO - confirm function works

    def attach_file(self,issue,file):
        self.logger.info('Starting RCR attach file Process : {}'.format(file))
        self.jira.add_attachment(issue=issue, attachment=file)
        #TODO - confirm function works

    def issue_search(self,search_file_name):
        search_file = open('jira_search/' + search_file_name, "r")
        search = search_file.readlines()
        jira_search = ' AND '.join(search)
        jira_search = jira_search + ' ORDER BY created DESC'
        list_issues = self.jira.search_issues(jira_search)
        return list_issues

    def issue_reaction_list(self,issue):
        ans = 1
        while ans > 0:
            time.sleep(1)
            ans = int(input(
                '5 - *Special* Set Rel Top Risk \n4 - Set all feilds \n3 - Set Reliability Relevant \n2 - Set Reliability Contact\n1 - Watch issue\n0 - Next issue\nInput: '))
            if ans == 5:
                self.set_rel_top_risk_label(issue)
            if ans == 4:
                self.set_all_rel_fields(issue)
                ans = 0
            if ans == 3:
                self.set_reliability_relevant(issue)
                self.set_watch_issue(issue)
                ans = 0
            elif ans == 2:
                self.set_reliability_team(issue)
                self.set_watch_issue(issue)
                ans = 0
            elif ans == 1:
                self.set_watch_issue(issue)
                ans = 0
            else:
                ans = 0

    ''' Get Functions'''
    def get_issue_details(self,issue):
        self.logger.info('Getting {} details'.format(str(issue)))
        issue_id = self.jira.issue(issue)
        title = issue_id.fields.summary
        link = self.Access['link'] + str(issue)
        watch_status = self.get_watch_status(issue)
        relrel_status = self.get_reliability_relevant(issue)
        rel_team = self.get_reliability_team(issue)
        severity = self.get_field_info(issue,'Severity')
        labels = self.get_labels(issue)
        self.logger.info('Title: {}'.format(title))
        self.logger.info('link: {}'.format(link))
        self.logger.info('Severity: {}'.format(severity))
        self.logger.info('Rel Relevant status: {}'.format(relrel_status))
        self.logger.info('Rel Team status: {}'.format(rel_team))
        self.logger.info('Watch status: {}'.format(watch_status))
        self.logger.info('Labels: {}'.format(labels))

    def get_watch_status(self,issue):
        watcher = self.jira.watchers(issue=issue)
        return watcher.isWatching

    def get_reliability_relevant(self,issue):
        issue = self.jira.issue(issue)
        supporting_department = getattr(issue.fields, self.nameMap['Supporting Department'])
        if type(supporting_department) == list:
            for elem in supporting_department:
                if elem.value == 'Engineering-Reliability':
                    return True
            return False
        else:
            return False

    def get_reliability_team(self,issue):
        issue = self.jira.issue(issue)
        supporting_team = getattr(issue.fields, self.nameMap['Reliability Team'])
        return supporting_team

    def get_labels(self,issue):
        issue = self.jira.issue(issue)
        labels = getattr(issue.fields, self.nameMap['Labels'])
        return labels

    def get_Rel_top_risk_labels(self,issue):
        issue = self.jira.issue(issue)
        labels = getattr(issue.fields, self.nameMap['Labels'])
        if type(labels) == list:
            for elem in labels:
                if elem.value == 'Top-REL-Risk':
                    return True
            return False
        else:
            return False

    def get_field_info(self,issue,field):
        issue = self.jira.issue(issue)
        try:
            field_status = getattr(issue.fields, self.nameMap[field])
            return field_status
        except:
            self.logger.error('Field not recognized')
            field_status = 'NULL'
            print(self.nameMap[field])
            return field_status

    ''' Set Functions'''
    def set_all_rel_fields(self,issue):
        self.set_reliability_relevant(issue)
        self.set_reliability_team(issue)
        self.set_watch_issue(issue)

    def set_reliability_relevant(self,issue):
        self.logger.info('Setting issues as Reliability Relevant')
        issue = self.jira.issue(issue)
        status = self.get_reliability_relevant(issue)
        if status == False:
            # issue.update(fields={"update": {self.nameMap['Supporting Department']: [{"add": {'value': 'Engineering-Reliability'}}]}}, notify=False)
            issue.add_field_value(field=self.nameMap['Supporting Department'],value={'value': 'Engineering-Reliability'})
            self.logger.info('~~ Added Supporting Department')

    def set_reliability_team(self,issue):
        self.logger.info('Setting Reliability Contact')
        issue = self.jira.issue(issue)
        status = self.get_reliability_team(issue)
        if status == False:
            # issue.update(fields={"update": {self.nameMap['Reliability Team']: [{"add": {'accountId': self.User['id']}}]}}, notify=False)
            issue.add_field_value(field=self.nameMap['Reliability Team'], value={'accountId': self.User['id']})
            self.logger.info('~~ Added to Rel Team')

    def set_rel_top_risk_label(self,issue):
        self.logger.info('Setting Reliability Contact')
        issue = self.jira.issue(issue)
        status = self.get_Rel_top_risk_labels(issue)
        if status == False:
            # issue.update(fields={"update": {self.nameMap['Labels']: [{"add": {'value': 'Top-REL-Risk'}}]}}, notify=False)
            issue.add_field_value(field=self.nameMap['Labels'], value={'value': 'Top-REL-Risk'})
            self.logger.info('~~ Added to Rel Top Risk')

    def set_watch_issue(self,issue):
        self.logger.info('Start watching the issue')
        self.jira.add_watcher(issue, self.User['id'])


if __name__ == "__main__":
    test = RCR()
    # test.rcr_review_triage(search_file_name='rcr_triage_search.txt')
    test.rcr_triage(search_file_name='rcr_contactor_search.txt')
    test.rcr_triage(search_file_name='rcr_thermal_search.txt')
    test.rcr_triage(search_file_name='rcr_ess_search.txt')
    # test.set_watch_issue('RC-15454')
    # test.set_all_rel_fields('RC-')
    # test.issue_review_loop()

"""
Development Notes / Next Steps


"""