import os
from atlassian import Confluence
from atlassian.bitbucket import Cloud
import requests
from requests.auth import HTTPBasicAuth
from configparser import ConfigParser
import logging
import time
import json


class Landing_Page:
    def __init__(self):
        logging.basicConfig(format='%(asctime)s - %(levelname)s - %(message)s')
        self.logger = logging.getLogger()
        self.logger.setLevel(logging.INFO)

        self.root = r"/Users/jtbruschetto/GIT/JBPL/source/tools/confluence"
        os.chdir(self.root)

        self.logger.info('Loading Configuration')
        self.config = ConfigParser()
        self.config.read('config.ini')
        self.Access  = dict(self.config.items('Access'))
        self.User = dict(self.config.items('User'))
        self.MainPage = dict(self.config.items('MainPage'))


        self.logger.info('Connecting to Confluence')
        self.confluence = Confluence(url=self.Access['server'],username=self.Access['user'],password=self.Access['token'], cloud=True)
        label_list = self.confluence.get_page_labels(page_id=self.MainPage['page_id'], prefix=None, start=None, limit=None)
        print(label_list)



        # allfields = self.jira.fields()
        # self.nameMap = {field['name']: field['id'] for field in allfields}

    """ Main Functions """

    """ Supporting Functions """

    ''' Get Functions'''

    ''' Set Functions'''

if __name__ == "__main__":
    test = Landing_Page()


"""
Development Notes / Next Steps


"""