from O365 import Account

credentials = ('jtbruschetto@rivian.com','')

account = Account(credentials)
account.authenticate(scopes=['basic', 'message_all'])

m = account.new_message()
m.to.add('jtbruschetto@rivian.com')
m.subject = 'Testing!'
m.body = "George Best quote: I've stopped drinking, but only while I'm asleep."
m.send()